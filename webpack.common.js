const { CheckerPlugin } = require('awesome-typescript-loader');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
const { ProvidePlugin } = require('webpack');

const outputPath = path.join(__dirname, 'dist', 'public');

module.exports = {
  devtool: 'inline-source-map',

  entry: {
    'javascripts/main': path.join(__dirname, 'src/public/javascripts/main/index.ts'),
    style: [
      path.join(__dirname, 'src/public/stylesheets/style.scss'),
      path.join(__dirname, 'node_modules/photoswipe/src/css/main.scss'),
      path.join(__dirname, 'node_modules/photoswipe/src/css/default-skin/default-skin.scss'),
      path.join(__dirname, 'node_modules/leaflet/dist/leaflet.css')
    ],
    'vdm-icon': path.join(__dirname, 'node_modules', '@villemontreal', 'font-icon', 'icon-dist', 'vdm.css')
  },

  output: {
    filename: '[name].js',
    path: path.join(outputPath)
  },

  resolve: {
    extensions: ['.ts', '.js']
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          {
            loader: 'awesome-typescript-loader',
            options: {
              configFileName: path.join(__dirname, 'src', 'public', 'javascripts', 'tsconfig.json')
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: true,
                sourceMap: true
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                sourceMap: true
              }
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true
              }
            }
          ]
        })
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: true,
                sourceMap: true
              }
            }
          ]
        })
      },
      {
        test: /\.(png|gif|jpg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]'
            }
          }
        ]
      },
      { test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader' },
      { test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader' },
      { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader' },
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader' },
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader' }
    ]
  },

  plugins: [
    new CheckerPlugin(),
    new CopyWebpackPlugin([
      {
        context: 'src/public/images',
        from: '**/*.@(png|gif|jpg|svg)',
        to: 'images'
      },
      {
        from: 'src/public/glyphs',
        to: 'glyphs'
      }
    ]),
    new ExtractTextPlugin({
      filename: '[name].css'
    }),
    new ProvidePlugin({
      jQuery: 'jquery',
      Popper: ['popper.js', 'default']
    })
  ]
};
