import { Router } from 'express';
import { contentApi } from '../content-api';
import { asyncMiddleware } from '../utils';

const searchRouter = Router();
const limit = 10;

const searchMiddleware = asyncMiddleware(async (req, res, next) => {
  const term = req.query.q;
  const page = req.query.page || 1;
  const decodedTerm = decodeURIComponent(term);

  const results = await contentApi.getEntries(res.locals.language, {
    term,
    limit,
    offset: limit * (page - 1)
  });

  res.render('search/SearchPage', {
    results,
    decodedTerm
  });
});

searchRouter.get('/recherche', searchMiddleware);
searchRouter.get('/en/search', searchMiddleware);

export { searchRouter };
