import { Router } from 'express';
import { asyncMiddleware } from '../utils';

const successRouter = Router();

const successMiddleware = asyncMiddleware(async (req, res, next) => {
  res.render('success/SuccessPage');
});

successRouter.get('/succes', successMiddleware);
successRouter.get('/en/success', successMiddleware);

export { successRouter };
