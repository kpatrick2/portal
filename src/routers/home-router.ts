import { Router } from 'express';
import { contentApi, ContentTypesParagraph, IParagraph2, paragraph2Helper } from '../content-api';
import { asyncMiddleware, Language } from '../utils';
import { constructOrderBy } from '../utils/api-request-helpers';

const homeRouter = Router();

const homeMiddleware = asyncMiddleware(async (req, res, next) => {
  const content = await contentApi.getHome(res.locals.language);
  const news = await getNews(res.locals.language, content.mtl_content.accueil.paragraph_2);

  res.render('home/HomePage', {
    content,
    news
  });
});

homeRouter.get('/', homeMiddleware);
homeRouter.get('/en/', homeMiddleware);

async function getNews(language: Language, paragraph2: IParagraph2[]) {
  if (!paragraph2 || paragraph2.length === 0) {
    return undefined;
  }

  const type = paragraph2Helper.mapContentTypes(language, ContentTypesParagraph.News);
  const orderBy = constructOrderBy(paragraph2[0].sort_on, paragraph2[0].sort);

  return await contentApi.getEntries(language, {
    type,
    orderBy,
    offset: 0,
    limit: 3
  });
}

export { homeRouter };
