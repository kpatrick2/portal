import { Request, Router } from 'express';
import { TranslationFunction } from 'i18next';
import * as moment from 'moment';
import * as queryString from 'query-string';
import {
  contentApi,
  ContentTypes,
  ContentTypesParagraph,
  ESystemName,
  ETaxonomyName,
  IContentType,
  IContentTypeSearch,
  IElectedContent,
  IEventContent,
  IFilter,
  IHubContent,
  IImportantDateContent,
  ILocationContentV2,
  INewsContent,
  INodeReference,
  ISortData,
  ITaxonomyFilterWrapper,
  paragraph2Helper
} from '../content-api';
import { asyncMiddleware, Language } from '../utils';
import { constructOrderBy } from '../utils/api-request-helpers';
import { setfilterAttr } from '../utils/url-helpers';
import { ILocalsProps } from '../views/pages/locals-interface';
import { paginationHelpers } from '../views/ui';

const contentRouter = Router();

const contentMiddleware = asyncMiddleware(async (req, res, next) => {
  const slug = extractSlugFromUrl(req.url);

  try {
    const content = await contentApi.getEntry(res.locals.language, slug);
    const extra = await retrieveExtraContent(req, content, res.locals);

    res.render('content/ContentPage', {
      content,
      extra,
      form: req.body
    });
  } catch (error) {
    if (error.response && error.response.status === 404) {
      res.sendStatus(404);
    } else {
      throw error;
    }
  }
});

function extractSlugFromUrl(url: string) {
  let path = url.split('?')[0];

  // Remove '/en' from the URL since it's not part of the identifier
  if (path.startsWith('/en/')) {
    path = path.substring(3);
  }

  return path;
}

// Some content types need more than their own data
// tslint:disable-next-line:cyclomatic-complexity
// tslint:disable-next-line:max-func-body-length
// tslint:disable-next-line:cyclomatic-complexity
async function retrieveExtraContent(req: Request, content: IContentType, locals: ILocalsProps) {
  const language: Language = locals.language;
  const t: TranslationFunction = locals.t;
  // API needs to receive time in UTC format (Z)
  const today = moment()
    .utc()
    .format();
  try {
    switch (content.dc_type.machine_name) {
      case ContentTypes.Hub:
        const hubContent = content as IHubContent;
        if (
          hubContent.mtl_content.collectrices.collectrice_para_2 &&
          hubContent.mtl_content.collectrices.collectrice_para_2.length > 0
        ) {
          const paragraph2 = hubContent.mtl_content.collectrices.collectrice_para_2[0];
          const page = req.query.page || 1;

          // Location vs other hubs
          if (paragraph2.display_type === 'List+map') {
            const term = req.query.q;
            const category = req.query.category;
            const filtersBy = [
              !category && !term
                ? { key: 'dc_subject', value: language === 'en' ? 'Point of service' : 'Point de service' }
                : { key: 'dc_subject', value: category }
            ];

            const search = await contentApi.getEntries(language, {
              filtersBy,
              term,
              type: paragraph2Helper.mapContentTypes(language, ContentTypesParagraph.Location),
              offset: 5 * (page - 1),
              limit: 5
            });
            return { search, taxoLabel: null };
            // tslint:disable-next-line:no-else-after-return
          } else {
            const params = queryString.parse(queryString.extract(req.url));
            const filtersBy = setfilterAttr(params);
            const pageSpec = req.query.page || 1;

            let search: any;
            if (paragraph2.content_type !== ContentTypesParagraph.Elected) {
              search = await contentApi.getEntries(language, {
                filtersBy,
                term: req.query.q,
                type: paragraph2Helper.mapContentTypes(language, paragraph2.content_type as ContentTypesParagraph),
                orderBy: constructOrderBy(paragraph2.sort_on, paragraph2.sort),
                offset: (paragraph2.records_per_page || 0) * (pageSpec - 1),
                limit: paragraph2.records_per_page
              });
            } else {
              const isMtlElected = {
                key: 'dc_coverage.municipalities.keyword',
                value: 'Ville de Montréal'
              };
              filtersBy.push(isMtlElected);
              search = await contentApi.getEntries(language, {
                filtersBy,
                term: req.query.q,
                type: paragraph2Helper.mapContentTypes(language, paragraph2.content_type as ContentTypesParagraph),
                orderBy: constructOrderBy(paragraph2.sort_on, paragraph2.sort),
                offset: (paragraph2.records_per_page || 0) * (pageSpec - 1),
                limit: paragraph2.records_per_page
              });
              const nbPages = paginationHelpers.calculateNumberOfPages(search.total, search.limit);
              if (nbPages > 1) {
                for (let i = 2; i <= nbPages; i++) {
                  const searchsTemp = await contentApi.getEntries(language, {
                    filtersBy,
                    term: req.query.q,
                    type: paragraph2Helper.mapContentTypes(language, paragraph2.content_type as ContentTypesParagraph),
                    orderBy: constructOrderBy(paragraph2.sort_on, paragraph2.sort),
                    offset: (search.limit || 0) * (i - 1),
                    limit: search.limit
                  });

                  searchsTemp.entries.forEach(searchTemp => {
                    search.entries.push(searchTemp);
                  });
                }
              }
            }

            if (
              (paragraph2.content_type === ContentTypesParagraph.Elected ||
                paragraph2.content_type === ContentTypesParagraph.Petition) &&
              paragraph2.filters.length > 0
            ) {
              const taxoLabel = await taxonomyToFilterAdapter(paragraph2.filters, locals);
              return { search, taxoLabel };
            }
            return { search, taxoLabel: null };
          }
        }
        break;
      case ContentTypes.Elected:
        const responseInstances: any = await filterRelationContent(
          language,
          ContentTypes.Instance,
          content.dc_identifier
        );
        const responseCommissions: any = await filterRelationContent(
          language,
          ContentTypes.Commission,
          content.dc_identifier
        );
        return { instances: responseInstances.entries, commissions: responseCommissions.entries };

      case ContentTypes.Commission:
      case ContentTypes.Instance: {
        let members: IElectedContent[];
        if (content.dc_type.machine_name === ContentTypes.Commission) {
          members = content.mtl_content.commissions.members;
        } else {
          members = content.mtl_content.instances.members;
        }
        let elus: any = {};
        if (members) {
          const promise = await members.map(async (member: any) => {
            try {
              return (await contentApi.getEntry(language, member.elected_name[0].url)) as IElectedContent[];
            } catch (error) {
              return null;
            }
          });
          elus = Promise.all(promise);
        } else {
          elus = null;
        }
        let demarche: any = {};
        try {
          demarche = await contentApi.getEntries(language, {
            type: language === 'fr' ? 'demarches' : 'how-to',
            filtersBy: [
              {
                key: 'dc_relation.url',
                value: content.dc_identifier
              }
            ]
          });
          demarche = demarche.entries;
        } catch (error) {
          demarche = null;
        }

        return { elus: await elus, demarches: demarche };
      }
      case ContentTypes.Topic:
        let demarches: any = await contentApi
          .getEntries(language, {
            type: language === 'fr' ? 'demarches' : 'how-to',
            filtersBy: [
              {
                key: 'dc_relation.url',
                value: content.dc_identifier
              }
            ]
          })
          .catch(err => {
            return null;
          });
        if (demarches) {
          demarches = demarches.entries;
        }
        return demarches;
      case ContentTypes.ServiceRecord:
        const boroughs = await contentApi.getTaxonomy(language, t('common:taxonomies.borough'));
        return boroughs.filter(borough => borough.field_code_3l !== 'MTL');
      case ContentTypes.Demarche:
        const boroughsD = await contentApi.getTaxonomy(language, t('common:taxonomies.borough'));
        return boroughsD.filter(borough => borough.field_code_3l !== 'MTL');
      case ContentTypes.Collect:
        const boroughsC = await contentApi.getTaxonomy(language, t('common:taxonomies.borough'));
        return boroughsC.filter(borough => borough.field_code_3l !== 'MTL');

      case ContentTypes.Borough:
        const boroughMayor: INodeReference = content.mtl_content.vitrine_arrondissement.democratic_participation.mayor;
        const importantDates: INodeReference[] = content.mtl_content.vitrine_arrondissement.important_dates;

        // Responses from contentApi
        const responseBoroughMayor: IElectedContent = await contentApi
          .getEntry(language, boroughMayor.url)
          .catch(err => {
            return undefined;
          });
        const filtersSeances = [
          { key: 'dc_creator', value: content.dc_creator.toString() },
          { key: 'dc_temporal.end', value: today },
          {
            key: 'mtl_content.evenements.event_type.keyword',
            value: language === 'en' ? 'Council and committee' : 'Conseil et comité'
          }
        ];
        const responseBoroughEventsSeances: IContentTypeSearch = await getBoroughEvents(language, filtersSeances, 3);
        const responseImportantDates: IImportantDateContent[] = await getImportantDates(language, importantDates);

        let eventsBorough: IEventContent[] = [];
        let newsBorough: INewsContent[] = [];

        if (content.mtl_content.vitrine_arrondissement.events.visible) {
          const filtersEvents = [
            { key: 'dc_creator', value: content.dc_creator.toString() },
            { key: 'dc_temporal.end', value: today }
          ];
          const excludedFiltersEvents = [
            {
              key: 'mtl_content.evenements.event_type.keyword',
              value: language === 'en' ? 'Council and committee' : 'Conseil et comité'
            }
          ];
          const eBorough = await getBoroughEvents(language, filtersEvents, 3, excludedFiltersEvents);
          eventsBorough = eBorough.entries;
        }

        if (content.mtl_content.vitrine_arrondissement.news.visible) {
          const filtersNews = [{ key: 'dc_creator', value: content.dc_creator.toString() }];
          const nBorough = await getBoroughNews(language, filtersNews, 3);
          newsBorough = nBorough.entries;
        }

        return {
          importantDates: await responseImportantDates,
          eluBorough: responseBoroughMayor,
          eventsSeance: responseBoroughEventsSeances.entries,
          eventsBorough,
          newsBorough
        };

      case ContentTypes.Event:
        let relatedEvents: any = await contentApi
          .getEntries(language, {
            type: language === 'fr' ? 'evenements' : 'evenements'
            // filtersBy: [
            //   {
            //     key: 'dc_relation.url',
            //     value: content.dc_identifier
            //   }
            // ]
          })
          .catch(err => {
            return null;
          });
        if (relatedEvents) {
          relatedEvents = relatedEvents.entries.filter((event: IEventContent) => {
            if (
              event.dc_title !== content.dc_title &&
              event.mtl_content.evenements.event_type === content.mtl_content.evenements.event_type
            ) {
              return event;
            }
          });
        }

        let referredLocation: ILocationContentV2 | undefined;

        if (content.mtl_content.evenements.event_information.event_location) {
          referredLocation = await contentApi
            .getEntry(language, content.mtl_content.evenements.event_information.event_location.url)
            .catch(err => {
              return undefined;
            });
        }

        return { events: relatedEvents, location: await referredLocation };

      case ContentTypes.Location:
        if (content.dc_provenance.system_name === ESystemName.SYSTEM_GCS) {
          const filtersBy: any[] = [
            {
              key: 'dc_temporal.end',
              value: today
            },
            {
              key: 'dc_relation.url',
              value: content.dc_identifier
            }
          ];
          const orderBy: ISortData = { sortBy: 'dc_temporal.start', sortOrder: 'Asc' };
          const eventLocation: IContentTypeSearch | null = await contentApi
            .getEntries(language, {
              type: ContentTypes.Event,
              filtersBy,
              orderBy: constructOrderBy(orderBy.sortBy, orderBy.sortOrder),
              limit: 3
            })
            .catch(err => {
              return null;
            });
          return { events: eventLocation ? eventLocation.entries : null };
        }
        break;
      default:
        break;
    }
  } catch (e) {
    return null;
  }
}

// Une fonction pour filtrer les membre des instances et commissions
async function filterRelationContent(
  language: Language,
  contentType: ContentTypes,
  dcIdentifier: string,
  orderBy?: ISortData,
  filtersBy?: any[]
) {
  return await contentApi
    .getEntries(language, {
      type: contentType,
      filtersBy: [
        {
          key: 'dc_relation.url',
          value: dcIdentifier
        }
      ],
      orderBy: orderBy ? constructOrderBy(orderBy.sortBy, orderBy.sortOrder) : constructOrderBy('dc_title', 'Asc')
    })
    .catch(err => {
      return null;
    });
}

async function taxonomyToFilterAdapter(filers: IFilter[], locals: ILocalsProps): Promise<ITaxonomyFilterWrapper[]> {
  const language: Language = locals.language;

  const filterTemp: any[] = [];

  for (const filter of filers) {
    let boroughsList = await contentApi.getTaxonomy(language, filter.used_taxonomy);
    if (
      filter.used_taxonomy === ETaxonomyName.TAXONOMY_BOROUGH_EN ||
      filter.used_taxonomy === ETaxonomyName.TAXONOMY_BOROUGH_FR
    ) {
      boroughsList = boroughsList.filter(boroughC => boroughC.field_code_3l !== 'MTL');
    }
    const taxoLabel: ITaxonomyFilterWrapper = {
      taxonomy: boroughsList,
      label: filter.label,
      taxoName: filter.used_taxonomy,
      query_param: filter.search_field
    };
    filterTemp.push(taxoLabel);
  }
  return Promise.all(filterTemp) as Promise<ITaxonomyFilterWrapper[]>;
}

async function getImportantDates(
  language: Language,
  importantDates: INodeReference[]
): Promise<IImportantDateContent[]> {
  const promiseImportantDates = await importantDates.map(async (importantDate: INodeReference) => {
    try {
      return await contentApi.getEntry(language, importantDate.url);
      // tslint:disable-next-line:no-empty
    } catch (error) {}
  });

  const results: IImportantDateContent[] = await Promise.all(promiseImportantDates);

  return await results.filter((result: IImportantDateContent) => result !== undefined);
}

async function getBoroughNews(language: Language, filters: { key: string; value: string }[], limitNumb: number) {
  const type = paragraph2Helper.mapContentTypes(language, ContentTypesParagraph.News);
  const orderBy = constructOrderBy('dc_valid.start', 'Desc');

  return await contentApi.getEntries(language, {
    type,
    filtersBy: filters,
    orderBy,
    offset: 0,
    limit: limitNumb
  });
}

async function getBoroughEvents(
  language: Language,
  filters: { key: string; value: string }[],
  limitNumb: number = 3,
  excludedFilters?: { key: string; value: string }[]
) {
  const orderBy = constructOrderBy('dc_temporal.start', 'Asc');
  return await contentApi.getEntries(language, {
    type: ContentTypes.Event,
    filtersBy: filters,
    excludedFilters,
    orderBy,
    offset: 0,
    limit: limitNumb
  });
}

contentRouter.get('/*', contentMiddleware);
contentRouter.get('/en/*', contentMiddleware);
contentRouter.post('/*', contentMiddleware);
contentRouter.post('/en/*', contentMiddleware);

export { contentRouter };
