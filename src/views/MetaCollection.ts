export type IMeta = INamedMeta | IPropertyMeta;

interface INamedMeta {
  name: string;
  content: string;
}

interface IPropertyMeta {
  property: string;
  content: string;
}

export class MetaCollection {
  private _meta: IMeta[] = [
    { name: 'robots', content: 'index,follow' },
    { name: 'author', content: 'Ville de Montréal' },
    { name: 'copyright', content: 'Ville de Montréal' },
    { property: 'og:type', content: 'website' },
    { property: 'og:url', content: 'https://beta.montreal.ca/' },
    {
      property: 'og:image',
      content: 'https://res.cloudinary.com/villemontreal/image/upload/v1523647952/logo-montreal.jpg'
    },
    { property: 'twitter:card', content: 'summary_large_image' },
    { property: 'twitter:site', content: '@MTL_Ville' },
    { property: 'twitter:creator', content: '@MTL_Ville' },
    {
      property: 'twitter:image',
      content: 'https://res.cloudinary.com/villemontreal/image/upload/v1523647952/logo-montreal.jpg'
    }
  ];

  public get meta() {
    return this._meta.slice();
  }

  public addByName(name: string, content: string) {
    let existingMeta = this._meta.find(meta => isNamedMeta(meta) && meta.name === name);

    if (!existingMeta) {
      existingMeta = { name } as INamedMeta;
      this._meta.push(existingMeta);
    }

    existingMeta.content = content;
  }

  public addByProperty(property: string, content: string) {
    let existingMeta = this._meta.find(meta => !isNamedMeta(meta) && meta.property === property);

    if (!existingMeta) {
      existingMeta = { property } as IPropertyMeta;
      this._meta.push(existingMeta);
    }

    existingMeta.content = content;
  }
}

export function isNamedMeta(arg: any): arg is INamedMeta {
  return typeof arg.name !== 'undefined';
}
