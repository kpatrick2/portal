import * as React from 'react';
import { ISection } from '../../content-api';
import { isDefined } from './is-defined';
import { OnDomReadyScript } from './OnDomReadyScript';

interface IViewPageProps {
  title: string;
  id?: string;
  contentType?: string;
  sections?: ISection[];
}

export class ViewPage extends React.PureComponent<IViewPageProps> {
  public render() {
    const script = `
  var title = "${isDefined(this.props.title) ? this.props.title.replace(/\"/g,"") : '' }";
  var id = "${isDefined(this.props.id) ? this.props.id: '' }";
  var contentType = "${isDefined(this.props.contentType) ? this.props.contentType : ''}";
  var sections = ${isDefined(this.props.sections) ? JSON.stringify(this.props.sections) : "''" };

  analytics.viewPage(title, id, contentType, sections);
`;

    return <OnDomReadyScript script={script} />;
  }
}
