import * as React from 'react';
import { isDefined } from './is-defined';
import { OnDomReadyScript } from './OnDomReadyScript';

interface IFormProps {
  title: string;
  eventType: string;
  formName: string;
  formType: string;
  status: 'demarree' | 'completee',
  stepNumber: string;
  stepName: string;
}

export class Form extends React.PureComponent<IFormProps> {
  public render() {
    const script = `
  var title = "${isDefined(this.props.title) ? this.props.title.replace(/\"/g,"") : '' }";
  var eventType = '${this.props.eventType}';
  var formName = '${this.props.formName}';
  var formType = '${this.props.formType}';
  var status = '${this.props.status}';
  var stepNumber = '${this.props.stepNumber}';
  var stepName = '${this.props.stepName}';

  analytics.form(title, eventType, formName, formType, status, stepNumber, stepName);
`;
    return <OnDomReadyScript script={script} />;
  }
}
