import * as React from 'react';
import { isDefined } from './is-defined';
import { OnDomReadyScript } from './OnDomReadyScript';

interface IFormErrorProps {
  reason: string;
  message: string;
}

export class FormError extends React.PureComponent<IFormErrorProps> {
  public render() {
    const script = `
  var reason = "${isDefined(this.props.reason) ? this.props.reason.replace(/\"/g,"") : '' }";
  var message = "${isDefined(this.props.message) ? this.props.message.replace(/\"/g,"") : '' }";

  analytics.formError(reason, message);
`;

    return <OnDomReadyScript script={script} />;
  }
}
