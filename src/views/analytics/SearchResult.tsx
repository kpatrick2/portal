import * as React from 'react';
import { isDefined } from './is-defined';
import { OnDomReadyScript } from './OnDomReadyScript';

interface ISearchResultProps {
  title: string;
  searchTerm: string;
  numberOfResults: number;
  totalPages: number;
  currentPage: number;
  contentTypeFilter?: string;
  categories?: string[];
}

export class SearchResult extends React.PureComponent<ISearchResultProps> {
  public render() {
    const script = `
  var title = "${isDefined(this.props.title) ? this.props.title.replace(/\"/g,"") : '' }";
  var searchTerm = "${isDefined(this.props.searchTerm) ? this.props.searchTerm.replace(/\"/g,"") : '' }";
  var numberOfResults = ${this.props.numberOfResults};
  var totalPages = ${this.props.totalPages};
  var currentPage = ${this.props.currentPage};
  var contentTypeFilter = "${isDefined(this.props.contentTypeFilter) ?  this.props.contentTypeFilter : ''}" ;
  var categories = ${isDefined(this.props.categories) ? JSON.stringify(this.props.categories) : '[]' };

  analytics.searchResult(title, searchTerm, numberOfResults, totalPages, currentPage, contentTypeFilter, categories);
`;

    return <OnDomReadyScript script={script} />;
  }
}
