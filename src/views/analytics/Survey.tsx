import * as React from 'react';
import { isDefined } from './is-defined';
import { OnDomReadyScript } from './OnDomReadyScript';

interface ISurveyProps {
  title: string;
  eventType: string;
  status: 'demarree' | 'completee',
  stepNumber: string;
  stepName: string;
  fields: IAnalyticField[];
}

interface IAnalyticField {
  type: 'radio' | 'zone de texte';
  question: string;
  answerType: '';
  answer: string;
}

export class Survey extends React.PureComponent<ISurveyProps> {
  public render() {
    const script = `
  var title = "${isDefined(this.props.title) ? this.props.title.replace(/\"/g,"") : '' }";
  var eventType = '${this.props.eventType}';
  var status = '${this.props.status}';
  var stepNumber = '${this.props.stepNumber}';
  var stepName = '${this.props.stepName}';
  var fields = ${JSON.stringify(this.props.fields)};

  analytics.survey(title, eventType, status, stepNumber, stepName, fields);
`;

    return <OnDomReadyScript script={script} />;
  }
}
