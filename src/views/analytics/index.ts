import { Form } from './Form';
import { FormError } from './FormError';
import { SearchResult } from './SearchResult';
import { Survey } from './Survey';
import { ViewPage } from './ViewPage';

// tslint:disable-next-line:variable-name
export const Analytics = {
  Form,
  FormError,
  SearchResult,
  Survey,
  ViewPage
};
