import * as React from 'react';

interface IOnDomReadyScriptProps {
  script: string;
}

export class OnDomReadyScript extends React.PureComponent<IOnDomReadyScriptProps> {
  public render() {
    return (
      <script
        dangerouslySetInnerHTML={{
          __html: `document.addEventListener('readystatechange', function (evt) { if(evt.target.readyState==="complete") {${this.props.script}}}, false);`
        }}
      />
    );
  }
}
