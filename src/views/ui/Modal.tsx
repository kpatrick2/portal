import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IItem } from '../../content-api';
import { LocalizedLink } from '../LocalizedLink';

interface IListGroupProps {
    t: TranslationFunction;
    taxonomy: IItem[];
    title: string;
    id_target: string;
    id_labelledby: string;


}

export class Modal extends React.PureComponent<IListGroupProps> {
    public render() {

        if (this.props.taxonomy && this.props.taxonomy.length === 0) {
            return null;
        }
        return (

            <div className="modal fade" id={this.props.id_target} role="dialog" tabIndex={-1} aria-labelledby={this.props.id_labelledby} aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id={this.props.id_labelledby}>{this.props.title}</h5>
                            <button type="button" className="close btn-close" data-dismiss="modal" aria-label="Close">
                                <span className="vdm vdm-046-grand-x"></span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <ul>
                                {this.renderList()}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    private renderList() {
        if (this.props.taxonomy.length === 0) {
            return null;
        }

        const sectionData = [];
        for (const item of this.props.taxonomy) {
            sectionData.push(<li>
                <LocalizedLink
                    to="#"
                    title={item.value}
                    t={this.props.t}
                >{item.value}
                </LocalizedLink>

            </li>);
        }

        return sectionData;

    }

}