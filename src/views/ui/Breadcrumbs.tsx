import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ISection } from '../../content-api';
import { LocalizedLink } from '../LocalizedLink';

interface IBreadcrumbsProps {
  current: string;
  data?: ISection[];
  t: TranslationFunction;
}

export class Breadcrumbs extends React.PureComponent<IBreadcrumbsProps> {
  public render() {
  
    return (
      <nav className="breadcrumb-container" aria-label="breadcrumb">
        <ol className="breadcrumb">
          <li className="breadcrumb-item">
            <LocalizedLink to="/" t={this.props.t}>{this.props.t('common:home')}</LocalizedLink>
          </li>
          {this.renderSection()}
          <li className="breadcrumb-item">{this.props.current}</li>
        </ol>
      </nav>
    );
  }

  private renderSection(){
    if(!this.props.data || this.props.data.length === 0) {
      return null;
    }

    const sectionData = [];
    for (let i = 0; i < this.props.data.length; i++) {
        sectionData.push(<li className="breadcrumb-item" key={i}><LocalizedLink to={this.props.data[i].url || '/'} t={this.props.t}>{this.props.data[i].title}</LocalizedLink>
        </li>);
    }

    return sectionData;

  }

}
