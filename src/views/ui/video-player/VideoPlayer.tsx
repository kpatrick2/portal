import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IVideo } from '../../../content-api';
import { LocalizedLink } from '../../LocalizedLink';

interface IVideoPlayerProps {
  video: IVideo[];
  t: TranslationFunction;
}

export class VideoPlayer extends React.PureComponent<IVideoPlayerProps> {
  public render() {
    if (this.props.video.length === 0) {
      return null;
    }

    const video = this.props.video[0];


    return (
      <div className="section-inner section-inner-lg section-inner-media">
        <div className="media embed-responsive embed-responsive-16by9 ui-videoplayer">
        <LocalizedLink to={video.url} t={this.props.t}>
            {video.name}
          </LocalizedLink>        </div>
      </div>
    );
  }
}
