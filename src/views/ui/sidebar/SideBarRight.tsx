import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IAppelActionGroup } from '../../../content-api';
import { LocalizedLink } from '../../LocalizedLink';

interface ISideBarSectionProps {
    className: string;
    itemElements?: IAppelActionGroup | null;
    t: TranslationFunction;

}
export class SideBarRight extends React.PureComponent<ISideBarSectionProps> {
    public render() {
        if(!this.props.itemElements){
            return null;
        }
        return (
            <aside className={this.props.className} role="complementary">
                <div className="sidebar">
                    <section className="sb-block">

                        {(this.props.itemElements && this.props.itemElements.title) ? (
                            <h2 className="sidebar-title">
                                {this.props.itemElements.title}
                            </h2>
                        ) : null}

                        {(this.props.itemElements && this.props.itemElements.text) ? (
                            <p>
                                {this.props.itemElements.text}
                            </p>
                        ) : null}
                        <LocalizedLink
                            to={this.props.itemElements && this.props.itemElements.call_to_action && this.props.itemElements!.call_to_action.length > 0 ? (this.props.itemElements.call_to_action[0].url) : "#"}
                            className="btn btn-secondary"
                            t={this.props.t}
                        >
                            {this.props.itemElements && this.props.itemElements.call_to_action && this.props.itemElements.call_to_action.length > 0 ? this.props.itemElements.call_to_action[0].title : ""}
                        </LocalizedLink>
                    </section>
                </div>
            </aside>
        );
    }
}
