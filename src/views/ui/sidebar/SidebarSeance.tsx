import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { LocalizedLink } from '../../LocalizedLink';

// le TC Evenement n'est pas encore disponible

interface ISideBarSeance {
  t: TranslationFunction;
  seance?: any;
}
export class SideBarSeance extends React.PureComponent<ISideBarSeance> {
  public render() {
    if (this.props.seance) {
      return (
        <aside className="col-12 col-lg-4 sidebar-right" role="complementary">
          <div className="sidebar">
            <section className="sb-block">

              <h2 className="sidebar-title">
                Prochaine séance
                </h2>
              <ul className="list-unstyled">
                <li>
                  <span className="icon left">
                    <span className="vdm vdm-005-calendrier" aria-hidden="true"></span>
                    15 septembre 2018, à 8h30
                        {/* 
                        <br/><DateTime date={this.props.content.dc_created} format="date-time" t={this.props.t} />
                        */}
                  </span></li>
                <li>
                  <span className="icon left">
                    <span className="vdm vdm-091-localisation " aria-hidden="true"></span>
                    <address>
                      <strong>Nom du lieu</strong>
                      <br />123, bord de l'eau
                          <br />Montréal, QC, H3K 2T5
                        </address>
                  </span>
                </li>
                <li>
                  <span className="no-icon left">
                    <LocalizedLink
                      to="#"
                      className="icon right read-more"
                      t={this.props.t}
                    >
                      En savoir plus <span className="vdm vdm-063-fleche-droite " aria-hidden="true"></span></LocalizedLink>
                  </span>
                </li>
              </ul>
              {/* La page de présentation de l'ensemble des séances n'existe pas. Cela fera l'objet d'une autre tâche incluant maquette. */}
              <button type="button" className="btn btn-secondary">Toutes les séances</button>
            </section>
          </div>
        </aside>
      );
    }
    return null;

  }
}
