import classnames from 'classnames';
import * as React from 'react';

interface ISideBarProps {
  stroke?: boolean;
  order?: 'first' | 'last';
}

export class SideBar extends React.PureComponent<ISideBarProps> {
  public render() {

    const asideClasses = {
      'col-12 col-lg-5 col-xl-4': true,
      'order-lg-last': this.props.order === "last",
      'order-lg-first': this.props.order === "first"
    }

    const classes = {
      sidebar: true,
      'sidebar-top-stroke': this.props.stroke
    };

    return (
      <aside className={classnames(asideClasses)} role="complementary">
        <div className={classnames(classes)}>
          {this.props.children}
        </div>
      </aside>
    );
  }
}
