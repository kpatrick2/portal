import * as classnames from 'classnames';
import * as React from 'react';

interface ISideBarItemProps {
  icon?: string;
  label?: string;
  title?: string;
}

export class SideBarItemBlock extends React.PureComponent<ISideBarItemProps> {
  public render() {
    const iconClasses = this.props.icon ? classnames({
      vdm: true,
      [`vdm-${this.props.icon}`]: true
    }) : undefined;

    return (
      <section className="sb-block">
        {this.props.title &&
          <h2 className="sidebar-title ">
            {this.props.title}
          </h2>}
        <span className={iconClasses} aria-hidden="true"></span>
        <div className="list-icon-content">
          {this.props.label ? (
            <span className="list-icon-label">{this.props.label}</span>
          ) : null}
          {this.props.children}
        </div>
        </section>
    );
  }
}
