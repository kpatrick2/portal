import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IFileInformation } from '../../../content-api';
import { LocalizedLink } from '../../LocalizedLink';
import { SideBarItemBlock } from './SideBarItemBlock';

interface ISideBarItemProps {
  title: string;
  documents: IFileInformation[];
  t: TranslationFunction;
}

export class SideBarItemBlockDocument extends React.PureComponent<ISideBarItemProps> {
  public render() {
    if (this.props.documents && this.props.documents.length > 0) {
      return (
        <SideBarItemBlock title={this.props.title}>
          <ul>
            {this.checkDocumentType()}
          </ul>
        </SideBarItemBlock>
      )
    } else {
      return null;
    }
  }

  private checkDocumentType() {
    return this.props.documents.map((doc, index) => {
      const fileName = (doc.description && doc.description.length > 0 ? doc.description : doc.file_name);
      return <li key={index}>
        <LocalizedLink title={fileName} to={doc.url} t={this.props.t} />
      </li>
    })
  }
}
