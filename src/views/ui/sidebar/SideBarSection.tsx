import * as React from 'react';

interface ISideBarSectionProps {
  title?: string;
}

export class SideBarSection extends React.PureComponent<ISideBarSectionProps> {
  public render() {
    return (
      <>
        {this.props.title &&
          <h2 className="sidebar-title ">
            {this.props.title}
          </h2>}
        <li className="list-icon-item">
          {this.props.children}
        </li>
      </>
    );
  }
}
