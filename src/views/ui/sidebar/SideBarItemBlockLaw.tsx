import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IDocument } from '../../../content-api';
import { LocalizedLink } from '../../LocalizedLink';
import { SideBarItemBlock } from './SideBarItemBlock';

interface ISideBarItemProps {
  title: string;
  data?:IDocument[];
  t: TranslationFunction;
}

export class SideBarItemBlockLaw extends React.PureComponent<ISideBarItemProps> {
  public render() {
    if (this.props.data && this.props.data.length > 0) {
      return (
        <SideBarItemBlock title={this.props.title}>
          <ul>
            {this.props.data.map((doc, index) => {
              return <li key={index}><LocalizedLink title={doc.title} to={doc.url} type={doc.type} t={this.props.t} /></li>
            })}
          </ul>
        </SideBarItemBlock>
      )
    }else{
      return null;
    }
  }
}
