import * as React from 'react';

interface IToolboxColumnListProps {
  items: any[];
  nbColumn: number;
}

export class ToolboxColumnList extends React.PureComponent<IToolboxColumnListProps> {
  public render() {
    return (
        <ul>
          {this.props.items.map((item, key) => {
            return (
              <li key={key}>
                <div dangerouslySetInnerHTML={{ __html: item }} />
              </li>
            );
          })}
        </ul>
    );
  }
}
