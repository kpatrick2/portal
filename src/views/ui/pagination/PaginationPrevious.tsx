import { TranslationFunction } from 'i18next';
import * as React from 'react';

interface IPaginationPreviousProps {
  href?: string;
  t: TranslationFunction;
}

export class PaginationPrevious extends React.PureComponent<IPaginationPreviousProps> {
  public render() {
    return (
      <li className="page-item">
        <a aria-label={this.props.t('search:pagination.previous')} href={this.props.href} className="page-link">
        <span className="vdm vdm-058-chevron-gauche " aria-hidden="true"></span>
          <span className="sr-only">{this.props.t('search:pagination.previous')}</span>
        </a>
      </li>
    );
  }
}
