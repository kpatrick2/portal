import * as classnames from 'classnames';
import * as React from 'react';

interface IPaginationPageProps {
  page: number;
  currentPage: number;
  href?: string;
}

export class PaginationPage extends React.PureComponent<IPaginationPageProps> {
  public render() {
    const classNames = classnames({
      'active': this.props.page === this.props.currentPage,
      'page-item': true
    });

    return (
      <li className={classNames}>
        <a href={this.props.href} className="page-link">
          {this.props.page}
        </a>
      </li>
    );
  }
}
