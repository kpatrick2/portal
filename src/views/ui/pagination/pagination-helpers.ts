import { contentApi, ContentTypes, IContentType } from '../../../content-api';
import { Language } from '../../../utils';

export function calculateCurrentPage(offset: number, limit: number) {
  return Math.floor((offset || 0) / limit) + 1;
}

export function calculateNumberOfPages(total: number, limit: number) {
  const safeTotal = Math.max(total, 1);
  const safeLimit = Math.max(limit, 1);
  return Math.ceil(safeTotal / safeLimit);
}

export async function paginationContentType(
  contentTypes: ContentTypes,
  language: Language,
  limit?: number
): Promise<IContentType> {
  // On doit changer le paramettre limit avec contentTypeSearch.limit dés qu'on soit sur que tous les TCs retournent le champ limit (Tâche en attente d'approbation)
  const responseInstances = await contentApi.getEntries(language, {
    type: contentTypes
  });
  const nbPages = calculateNumberOfPages(responseInstances.total, 10);
  if (nbPages > 1) {
    for (let i = 2; i <= nbPages; i++) {
      const searchsTemp = await contentApi.getEntries(language, {
        type: responseInstances.entries[0].dc_type.machine_name as ContentTypes,
        offset: (limit || 0) * (i - 1),
        limit
      });

      searchsTemp.entries.forEach(searchTemp => {
        responseInstances.entries.push(searchTemp);
      });
    }
  }

  return responseInstances;
}
