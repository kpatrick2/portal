import * as React from 'react';

export class PaginationEllipsis extends React.PureComponent {
  public render() {
    return (
      <li className="page-item">
        <a className="page-link">
          ...
        </a>
      </li>
    );
  }
}
