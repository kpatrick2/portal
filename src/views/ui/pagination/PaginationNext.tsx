import { TranslationFunction } from 'i18next';
import * as React from 'react';

interface IPaginationNextProps {
  href?: string;
  t: TranslationFunction;
}

export class PaginationNext extends React.PureComponent<IPaginationNextProps> {
  public render() {
    return (
      <li className="page-item">
        <a aria-label={this.props.t('search:pagination.next')} href={this.props.href} className="page-link">
          <span className="vdm vdm-056-chevron-droite " aria-hidden="true"></span>
          <span className="sr-only">{this.props.t('search:pagination.next')}</span>
        </a>
      </li>
    );
  }
}
