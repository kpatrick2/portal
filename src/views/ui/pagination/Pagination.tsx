import { TranslationFunction } from 'i18next';
import * as _ from 'lodash';
import * as React from 'react';
import { urlHelpers } from '../../../utils';
import * as paginationHelpers from './pagination-helpers';
import { PaginationEllipsis } from './PaginationEllipsis';
import { PaginationNext } from './PaginationNext';
import { PaginationPage } from './PaginationPage';
import { PaginationPrevious } from './PaginationPrevious';

interface IPaginationProps {
  currentUrl: string;
  limit: number;
  offset: number;
  total: number;
  t: TranslationFunction;
}

export class Pagination extends React.PureComponent<IPaginationProps> {
  constructor(props: IPaginationProps) {
    super(props);
    this.isValidPageNumber = this.isValidPageNumber.bind(this);
  }

  private get currentPage() {
    return paginationHelpers.calculateCurrentPage(this.props.offset, this.props.limit);
  }

  private get numberOfPages() {
    return paginationHelpers.calculateNumberOfPages(this.props.total, this.props.limit);
  }

  private get pagesToShow() {
    const range = 2;
    return _.range(-range, range + 1)
      .map(offset => this.currentPage + offset)
      .filter(this.isValidPageNumber);
  }

  public render() {

    if (this.props.total <= this.props.limit) {
      return null;
    }
    return (
      <div className="pagination-wrapper">
          <nav aria-label={this.props.t('search:pagination.label')}>
            <ul className="pagination justify-content-center">
              <PaginationPrevious href={this.getHref(this.currentPage - 1)} t={this.props.t} />
              {this.pagesToShow[0] > 1 ? <PaginationEllipsis /> : null}
              {this.pagesToShow.map(page => (
                <PaginationPage
                  key={page}
                  page={page}
                  currentPage={this.currentPage}
                  href={this.getHref(page)}
                />
              ))}
              {_.last(this.pagesToShow)! < this.numberOfPages ? <PaginationEllipsis /> : null}
              <PaginationNext href={this.getHref(this.currentPage + 1)} t={this.props.t} />
            </ul>
          </nav>
      </div>
    );
  }

  private isValidPageNumber(page: number): boolean {
    return page > 0 && page <= this.numberOfPages;
  }

  private getHref(page: number) {
    if (!this.isValidPageNumber(page) || page === this.currentPage) {
      return undefined;
    }

    return urlHelpers.updateQueryStringParameter(this.props.currentUrl, 'page', page.toString());
  }
}
