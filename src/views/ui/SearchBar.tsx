import classnames from 'classnames';
import * as React from 'react';

interface ISearchBarProps {
  placeholder: string;
  value: string;
  xs?: boolean;
  width?: 'col-lg-8';
  light_border?: boolean;
}

export class SearchBar extends React.PureComponent<ISearchBarProps> {
  public render() {

    const inputGroupClass = classnames({
      "input-group-icon": true,
      "input-group-icon-left": true,
      "input-group-icon-lg": !this.props.xs
    })

    const inputClass = classnames({
        "form-control": true,
        "form-control-dark": !this.props.light_border,
        "form-control-lg": !this.props.xs
    });

    const colClass = classnames({
        "col-12": true,
        "col-lg-8": (this.props.width && this.props.width === 'col-lg-8')
    });

    return (
      <div className="row">
        <div className={colClass}>
          <div className="form-group">
            <div className={inputGroupClass} >
              <span className="vdm vdm-002-recherche " aria-hidden="true"></span>
              <input type="text" className={inputClass} id="input-group-search" placeholder={this.props.placeholder} aria-label={this.props.placeholder} name="q" defaultValue={this.props.value}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
