import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IPhone } from '../../content-api';

interface IPhoneProps {
  phone: IPhone;
  t: TranslationFunction;
}

export class PhoneLabel extends React.PureComponent<IPhoneProps> {
  public render() {
    return (
    <>
          <span>{this.props.phone.type && this.props.phone.type === 'Information' ? `Tel. : ${this.props.phone.number}` : `Fax : ${this.props.phone.number}`}</span>
          {this.props.phone.extension ? this.props.t('content:location.phoneExt') + " " + this.props.phone.extension : null } <br />
        </>
    );
  }
}



