import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IAddress, IUrl } from '../../../content-api';
import { googleMapsAddessLink } from '../../../utils';
import { LocalizedLink } from '../../LocalizedLink';

interface IAddressProps {
  link?: IUrl;
  address: IAddress;
  coordinates?: number[];
  t?: TranslationFunction;
}

export class Address extends React.PureComponent<IAddressProps> {
  public render() {

    const address = this.props.address;
    const link = this.props.link;
    const blocAddress = [];
    blocAddress.push(this.formatAddress(address, link));
    if (address && address.address) {
      blocAddress.push(this.addItineraryLink(address))
    }
    return (
      blocAddress
    );
  }
  public formatAddress(address: IAddress, link: any) {
    const content = [];

    let addressLink: any;

    if (link && this.props.t) {
      addressLink = <div className="mb-1"><LocalizedLink to={link.uri} title={link.title} t={this.props.t} /></div>;
      content.push(addressLink);
    }

    if (address.address) {
      content.push(address.address);
      content.push(<br />);
    }

    if (address.address2) {
      content.push(address.address2);
      content.push(<br />);
    }

    if (address.city) {
      content.push(address.city);
      content.push(' ');
    }

    if (address.province) {
      content.push('(' + address.province + ')');
      content.push(' ');
    }

    if (address.postal_code) {
      content.push(address.postal_code);
    }

    return <address>{content}</address>;
  }

  private addItineraryLink(address:IAddress) {
    let itineraryLink = null;
    if (this.props.t) {
      itineraryLink = (
        <div className="mt-1">
          <LocalizedLink to={googleMapsAddessLink(address)}
            t={this.props.t}
            title={this.props.t("common:map_route_label")}>
          </LocalizedLink>
        </div>
      );
    }
    return itineraryLink;
  }
}
