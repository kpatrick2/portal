import * as classnames from 'classnames';
import * as React from 'react';

interface IToolboxIconProps {
  name: string;
  iconClass?: string;
}

export class ToolboxIcon extends React.PureComponent<IToolboxIconProps> {
  public render() {
    const classes = classnames({
      vdm: true,
      [this.props.name]: true,
      [this.props.iconClass!]: !!this.props.iconClass
    });

    return <span className={classes} />;
  }
}
