import * as classnames from 'classnames';
import * as React from 'react';
import { ESocialNetworks, IAppelAction } from '../../content-api';
interface ISocialMediaProps {
    media?: IAppelAction[];
}
export class SocialMedia extends React.PureComponent<ISocialMediaProps> {
    public render() {
        let element: JSX.Element | null = null;
        if (this.props.media && this.props.media.length) {
            element = (<ul className="social-media-list social-media-list-icons">
                {this.props.media.map(network => this.addSocialMediaList(network))}
            </ul>);
        }
        return element;
    }
    private addSocialMediaList(network: IAppelAction) {
        let classes: any = {
            'vdm': true
        }

        classes[ESocialNetworks[network.title.toLowerCase() as any]] = true;
        classes = classnames(classes);
        return (<li>
            <a href={network.url} title={network.title}>
                <span className={classes} aria-hidden="true" />
                <span className="sr-only">{network.title}</span>
            </a>
        </li>)
    }

}