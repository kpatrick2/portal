import * as React from 'react';
import { IFeatureCollection } from '../../content-api/interfaces/geojson-interfaces';

interface ILeafletMapProps {
  center?: ILeafletCoordinates;
  zoom?: number;
  markers?: ILeafletMarker[];
  markerClickable?: boolean;
  zooming?: boolean;
  dragging?: boolean;
  coordinates?: IFeatureCollection;
  selected?: boolean;
  clickable?: boolean;
}

interface ILeafletCoordinates {
  latitude: number;
  longitude: number;
}

interface ILeafletMarker extends ILeafletCoordinates {
  onSelectionAddClassToId?: string;
}

export class LeafletMap extends React.PureComponent<ILeafletMapProps> {
  public render() {
    const serializedData = JSON.stringify({
      center: this.props.center || { latitude: 45.553486897158, longitude: -73.70967864990234 },
      zoom: this.props.zoom || 11,
      markers: this.props.markers,
      coordinates: this.props.coordinates,
      zooming: this.props.zooming,
      dragging: this.props.dragging,
      selected: this.props.selected,
      clickable: this.props.clickable
    });

    return <div className="ui-leaflet" data-leaflet-map={serializedData} />;
  }
}
