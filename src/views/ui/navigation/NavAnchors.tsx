import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { Anchor } from './Anchor';

interface INavAnchors {
  text?: string[];
  tagLink?: string[];
  idSuffix?: string;
  t?: TranslationFunction;
}

export class NavAnchors extends React.PureComponent<INavAnchors> {
  public render() {
    const anchorId = this.props.idSuffix ? "navAnchors" + this.props.idSuffix : "navAnchors";
    const boroughSuffix = this.props.idSuffix ? '-' + this.props.idSuffix : '';

    return (
      <nav id={anchorId} data-toggle={`sticky-onscroll${boroughSuffix}`} className="sticky">
        <ul className="nav nav-anchors">
          {this.props.text && this.props.tagLink && this.props.t && this.putNav()}
          {this.props.children}
        </ul>
      </nav>
    );
  }

  private putNav() {
    const sectionData = [];
    for (let i = 0; i < this.props.tagLink!.length; i++) {
      if (this.props.tagLink![i].indexOf("navTop") > -1) {
        sectionData.push(<Anchor text={this.props.text![i]} to={this.props.tagLink![i]} t={this.props.t!} abstract />)
      } else {
        sectionData.push(<Anchor text={this.props.text![i]} to={this.props.tagLink![i]} t={this.props.t!} />)
      }
    }

    return sectionData;
  }

}