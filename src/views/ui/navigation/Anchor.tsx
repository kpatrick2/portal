import classnames from 'classnames';
import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { LocalizedLink } from '../../LocalizedLink';

interface INavAnchors {
  text: string;
  to: string;
  abstract?:boolean;
  t: TranslationFunction;
}

export class Anchor extends React.PureComponent<INavAnchors> {
  public render() {
    const classes = classnames({
      "nav-link":true,
      "js-scroll-trigger":true,
    })

    const liClasses = classnames({
      "nav-item": true,
      "nav-top": this.props.abstract
    })

    return (
      <li className={classnames(liClasses)}>
        <LocalizedLink
          to={this.props.to}
          className={classes}
          t={this.props.t}>
          {this.props.text}
        </LocalizedLink>
      </li>
    );
  }
}