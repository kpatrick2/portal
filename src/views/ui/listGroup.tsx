import * as React from 'react';

interface IListGroupProps {
    title: string;
    items: string[];
}

export class ListGroup extends React.PureComponent<IListGroupProps> {
    public render() {
        if(!this.props.items || this.props.items.length === 0){
            return null;
        }
        return (

            <div className="page-section pt-0">
                <h2 className="section-heading">{this.props.title}</h2>
                <ul className="list-group ">
                    {this.renderList()}
                </ul>
            </div>
        );
    }

    private renderList() {
        if (this.props.items.length === 0) {
            return null;
        }

        const sectionData = [];
        for(const item of this.props.items){
            sectionData.push(<li className="list-group-item" >{item}</li>);
        }

        return sectionData;

    }

}
