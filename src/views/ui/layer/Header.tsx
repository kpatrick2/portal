import * as classnames from 'classnames';
import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { DateTime } from '../../DateTime';
import { LocalizedLink } from '../../LocalizedLink';

interface IHeaderProps {
  t: TranslationFunction;
  title: string;
  updatedAtLabel?: string;
  date?: string;
  hub_link?: string;
  hub_url?: string;
  dark?: boolean;
  gray?: boolean;
  titleheader?: 'display-4' | 'display-1';
  fluid?: boolean;
  infos?: JSX.Element;
  subtitle?: string;
}

export class Header extends React.PureComponent<IHeaderProps> {
  public render() {
    const sectionClasses = classnames({
      'content-header': true,
      'content-header-dark': this.props.dark,
      'bg-gray-sky': this.props.gray,
    });
    const headingClasses:any = {
      'display-2':!this.props.titleheader, 
    }

    const containerClasses = classnames({
      'container-fluid': true,
      'container-fluid-xl': this.props.fluid
    })

    if(this.props.titleheader) {
      headingClasses[this.props.titleheader] = true;
    }
    const h1Class = classnames(headingClasses);
    return (
      <header className={classnames(sectionClasses)}>
        <div className={classnames(containerClasses)}>
          <div className="row">
            <div className="col-12">
              {this.props.subtitle ? (
                <div className="content-header-subtitle">{this.props.subtitle}</div>
              ) : null}
              <div className="content-header-title">
                <h1 className={classnames(h1Class)}>{this.props.title}</h1>
              </div>
            </div>
            {this.props.date ? (
              <div className="col order-1">
                <div className="content-header-metadata">
                  <div className="date-time updated">
                    {this.props.updatedAtLabel ? this.props.updatedAtLabel : this.props.t('common:publicationDate.lastUpdated.label')}
                    <time dateTime={this.props.date}>
                      <DateTime date={this.props.date} format="date" t={this.props.t} />
                    </time>
                  </div>
                </div>
              </div>
            ) : null}

            {this.props.infos ? (
                this.props.infos
            ): null}

            {this.props.hub_link && this.props.hub_url ? (
              <div className="col-12 col-xl-auto order-first order-xl-last">
                <div className="hub-link">
                  <LocalizedLink to={this.props.hub_url} title={this.props.hub_link} t={this.props.t} />
                </div>
              </div>
            ) : null}
          </div>
        </div>
        {this.props.children}
      </header>
    );
  }
}


