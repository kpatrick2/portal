import * as classnames from 'classnames';
import * as React from 'react';

interface IPageSectionProps {
  title?: string;
  title_align?: boolean;
  dark?:boolean;
}

export class PageSection extends React.PureComponent<IPageSectionProps> {

  public render() {
    const sectionClasses = classnames({
      'page-section': true,
      'bg-gray-sky': this.props.dark,
    });

    const titleClasses = classnames({
      'col-12': true,
      'text-center': !this.props.title_align
    })

    return (
      <section className={classnames(sectionClasses)} >
        <div className="container" >
          {this.props.title &&
            <div className="row" >
              <div className={classnames(titleClasses)} >
                <h2 className="section-heading" > {this.props.title}</h2>
              </div>
            </div>
          }
          {this.props.children}
        </div>
      </section>
    );
  }
}