import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ISection } from '../../../content-api';


interface IBreadcrumbsProps {
  // current: string;
  data?: ISection[];
  t: TranslationFunction;
}

export class Breadcrumbs extends React.PureComponent<IBreadcrumbsProps> {
  public render() {
  
    return (
      <div className="container-fluid container-fluid-max-xl">
    <div className="row">
      <div className="col-12">
      <nav className="breadcrumb-container px-4" aria-label="breadcrumb">
        <ol className="breadcrumb ">
          <li className="breadcrumb-item"><a href="#">Accueil</a></li>
          <li className="breadcrumb-item"><a href="#">Niveau 1</a></li>
        </ol>
      </nav>
      
      </div>
    </div>
  </div>
    );
  }

  // en suspens
  // private renderSection(){
  //   if(!this.props.data || this.props.data.length === 0) {
  //     return null;
  //   }

  //   const sectionData = [];
  //   for (let i = 0; i < this.props.data.length; i++) {
  //       sectionData.push(<li className="breadcrumb-item" key={i}><LocalizedLink to={this.props.data[i].url || '/'} t={this.props.t}>{this.props.data[i].title}</LocalizedLink>
  //       </li>);
  //   }

  //   return sectionData;

  // }

}
