import * as classnames from 'classnames';
import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IAddress, IContactInformation, IImage, ITaxoCode, ITemporal } from '../../../content-api';
import { EEventStatus } from '../../../content-api/enumerations/event-status';
import { cloudinaryUrlAdapter } from '../../../utils/cloudinary-adapter';
import { DateInterval } from '../../DateInterval';
import { LocalizedLink } from '../../LocalizedLink';
import { Address } from '../address/Address';
import { ImageGalleryV2 } from '../images';

interface IHeaderJumbotronProps {
  t: TranslationFunction;
  images?: IImage[];
  title: string;
  rate?: string;
  entry_right?: string;
  temporal?: ITemporal;
  status?: ITaxoCode;
  address?: IAddress;
  borough?: string;
  maps?: boolean;
  contact_informations?: IContactInformation;
}

// tslint:disable-next-line:cyclomatic-complexity
export class HeaderJumbotron extends React.PureComponent<IHeaderJumbotronProps> {
  // tslint:disable-next-line:cyclomatic-complexity
  public render() {
    let status = this.props.status;
    let imageStyle: any = {};

    const bsClasses = classnames({
      'col-12': true,
      'col-lg-6 col-xl-5': this.props.images && this.props.images.length > 0,
      'col-xl-8': this.props.images && this.props.images.length === 0
    });

    const badgeClasses: any = {
      "badge": true,
      "badge-pill": true
    };

    if (status) {
      const code: any = status.code;
      if (EEventStatus[code]) {
        badgeClasses[EEventStatus[code] as string] = true;
      } else {
        status = undefined;
      }
    }

    if (this.props.images && this.props.images.length) {
      imageStyle = {
        backgroundImage: `url(${cloudinaryUrlAdapter(this.props.images[0].id)})`
      }
    }

    return (
      <header className="content-header content-header-jumbotron mt-4 mt-lg-11">
        <div className="container-fluid">
          <div className="row mx-n4 no-gutters bg-gray-sky">
            <div className={classnames(bsClasses)}>
              <div className="content-header-wrapper">
                <div className="content-header-headings">
                  {status &&
                    <div className="content-header-headings-state">
                      <span className={classnames(badgeClasses)}>{status.name}</span>
                    </div>
                  }
                  <h1 className="h2">{this.props.title}</h1>


                  {this.props.borough &&
                    <div className="content-header-label">
                      <span className="label">{this.props.t("content:locationV2.borough_label")}</span>
                      {this.props.borough}
                    </div>
                  }

                </div>
                <div className="content-header-metadata">
                  <ul className="list-icon row no-gutters">

                    {this.props.temporal &&
                      <li className="list-icon-item col-12">
                        <span className="vdm vdm-005-calendrier " aria-hidden="true"></span>
                        <div className="list-icon-content">
                          <span className="list-icon-label">{this.props.t("content:evenements.date_hour")}</span>
                          <DateInterval en={this.props.t('common:languageCode') === 'en'} start={this.props.temporal.start} end={this.props.temporal.end} t={this.props.t} />
                        </div>
                      </li>
                    }

                    {typeof this.props.address !== 'undefined' && this.props.address &&
                      <li className="list-icon-item ">
                        <span className="vdm vdm-091-localisation " aria-hidden="true"></span>
                        <div className="list-icon-content">
                          <span className="list-icon-label">{this.props.t("content:locationV2.address_title")}</span>

                          <div className="list-icon-block">
                            <Address address={this.props.address}></Address>
                          </div>

                          {this.props.maps &&
                            <div className="list-icon-block">
                              <LocalizedLink to="#mapLieu001" className="js-scroll-trigger" t={this.props.t}>
                                {this.props.t("common:content:map_display_link_label")}
                              </LocalizedLink>
                            </div>
                          }
                        </div>

                      </li>
                    }

                    {this.props.contact_informations && this.props.contact_informations.phone &&
                      <li className="list-icon-item">
                        <span className="vdm vdm-012-telephone " aria-hidden="true"></span>
                        <div className="list-icon-content">
                          <span className="list-icon-label">{this.props.t("content:locationV2.phone_label")}</span>
                          {this.props.contact_informations.phone}
                        </div>
                      </li>
                    }
                    {this.props.rate &&
                      <li className="list-icon-item col-lg-6">
                        <span className="vdm vdm-113-prix " aria-hidden="true"></span>
                        <div className="list-icon-content">
                          <span className="list-icon-label">{this.props.t("content:evenements.rate")}</span>
                          <span>{this.props.rate}</span>
                        </div>
                      </li>
                    }
                    {this.props.entry_right &&
                      <li className="list-icon-item col-lg-6">
                        <span className="vdm vdm-007-billet " aria-hidden="true"></span>
                        <div className="list-icon-content">
                          <span className="list-icon-label">{this.props.t("content:evenements.entry")}</span>
                          <span>{this.props.entry_right}</span>
                        </div>
                      </li>
                    }
                  </ul>
                </div>
              </div>
            </div>
            {this.props.images && this.props.images.length > 0 &&
              <div className="col-12 col-lg-6 col-xl-7 order-first order-lg-last">
                <ImageGalleryV2 images={this.props.images} t={this.props.t}>
                  <div className="media-cover" role="image" aria-label="media-image.mediaAlt" style={imageStyle}></div>
                </ImageGalleryV2>
              </div>
            }
          </div>
        </div>
      </header>
    );
  }

}