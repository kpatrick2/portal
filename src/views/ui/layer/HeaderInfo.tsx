import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { DateInterval } from '../../DateInterval';

interface IHeaderInfoProps {
  t: TranslationFunction;
  siteName?: string;
  badge: string;
  prebadge?: any;
  strike?: boolean;
  startDate?: string;
  endDate?: string;
}

export class HeaderInfo extends React.PureComponent<IHeaderInfoProps> {
  public render() {

    return (
      <div className="col order-1">
        <div className="content-header-metadata metadata-infos">
          {this.props.prebadge}
          <div className="infos-event ">
            {this.props.startDate && this.props.endDate &&
              <span className="infos-date">
              {this.props.startDate && this.props.endDate ?
                (this.props.strike ? (
                  <s><DateInterval en= {this.props.t('common:languageCode') === 'en'} start={this.props.startDate} end={this.props.endDate} t={this.props.t} /></s>
                  // <s>{this.formatDate()}</s>
                ) : (<DateInterval en= {this.props.t('common:languageCode') === 'en'} start={this.props.startDate} end={this.props.endDate} t={this.props.t} />)
                ) : null
              }
              </span>
            }
            {!this.props.strike && this.props.siteName &&
              <span className="infos-lieu">{this.props.siteName}</span>}
          </div>

          <a href="#" className="badge">{this.props.badge}</a>
        </div>
      </div>
    );
  }
}
