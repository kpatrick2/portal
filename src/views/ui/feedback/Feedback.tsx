import { TranslationFunction } from 'i18next';
import * as queryString from 'query-string';
import * as React from 'react';
import { Language } from '../../../utils';
import { FeedbackForm } from './FeedbackForm';

interface IFeedbackProps {
  currentUrl: string;
  t: TranslationFunction;
  language: Language;
  appVersion: string;
}

export class Feedback extends React.PureComponent<IFeedbackProps> {
  public render() {
    const queryStrings = queryString.parse(queryString.extract(this.props.currentUrl));


    return (
      <section className="bg-gray-sky">
        <div className="container">
          <div className="row justify-content-lg-center">
            <div className="col-12 col-lg-8">
                <div className="feedback-form-container">{queryStrings.wufoo ? this.showThanks() : this.showForm()}</div>
              </div>
          </div>
        </div>
      </section>
    );
  }

  private showThanks() {
    return (
      <div className="text-center">
        <span className="vdm vdm-034-ampoule vert-mtl d-none d-md-inline" />
        <span className="h4" dangerouslySetInnerHTML={{ __html: this.props.t('feedback:thanks') }} />
      </div>
    );
  }

  private showForm() {
    return (
      <>
        <div className="feedback-form-header">
          <span className="vdm vdm-034-ampoule" aria-hidden="true" />
          <span className="h5" dangerouslySetInnerHTML={{ __html: this.props.t('feedback:title') }} />
        </div>
        <FeedbackForm
          t={this.props.t}
          currentUrl={this.props.currentUrl}
          language={this.props.language}
          appVersion={this.props.appVersion}
        />
      </>
    );
  }
}
