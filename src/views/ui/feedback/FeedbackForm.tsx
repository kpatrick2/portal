import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { Language } from '../../../utils';

interface IFeedbackFormProps {
  currentUrl: string;
  t: TranslationFunction;
  language: Language;
  appVersion: string;
}

export class FeedbackForm extends React.PureComponent<IFeedbackFormProps> {
  public render() {
    return (
      <form
        id="form20"
        name="form20"
        className="wufoo feedback-form"
        acceptCharset="UTF-8"
        autoComplete="off"
        encType="multipart/form-data"
        method="post"
        noValidate={true}
        action="https://vdmtl.wufoo.com/forms/m1f5uunb0dkaqrh/#public"
      >
        <div className="feedback-question">
          <span
            className="feedback-question-label"
            dangerouslySetInnerHTML={{ __html: this.props.t('feedback:main.question') }}
          />
          <div className="feedback-answers form-group">
            <div className="custom-control custom-radio">
              <input className="custom-control-input" type="radio" name="Field10" value="oui" id="rdioFeedback01-1" />
              <label className="custom-control-label" htmlFor="rdioFeedback01-1">
                {this.props.t('feedback:main.yes')}
              </label>
            </div>
            <div className="custom-control custom-radio">
              <input className="custom-control-input" type="radio" name="Field10" value="oui_en_partie" id="rdioFeedback01-2" />
              <label className="custom-control-label" htmlFor="rdioFeedback01-2">
                {this.props.t('feedback:main.partially')}
              </label>
            </div>
            <div className="custom-control custom-radio">
              <input className="custom-control-input" type="radio" name="Field10" value="no" id="rdioFeedback01-3" />
              <label className="custom-control-label" htmlFor="rdioFeedback01-3">
                {this.props.t('feedback:main.no')}
              </label>
            </div>
          </div>
        </div>
        <div className="feedback-question feedback-details-content">
          <span
            className="feedback-question-label"
            dangerouslySetInnerHTML={{ __html: this.props.t('feedback:content.question') }}
          />
          <div className="feedback-answers form-group">
            <div className="custom-control custom-radio">
              <input className="custom-control-input" type="radio" name="Field11" value="contenu_adequat" id="rdioFeedback02-1" />
              <label className="custom-control-label" htmlFor="rdioFeedback02-1">
                {this.props.t('feedback:content.good')}
              </label>
            </div>

            <div className="custom-control custom-radio">
              <input className="custom-control-input" type="radio" name="Field11" value="contenu_pas_assez_precis" id="rdioFeedback02-2" />
              <label className="custom-control-label" htmlFor="rdioFeedback02-2">
                {this.props.t('feedback:content.precision')}
              </label>
            </div>
            <div className="form-group">
              <textarea
                className="form-control"
                placeholder={this.props.t('feedback:content.placeholder')}
                name="Field14"
              />
              <div
                className="alert alert-danger"
                dangerouslySetInnerHTML={{ __html: this.props.t('feedback:content.required') }}
              />
            </div>
            <div className="custom-control custom-radio">
              <input className="custom-control-input" type="radio" name="Field11" value="contenu_trop_complexe" id="rdioFeedback02-3" />
              <label className="custom-control-label" htmlFor="rdioFeedback02-3">
                {this.props.t('feedback:content.complexity')}
              </label>
            </div>


            <div className="form-group">
              <textarea
                className="form-control"
                placeholder={this.props.t('feedback:content.placeholder')}
                name="Field14"
              />
              <div
                className="alert alert-danger"
                dangerouslySetInnerHTML={{ __html: this.props.t('feedback:content.required') }}
              />
            </div>
            <div className="custom-control custom-radio">
              <input className="custom-control-input" type="radio" name="Field11" value="contenu_errone" id="rdioFeedback02-4" />
              <label className="custom-control-label" htmlFor="rdioFeedback02-4">
                {this.props.t('feedback:content.error')}
              </label>
            </div>


            <div className="form-group">
              <textarea
                className="form-control"
                placeholder={this.props.t('feedback:content.placeholder')}
                name="Field14"
              />
              <div
                className="alert alert-danger"
                dangerouslySetInnerHTML={{ __html: this.props.t('feedback:content.required') }}
              />
            </div>


            <div className="custom-control custom-radio">
              <input className="custom-control-input" type="radio" name="Field11" value="contenu_non_attendu" id="rdioFeedback02-5" />
              <label className="custom-control-label" htmlFor="rdioFeedback02-5">
                {this.props.t('feedback:content.expectation')}
              </label>
            </div>


            <div className="form-group">
              <textarea
                className="form-control"
                placeholder={this.props.t('feedback:content.placeholder')}
                name="Field14"
              />
              <div
                className="alert alert-danger"
                dangerouslySetInnerHTML={{ __html: this.props.t('feedback:content.required') }}
              />
            </div>
            <div className="custom-control custom-radio">
              <input className="custom-control-input" type="radio" name="Field11" value="pas_de_reponse" id="rdioFeedback02-6" />
              <label className="custom-control-label" htmlFor="rdioFeedback02-6">
                {this.props.t('feedback:content.no_answer')}
              </label>
            </div>


          </div>
        </div>
        <div className="feedback-question feedback-details-display">
          <span
            className="feedback-question-label"
            dangerouslySetInnerHTML={{ __html: this.props.t('feedback:display.question') }}
          />
          <div className="feedback-answers">
            <div className="custom-control custom-radio">
              <input className="custom-control-input" type="radio" name="Field12" value="affichage_adequat" id="rdioFeedback03-1" />
              <label className="custom-control-label" htmlFor="rdioFeedback03-1">
                {this.props.t('feedback:display.good')}
              </label>
            </div>
            <div className="custom-control custom-radio">
              <input className="custom-control-input" type="radio" name="Field12" value="page_vide" id="rdioFeedback03-2" />
              <label className="custom-control-label" htmlFor="rdioFeedback03-2">
                {this.props.t('feedback:display.empty_page')}
              </label>
            </div>
            <div className="custom-control custom-radio">
              <input className="custom-control-input" type="radio" name="Field12" value="certaines_sections_vides" id="rdioFeedback03-3" />
              <label className="custom-control-label" htmlFor="rdioFeedback03-3">
                {this.props.t('feedback:display.empty_sections')}
              </label>
            </div>
            <div className="custom-control custom-radio">
              <input className="custom-control-input" type="radio" name="Field12" value="affichage_problematique_certaines_sections" id="rdioFeedback03-4" />
              <label className="custom-control-label" htmlFor="rdioFeedback03-4">
                {this.props.t('feedback:display.problem_sections')}
              </label>
            </div>
            <div className="custom-control custom-radio">
              <input className="custom-control-input" type="radio" name="Field12" value="affichage_non_adapte_appareil" id="rdioFeedback03-5" />
              <label className="custom-control-label" htmlFor="rdioFeedback03-5">
                {this.props.t('feedback:display.problem_device')}
              </label>
            </div>
          </div>
        </div>

        {/* URL de la page */}
        <input name="Field4" type="hidden" value={this.props.currentUrl} />

        {/* Type d'appareil */}
        <input name="Field5" type="hidden" value="currentDevice" />

        {/* Navigateur */}
        <input name="Field6" type="hidden" value="currentNavigator" />

        {/* Version du navigateur */}
        <input name="Field7" type="hidden" value="currentNavigatorVersion" />

        {/* Langue */}
        <input name="Field8" type="hidden" value={this.props.language} />

        {/* Version de l'application */}
        <input name="Field16" type="hidden" value={this.props.appVersion} />

        <p className="notice" dangerouslySetInnerHTML={{ __html: this.props.t('feedback:notice') }} />

        <div className="actions-buttons">
          <button
            name="saveForm"
            className="btn btn-sm btn-primary"
            type="submit"
            disabled
            dangerouslySetInnerHTML={{ __html: this.props.t('feedback:submit') }}
          />
          <button
            type="button"
            id="cancel"
            className="btn btn-sm btn-link reset-button"
            disabled
            hidden
            dangerouslySetInnerHTML={{ __html: this.props.t('feedback:cancel') }}
          />
        </div>

        <input type="hidden" name="comment" />
        <input type="hidden" name="idstamp" value="s6ca0fAPki7zk4nFCtzEiJYsfTA8bSL6BKoW46uSjgY=" />
      </form>
    );
  }
}
