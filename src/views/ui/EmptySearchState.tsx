import { TranslationFunction } from 'i18next';
import * as React from 'react';

interface IEmptySearchStateProps {
  msg: string;
  t: TranslationFunction;
}

export class EmptySearchState extends React.PureComponent<IEmptySearchStateProps> {
  public render() {
    return (
      <div className="container mt-4">
        <div className="row">
          <div className="col-12">
            <div className="empty-state-container">
              <div className="empty-state-icon">
                <span className="vdm vdm-105-petition" aria-hidden="true"></span>
              </div>
              <p className="text-center">
                {this.props.msg}
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
