import { decode } from 'he';
import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ITaxonomy, ITaxonomyFilterWrapper } from '../../../content-api';
// import { LocalizedLink } from '../../LocalizedLink';

interface IModalFilter {
  taxoLabel?: ITaxonomyFilterWrapper[];
  params: Map<string, any>;
  labelButton: string;
  t: TranslationFunction;
}

export class ModalFilter extends React.PureComponent<IModalFilter> {
  public render() {

    if (this.props.taxoLabel) {
      return (
        <>
        {/* Render le modal avec les inputs pour les filters */}
          {this.renderModal()}
          <div className="container">
            <div className="row">
              <div className="col-12 d-block d-md-none pt-4">
                <button type="button" className="btn btn-secondary btn-block" data-toggle="modal" data-target="#navModalFilters-1" data-backdrop="false">
                  {this.props.labelButton}
                </button>
              </div>
            </div>
          </div>
        </>
      );
    }
    return null;
  }

  private renderModal() {
    return (
      <section id="nav-filters" className="section-filters">
        <div className="container">
          {/* Nav Modal on mobile, becomes static nav with dropdowns on tablets and up  */}
          <div className="nav-modal modal modal-left-pane fade" id="navModalFilters-1" aria-labelledby="navFiltersModalLabel" style={{ display: 'none' }} aria-hidden="true">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header d-flex d-md-none">
                  <h5 className="modal-title" id="navFiltersModalLabel">{this.props.t('content:modal_filter.title_filter')}</h5>
                  <button type="button" className="btn-close close" data-dismiss="modal" aria-label="Close">
                    <span className="vdm vdm-046-grand-x"></span>
                  </button>
                </div>
                <div className="modal-body">
                  <ul className="nav nav-filters" id="filters">
                    {this.props.taxoLabel && this.props.taxoLabel.length > 0 ? (
                      this.props.taxoLabel.map((value: ITaxonomyFilterWrapper) => this.putList(value))
                    ) : null}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }

  private putList(taxoLabel: ITaxonomyFilterWrapper) {
    return (
      <li className="nav-item dropdown dropdown">
        <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" data-display="static">
          {taxoLabel.label}
        </a>
        <div className="dropdown-menu">
          {this.renderInputBoroughs(taxoLabel)}
        </div>
      </li>
    );
  }

  private renderInputBoroughs(taxoLabel: ITaxonomyFilterWrapper) {
    if (taxoLabel && taxoLabel.taxonomy.length > 0) {
      return (
        <form id={`select${taxoLabel.taxoName}Filter`}>
          {taxoLabel.taxonomy.map((taxonomy: ITaxonomy, index: number) => {
            return (
              <div className="dropdown-item form-group">
                <div className="custom-control custom-radio">
                  {/* "rdioFilterArron-1" */}
                  <input type="radio" data-query={taxoLabel.query_param} checked={this.props.params && decode(taxonomy.name) === decode((this.props.params.get(taxoLabel.query_param)) ? this.props.params.get(taxoLabel.query_param) : "")} id={`rdioFilter${taxoLabel.taxoName}-${index}`} name={`radios${taxoLabel.taxoName}Group`} className="custom-control-input get-filter" value={decode(taxonomy.name)} />
                  <label className="custom-control-label" htmlFor={`rdioFilter${taxoLabel.taxoName}-${index}`}>{decode(taxonomy.name)}</label>
                </div>
              </div>
            );
          })}

        </form>
      );
    }
    return null;
  }
}