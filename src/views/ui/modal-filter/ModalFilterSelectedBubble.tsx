import { TranslationFunction } from 'i18next';
import * as React from 'react';

interface IModalFilter {
  selectedParams: Map<string, any>;
  t: TranslationFunction;
}

export class ModalFilterSelectedBubble extends React.PureComponent<IModalFilter> {
  public render() {

    if (this.props.selectedParams) {
      return this.genBubbles();
    }
    return null;
  }

  private genBubbles() {
    let isParams: boolean = false;
    const rendered = (
      <>
        <div className="container mt-4">
          <div className="row">
            <span className="col-12 filters-applied">
              <span className="sr-only">{this.props.t('content:modal_filter.selected_filter')} </span>
              <div className="badge-group badges-lg">
                {Array.from(this.props.selectedParams, ([key, value]) => {
                  if (value && key !== 'q') {
                    isParams = true;
                    return (
                      <a href="#" className="badge badge-dismiss" aria-label={"Supprimer le filtre: " + value} data-filter-label={key}>{value}</a>
                    )
                  }
                })}
                {isParams &&
                  <button type="button" className="btn btn-link btn-sm btn-reset">{this.props.t('content:modal_filter.clear_all')}</button>
                }
              </div>
            </span>
          </div>
        </div>
      </>
    )

    if (isParams) {
      return rendered;
    }

    return null;
  }
}