import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IPhone } from '../../content-api';

interface IPhoneProps {
  phone: IPhone;
  t: TranslationFunction;
}

export class Phone extends React.PureComponent<IPhoneProps> {
  public render() {
    return (
      <span className="phone-number">
        <a href={`tel:${this.props.phone.number}`}>
          <span>{this.props.phone.number}</span>
          {this.props.phone.extension ? this.props.t('content:location.phoneExt') + " " + this.props.phone.extension : null }
        </a>
      </span>
    );
  }
}



