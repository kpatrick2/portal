import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { getSelectedBorough } from '../../utils';
import { BoroughModal } from '../pages/borough-select/borough-modal';
import { AbstractText } from './AbstractText';

interface IAbstractTextProps {
  extra: any[];
  currentUrl: string;
  text?: string;
  selectText: string;
  t: TranslationFunction;
}

export class AbstractModalBorough extends React.PureComponent<IAbstractTextProps> {
  private selectedBorough: any;

  public render() {
    this.selectedBorough = getSelectedBorough(this.props.currentUrl, this.props.extra);
    return this.renderAbstract();
  }

  private renderAbstract() {
    if (this.selectedBorough) {
      return (
        <AbstractText text={this.props.text} side="right">
          <div className="lead-left-content">
            <span className="label">{this.props.t("common:content.borough_yours")}</span>
            <span className="title">{this.selectedBorough}</span>
            <button className="btn btn-link" data-toggle="modal" data-target="#selectArrondissementModal">{this.props.t("common:content.borough_change")}</button>
          </div>
          <BoroughModal boroughs={this.props.extra} t={this.props.t} currentUrl={this.props.currentUrl}>
          </BoroughModal>
        </AbstractText>
      )
    } else {
      return <AbstractText text={this.props.text} side="right">
      <div className="lead-left-content"  id="borough-abstract-active" style={{display:'none'}}>
            <span className="label">{this.props.t("common:content.borough_yours")}</span>
            <span className="title">{this.selectedBorough}</span>
            <button className="btn btn-link" data-toggle="modal" data-target="#selectArrondissementModal">{this.props.t("common:content.borough_change")}</button>
          </div>
        <div className="lead-left-content" id="borough-abstract-unselected">
          <span className="title">{this.props.selectText}</span>
          <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#selectArrondissementModal">{this.props.t("common:content.borough_select")}</button>
        </div>
        <BoroughModal boroughs={this.props.extra} t={this.props.t} currentUrl={this.props.currentUrl}>
        </BoroughModal>
      </AbstractText>
    }
  }
}