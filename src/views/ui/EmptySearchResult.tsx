import { TranslationFunction } from 'i18next';
import * as React from 'react';

interface IEmptySearchResultProps {
  t: TranslationFunction;
  term: string;
}

export class EmptySearchResult extends React.PureComponent<IEmptySearchResultProps> {
  public render() {
    return (
      <div className="empty-result">
        <span className="list-group-heading">
          {this.props.t('search:notFound.header.1')}
          <strong>"{this.props.term}"</strong>
          {this.props.t('search:notFound.header.2')}
        </span>
        <p>{this.props.t('search:notFound.answer')}</p>
        <ul>
          <li>{this.props.t('search:notFound.advices.1')}</li>
          <li>{this.props.t('search:notFound.advices.2')}</li>
          <li>{this.props.t('search:notFound.advices.3')}</li>
        </ul>
      </div>
    );
  }
}
