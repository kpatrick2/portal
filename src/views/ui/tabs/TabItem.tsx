import * as classnames from 'classnames';
import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { urlHelpers } from '../../../utils';
import { LocalizedLink } from '../../LocalizedLink';

export interface ITabItemProps {
  title: string;
  currentUrl: string;
  t: TranslationFunction;
}

interface ITabItemParentProps {
  index: number;
  isSelected: boolean;
}

export class TabItem extends React.PureComponent<ITabItemProps> {
  get parentProps(): ITabItemParentProps {
    return this.props as any;
  }

  public render() {
    const url = urlHelpers.replaceQueryString(this.props.currentUrl, 'tab', this.parentProps.index.toString());
    const linkClasses = classnames({
      'nav-link': true,
      'tab-async': true,
      'active show': this.parentProps.isSelected
    });
  
    return (
      <li className="nav-item" role="presentation">
        <LocalizedLink to={url} className={linkClasses} t={this.props.t}>
          {this.props.title}
        </LocalizedLink>
      </li>
    );
  }
}
