import * as React from 'react';

interface ITabsProps {
  selectedTab: number;
  hidden:boolean;
}

export class Tabs extends React.PureComponent<ITabsProps> {
  public render() {
    if (React.Children.count(this.props.children) <= 1) {
      return null;
    }

    return [this.renderDesktopTabs()];
  }

  private renderDesktopTabs() {
    return (
      <div className="nav-tabs-container" id="nav-tabs" style={{ display: (this.props.hidden ? 'none' : 'block') }}>
        <nav>
          <ul className="nav nav-tabs tabs-async" role="tablist">
            {React.Children.map(this.props.children, (child: any, index) =>
              React.cloneElement(child, {
                index,
                isSelected: this.props.selectedTab === index
              })
            )}
          </ul>
        </nav>
      </div>
    );
  }
}
