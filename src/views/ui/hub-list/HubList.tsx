import * as React from 'react';

export class HubList extends React.PureComponent {
    public render() {
        return (
            <div className="row">
                <div className="col-12">
                    <div className="list-group list-group-hub">
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}