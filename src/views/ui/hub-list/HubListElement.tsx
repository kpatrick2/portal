import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { LocalizedLink } from '../../LocalizedLink';

interface ICardProps {
  title: string;
  text?: string;
  dc_identifier: string;
  t: TranslationFunction;
}

export class HubListElement extends React.PureComponent<ICardProps> {
  public render() {

    return (
      <LocalizedLink to={this.props.dc_identifier} className="list-group-item list-group-item-action" t={this.props.t}>
        <div className="list-group-item-body">
          <div className="list-group-max-width">
            <span className="list-group-item-title">{this.props.title}</span>
            <div className="list-group-item-content ">
              {this.props.text &&
                <span >{this.props.text.replace(/<\/?[^>]+(>|$)/g, "")}</span>
              }
            </div>
          </div>
        </div>
        <div className="list-group-item-footer">
          <span className="vdm vdm-063-fleche-droite " aria-hidden="true"></span>
        </div>
      </LocalizedLink>
    );
  }
}   