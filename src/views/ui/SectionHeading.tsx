import * as React from 'react';

export class SectionHeading extends React.PureComponent {
  public render() {
    return (
      <div>
        <div className="section-divider" />
        <div className="h3">{this.props.children}</div>
      </div>
    );
  }
}
