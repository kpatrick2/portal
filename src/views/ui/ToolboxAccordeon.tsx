import * as classnames from 'classnames';
import * as React from 'react';

interface IToolboxAccordeonProps {
  header: string;
  id:any;
  expanded?:boolean;
}

export class ToolboxAccordeon extends React.PureComponent<IToolboxAccordeonProps> {
  public render() {
    const classes = classnames({
      "collapse":true,
      "show":this.props.expanded
    });
    return (
      <div className="collapsible-item">
        <div className="collapsible-header" role="tab" id="heading-howto-rdp-enligne">
          <div className="collapsible-title">
            <a data-toggle="collapse" href="#" data-target={`#howto-rdp-enligne${this.props.id}`} aria-expanded={this.props.expanded} aria-controls={`howto-rdp-enligne${this.props.id}`} className="collapsed">
              <span className="collapsible-icon circle" aria-hidden="true"></span>
              <span>{this.props.header}</span>
            </a>
          </div>
        </div>
        <div id={`howto-rdp-enligne${this.props.id}`} className={classnames(classes)} role="tabpanel" aria-labelledby="heading-howto-rdp-enligne" >
          <div className="collapsible-block">
            {this.props.children}
          </div>
        </div>
      </div >
    );
  }
}
