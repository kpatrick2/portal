import * as classnames from 'classnames';
import { TranslationFunction } from 'i18next';
import * as React from 'react';

interface IAbstractTextProps {
  text?: string;
  className?: string;
  t?: TranslationFunction;
  side?: 'left' | 'right';
}

export class AbstractText extends React.PureComponent<IAbstractTextProps> {
  public render() {
    if (!this.props.text) {
      return null;
    }

    return <div className="bg-gray-sky lead-container ">
      <div className="section-content-fluid">
        {this.renderMetaData()}
      </div>
    </div>;
  }

  private renderMetaData() {
    let classDiv1:any;
    let classDiv2:any;
    classDiv1 = classnames({
      'col-12':true,
      'col-lg-7':true,
      'order-lg-1': this.props.side && this.props.side === 'left',
      'order-lg-2': this.props.side && this.props.side !== 'left'
    });
    
    classDiv2 = classnames({
      'col-12':true,
      'col-lg-5':true,
      'order-lg-1': this.props.side && this.props.side !== 'left',
      'order-lg-2': this.props.side && this.props.side === 'left'
    })
    
    if (this.props.text) {
      if (this.props.children) {
        return (
          <div className="row no-gutters section-inner section-inner-lg section-lead">
            <div className={classDiv1}>
              <div className="lead" dangerouslySetInnerHTML={{ __html: this.props.text }}></div>
            </div>
            <div className={classDiv2}>
              {this.props.children}
            </div>
          </div>
        )
      } else {
        return (
          <div className="section-inner section-inner-lg">
            <div className="lead" dangerouslySetInnerHTML={{ __html: this.props.text }} ></div>
          </div>
        )
      }
    }
  }
}