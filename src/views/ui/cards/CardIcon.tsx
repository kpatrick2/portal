import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { INodeReferenceWithIconV2 } from '../../../content-api';
import { LocalizedLink } from '../../LocalizedLink';

interface ICardProps {
  reference: INodeReferenceWithIconV2;
  position: number;
  t: TranslationFunction;
}

export class CardIcon extends React.PureComponent<ICardProps> {
  public render() {
  
    if (!this.props.reference) {
      return null;
    }

    return (
      <div className="col-12 col-md-6 col-lg-4">
        <LocalizedLink className="card" to={this.props.reference.call_to_action.url} t={this.props.t} data-link-position={(this.props.position + 1).toString()}>
            <span className={`card-icon-top vdm ${this.props.reference.icon}`} />
            <div className="card-body">
                <span className="card-title">{this.props.reference.call_to_action.title}</span>
              </div>
        </LocalizedLink>
      </div>
    );
  }
}
