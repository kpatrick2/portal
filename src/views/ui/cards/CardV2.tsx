import classnames from 'classnames';
import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IImage, ITemporal } from '../../../content-api';
import { getImagePlaceholder } from '../../../utils/image-helpers';
import { LocalizedLink } from '../../LocalizedLink';
import { SingleImage } from '../images/SingleImage';
import { CardV2Body } from './cardV2-body';


interface ICardProps {
  title: string;
  text?: string;
  images?: IImage[];
  imageFirst?: boolean; // Default to last
  footerText?: string;
  footer?: boolean;
  date?: string;
  date_format?: 'date' | 'time' | 'date-time';
  dc_identifier: string;
  badge?: string[];
  card_type?: string;
  tc?: string;
  type?: 'link' | 'row' | 'teaser' | 'large' | 'featured' | 'plain'; // default is card-row
  temporal?: ITemporal;
  bg_variant?: boolean;
  tag?: string;
  t: TranslationFunction;
}


export class CardV2 extends React.PureComponent<ICardProps> {

  public render() {

    let images: IImage[] = this.props.images!;
    const imagePlaceholder: IImage | undefined = getImagePlaceholder(images, this.props.t, this.props.tc);

    if(imagePlaceholder) {
      images = [imagePlaceholder];
    }
    
    const cardClasses = classnames({
      "card": true,
      "card-fw card-row-layout": this.props.type === undefined || this.props.type === 'row',
      "card-link": this.props.type && this.props.type === 'link',
      "card-teaser": this.props.type && this.props.type === 'teaser',
      "card-large": this.props.type && this.props.type === 'large',
      "card-featured": this.props.type && this.props.type === 'featured',
      "card-bg-variant": this.props.bg_variant
    });

    const cardImgClasses = classnames({
      "card-img-top": true,
      "card-img-first": this.props.imageFirst
    })

    return (

      <LocalizedLink to={this.props.dc_identifier} className={cardClasses} t={this.props.t}>
        {images &&
          <SingleImage
            className={cardImgClasses}
            images={images}
            transformations={{ width: 600, ar: '16:9', crop: 'fill' }}
          />
        }
        <CardV2Body 
          title={this.props.title}
          text={this.props.text}
          footerText={this.props.footerText}
          footer={this.props.footer}
          date={this.props.date}
          date_format={this.props.date_format}
          badge={this.props.badge}
          card_type={this.props.card_type}
          type={this.props.type}
          temporal={this.props.temporal}
          tag={this.props.tag}
          t={this.props.t}
          tc={this.props.tc}
        />
        
      </LocalizedLink>
    );
  }

}