import * as React from 'react';
interface ICardProps {
    className?: string;
  }
export class CardList extends React.PureComponent<ICardProps> {
    public render() {
        return (
            <div className={this.props.className ? this.props.className : "row cards" }>
                {this.props.children}
            </div>
        )
    }
}