import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { LocalizedLink } from '../../LocalizedLink';

interface ICardProps {
  title: string;
  text: string;
  subtitle?: string;
  link: string;
  t: TranslationFunction;
}

export class Card extends React.PureComponent<ICardProps> {
  public render() {
    return (
      <LocalizedLink
        to={this.props.link}
        className="card card-fw card-row-layout"
        t={this.props.t}
      >
        <div className="card-body">
        {this.props.title.length > 0 ? (
            <span className="card-title">{this.props.title}</span>
          ) : null}
          
          {(this.props.subtitle && this.props.subtitle.length > 0) ? (
            <span className="card-subtitle">{this.props.subtitle}</span>
          ) : null}
          
          {this.props.text.length > 0 ? (
            <p className="card-text">{this.props.text}</p>
          ) : null}
          
        </div>
        <div className="card-footer">
          <span className="vdm vdm-063-fleche-droite" aria-hidden="true"></span>
        </div>
      </LocalizedLink>
    );
  }
}   