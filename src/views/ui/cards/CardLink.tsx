import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { LocalizedLink } from '../../LocalizedLink';

interface ICardProps {
    title: string;
    link: string;
    t: TranslationFunction;
}

export class CardLink extends React.PureComponent<ICardProps> {
    public render() {
        return (
            <div className="col-12 col-md-6 col-lg-4">

                <LocalizedLink
                    to={this.props.link}
                    className="card card-link"
                    t={this.props.t}
                >

                    <div className="card-body">
                        <span className="card-title">{this.props.title}</span>
                    </div>
                    <div className="card-footer">
                        <span className="vdm vdm-063-fleche-droite" aria-hidden="true"></span>
                    </div>
                </LocalizedLink>
            </div>
        );
    }
}