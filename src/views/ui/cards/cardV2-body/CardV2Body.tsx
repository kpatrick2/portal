import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ITemporal } from '../../../../content-api';
import { DateInterval } from '../../../DateInterval';
import { DateTime } from '../../../DateTime';


interface ICardV2BodyProps {
  title: string;
  text?: string;
  footerText?: string;
  footer?: boolean;
  date?: string;
  date_format?: 'date' | 'time' | 'date-time';
  badge?: string[];
  card_type?: string;
  tc?: string;
  type?: 'link' | 'row' | 'teaser' | 'large' | 'featured' | 'plain'; // default is card-row
  temporal?: ITemporal;
  tag?: string;
  t: TranslationFunction;
}

export class CardV2Body extends React.PureComponent<ICardV2BodyProps> {
  public render() {

    const footerPosition: string | undefined = this.getFooterPosition(this.props.type);

    return (
      <>
        <div className="card-body">
          {this.props.tag &&
            <span className="card-tags">{this.props.tag}</span>
          }

          {this.props.card_type &&
            <span className="card-type">{this.props.card_type}</span>
          }

          <span className="card-title">{this.props.title}</span>

          {this.props.badge &&
            <span className="badge">{this.props.badge}</span>
          }

          {this.props.text &&
            <div className="card-text" dangerouslySetInnerHTML={{ __html: this.props.text }}></div>
          }

          {this.props.date &&
            <span className="card-date"><DateTime date={this.props.date} format={this.props.date_format ? this.props.date_format : 'date'} t={this.props.t} /></span>
          }

          {this.props.temporal &&
            <div className="card-text">
              <DateInterval en={this.props.t('common:languageCode') === 'en'} start={this.props.temporal.start} end={this.props.temporal.end} t={this.props.t} />
            </div>
          }

          {this.renderInnerFooter(footerPosition)}

        </div>
        
        {this.renderOutterFooter(footerPosition)}
      </>
    );
  }

  // Render inner footer
  private renderInnerFooter(footerPosition: string | undefined) {
    if(this.props.footerText && footerPosition === 'inner') {
      return (
        <div className="card-footer">
          <span className="card-footer-label">
            {this.props.footerText}
          </span>
          <span className="vdm vdm-063-fleche-droite" aria-hidden="true"></span>
        </div>
      )
    }
  }

  // Render outter footer
  private renderOutterFooter(footerPosition: string | undefined) {
    if(footerPosition === 'outter' && (this.props.footer || this.props.footerText)) {
      return (
        <div className="card-footer">
          {this.props.footerText &&
            <span className="card-footer-label">
              {this.props.footerText}
            </span>
          }
          <span className="vdm vdm-063-fleche-droite" aria-hidden="true"></span>
        </div>
      )
    }
  }

  // Card footer position is based on card type.
  // Default to outter.
  private getFooterPosition(type: any) {
    
    let footerPosition: string | undefined;
    if(type || this.props.footer) {
      if (type === "featured") {
        footerPosition = 'inner'
      } else {
        footerPosition = 'outter'
      }
    } else {
      footerPosition = undefined;
    }

    return footerPosition;
  }



}
