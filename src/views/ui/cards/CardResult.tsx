import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { LocalizedLink } from '../../LocalizedLink';

interface ICardProps {
    title: string;
    text?: any;
    text2?: string;
    link: string;
    t: TranslationFunction;
}

export class CardResult extends React.PureComponent<ICardProps> {
    public render() {
        return (
            <LocalizedLink
                to={this.props.link}
                className="list-group-item list-group-item-action "
                t={this.props.t}
            >
                <div className="list-group-item-body">
                    <div className="list-group-max-width">
                        <span className="list-group-item-title">{this.props.title}</span>
                        {(this.props.text || this.props.text2) &&
                            <div className="list-group-item-content ">
                                <span>{this.props.text}</span>
                                {this.props.text2 && this.props.text2 !== "" &&
                                    <span className="text-dark separator-brand">{this.props.text2}</span>}
                            </div>
                        }
                    </div>
                </div>
                <div className="list-group-item-footer">
                    <span className="vdm vdm-063-fleche-droite " aria-hidden="true"></span>
                </div>
            </LocalizedLink>
        );
    }
}