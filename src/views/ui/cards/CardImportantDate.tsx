import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ITimeSpan } from '../../../content-api';
import { LocalizedLink } from '../../LocalizedLink';
import { TimeSpan } from '../../TimeSpan';

interface ICardImportantDateProps {
  title: string;
  t: TranslationFunction;
  linkLabel?: string;
  linkUrl?: string;
  timeSpan?: ITimeSpan;
}

export class CardImportantDate extends React.PureComponent<ICardImportantDateProps> {

  public render() {

    return (
      <div className="card card-plain">
        <div className="card-body">

          {this.props.timeSpan &&
            <span className="card-date">
              <TimeSpan timeSpan={this.props.timeSpan} t={this.props.t} en={this.props.t('common:languageCode') === 'en'}/>
            </span>
          }

          {this.props.title &&
            <span className="card-title">{this.props.title}</span>
          }

          <LocalizedLink to={this.props.linkUrl} title={this.props.linkLabel} className="btn btn-secondary btn-sm" t={this.props.t} />

        </div>
      </div>
    );
  }
}   