import * as classnames from 'classnames';
import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IBlocMember, IImage } from '../../../content-api';
import { LocalizedLink } from '../../LocalizedLink';
import { SingleImage } from '../images/SingleImage';

interface ICardMemberProps {
  member: IBlocMember;
  t: TranslationFunction;
  memberImage: IImage[];
  displayNone?: boolean;
}

export class CardMember extends React.PureComponent<ICardMemberProps> {
  public render() {
    const sectionClasses = classnames({
      'col-12 col-md-6': true,
      'd-none': this.props.displayNone,
    });
    return (
      <div className={sectionClasses}>
        <LocalizedLink
          to={this.props.member.elected_name && this.props.member.elected_name.length > 0 ? (this.props.member.elected_name[0].url) : "#"}
          className="card card-large card-row-layout card-member"
          t={this.props.t}
        >
          <SingleImage images={this.props.memberImage} transformations={{ ar: 1, crop: 'crop' }} className="card-img-top rounded-circle" />
          <div className="card-body">
            <span className="card-title">{this.props.member.elected_name && this.props.member.elected_name.length > 0 ? (this.props.member.elected_name[0].title) : null}</span>
            <span className="card-subtitle">{this.props.member.functions && this.props.member.functions.length > 0 ? this.props.member.functions[0].label : ""}</span>

            {(this.props.member.responsabilities && this.props.member.responsabilities.length > 0) ? (
              <p className="card-text">{this.props.member.responsabilities.join(", ")}</p>
            ) : null}
          </div>
        </LocalizedLink>

      </div>
    );
  }
}