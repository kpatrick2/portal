import * as classnames from 'classnames';
import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IElectedContent } from '../../../content-api';
import { LocalizedLink } from '../../LocalizedLink';
import { SingleImage } from '../images/SingleImage';

interface ICardEluProps {
  elu: IElectedContent;
  t: TranslationFunction;
}

export class CardElu extends React.PureComponent<ICardEluProps> {
  public render() {

    if(!this.props.elu) {
      return null;
    }

    const sectionClasses = classnames({
      'col-12': true
    });

    return (
      <div className={sectionClasses}>
        <LocalizedLink
          to={this.props.elu.dc_identifier}
          className="card card-featured card-featured-member"
          t={this.props.t}
        >
          <div className="card-img-container">
            <SingleImage images={this.props.elu.mtl_images} transformations={{ ar: 1, crop: 'crop' }} className="card-img-top rounded-circle" />
          </div>
          <div className="card-body">
            <span className="card-title">{this.props.elu.dc_title}</span>
            {this.props.elu.mtl_content.elus.functions && this.props.elu.mtl_content.elus.functions.length > 0 &&
              <span className="card-subtitle">{this.props.elu.mtl_content.elus.functions[0].label}</span>
            }
            <div className="card-footer">
              <span className="card-footer-label">{this.props.t("content:vitrine_arrondissement.mayor_label")}</span>
              <span className="vdm vdm-063-fleche-droite" aria-hidden="true"></span>
            </div>
          </div>
        </LocalizedLink>
      </div>
    );
  }
}