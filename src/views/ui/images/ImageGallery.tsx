import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IImage } from '../../../content-api';
import { LocalizedLink } from '../../LocalizedLink';
import { CloudinaryImage } from './CloudinaryImage';

interface IImageGalleryProps {
  images: IImage[];
  t: TranslationFunction;
}

export class ImageGallery extends React.PureComponent<IImageGalleryProps> {
  public render() {
    if (this.props.images.length === 0) {
      return null;
    }

    return (
      <div className="media-gallery-container">
        <ul className="media-gallery">
        {this.props.images.map((image, index) => (
          <li>
            {/* <LocalizedLink to={image.url} t={this.props.t}> */}
              <CloudinaryImage
                id={image.id}
                alt={image.alt}
                title={image.title}
                data-size={`${image.width}x${image.height}`}
                className="img-fluid"
              />
            {/* </LocalizedLink> */}
          </li>
        ))}
        </ul>
      </div>
    )

    return [
      <div key="gallery" className="row ui-imagegallery">
        {this.props.images.map((image, index) => (
          <div key={index} className="col-4">
            <LocalizedLink to={image.url} t={this.props.t}>
              <CloudinaryImage
                id={image.id}
                alt={image.alt}
                title={image.title}
                data-size={`${image.width}x${image.height}`}
              />
            </LocalizedLink>
          </div>
        ))}
      </div>,
      <div key="root" className="pswp" tabIndex={-1} role="dialog" aria-hidden="true">
        <div className="pswp__bg" />
        <div className="pswp__scroll-wrap">
          <div className="pswp__container">
            <div className="pswp__item" />
            <div className="pswp__item" />
            <div className="pswp__item" />
          </div>
          <div className="pswp__ui pswp__ui--hidden">
            <div className="pswp__top-bar">
              <div className="pswp__counter" />
              <button className="pswp__button pswp__button--close" title="Close (Esc)" />
              <div className="pswp__preloader">
                <div className="pswp__preloader__icn">
                  <div className="pswp__preloader__cut">
                    <div className="pswp__preloader__donut" />
                  </div>
                </div>
              </div>
            </div>
            <div className="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
              <div className="pswp__share-tooltip" />
            </div>
            <button className="pswp__button pswp__button--arrow--left" title="Previous (arrow left)" />
            <button className="pswp__button pswp__button--arrow--right" title="Next (arrow right)" />
            <div className="pswp__caption">
              <div className="pswp__caption__center" />
            </div>
          </div>
        </div>
      </div>
    ];
  }
}
