import * as React from 'react';
import { ICloudinaryImageTransformations, IImage } from '../../../content-api';
import { CloudinaryImage } from './CloudinaryImage';

interface ISingleImageProps {
  images?: IImage[];
  className?: string;
  transformations?: ICloudinaryImageTransformations;
}

export class SingleImage extends React.PureComponent<ISingleImageProps> {
  public render() {
    if (!this.props.images || this.props.images.length === 0) {
      return null;
    }

    const image = this.props.images[0];

    return (
      <CloudinaryImage
        id={image.id}
        alt={image.alt}
        title={image.title}
        className={this.props.className}
        transformations={this.props.transformations}
      />
    );
  }
}
