import * as React from 'react';
import { ICloudinaryImageTransformations } from '../../../content-api';
import { cloudinaryUrlAdapter } from '../../../utils/cloudinary-adapter';

interface ICloudinaryImage {
  id: string;
  alt: string;
  title: string;
  className?: string;
  transformations?: ICloudinaryImageTransformations;
  'data-size'?: string;
  itemProp?: string;
}

export class CloudinaryImage extends React.PureComponent<ICloudinaryImage> {
  public render() {
    const url = cloudinaryUrlAdapter(this.props.id, this.props.transformations)

    const hackAttr = {
      itemProp: this.props.itemProp
    }

    return <img src={url} alt={this.props.alt} title={this.props.title} className={this.props.className} {...hackAttr} data-size={this.props['data-size']} />;
  }
}
