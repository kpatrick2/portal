import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IImage } from '../../../content-api';
import { LocalizedLink } from '../../LocalizedLink';
import { CloudinaryImage } from './CloudinaryImage';


interface IImageGalleryV2Props {
  images: IImage[];
  t: TranslationFunction;
}

export class ImageGalleryV2 extends React.PureComponent<IImageGalleryV2Props> {
  public render() {
    if (this.props.images.length === 0) {
      return null;
    }

    return (
      <>
        <div className="media-gallery-button">
          <button className="btn-media js-gallery-trigger" aria-hidden="true">
            {this.props.children}
          </button>
          <button type="button" className="btn btn-primary btn-light btn-hide-label-xs btn-media-gallery js-gallery-trigger" data-toggle="modal" data-target="#dataTarget">
            <span className="vdm vdm-021-dossier" aria-hidden="true"></span>
            <span className="btn-label">{this.props.t("content:evenements.display_gallery")}</span>
          </button>
        </div>
        <ul className="media-gallery d-none" itemScope itemType="http://schema.org/ImageGallery">
          {this.props.images && this.props.images.map(image => {
            let dataSize;
            if(image.width && image.height){
              dataSize = image.width + 'x' + image.height;
            }
            return <li key={image.id} itemProp="associatedMedia" itemScope itemType="http://schema.org/ImageObject">
              <LocalizedLink to={image.url} t={this.props.t} itemProp="contentUrl" data-size={dataSize} data-index=""
              // data-title="This is a dummy caption" 
              // data-author="Jamie Mink"
              >
                <CloudinaryImage
                  id={image.id}
                  alt={image.alt}
                  title={image.title}
                  data-size={`${image.width}x${image.height}`}
                  className="img-fluid"
                  itemProp="thumbnail"
                />
              </LocalizedLink>
            </li>
          })}
        </ul>
        <div className="pswp" role="dialog" aria-hidden="true">
          <div className="pswp__bg"></div>
          <div className="pswp__scroll-wrap">


            <div className="pswp__container">
              <div className="pswp__item"></div>
              <div className="pswp__item"></div>
              <div className="pswp__item"></div>
            </div>

            <div className="pswp__ui pswp__ui--hidden">

              <div className="pswp__top-bar">


                <div className="pswp__counter"></div>

                <button className="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button className="pswp__button pswp__button--share" title="Share"></button>

                <button className="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button className="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <div className="pswp__preloader">
                  <div className="pswp__preloader__icn">
                    <div className="pswp__preloader__cut">
                      <div className="pswp__preloader__donut"></div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div className="pswp__share-tooltip"></div>
              </div>

              <button className="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
              </button>

              <button className="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
              </button>

              <div className="pswp__caption">
                <div className="pswp__caption__center"></div>
              </div>

            </div>

          </div>
        </div>
      </>
    )

  }
}
