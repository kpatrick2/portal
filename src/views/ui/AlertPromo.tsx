import * as classnames from 'classnames';
import { TranslationFunction } from 'i18next';
import * as React from 'react';

interface IAlertPromoProps {
  t: TranslationFunction;
  sideBar?:boolean;
}

export class AlertPromo extends React.PureComponent<IAlertPromoProps> {
  public render() {
    const btnClass = classnames({
      "btn btn-primary btn-sm": !this.props.sideBar,
      "btn btn btn-secondary": this.props.sideBar
    })

    return (
      <>
        {!this.props.sideBar && 
          <span className="title">{this.props.t('content:alert.promo.title')}</span>
        }
        <p>{this.props.t('content:alert.promo.description')}</p>
        <a href={this.props.t('content:alert.promo.buttonUrl')} className={btnClass}>{this.props.t('content:alert.promo.buttonLabel')}</a>
      </>
    );
  }
}