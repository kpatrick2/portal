import { FeatureCollection } from 'geojson';
import * as React from 'react';

interface ILeafletMapProps {
  center?: IMapCoordinates;
  zoom?: number;
  dragging?: boolean;
  zooming?: boolean;
  markers?: IMapMarkerOptions[];
  coordinates?: FeatureCollection;
  selected?: boolean;
  clickable?: boolean;
}

interface IMapCoordinates {
  latitude: number;
  longitude: number;
}

interface IMapMarkerOptions {
  latitude: number;
  longitude: number;
  onSelectionAddClassToId?: string;
}

export interface IFeatureProperties {
  id?: string;
  description?: string;
}

export class MapBoxMap extends React.PureComponent<ILeafletMapProps> {
  public render() {
    const serializedData = JSON.stringify({
      center: this.props.center || { latitude: 45.553486897158, longitude: -73.70967864990234 },
      zoom: this.props.zoom || 11,
      markers: this.props.markers,
      coordinates: this.props.coordinates,
      zooming: this.props.zooming,
      dragging: this.props.dragging,
      selected: this.props.selected,
      clickable: this.props.clickable
    });

    return <div id='mapbox' className="ui-leaflet" data-mapbox-map data-map-map={serializedData}></div>;
  }
}
