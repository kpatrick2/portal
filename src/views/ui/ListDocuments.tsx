import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IDocument } from '../../content-api';
import { LocalizedLink } from '../LocalizedLink';

interface IListDocumentsProps {
    documents?: IDocument[];
    t: TranslationFunction;
    title: string;
}

export class ListDocuments extends React.PureComponent<IListDocumentsProps> {
    public render() {
        if (this.props.documents && this.props.documents.length > 0) {
            const documents = this.props.documents.map((document => this.renderList(document, this.props.t)));
            if (documents) {
                return (
                    <div className="section-inner section-inner-md">
                        <h2>{this.props.title}</h2>
                        <ul>
                            {documents}
                        </ul>
                    </div>
                );
            }
        }

        return null;
    }

    private renderList(document: IDocument, t: TranslationFunction) {
        if (document && document.url && document.title) {
            return (
                <li>
                    <LocalizedLink to={document.url} t={t}>{document.title}</LocalizedLink>
                </li>
            );
        }
        return null;
    }

}