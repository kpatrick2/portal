import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { LocalizedLink } from '../LocalizedLink';

interface IMessageBannerProps {
  t: TranslationFunction;
  classname: string;
  title?: string;
  text: string;
  linkUrl?: string;
  linkLabel?: string;
  linkClass: string;
}

export class MessageBanner extends React.PureComponent<IMessageBannerProps> {
  public render() {

    return (
      <section>
        <div className="alert alert-info-dark mb-0 px-0" role="alert">
          <div className="container text-center">
            {this.props.title &&
              <strong className="mr-1">{this.props.title}</strong>
            }
            {this.props.text}
            <LocalizedLink
              to={this.props.linkUrl}
              title={this.props.linkLabel}
              t={this.props.t}
              className={this.props.linkClass}
            />
          </div>
        </div>
      </section>
    );
  }
}