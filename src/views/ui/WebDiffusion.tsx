import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IAppelActionWebDiffusion } from '../../content-api';
import { cloudinaryUrlAdapter } from '../../utils/cloudinary-adapter';
import { LocalizedLink } from '../LocalizedLink';

interface IWebDiffusion {
    btnLabel: string;
    webDiffusion?: IAppelActionWebDiffusion[];
    t: TranslationFunction;
}

export class WebDiffusion extends React.PureComponent<IWebDiffusion> {

    // tslint:disable-next-line:cyclomatic-complexity
    public render() {
        if (this.props.webDiffusion && this.props.webDiffusion.length > 0) {
            const style = (this.props.webDiffusion[0].image && this.props.webDiffusion[0].image!.length > 0 && this.props.webDiffusion[0].image![0] && this.props.webDiffusion[0].image![0].length > 0 && this.props.webDiffusion[0].image![0][0]) ? { backgroundImage: `url(${cloudinaryUrlAdapter(this.props.webDiffusion![0].image![0][0].id)})` } : undefined;
            return (
                <section id="webdiffusion" className="bg-image-section" style={style}>
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-lg-6">
                                <div className="featured-content">
                                    <h2 className="h1">
                                        {this.props.webDiffusion[0].title}
                                    </h2>
                                    <p>{this.props.webDiffusion[0].text}</p>
                                    {(this.props.webDiffusion[0].call_to_action && this.props.webDiffusion[0].call_to_action!.length > 0) ? (
                                            (
                                                <LocalizedLink
                                                    to={this.props.webDiffusion[0].call_to_action![0].url}
                                                    className="btn btn-primary"
                                                    t={this.props.t}
                                                >
                                                    {this.props.webDiffusion[0].call_to_action![0].title}
                                                </LocalizedLink>)
                                        ) : null}

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            )
        } else {
            return null;
        }
    }




}
