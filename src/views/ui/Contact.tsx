import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { Address } from '.';
import { IAdminContact } from '../../content-api';
import { addressExists } from '../../utils';

interface IContactProps {
    data: IAdminContact;
    t?: TranslationFunction;
}

export class Contact extends React.PureComponent<IContactProps> {
    public render() {

        if (this.props.data) {
            return (

                <div className="content-block content-block-contact">
                    {this.props.data.contact_name || this.props.data.service_name ? (
                        <div className="content-block-infos">
                            {this.props.data.contact_name ? (
                                <span className="font-size-xl bold">{this.props.data.contact_name}</span>
                            ) : null}
                            {this.props.data.service_name ? (
                                <span>{this.props.data.service_name}</span>
                            ) : null}
                        </div>
                    ) : null}

                    <ul className="list-icon">
                        {addressExists(this.props.data)&& (
                            <li className="list-icon-item">
                                <span className="vdm vdm-091-localisation " aria-hidden="true"></span>
                                <span>
                                    <Address address={this.props.data} > </Address>
                                </span>
                            </li>
                        )}

                        {this.props.data.phone && (
                            <li className="list-icon-item">
                                <span className="vdm vdm-012-telephone " aria-hidden="true"></span>
                                <span title={`${(this.props.data.phone_note ? this.props.data.phone_note : null)}`}>{this.props.data.phone}</span>
                            </li>
                        )}

                        {this.props.data.fax && (
                            <li className="list-icon-item">
                                <span className="vdm vdm-114-telecopieur " aria-hidden="true"></span>
                                <span>{this.props.data.fax}</span>
                            </li>
                        )}

                        {this.props.data.email && (
                            <li className="list-icon-item">
                                <span className="vdm vdm-028-enveloppe " aria-hidden="true"></span>
                                <a href={`mailto:${(this.props.data.email)}`} className="icon left">{(this.props.data.email)}</a>
                            </li>
                        )}

                        {(this.props.data.website_url && this.props.data.website_name) && (
                            <li className="list-icon-item">
                                <span className="vdm vdm-031-ordinateur " aria-hidden="true"></span>
                                <a href={`${this.props.data.website_url}`} className="icon left">{this.props.data.website_name}</a>
                            </li>
                        )}
                    </ul>

                </div>
            );
        }
        return null;

    }
}