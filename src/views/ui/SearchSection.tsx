import { TranslationFunction } from 'i18next';
import * as queryString from 'query-string';
import * as React from 'react';
import { ISearchSection } from '../../content-api';
import { cloudinaryUrlAdapter } from '../../utils/cloudinary-adapter';
import { LocalizedLink } from '../LocalizedLink';
import { SearchBar } from '../ui';

interface ISearchSectionProps {
  search: ISearchSection;
  currentUrl: string;
  t: TranslationFunction;
}

export class SearchSection extends React.PureComponent<ISearchSectionProps> {

  public render() {
    const style = (this.props.search.picture) ? { backgroundImage: `url(${cloudinaryUrlAdapter(this.props.search.picture.id)})` } : undefined;
    const queryStrings = queryString.parse(queryString.extract(this.props.currentUrl));

    return (
      <section id="featured-search" className="bg-image-section overlay-mid" style={style}>
        <div className="container">
          <div className="row">
            <div className="col-12 col-lg-8 offset-lg-2">
              <h2 className="section-heading">{this.props.t("content:vitrine_arrondissement.search_title")}</h2>
              <form method="GET" action={this.props.t("content:vitrine_arrondissement.searchUrl")}>
                <SearchBar placeholder={this.props.t("content:vitrine_arrondissement.search_placeholder")} value={queryStrings.q} />
              </form>
              {this.props.search.suggested_links && this.props.search.suggested_links.length > 0 &&
                <div className="quick-links">

                  <ul className="list-inline">
                    {this.props.search.suggested_links.map((link, index) => {
                      return <li className="list-inline-item" key={index}><LocalizedLink to={link.url} title={link.title} t={this.props.t} className="btn btn-sm btn-secondary btn-inverse"/></li>;
                    })}
                  </ul>
                </div>
              }
            </div>
          </div>
        </div>
      </section>
    )
  }
}
