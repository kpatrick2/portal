import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IGlobalRates } from '../../../content-api';
import { LocalizedLink } from '../../LocalizedLink';

interface IGlobalRatesProps {
    data: IGlobalRates;
    t: TranslationFunction;
}

export class GlobalRates extends React.PureComponent<IGlobalRatesProps> {
    public render() {

        if (this.props.data) {
            return (
                <div className="col-12">
                    <div className="card card-fw card-tarif">
                        <div className="row">
                            <div className="col order-1">
                                <div className="card-section">
                                    <div className="card-body">
                                        <span className="card-label">{this.props.data.title}</span>
                                        {this.props.data.condition &&
                                        <span className="card-description">{this.props.data.condition}</span>
                                        }
                                    </div>
                                </div>
                            </div>
                            {this.props.data.approach && this.props.data.approach.length > 0 &&
                                <div className="col-12 col-lg-auto order-3 order-lg-2">
                                    <div className="card-section card-footer">
                                        <LocalizedLink
                                            to={this.props.data.approach[0].url!}
                                            className="card-link"
                                            t={this.props.t}
                                        >
                                            {this.props.data.approach[0].title} <span className="vdm vdm-063-fleche-droite " aria-hidden="true"></span></LocalizedLink>

                                    </div>
                                </div>
                            }
                            <div className="col-12 order-2 order-lg-3 ">
                                <div className="card-section">
                                    {this.props.data.rate && this.props.data.rate_currency ? (
                                        <div className="card-body text-center">{this.props.data.rate_details} <strong>{this.props.data.rate}</strong></div>
                                    ) : (
                                            <div className="card-body text-center">{this.props.data.rate_details} </div>)}

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            );
        }
    }
}