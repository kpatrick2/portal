import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IBoroughRate } from '../../../content-api';
import { LocalizedLink } from '../../LocalizedLink';

interface IBoroughRateProps {
    data: IBoroughRate;
    t: TranslationFunction;
}

export class BoroughRate extends React.PureComponent<IBoroughRateProps> {
    public render() {

        if (this.props.data) {
            return (
                <div className="col-12">
                    <div className="card card-fw card-tarif">
                        <div className="row">
                            <div className="col order-1">
                                <div className="card-section">
                                    <div className="card-body">
                                        <span className="card-label">{this.props.data.title}</span>
                                        {this.props.data.condition &&
                                        <span className="card-description">{this.props.data.condition}</span>
                                        }
                                    </div>
                                </div>
                            </div>
                            {this.props.data.approach && this.props.data.approach.length > 0 &&
                                <div className="col-12 col-lg-auto order-3 order-lg-2">
                                    <div className="card-section card-footer">
                                        <LocalizedLink
                                            to={this.props.data.approach[0].url!}
                                            className="card-link"
                                            t={this.props.t}
                                        >
                                            {this.props.data.approach[0].title} <span className="vdm vdm-063-fleche-droite " aria-hidden="true"></span></LocalizedLink>
                                    </div>
                                </div>
                            }
                            {this.props.data.rate_details &&
                                <div className="col-12 order-2 order-lg-3 ">
                                    <div className="card-section">
                                        <div className="card-body text-center">{this.props.data.rate_details}</div>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            );
        }
    }
}