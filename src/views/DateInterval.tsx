import { TranslationFunction } from 'i18next';
import * as moment from 'moment';
import * as React from 'react';
import { Capitalize } from '../utils';
import { DateTime } from './DateTime';

interface IDateIntervalProps {
  start: string;
  end: string;
  format?: 'short';
  t: TranslationFunction;
  en: boolean;
}

export class DateInterval extends React.PureComponent<IDateIntervalProps> {
  public render() {
    const start = moment(this.props.start);
    const end = moment(this.props.end);

    let renderedDate;
    // Single date with start time
    if (this.props.start === this.props.end) {
      renderedDate = this.formatSingleDate(this.props.start);
    } else if (start.diff(end, 'months') === 0 && start.diff(end, 'days') === 0){
      // Single date with start time and end time
      renderedDate = this.formatSameDayDateTime(this.props.start, this.props.end);
    } else {
      renderedDate = this.formatRangeDateTime(this.props.start, this.props.end);
    }

    return renderedDate;
  }

  // formatSingleDate
  private formatSingleDate(start:string) {
    return (
      <time dateTime={this.props.start}>
        <DateTime date={start} t={this.props.t} format="date-time-long" capitalizeFirst />
      </time>
    )
  }

  // formatSameDayDateTime
  private formatSameDayDateTime(start:string, end:string) {
    if (!this.props.format) {
      return (
        <>
          <time dateTime={start}>
            <DateTime capitalizeFirst date={end} t={this.props.t} format="date-long" />
            {' ' + this.props.t("common:of") + ' '}
            <DateTime date={start} t={this.props.t} format="time" />
          </time>
          {this.props.t("common:toTime")}
          <time dateTime={end}>
            <DateTime date={end} t={this.props.t} format="time" />
          </time>
        </>
      )
    } else if (this.props.format === 'short') {
      return (
        <time dateTime={end}>
          <DateTime capitalizeFirst date={end} t={this.props.t} format="date-long" />
        </time>
      );
    }
  }

  // formatRangeDateTime
  private formatRangeDateTime(start:string, end:string) {
    if (!this.props.format) {
      return (
        <>
          {Capitalize(this.props.t("common:from"))}
          <time dateTime={start}>
            <DateTime date={start} t={this.props.t} format="date" />
            {' - '}
            <DateTime date={start} t={this.props.t} format="time" />
          </time>
          <br />
          {this.props.t("common:to")}
          <time dateTime={end}>
            <DateTime date={end} t={this.props.t} format="date" />
            {' - '}
            <DateTime date={end} t={this.props.t} format="time" />
          </time>
        </>
      )
    } else if (this.props.format === 'short') {
      return (
        <>
          <span>{Capitalize(this.props.t("common:from"))}
            <time dateTime={start}>
              <DateTime date={start} t={this.props.t} format="day-month" />
            </time>
          </span>
          <span>{this.props.t("common:to")}
            <time dateTime={end}>
              <DateTime date={end} t={this.props.t} format="day-month" />
            </time>
          </span>
        </>
      )
    }
  }
}
