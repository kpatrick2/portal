import * as React from 'react';
import { IVideo } from '../content-api';

interface ISingleVideoProps {
  video?: IVideo[];
  className?: string;
}

export class SimpleVideoPlayer extends React.PureComponent<ISingleVideoProps> {
  public render() {
    if (!this.props.video || this.props.video.length === 0) {
      return null;
    }

    const video = this.props.video[0];
// verification de la provenance de l'url pour faire le traitement adéquat
// lien de test pour video vimeo : "https://vimeo.com/245958356"
    let url = video.url;
    // verification si le lien est un lien youtube
    if (url.search('youtube') !== -1) {
      // refactoring du lien pour qu'il puisse être embendé dans le iframe
      const linkMatch = url.match(/youtube\.com.*(?:\?v=|\/embed\/)(.{11})/);
      if (linkMatch) {
        const splitLink = linkMatch[0].split('/');
        url = 'https://' + splitLink[0] + '/embed/' + linkMatch[1];
      }
    }
    // verification si la video est un lien de vimeo
    if (url.search('vimeo') !== -1) {
      // refactoring du lien pour qu'il puisse être embendé dans le iframe
      const linkMatch = url.match(/vimeo\.com.*/);
      if (linkMatch) {
        const splitLink = linkMatch[0].split('/');
        url = 'https://player.' + splitLink[0] + '/video/' + splitLink[1];
      }
    }

    return (
      // il faudra rajouter un texte au milieu de la balise dans la langue du site pour dire qu'il y a un problème
      <p> {video.name}
        <iframe src={url} title={video.name} frameBorder="0" className={this.props.className} width="640" height="360" allowFullScreen></iframe>
      </p>

    );
  }
}
