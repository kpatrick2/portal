import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ITimeSpan } from '../content-api';
import { Capitalize } from '../utils';
import { DateTime } from './DateTime';

interface ITimeSpanProps {
  timeSpan: ITimeSpan;
  en?: boolean;
  t: TranslationFunction;
}

export class TimeSpan extends React.PureComponent<ITimeSpanProps> {
  public render() {

    const start = this.props.timeSpan.start_date;
    const end = this.props.timeSpan.end_date;
    const type = this.props.timeSpan.type;

    const formatTimeSpan = this.formatTimeSpan(start, end, type);

    return formatTimeSpan;
  }

  // formatSingleDate
  private formatTimeSpan(start:string| undefined, end:string| undefined, type:string) {

    let formattedDate: JSX.Element | null;

    if( type === 'from_date') {
      formattedDate = this.formatFromDate(start);
    } else if (type === 'to_date') {
      formattedDate = this.formatToDate(start);
    } else if (type === 'date_range') {
      formattedDate = this.formatDateRange(start, end);
    } else {
      formattedDate = this.formatFixedDate(start);
    }

    return formattedDate
  }

  private formatFromDate(start:string| undefined) {
    if(!this.props.timeSpan.start_date) {
      return null;
    }

    return (
      <>
        {this.props.t("common:timeSpan.beginning") + " "}
        <time dateTime={start}>
          <DateTime date={this.props.timeSpan.start_date} t={this.props.t} format="day-month" type="date" />
        </time>
      </>
    )
  }

  private formatToDate(end:string| undefined) {

    if(!this.props.timeSpan.end_date) {
      return null;
    }

    // Position text before or after date based on language.
    return (
      <>
        {this.props.en &&
          this.props.t("common:timeSpan.noLaterThan")
        }
        <time dateTime={end}>
          <DateTime date={this.props.timeSpan.end_date} t={this.props.t} format="day-month" type="date" />
        </time>
        {!this.props.en && 
          this.props.t("common:timeSpan.noLaterThan")
        }
      </>
    )
  }

  private formatFixedDate(start:string| undefined) {
    if(!this.props.timeSpan.start_date) {
      return null;
    }
    return (
      <>
        {this.props.en ? null : this.props.t("common:timeSpan.on") + " "}
        <time dateTime={start}>
          <DateTime date={this.props.timeSpan.start_date} t={this.props.t} format="day-month" type="date" />
        </time>
      </>
    )
  }

  private formatDateRange(start:string| undefined, end:string| undefined) {

    if(!this.props.timeSpan.start_date || !this.props.timeSpan.end_date) {
      return null;
    }
    return (
      <>
        {this.props.en ? null : Capitalize(this.props.t("common:from"))}
        <time dateTime={start}>
          <DateTime date={this.props.timeSpan.start_date} t={this.props.t} format="day-month" type="date" />
        </time>
        {this.props.en ? " - " : this.props.t("common:toDate")}
        <time dateTime={start}>
          <DateTime date={this.props.timeSpan.end_date} t={this.props.t} format="day-month" type="date" />
        </time>
      </>
    )
  }
}
