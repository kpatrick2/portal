import { TranslationFunction } from 'i18next';
import * as React from 'react';
import {
  IContentType} from '../../../content-api';
import { ContentTypesSpecialPages } from '../../../content-api/interfaces/content-types';
import { IContactPage } from '../../../content-api/interfaces/data/contact-data';
import { Language } from "../../../utils";
import ContactPage from './implementations/contact/ContactPage';

interface ISpecialPageProps {
  content: IContentType;
  extra?: any;
  currentUrl: string;
  t: TranslationFunction;
  language: Language;
}

export class SpecialPageFactory extends React.PureComponent<ISpecialPageProps> {
  public render() {
    // tslint:disable-next-line:switch-default
    switch (this.props.content.dc_hasVersion.fr) {
      case ContentTypesSpecialPages.Contact:
        return <ContactPage content={this.props.content as IContactPage} 
        t={this.props.t} language={this.props.language} />;
      default:
        const mtlData: IContentType = this.props.content; 
        // tslint:disable-next-line:no-console
        console.warn(`No content type component for ${(mtlData as any).dc_type.machine_name}`);
        return null;
    }
  }
}
