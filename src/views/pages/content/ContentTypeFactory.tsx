import { TranslationFunction } from 'i18next';
import * as React from 'react';
import {
  ContentTypes,
  ESystemName,
  IAlertContent,
  IBoroughContent,
  ICollectContent,
  ICommissionContent,
  IContentType,
  IElectedContent,
  IHubContent,
  IInstanceContent,
  ILocationContent,
  ILocationContentV2,
  INewsContent,
  IProductContent,
  IServiceRecordContent,
  ITopicsContent
} from '../../../content-api';
import { IDemarchesContent } from '../../../content-api/interfaces/data/demarches-data';
import { Language } from "../../../utils";
import {
  BoroughContent,
  CollectContent,
  CommissionContent,
  DemarchesContent,
  ElectedContent,
  EventContent,
  HubContent,
  InstanceContent,
  NewsContent,
  ProductContent,
  ServiceRecordView,
  TopicContent
} from './implementations';
import { AlertContent } from './implementations/alert/AlertContent';
import { LocationContent } from "./implementations/location";
import { LocationContentV2 } from './implementations/location/locationV2/LocationContentV2';
import { SpecialPageFactory } from './SpecialPageFactory';

interface IContentTypeFactoryProps {
  content: IContentType;
  extra?: any;
  currentUrl: string;
  t: TranslationFunction;
  language: Language;
}

export class ContentTypeFactory extends React.PureComponent<IContentTypeFactoryProps> {
  public render() {
    // tslint:disable-next-line:switch-default, cyclomatic-complexity
    switch (this.props.content.dc_type.machine_name) {
      case ContentTypes.Alert:
        return <AlertContent content={this.props.content as IAlertContent} t={this.props.t} />;

      case ContentTypes.News:
        return <NewsContent content={this.props.content as INewsContent} t={this.props.t} />;

      case ContentTypes.Product:
        return <ProductContent content={this.props.content as IProductContent} t={this.props.t} />;

      case ContentTypes.Elected:
        return <ElectedContent language={this.props.language}
          content={this.props.content as IElectedContent}
          extras={this.props.extra}
          t={this.props.t} />;

      case ContentTypes.Hub:
        return (
          <HubContent
            content={this.props.content as IHubContent}
            extras={this.props.extra}
            currentUrl={this.props.currentUrl}
            t={this.props.t}
          />
        );

      case ContentTypes.Instance:
        return <InstanceContent content={this.props.content as IInstanceContent}
          t={this.props.t}
          extras={this.props.extra}
        />;
      
      case ContentTypes.Commission:
        return <CommissionContent content={this.props.content as ICommissionContent}
          t={this.props.t}
          extras={this.props.extra}
        />;
      case ContentTypes.Location: {
        if (this.props.content.dc_provenance.system_name === ESystemName.SYSTEM_LEB) {
          return <LocationContent content={this.props.content as ILocationContent} t={this.props.t} />;
        }
        return <LocationContentV2 content={this.props.content as ILocationContentV2} t={this.props.t} extras={this.props.extra} />;
      }
      case ContentTypes.ServiceRecord:
        return (
          <ServiceRecordView
            content={this.props.content as IServiceRecordContent}
            boroughs={this.props.extra}
            currentUrl={this.props.currentUrl}
            t={this.props.t}
          />
        );

      case ContentTypes.Collect:
        return (
          <CollectContent
            content={this.props.content as ICollectContent}
            extra={this.props.extra}
            currentUrl={this.props.currentUrl}
            t={this.props.t}
          />
        );
      
      case ContentTypes.Topic:
        return <TopicContent content={this.props.content as ITopicsContent} extras={this.props.extra as IDemarchesContent[]} t={this.props.t} />;

      case ContentTypes.ExternalContent:
      case ContentTypes.Home:
        // These cases should never happen
        return null;
      
      case ContentTypes.Demarche:
        return <DemarchesContent content={this.props.content} t={this.props.t} currentUrl={this.props.currentUrl} extra={this.props.extra} />;
      
      case ContentTypes.Event:
        return <EventContent content={this.props.content} t={this.props.t} extras={this.props.extra}></EventContent>
      
      case ContentTypes.Borough:
        return <BoroughContent content={this.props.content as IBoroughContent} currentUrl={this.props.currentUrl} extras={this.props.extra} t={this.props.t} />;
      
      case ContentTypes.SpecialPage:
        return <SpecialPageFactory content={this.props.content} t={this.props.t} currentUrl={this.props.currentUrl} language={this.props.language} />
      default:
        const mtlData: IContentType = this.props.content;
        // tslint:disable-next-line:no-console
        console.warn(`No content type component for ${(mtlData as any).dc_type.machine_name}`);
        return null;
    }
  }
}
