import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { EInstanceType, IElectedContent, IElectedCoordinates, IInstanceContent} from '../../../../../content-api';
import { addressExists,Language } from '../../../../../utils';
import { Address, Card, ListGroup, SideBar, SocialMedia } from '../../../../ui';
import { SingleImage } from '../../../../ui/images/SingleImage';

interface IElectedContentProps {
  content: IElectedContent;
  t: TranslationFunction;
  language: Language;
  extras: { instances: any; commissions: any }; // TODO review
}

export class ElectedContent extends React.PureComponent<IElectedContentProps> {
  public render() {
    return (
      <main className="main-content " id="main-content" aria-label="Main Content">
        <header className="content-header content-header-profile bg-gray-sky">
          <div className="container">
            <div className="row">
              <div className="col-12 col-lg-7 col-xl-8">
                <div className="content-header-title">
                  <h1 className="display-2">{this.props.content.dc_title}</h1>
                </div>
                <div className="row">
                  <div className="col-12 mb-4">
                    <div className="content-header-infos">
                      {this.getFonctions()}
                    </div>
                  </div>
                  {this.props.content.mtl_content.elus.political_party ? (
                    <div className="col-12 mb-3">
                      <div className="content-header-infos">
                        <span className="profile-label">{this.props.t('content:elu.label_party')}</span>
                        <span className="profile-value">{this.props.content.mtl_content.elus.political_party}</span>
                      </div>
                    </div>
                  ) : null}
                  {this.props.content.dc_coverage.boroughs.length ?
                    <div className="col-12 col-lg-6 mb-3">
                      <div className="content-header-infos">
                        <span className="profile-label">{this.props.t('content:elu.label_borough')}</span>
                        {this.getBoroughs()}
                      </div>
                    </div>
                    : null}
                  {this.props.content.mtl_content.elus.district ? (
                    <div className="col-12 col-lg-6 mb-3">
                      <div className="content-header-infos">
                        <span className="profile-label">{this.props.t('content:elu.label_district')}</span>
                        <span className="profile-value">{this.props.content.mtl_content.elus.district}</span>
                      </div>
                    </div>
                  ) : null}
                </div>
              </div>
              <div className="col-12 col-lg-5 col-xl-4">
                <div className="img-profile">
                  <SingleImage images={this.props.content.mtl_images} transformations={{ ar: 1, crop: 'crop' }} className="img-fluid rounded-circle" />
                </div>
              </div>
            </div>
          </div>
        </header>

        <div className="region-content ">
          <div className="container ">
            <div className="row">
              <article className="col-12 col-lg-7 col-xl-8">
                {this.getResponsabilities(this.props.extras.instances, this.props.extras.commissions)}
                <ListGroup
                  items={this.props.content.mtl_content.elus.other_responsabilities}
                  title={this.props.t('content:elu.label_other_responsibilities')}
                />
              </article>
              <SideBar>
                <section className="sb-block ">
                  {this.sideBarList()}
                  <SocialMedia media={this.props.content.mtl_content.elus.social_networks} />
                </section>
              </SideBar>
            </div>
          </div>
        </div>
      </main>
    );
  }

  private getBoroughs() {
    if (this.props.content.dc_coverage.boroughs.length === 0) {
      return null;
    }
    const sectionData = [];

    for (const [index, borough] of this.props.content.dc_coverage.boroughs.entries()) {
      if (index === 0) {
        sectionData.push(
          <span key={index} className="profile-value">
            {borough}
          </span>
        );
      }
    }

    return sectionData;
  }
  private getFonctions() {
    if (this.props.content.mtl_content.elus.functions.length === 0) {
      return null;
    }

    const sectionData = [];
    for (const [index, fonction] of this.props.content.mtl_content.elus.functions.entries()) {
      sectionData.push(
        <span key={index} className="profile-title">
          {fonction.label}
        </span>
      );
    }

    return sectionData;
  }

  private getResponsabilities(instances: any[], commissions: any[]) {
    if ((!instances || instances.length === 0) && (!commissions || commissions.length === 0)) {
      return null;
    }

    return (
      <div className="page-section pt-0">
        <h2 className="section-heading">{this.props.t('content:elu.label_roles')}</h2>
        <div className="cards">
          {this.putCardsInstances(instances)}
          {this.putCardsCommissions(commissions)}
        </div>
      </div>
    );
  }

  private putCardsInstances(instances: any[]) {
    if (instances.length === 0) {
      return null;
    }

    const sectionData = [];
    const instancesOrder: any = {};
    instancesOrder[EInstanceType.COMITE_EXECUTIF] = 0;
    instancesOrder[EInstanceType.CONSEIL_MUNICIPAL] = 1;
    instancesOrder[EInstanceType.CONSEIL_AGGLO] = 2;
    instancesOrder[EInstanceType.CONSEIL_ARRONDISSEMENT] = 3;

    instances.sort((a: IInstanceContent, b: IInstanceContent) => {
      if (!a.mtl_content.instances.instance_type || !b.mtl_content.instances.instance_type) {
        return 0;
      } else {
        return instancesOrder[a.mtl_content.instances.instance_type.code] - instancesOrder[b.mtl_content.instances.instance_type.code];

      }
    });

    for (const instance of instances) {
      const resAndfonction = this.filterInstancesOrCommissions(instance.mtl_content.instances.members);
      sectionData.push(
        <Card title={resAndfonction.functionElected} subtitle={instance.dc_title} link={instance.dc_identifier} t={this.props.t} text={resAndfonction.responsabilities} />
      );
    }

    return sectionData;
  }

  private putCardsCommissions(commisssions: any[]) {
    if (commisssions.length === 0) {
      return null;
    }

    const sectionData = [];

    for (const [index, com] of commisssions.entries()) {
      const resAndfonction = this.filterInstancesOrCommissions(com.mtl_content.commissions.members);

      sectionData.push(
        <Card key={index} title={resAndfonction.functionElected} subtitle={com.dc_title} link={com.dc_identifier} t={this.props.t} text={resAndfonction.responsabilities} />
      );
    }

    return sectionData;
  }

  private filterInstancesOrCommissions(members: any) {
    let responsabilities: string = 'null';
    let functionElected: string = '';

    for (const elus of members) {
      if (elus.elected_name && elus.elected_name[0].title === this.props.content.dc_title) {

        // On concidére que l'élu a seulement une seule fonction
        functionElected = elus.functions.length > 0 ? elus.functions[0].label : null;
        responsabilities = elus.responsabilities.length > 0 ? elus.responsabilities.join(", ") : '';
        break;
      }
    }

    return { responsabilities, functionElected };
  }

  // may be used elsewhere
  // private putMemberOfInstance(instances: any[]) {
  //   if (instances) {
  //     const test = instances.filter(ins => ins.dc_hasVersion.fr === '/conseils-decisionnels/conseil-municipal');
  //     if (instances.length === 0 || test.length === 0) {
  //       return null;
  //     }

  //     return <span className="profile-member">{this.props.t('content:elu.label_siege_comite')}</span>;
  //   }

  //   return null;
  // }


  private sideBarList() {
    const sectionData = [];
    if (this.props.content.mtl_content.elus.contacts_information && this.props.content.mtl_content.elus.contacts_information.length) {
      sectionData.push(<h2 className="sidebar-title ">{this.props.content.mtl_content.elus.title_contacts_information}</h2>)

      for (const contact of this.props.content.mtl_content.elus.contacts_information) {
        const content = this.formatContactBloc(contact);
        if (content) {
          sectionData.push(content);
        }
      }

      if (sectionData.length === 0) {
        return null;
      } else {
        return sectionData;
      }
    }
  }

  private formatContactBloc(contact: IElectedCoordinates) {
    const content = [];
    const items = [];

    // Add title
    if (contact.title) {
      content.push(<div className="list-icon-heading">{contact.title}</div>);
    }

    // Add address
    
    if (addressExists(contact)) {
      items.push(
        <li className="list-icon-item">
          <span className="vdm vdm-091-localisation " aria-hidden="true" />
          <span>
            <Address address={contact}></Address>
          </span>
        </li>
      );
    }

    // Add phone, extension and phone note
    const phone = this.formatPhone(contact);
    if (phone) {
      items.push(
        <li className="list-icon-item">
          <span className="vdm vdm-012-telephone " aria-hidden="true" />
          <span>{phone}</span>
        </li>
      );
    }

    // Add email
    const email = this.formatEmail(contact);
    if (email) {
      items.push(
        <li className="list-icon-item">
          <span className="vdm vdm-028-enveloppe " aria-hidden="true" />
          {email}
        </li>
      );
    }

    if (items.length > 0) {
      content.push(<ul className="list-icon">{items}</ul>);
    }

    // If there is content, return the content
    if (content.length === 0) {
      return null;
    } else {
      return content;
    }
  }

  private formatPhone(contact: IElectedCoordinates) {
    const content = [];
    if (!contact.phone && !contact.phone_note) {
      return null;
    }

    if (contact.phone) {
      content.push(contact.phone);


      if (contact.phone_note) {
        content.push(", ");
        content.push(this.props.t('content:elu.extension'));
        content.push(" ");
        content.push(contact.phone_note);
      }

      return content;
    }
  }

  private formatEmail(contact: IElectedCoordinates) {
    const content = [];

    if (!contact.email) {
      return null;
    } else {
      content.push(
        <a href={`mailto:${contact.email}`} className="elided-text">
          {' '}
          {this.props.t('content:elu.label_send_email')}{' '}
        </a>
      );
    }

    return content;
  }
}
