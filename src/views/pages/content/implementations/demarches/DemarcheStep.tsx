import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IDemarchesAddress, IDemarchesBodyServiceStep, ISupportDocuments } from '../../../../../content-api/interfaces/data/demarches-data';
import { LocalizedLink } from '../../../../LocalizedLink';
import { ToolboxAccordeon } from '../../../../ui';

interface IDemarcheStepProps {
  step: IDemarchesBodyServiceStep;
  id: number;
  hasRates: boolean;
  borough: string;
  expanded?: boolean;
  t: TranslationFunction;
}
export class DemarcheStep extends React.PureComponent<IDemarcheStepProps> {
  public render() {
    return (
      <>
        <ToolboxAccordeon header={this.props.step.title} id={this.props.id} expanded={this.props.expanded}>
          <div dangerouslySetInnerHTML={{ __html: this.props.step.description }}></div>
          {this.props.step.address && this.props.step.address.length > 0 &&
            this.props.step.address.map((address: IDemarchesAddress) => {
              return <p>
                <strong>{address.title}</strong> <br />  
                {address.address_1} { address.address_2 ? ', ' + address.address_2 : null} <br />
                {address.city} ({address.province}) {address.postal_code}
              </p>
            })
          }
          {(this.props.step.documents || this.props.hasRates) &&
            <div className="list-group">
              {this.props.step.documents &&
                <a href="#" className="list-group-item list-group-item-action" data-toggle="modal" data-target={'#modalDocuments' + this.props.borough.replace(/[^a-zA-Z0-9]/g, '')+ this.props.id}><span className="vdm vdm-019-document " aria-hidden="true"></span>
                  {this.props.t("common:content.document_read")}</a>
              }
              {this.props.hasRates &&
                <a href={'#tarifs' + this.props.borough} className="list-group-item list-group-item-action js-scroll-trigger"><span className="vdm vdm-113-prix " aria-hidden="true"></span>
                  {this.props.t("common:content.rate_read")}</a>
              }
            </div>
          }
          {this.props.step.payment_mode && <>
            <h3>{this.props.t("common:content.payment_mode")}</h3>
            <div dangerouslySetInnerHTML={{ __html: this.props.step.payment_mode }}></div>
          </>
          }
          {this.renderActionButton()}
        </ToolboxAccordeon>
        <div className="modal" id={'modalDocuments' + this.props.borough.replace(/[^a-zA-Z0-9]/g, '') + this.props.id} tabIndex={-1} role="dialog" aria-labelledby="modalDocumentsLabel" aria-hidden="true" style={{ 'display': 'none' }}>
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="modalDocumentsLabel">{this.props.t("common:content.document_requested")}</h5>
                <button type="button" className="btn-close close" data-dismiss="modal" aria-label="Close">
                  <span className="vdm vdm-046-grand-x"></span>
                </button>
              </div>
              <div className="modal-body">
                {
                  this.props.step.documents && this.props.step.documents!.map((document: ISupportDocuments) => {
                    return (
                      <>
                        <h6>{document.title}</h6>
                        <p>{document.comments}</p>
                        <ul>
                          {document.supporting_document && document.supporting_document.map(value => {
                            return (
                              <li>{value}</li>
                            )
                          })}
                        </ul>
                      </>
                    )
                  })
                }
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }

  private renderActionButton() {
    if (this.props.step.call_to_action && this.props.step.call_to_action.length > 0) {
      return <LocalizedLink
        className="btn btn-sm btn-primary"
        t={this.props.t}
        title={this.props.step.call_to_action[0].title}
        type={this.props.step.call_to_action[0].type}
        to={this.props.step.call_to_action  [0].url}
      />
    }
  }
}
