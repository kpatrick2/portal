import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IDemarchesBodyPart, IDemarchesBodyServiceStep, IDemarchesContent } from '../../../../../content-api/interfaces/data/demarches-data';
import { getSelectedBorough, isFullCity } from '../../../../../utils';
import { getSelectedBoroughContent } from '../../../../../utils/borough-helper';
import { AbstractText, Header, SideBar, SideBarItemBlock, SideBarItemBlockLaw } from '../../../../ui';
import { AbstractModalBorough } from '../../../../ui/AbstractModalBorough';
import { SideBarItemBlockDocument } from '../../../../ui/sidebar/SideBarItemBlockDocument';
import { BoroughAd } from '../../../borough-select/borough-ad';
import { DemarcheRate } from './DemarcheRate';
import { DemarcheStep } from './DemarcheStep';

interface IDemarchesContentProps {
  content: IDemarchesContent;
  extra?: any;
  currentUrl: string;
  t: TranslationFunction;
}

export class DemarchesContent extends React.PureComponent<IDemarchesContentProps> {
  private body: IDemarchesBodyPart | undefined;
  private selectedBrorough: string | undefined;
  private isFullCity: any;

  public render() {
    this.getSelectedBoroughService();
    return (
      <>
        <Header
          title={this.props.content.dc_title}
          date={this.props.content.dc_modified}
          t={this.props.t}
          hub_link={this.props.t("content:demarches.allDemarches")}
          dark
        />
        {this.isFullCity ?
          <AbstractText text={this.props.content.dc_abstract} />
          :
          <AbstractModalBorough
            text={this.props.content.dc_abstract}
            extra={this.props.extra}
            currentUrl={this.props.currentUrl}
            selectText={this.props.t("common:content.borough_select_demarche")}
            t={this.props.t} />
        }
        {this.props.content.mtl_content.demarches.body && this.props.content.mtl_content.demarches.body.length > 0 &&
          this.props.content.mtl_content.demarches.body.map(bloc => {
            if (this.isFullCity) {
              return (<div id="FullCity" key="FullCity">
                {this.renderContent(bloc, '')}
                {this.renderRates(bloc, '')}
              </div>)
            } else {
              return bloc.boroughs.map(borough => {
                return (<div id={borough} key={borough} style={{ display: (!this.body || borough !== this.selectedBrorough) ? 'none' : 'block' }}>
                  {this.renderContent(bloc, borough)}
                  {this.renderRates(bloc, borough)}
                </div>)
              })
            }
          })
        }


        <div className="region-content" id="borough-no-data" style={{ display: (!this.body && this.selectedBrorough ? 'block' : 'none') }}>
          <div className="container">
            <div className="row">
              <article className="col-12 col-lg-8">
                {this.props.t("common:no_result")}
              </article>
            </div>
          </div>
        </div>
        {
          !this.selectedBrorough &&
          <BoroughAd text={this.props.t("common:content.borough_ad")} />
        }
      </>
    );
  }


  private filterSteps(bloc: IDemarchesBodyPart): IDemarchesBodyServiceStep[] {
    if (bloc) {
      const sortArray: any = {
        'service_walk_in': 1,
        'service_telephone': 2,
        'service_online': 0,
        'service_mail': 3,
        'service_email': 4,
        'service_fax': 5
      }

      return Object.keys(bloc)
        .filter((value) => {
          return value.indexOf("service") > -1;
        })
        .sort((a: string, b: string) => {
          return sortArray[a] - sortArray[b];
        })
        .reduce((ar: any, key: any) => {
          if (bloc![key] && bloc![key].length > 0 && bloc![key][0].title) {
            ar.push((bloc![key][0]));
            if (key === 'service-online') {
              ar[ar.length].expanded = true;
            }
          }
          return ar;
        }, [])
        ;
    }
    return [];
  }

  private renderSteps(bloc: IDemarchesBodyPart, borough: string) {
    const steps = this.filterSteps(bloc);
    const hasRate = (bloc.rates && bloc.rates.length > 0) ? true : false;;
    return (
      <div className="collapsible-variant pt-4" role="tablist" aria-multiselectable="true">
        {steps.map((step, index) => {
          const expanded = index === 0 || steps.length === 1 || (index === 1 && steps[0] === undefined)
          return (
            <DemarcheStep
              key={index}
              id={index}
              step={step}
              t={this.props.t}
              borough={borough}
              hasRates={hasRate}
              expanded={expanded} />
          )
        }
        )}
      </div>
    )
  }

  private renderContent(bloc: IDemarchesBodyPart, borough: string) {

    return (
      <div className="region-content" >
        <div className="container">
          <div className="row">
            <article className="col-12 col-lg-7 col-xl-8">
              <div dangerouslySetInnerHTML={{ __html: bloc.description }}>
              </div>
              {this.renderSteps(bloc, borough)}
            </article>
            {
              ((this.props.content.mtl_content.demarches.related_documents && this.props.content.mtl_content.demarches.related_documents.length > 0) ||
                (this.props.content.mtl_content.demarches.by_laws && this.props.content.mtl_content.demarches.by_laws.length > 0) ||
                (this.props.content.mtl_content.demarches.body[0].requirement)) &&
              <SideBar stroke>
                {this.props.content.mtl_content.demarches.body[0].requirement &&
                  <SideBarItemBlock title={this.props.content.mtl_content.demarches.body[0].requirement_title}>
                    <div dangerouslySetInnerHTML={{ __html: this.props.content.mtl_content.demarches.body[0].requirement }}></div>
                  </SideBarItemBlock>
                }
                {this.props.content.mtl_content.demarches.related_documents &&
                  <SideBarItemBlockDocument title={this.props.t("common:documents")} documents={this.props.content.mtl_content.demarches.related_documents} t={this.props.t} />}
                {this.props.content.mtl_content.demarches.by_laws &&
                  <SideBarItemBlockLaw title={this.props.t("common:rules")} data={this.props.content.mtl_content.demarches.by_laws} t={this.props.t} />}
              </SideBar>
            }
          </div>
        </div>
      </div>
    )
  }

  private renderRates(bloc: IDemarchesBodyPart, borough: string) {
    return <section className="page-section bg-gray-sky" id={'tarifs' + borough}  >
      {bloc.rates && bloc.rates.map((rate) => {
        return (<div className="container">
          <div className="row">
            <div className="col-12 text-center">
              <h2 className="section-heading">{this.props.t("content:location.fees")}</h2>
            </div>
          </div>
          {rate.service_rate.table &&
            <DemarcheRate rates={rate.service_rate.table} title={rate.service_rate.title} condition={rate.service_rate.condition} t={this.props.t} />
          }
        </div>)
      })}
    </section>
  }

  private getSelectedBoroughService() {
    this.isFullCity = isFullCity(this.props.content.mtl_content.demarches.body);
    if (this.isFullCity) {
      this.selectedBrorough = this.isFullCity;
    } else {
      this.selectedBrorough = getSelectedBorough(this.props.currentUrl, this.props.extra);
    }
    this.body = getSelectedBoroughContent(this.isFullCity, this.selectedBrorough!, this.props.content.mtl_content.demarches.body);
  }
}
