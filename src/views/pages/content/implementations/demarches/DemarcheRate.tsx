import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ITable } from '../../../../../content-api';
import { Table } from '../../../../Table';

interface IDemarcheStepProps {
  rates: ITable[];
  t: TranslationFunction;
  title: string;
  condition?: string;
}
export class DemarcheRate extends React.PureComponent<IDemarcheStepProps> {
  public render() {
    return (
      <div className="col-12">
        <div className="card card-fw card-tarif">
          <div className="row">
            <div className="col-12  order-1">
              <div className="card-section">
                <div className="card-body">
                  <span className="card-label">{this.props.title}</span>
                  {this.props.condition &&
                    <span className="card-description">{this.props.condition}</span>
                  }
                </div>
              </div>
            </div>
            <div className="col-12 order-last ">
              <div className="card-section ">
                <Table tables={this.props.rates} striped />
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}
