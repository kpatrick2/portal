import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { INodeReferenceWithIcon } from '../../../../../content-api';
import { HubParagraph1Tile } from './HubParagraph1Tile';

interface IHubParagraph1Props {
  references: INodeReferenceWithIcon[];
  t: TranslationFunction;
}

export class HubParagraph1 extends React.PureComponent<IHubParagraph1Props> {
  public render() {
    if (!this.props.references || this.props.references.length === 0) {
      return null;
    }

    return (
      <div id="navTop" className="page-section bg-gray-sky">
        <div className="container">
          <div className="row cards ">
            {this.props.references.map((reference, index) => (
              <HubParagraph1Tile key={index} reference={reference} t={this.props.t} />
            ))}
          </div>
        </div>
      </div>
    );
  }
}
