import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IParagraph4 } from '../../../../../content-api';
import { HubParagraph4List } from './HubParagraph4List';

interface IHubParagraph4Props {
  title?: string;
  sections: IParagraph4[];
  currentUrl: string;
  t: TranslationFunction;
}

export class HubParagraph4 extends React.PureComponent<IHubParagraph4Props> {
  public render() {
    if (!this.props.sections || this.props.sections.length === 0) {
      return null;
    }

    return (
      <section className="page-section">
        <div className="container">
          <div className="row hub-para4-container">
            {this.props.title ? (
              <div className="col-12">
                <h2 className="section-heading">{this.props.title}</h2>
              </div>
            ) : null}
            <div className="col-12 col-lg-7">
              {this.getSectionsData().map((section, index) => (
                <HubParagraph4List key={index} id={index.toString()} section={section} t={this.props.t} />
              ))}
            </div>
          </div>
        </div>
      </section>
    );
  }

  private getSectionsData() {
    return this.props.sections.filter(
      section =>
        section.title &&
        section.referenced_nodes &&
        section.referenced_nodes.length > 0 &&
        section.referenced_nodes[0][0]
    );
  }
}
