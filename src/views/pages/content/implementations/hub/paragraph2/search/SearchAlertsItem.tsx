import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IAlertContent } from '../../../../../../../content-api';
import { CardV2 } from '../../../../../../ui/cards/CardV2';

interface ISearchAlertsItemProps {
  data: IAlertContent;
  t: TranslationFunction;
}

export class SearchAlertsItem extends React.PureComponent<ISearchAlertsItemProps> {
  public render() {

    return (
      <CardV2 dc_identifier={this.props.data.dc_identifier}
        date={this.props.data.dc_created}
        date_format='time'
        title={this.props.data.dc_title}
        badge={this.props.data.sioc_topic}
        t={this.props.t} />
    );
  }
}
