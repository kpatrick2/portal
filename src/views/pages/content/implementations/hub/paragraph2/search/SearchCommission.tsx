import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ICommissionContent, IContentTypeSearch } from '../../../../../../../content-api';
import { CardResult } from '../../../../../../ui';

interface ISearchCommissionProps {
  results: IContentTypeSearch;
  t: TranslationFunction;
}

export class SearchCommission extends React.PureComponent<ISearchCommissionProps> {
  public render() {
    const header = this.props.t('content:hub.paragraph2.results', { count: this.props.results.total });

    return (
      <>
        <div className="hub-section-heading">
          <div className="row">
            <div className="col-12 col-lg">
              <h2 className="list-group-hub-heading">{header}</h2>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="list-group list-group-hub">
              {this.props.results.entries.map((entry: ICommissionContent) => (
                <CardResult title={entry.dc_title} link={entry.dc_identifier} t={this.props.t} />
              ))}
            </div>
          </div>
        </div>
      </>
    );
  }
}
