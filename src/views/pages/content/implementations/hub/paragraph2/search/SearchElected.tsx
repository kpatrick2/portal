import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IContentTypeSearch, IElectedContent } from '../../../../../../../content-api';
import { CardResult } from '../../../../../../ui';



interface ISearchNewsProps {
    results: IContentTypeSearch;
    t: TranslationFunction;
}

export class SearchElected extends React.PureComponent<ISearchNewsProps> {
    public render() {
        const header = this.props.t('content:hub.paragraph2.elected.results', { count: this.props.results.total });

        return (
            <>
                <div className="hub-section-heading">
                    <div className="row">
                        <div className="col-12 col-lg">
                            <h2 className="list-group-hub-heading">{header}</h2>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="list-group list-group-hub">
                            {this.props.results.entries.map((entry: IElectedContent) => (
                                <CardResult
                                    title={entry.dc_title}
                                    link={entry.dc_identifier}
                                    text={entry.mtl_content.elus.functions[0] ? entry.mtl_content.elus.functions[0].label : ""}
                                    text2={entry.dc_coverage.boroughs[0] ? entry.dc_coverage.boroughs[0] : ""}
                                    t={this.props.t} />
                            ))}
                        </div>
                    </div>
                </div>
            </>
        );
    }
}
