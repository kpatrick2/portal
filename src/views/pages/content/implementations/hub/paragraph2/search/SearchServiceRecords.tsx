import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IContentTypeSearch, IServiceRecordContent } from '../../../../../../../content-api';
import { HubList, HubListElement } from '../../../../../../ui';

interface ISearchServiceRecordsProps {
  results: IContentTypeSearch;
  t: TranslationFunction;
}

export class SearchServiceRecords extends React.PureComponent<ISearchServiceRecordsProps> {
  public render() {
    return (
        <HubList>
          {this.props.results.entries.map((entry: IServiceRecordContent) => (
            <HubListElement
              title={entry.dc_title}
              dc_identifier={entry.dc_identifier}
              t={this.props.t}
              text={entry.dc_abstract} />
          ))}
        </HubList>
    );
  }
}
