import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IContentTypeSearch, INewsContent } from '../../../../../../../content-api';
import { CardList } from '../../../../../../ui/cards/CardList';
import { CardV2 } from '../../../../../../ui/cards/CardV2';



interface ISearchNewsProps {
  results: IContentTypeSearch;
  t: TranslationFunction;
}

export class SearchNews extends React.PureComponent<ISearchNewsProps> {
  public render() {
    return (
      <CardList>
        {this.props.results.entries.map((entry: INewsContent) => (
          <div className="col-12">
            <CardV2
              title={entry.dc_title}
              dc_identifier={entry.dc_identifier}
              date={entry.dc_issued}
              t={this.props.t}
              images={entry.mtl_images}
              tc={entry.dc_type.machine_name}
            />
          </div>
        ))}
      </CardList>
    );
  }
}
