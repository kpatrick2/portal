import { TranslationFunction } from 'i18next';
import * as React from 'react';

interface ISearchAlertsGroupProps {
  date: Date;
  t: TranslationFunction;
}

export class SearchAlertsGroup extends React.PureComponent<ISearchAlertsGroupProps> {
  public render() {
    return (
      <li>
        <h2 className="date day-month">
          <span className="day">{this.getFormattedDay()}</span>{this.getFormattedMonth()}
        </h2>
        {this.props.children}
      </li>
    );
  }

  private getFormattedDay() {
    const day = this.props.date.getDate();
    return ('0' + day).slice(-2);
  }

  private getFormattedMonth() {
    const month = this.props.date.getMonth();
    const key = this.mapToTranslationKey(month);
    const translation = this.props.t(key);
    return translation.toLowerCase();
  }

  private mapToTranslationKey(month: number) {
    // tslint:disable-next-line:switch-default
    switch (month) {
      case 0:
        return 'common:months.january';
      case 1:
        return 'common:months.february';
      case 2:
        return 'common:months.march';
      case 3:
        return 'common:months.april';
      case 4:
        return 'common:months.may';
      case 5:
        return 'common:months.june';
      case 6:
        return 'common:months.july';
      case 7:
        return 'common:months.august';
      case 8:
        return 'common:months.september';
      case 9:
        return 'common:months.october';
      case 10:
        return 'common:months.november';
      case 11:
        return 'common:months.december';
      default:
        return '';
    }
  }
}
