import { TranslationFunction } from 'i18next';
import * as React from 'react';
import {
  ContentTypesParagraph,
  IExtraHub
} from '../../../../../../../content-api';
import { SearchAlerts } from './SearchAlerts';
import { SearchCommission } from './SearchCommission';
import { SearchElected } from './SearchElected';
import { SearchNews } from './SearchNews';
import { SearchPetition } from './SearchPetition';
import { SearchServiceRecords } from './SearchServiceRecords';

interface IHubSearchContentTypeFactoryProps {
  contentType: ContentTypesParagraph;
  results: IExtraHub;
  currentUrl: string;
  t: TranslationFunction;
}

export class HubSearchContentTypeFactory extends React.PureComponent<IHubSearchContentTypeFactoryProps> {
  public render() {
    // tslint:disable-next-line:switch-default
    switch (this.props.contentType) {
      case ContentTypesParagraph.Alert:
        return this.renderAlert();

      case ContentTypesParagraph.News:
        return <SearchNews results={this.props.results.search} t={this.props.t} />;


      case ContentTypesParagraph.ServiceRecord:
        return (
          <SearchServiceRecords
            results={this.props.results.search}
            t={this.props.t}
          />
        );

      case ContentTypesParagraph.Elected:
        return (
          <SearchElected
            results={this.props.results.search}
            t={this.props.t}
          />
        )

      case ContentTypesParagraph.Petition:
        return (
          <SearchPetition
            results={this.props.results.search}
            t={this.props.t}
          />
        )

      case ContentTypesParagraph.ExternalContent:
      case ContentTypesParagraph.Home:
      case ContentTypesParagraph.Hub:
      case ContentTypesParagraph.Location:
      case ContentTypesParagraph.Product:
      case ContentTypesParagraph.Collect:
      case ContentTypesParagraph.Instance:
      case ContentTypesParagraph.Commission:
      return (
        <SearchCommission
          results={this.props.results.search}
          t={this.props.t}
        />
      )
      case ContentTypesParagraph.Topic:



        // These cases should never happen
        // tslint:disable-next-line:no-console
        console.warn(`Unsupported content type for paragraph2 list: ${this.props.contentType}`);
        return null;

      default:
        const mtlData: never = this.props.contentType;
        // tslint:disable-next-line:no-console
        console.warn(`No content type component for ${(mtlData as any).mtl_node_type_machine_name}`);
        return null;
    }
  }

  private renderAlert() {
    return <SearchAlerts currentUrl={this.props.currentUrl} results={this.props.results.search} t={this.props.t} />;
  }
}
