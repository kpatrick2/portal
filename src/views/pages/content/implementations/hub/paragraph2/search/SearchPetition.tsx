import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IContentTypeSearch } from '../../../../../../../content-api';
import { IPetitionContent } from '../../../../../../../content-api/interfaces/data/petition-data';
import { DateTime } from '../../../../../../DateTime';
import { CardResult } from '../../../../../../ui';



interface ISearchPetitionsProps {
    results: IContentTypeSearch;
    t: TranslationFunction;
}

export class SearchPetition extends React.PureComponent<ISearchPetitionsProps> {
    public render() {
        const header = this.props.t('content:hub.paragraph2.petition.results', { count: this.props.results.total });

        return (
            <>
                <div className="hub-section-heading">
                    <div className="row">
                        <div className="col-12 col-lg">
                            <h2 className="list-group-hub-heading">{header}</h2>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="list-group list-group-hub">
                            {this.props.results.entries.map((entry: IPetitionContent) => (
                                <CardResult
                                    title={entry.dc_title}
                                    link={entry.dc_identifier}
                                    text={this.formatDate(entry)}
                                    text2={entry.dc_coverage.boroughs[0]}
                                    t={this.props.t} />
                            ))}
                        </div>
                    </div>
                </div>
            </>
        );
    }

    private formatDate(entry: IPetitionContent) {
        if (entry.mtl_content.petitions.publication) {
            return <DateTime date={entry.mtl_content.petitions.publication.publicationStart.createdAt} t={this.props.t} format="day-month" />;
        }
    }
}
