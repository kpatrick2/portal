import { TranslationFunction } from 'i18next';
import * as queryString from 'query-string';
import * as React from 'react';
import { ContentTypes, ContentTypesParagraph, IExtraHub, IParagraph2 } from '../../../../../../../content-api';
import { Analytics } from '../../../../../../analytics';
import { EmptySearchResult, Pagination, paginationHelpers, SearchBar } from '../../../../../../ui';
import { EmptySearchState } from '../../../../../../ui/EmptySearchState';
import { HubSearchContentTypeFactory } from './HubSearchContentTypeFactory';

interface IHubSearchListProps {
  sectionTitle?: string;
  title: string;
  paragraph2: IParagraph2;
  results: IExtraHub;
  currentUrl: string;
  titleHidden?: boolean;
  t: TranslationFunction;
}

export class HubSearchList extends React.PureComponent<IHubSearchListProps> {
  public render() {
    const queryStrings = queryString.parse(queryString.extract(this.props.currentUrl));
    const searchTerm = queryStrings.q;

    return (
      <>
        <Analytics.SearchResult
          title={this.props.title}
          searchTerm={searchTerm}
          numberOfResults={this.props.results.search.total}
          totalPages={paginationHelpers.calculateNumberOfPages(this.props.results.search.total, this.props.results.search.limit)}
          currentPage={paginationHelpers.calculateCurrentPage(this.props.results.search.offset, this.props.results.search.limit)}
          contentTypeFilter={this.props.paragraph2.content_type}
        />

        {
          this.props.paragraph2.content_type !== ContentTypesParagraph.Elected && 
          this.props.paragraph2.content_type !== ContentTypesParagraph.Petition ? (
            <form method="GET">
              <div className="hub-section-heading">
                <SearchBar placeholder={this.getSearchPlaceholder()} value={queryStrings.q} width="col-lg-8" />
                {!this.props.titleHidden &&
                  <div className="row">
                    <div className="col-12">
                      <h2 className="section-heading">{this.props.sectionTitle}</h2>
                    </div>
                  </div>
                }
              </div>
            </form>
          ) : null
        }
        {
          this.props.results.search.entries.length > 0 ? (
            <HubSearchContentTypeFactory
              contentType={this.props.paragraph2.content_type as ContentTypesParagraph}
              results={this.props.results}
              currentUrl={this.props.currentUrl}
              t={this.props.t}
            />
          ) : (
              this.props.paragraph2.content_type === ContentTypesParagraph.Petition ? 
              <EmptySearchState t={this.props.t} msg={this.props.t("content:hub.paragraph2.petition.no_available")}/>
              :
              <EmptySearchResult term={searchTerm} t={this.props.t} />
            )
        }

        {
            (this.props.results.search.entries.length > 0 && this.props.paragraph2.content_type !== ContentTypes.Alert && this.props.paragraph2.pager > 0) ? (
              <Pagination
                currentUrl={this.props.currentUrl}
                limit={this.props.results.search.limit}
                offset={this.props.results.search.offset}
                total={this.props.results.search.total}
                t={this.props.t}
              />
            ) : null
        }
      </>
    );
  }

  private getSearchPlaceholder() {
    switch (this.props.paragraph2.content_type) {
      case ContentTypesParagraph.Alert:
        return this.props.t('content:hub.paragraph2.alerts.inputPlaceholder');

      case ContentTypesParagraph.News:
        return this.props.t('content:hub.paragraph2.news.inputPlaceholder');

      case ContentTypesParagraph.ServiceRecord:
        return this.props.t('content:hub.paragraph2.services.inputPlaceholder');
      default:
        return null;
    }
  }
}
