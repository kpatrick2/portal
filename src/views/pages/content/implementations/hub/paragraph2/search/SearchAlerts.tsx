import { TranslationFunction } from 'i18next';
import * as moment from 'moment';
import * as React from 'react';
import { IAlertContent, IContentType, IContentTypeSearch } from '../../../../../../../content-api';
import { Pagination } from '../../../../../../ui';
import { SearchAlertsGroup } from './SearchAlertsGroup';
import { SearchAlertsItem } from './SearchAlertsItem';

interface ISearchAlertsProps {
  results: IContentTypeSearch;
  currentUrl: string;
  t: TranslationFunction;
}

export class SearchAlerts extends React.PureComponent<ISearchAlertsProps> {
  public render() {
    return (
      <div className="row">
        <div className="col-12">
          <ul className="date-list">

            {this.renderAlerts()}

          </ul>
          <Pagination
            currentUrl={this.props.currentUrl}
            limit={this.props.results.limit}
            offset={this.props.results.offset}
            total={this.props.results.total}
            t={this.props.t}
          />
        </div>
      </div>
      //       </div>
      //       <div className="col-12 col-lg-4 hub-para2-alert-promo">{this.renderSidePromo()}</div>
      //     </div>
      //   </div>
      // </div>
    );
  }

  private renderAlerts() {
    const days = this.getGroupResultsByDate();

    return days.map(day => [
      <SearchAlertsGroup key={day.key} date={day.key} t={this.props.t} >
      {...day.value.map((alert: IContentType) => (
        <SearchAlertsItem key={alert.dc_identifier} data={alert as IAlertContent} t={this.props.t} />
      ))}
      </SearchAlertsGroup>
    ]);
  }

  private getGroupResultsByDate() {
    const results: any[] = [];
    const alerts = this.props.results.entries;

    for (const alert of alerts) {
      let first = results.find(item => moment(item.key).isSame(alert.dc_issued, 'day'));

      if (!first) {
        first = { key: new Date(alert.dc_issued), value: [] };
        results.push(first);
      }

      first.value.push(alert);
    }

    return results;
  }
}
