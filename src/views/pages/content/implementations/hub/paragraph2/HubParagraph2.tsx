import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ContentTypesParagraph, IExtraHub, IParagraph2 } from '../../../../../../content-api';
import { LocalizedLink } from '../../../../../LocalizedLink';
import { AbstractText } from '../../../../../ui';
import { HubSearchListWithMap } from './map';
import { HubSearchList } from './search';

interface IHubParagraph2Props {
  title?: string;
  contentTitle: string;
  data: IParagraph2[];
  results: IExtraHub;
  currentUrl: string;
  t: TranslationFunction;
}

export class HubParagraph2 extends React.PureComponent<IHubParagraph2Props> {
  public render() {
    if (!this.props.data || this.props.data.length === 0) {
      return null;
    }

    const paragraph2 = this.props.data[0];

    return paragraph2.display_type === 'List+map' ? (
      <HubSearchListWithMap
        title={this.props.contentTitle}
        results={this.props.results.search}
        currentUrl={this.props.currentUrl}
        t={this.props.t}
      />
    ) : (
        <>
          <section className={`page-section ${paragraph2.content_type === 'nouvelle' || paragraph2.content_type === 'alertes' ? 'bg-gray-sky' : ''}`}>
            <div className="container">
              <HubSearchList
                sectionTitle={this.props.title}
                title={this.props.contentTitle}
                paragraph2={paragraph2}
                results={this.props.results}
                currentUrl={this.props.currentUrl}
                titleHidden={paragraph2.content_type === 'alertes'}
                t={this.props.t}
              >
              </HubSearchList>
            </div>
          </section >
          {this.petitionAbstract(paragraph2.content_type)}
        </>
      )

      ;
  }

  private petitionAbstract(contentType: string) {
    if (contentType === ContentTypesParagraph.Petition) {
      return <AbstractText text={this.props.t('content:hub.paragraph2.petition.abstract_text')} side="left">
        <div className="lead-right-content">
          <span className="title">{this.props.t('content:hub.paragraph2.petition.abstract_title')}</span>
          <p>{this.props.t('content:hub.paragraph2.petition.abstract_subtitle')}</p>
          <LocalizedLink className="btn btn-primary"
            t={this.props.t}
            title={this.props.t('content:hub.paragraph2.petition.create_my_petition')}
            to={this.props.t('content:hub.paragraph2.petition.abstract_link')} />
        </div>
      </AbstractText>
    }
  }
}
