import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { SearchBar } from '../../../../../../ui';

interface IMapSearchHeaderProps {
  term: string;
  category: string;
  t: TranslationFunction;
}

export class MapSearchHeader extends React.PureComponent<IMapSearchHeaderProps> {
  public render() {
    const categories = [
      { labelKey: 'content:hub.paragraph2.location.categories.category', value: '' },
      {
        labelKey: 'content:hub.paragraph2.location.categories.arena',
        value: this.props.t('content:hub.paragraph2.location.categories.arena')
      },
      {
        labelKey: 'content:hub.paragraph2.location.categories.library',
        value: this.props.t('content:hub.paragraph2.location.categories.library')
      },
      {
        labelKey: 'content:hub.paragraph2.location.categories.communityCentre',
        value: this.props.t('content:hub.paragraph2.location.categories.communityCentre')
      },
      {
        labelKey: 'content:hub.paragraph2.location.categories.artsCentre',
        value: this.props.t('content:hub.paragraph2.location.categories.artsCentre')
      },
      {
        labelKey: 'content:hub.paragraph2.location.categories.sportsCentre',
        value: this.props.t('content:hub.paragraph2.location.categories.sportsCentre')
      },
      {
        labelKey: 'content:hub.paragraph2.location.categories.serviceRoad',
        value: this.props.t('content:hub.paragraph2.location.categories.serviceRoad')
      },
      {
        labelKey: 'content:hub.paragraph2.location.categories.ecoCentre',
        value: this.props.t('content:hub.paragraph2.location.categories.ecoCentre')
      },
      {
        labelKey: 'content:hub.paragraph2.location.categories.ecoQuartier',
        value: this.props.t('content:hub.paragraph2.location.categories.ecoQuartier')
      },
      {
        labelKey: 'content:hub.paragraph2.location.categories.cityHall',
        value: this.props.t('content:hub.paragraph2.location.categories.cityHall')
      },
      {
        labelKey: 'content:hub.paragraph2.location.categories.boroughOffice',
        value: this.props.t('content:hub.paragraph2.location.categories.boroughOffice')
      },
      {
        labelKey: 'content:hub.paragraph2.location.categories.museum',
        value: this.props.t('content:hub.paragraph2.location.categories.museum')
      },
      {
        labelKey: 'content:hub.paragraph2.location.categories.park',
        value: this.props.t('content:hub.paragraph2.location.categories.park')
      },
      {
        labelKey: 'content:hub.paragraph2.location.categories.dogPark',
        value: this.props.t('content:hub.paragraph2.location.categories.dogPark')
      },
      {
        labelKey: 'content:hub.paragraph2.location.categories.indoorPool',
        value: this.props.t('content:hub.paragraph2.location.categories.indoorPool')
      },
      {
        labelKey: 'content:hub.paragraph2.location.categories.pointOfService',
        value: this.props.t('content:hub.paragraph2.location.categories.pointOfService')
      },
      {
        labelKey: 'content:hub.paragraph2.location.categories.animalShelter',
        value: this.props.t('content:hub.paragraph2.location.categories.animalShelter')
      }
    ];

    return (
      <form className="row" method="GET">
        <div className="col-xs-12 col-lg-8">
          <SearchBar
            placeholder={this.props.t('content:hub.paragraph2.location.inputPlaceholder')}
            value={this.props.term}
            light_border
            xs 
          />
        </div>
        <div className="col-xs-12 col-lg-4">
          <div className="form-group">
            <select className="form-control" name="category" defaultValue={this.props.category} data-auto-submit>
              {categories.map(category => (
                <option key={category.value} value={category.value}>
                  {this.props.t(category.labelKey)}
                </option>
              ))}
            </select>
          </div>
        </div>
      </form>
    );
  }
}
