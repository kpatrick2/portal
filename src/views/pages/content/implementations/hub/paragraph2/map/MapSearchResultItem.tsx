import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ILocationContent } from '../../../../../../../content-api';
import { Language } from '../../../../../../../utils';
import { LocalizedLink } from '../../../../../../LocalizedLink';

interface IMapSearchResultItemProps {
  content: ILocationContent;
  id: string;
  t: TranslationFunction;
}

export class MapSearchResultItem extends React.PureComponent<IMapSearchResultItemProps> {
  public render() {
    const data = this.props.content.mtl_content.lieux;
    const language: Language = this.props.t('common:languageCode');

    return (
      <LocalizedLink
        to={this.props.content.dc_identifier}
        className="list-group-item list-group-item-action "
        id={this.props.id}
        t={this.props.t}
      >
        <div className="list-group-item-body">
          <div className="list-group-max-width">
            <span className="list-group-item-title">{this.props.content.dc_title}</span>
            <div className="list-group-item-content ">
              <span className="">{data.category.name[language]}</span>
              <span className="text-dark separator-brand">{data.address.borough.name[language]}</span>
            </div>
          </div>
        </div>
        <div className="list-group-item-footer">
          <span className="vdm vdm-063-fleche-droite " aria-hidden="true"></span>
        </div>
      </LocalizedLink>
    );
  }
}
