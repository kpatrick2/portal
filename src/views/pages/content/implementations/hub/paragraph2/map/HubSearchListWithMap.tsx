import { Feature, FeatureCollection } from 'geojson';
import { TranslationFunction } from 'i18next';
import * as queryString from 'query-string';
import * as React from 'react';
import { ContentTypes, IContentTypeSearch } from '../../../../../../../content-api';
import { Analytics } from '../../../../../../analytics';
import { EmptySearchResult, MapBoxMap, Pagination, paginationHelpers } from '../../../../../../ui';
import { IFeatureProperties } from '../../../../../../ui/MapBoxMap';
import { MapSearchHeader } from './MapSearchHeader';
import { MapSearchResultItem } from './MapSearchResultItem';

interface IHubSearchListWithMapProps {
  title: string;
  results: IContentTypeSearch;
  currentUrl: string;
  t: TranslationFunction;
}

export class HubSearchListWithMap extends React.PureComponent<IHubSearchListWithMapProps> {
  public render() {
    const queryStrings = queryString.parse(queryString.extract(this.props.currentUrl));
    const searchTerm = queryStrings.q;
    const category = queryStrings.category;
    const geojson = this.convertMarkersToFeatureCollection(); 

    return (
      <section className="page-section pt-0">
        <div className="container-fluid">
          <Analytics.SearchResult
            title={this.props.title}
            searchTerm={searchTerm}
            numberOfResults={this.props.results.total}
            totalPages={paginationHelpers.calculateNumberOfPages(this.props.results.total, this.props.results.limit)}
            currentPage={paginationHelpers.calculateCurrentPage(this.props.results.offset, this.props.results.limit)}
            contentTypeFilter={ContentTypes.Location}
            categories={category ? [category] : []}
          />

            <div className="row">
              <div className="col-xs-12 col-lg-4">{this.renderResults(searchTerm)}</div>
              <div className="col-lg-8 order-first order-lg-2">
                <MapSearchHeader term={searchTerm} category={category} t={this.props.t} />
                <div style={{height:'800px'}}>
                  <MapBoxMap zooming={true} dragging={true} clickable={true} coordinates={geojson}/>
                </div>
              </div>
            </div>
        </div>
      </section>
    );
  }

  private convertMarkersToFeatureCollection(): FeatureCollection {
    const markers = this.props.results.entries.map(entry => ({
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: entry.dc_spatial!.coordinates,
      },
      properties:{
        id: this.getLocationId(entry.dc_identifier)
      } as IFeatureProperties
    } as Feature));

    const geojson: FeatureCollection = {
      type: 'FeatureCollection',
      features: markers
    };

    return geojson;
  }

  private renderResults(term: string) {
    if (this.props.results.entries.length === 0) {
      return <EmptySearchResult term={term} t={this.props.t} />;
    }

    const header = this.props.t('content:hub.paragraph2.location.results', { count: this.props.results.total });

    return (
      <>
        <div className="h2 list-group-heading">{header}</div>
        <div className="list-group list-group-hub">
          {this.props.results.entries.map(entry => (
            <MapSearchResultItem
              key={entry.dc_identifier}
              id={this.getLocationId(entry.dc_identifier)}
              content={entry as any}
              t={this.props.t}
            />
          ))}
        </div>
        <Pagination
          currentUrl={this.props.currentUrl}
          limit={this.props.results.limit}
          offset={this.props.results.offset}
          total={this.props.results.total}
          t={this.props.t}
        />
      </>
    );
  }

  private getLocationId(id: string) {
    return `hub-map-link-${id}`;
  }
}
