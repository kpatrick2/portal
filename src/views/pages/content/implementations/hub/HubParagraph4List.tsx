import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IParagraph4 } from '../../../../../content-api';
import { LocalizedLink } from '../../../../LocalizedLink';

interface IHubParagraph4ListProps {
  id: string;
  section: IParagraph4;
  t: TranslationFunction;
}

export class HubParagraph4List extends React.PureComponent<IHubParagraph4ListProps> {
  public render() {
    return (
      <div className="hub-list-container" id={this.props.id}>
        <h3>{this.props.section.title}</h3>
        <ul className="hub-list">
        {this.props.section.referenced_nodes!.map((reference, index) => (
          <li className="hub-list-item">
            <LocalizedLink to={reference[0]!.url} t={this.props.t}>
              {reference[0]!.title}
            </LocalizedLink>
            <p>{reference.description}</p>
          </li>
        ))}
        </ul>
      </div>
    );
  }
}
