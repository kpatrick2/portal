import * as classnames from 'classnames';
import * as i18next from 'i18next';
import * as queryString from 'query-string';
import * as React from 'react';
import { ContentTypesParagraph, IExtraHub, IHubContent, IReqParams } from '../../../../../content-api';
import { cloudinaryUrlAdapter } from '../../../../../utils/cloudinary-adapter';
import { setfilterAttr } from '../../../../../utils/url-helpers';
import { AbstractText, AlertPromo, ModalFilter } from '../../../../ui';
import { ModalFilterSelectedBubble } from '../../../../ui/modal-filter/ModalFilterSelectedBubble';
import { HubParagraph1 } from './HubParagraph1';
import { HubParagraph3 } from './HubParagraph3';
import { HubParagraph4 } from './HubParagraph4';
import { HubParagraph2 } from './paragraph2';

interface IHubContentProps {
  content: IHubContent;
  extras?: IExtraHub;
  currentUrl: string;
  t: i18next.TranslationFunction;
}

export class HubContent extends React.PureComponent<IHubContentProps> {

  public render() {
    const isAlert: boolean = this.props.content.mtl_content.collectrices.collectrice_para_2.length > 0 && this.props.content.mtl_content.collectrices.collectrice_para_2[0].content_type === ContentTypesParagraph.Alert;
    const isElected: boolean = this.props.content.mtl_content.collectrices.collectrice_para_2.length > 0 && this.props.content.mtl_content.collectrices.collectrice_para_2[0].content_type === ContentTypesParagraph.Elected;
    const isPetition: boolean = this.props.content.mtl_content.collectrices.collectrice_para_2.length > 0 && this.props.content.mtl_content.collectrices.collectrice_para_2[0].content_type === ContentTypesParagraph.Petition;

    return (
      <main className="main-content " id="main-content" aria-label="Main Content">
        {!isElected && !isPetition ? (
          <>
            <header className="content-header ">
              <div className="container-fluid container-fluid-xl">
                <div className="row">
                  <div className="col-12">
                    <div className="content-header-title">
                      <h1 className="display-1">{this.props.content.dc_title}</h1>
                    </div>
                    {(!isAlert && this.props.content.dc_description) &&
                      <div className="content-header-liner">
                        <div className="lead" dangerouslySetInnerHTML={{ __html: this.props.content.dc_description }}></div>
                      </div>
                    }

                  </div>
                </div>
              </div>
            </header>
            {this.putAlerteDesctiption(isAlert)}
          </>
        ) : (
            this.putFilterHeader(isElected, isPetition)
          )}

        <HubParagraph3
          title={this.props.content.mtl_content.collectrices.para3_title}
          references={this.props.content.mtl_content.collectrices.collectrice_para_3}
          t={this.props.t}
        />
        <HubParagraph1 references={this.props.content.mtl_content.collectrices.collectrice_para_1} t={this.props.t} />
        <HubParagraph4
          title={this.props.content.mtl_content.collectrices.para4_title}
          sections={this.props.content.mtl_content.collectrices.collectrice_para_4}
          currentUrl={this.props.currentUrl}
          t={this.props.t}
        />
        {this.props.extras &&
          <HubParagraph2
            title={this.props.content.mtl_content.collectrices.para2_title}
            contentTitle={this.props.content.dc_title}
            data={this.props.content.mtl_content.collectrices.collectrice_para_2}
            results={this.props.extras}
            currentUrl={this.props.currentUrl}
            t={this.props.t}
          />
        }
      </main>
    );
  }
  private putAlerteDesctiption(isAlerte: boolean) {
    if (isAlerte) {
      return (
        <AbstractText text={this.props.content.dc_description} side="left">
          <div className="lead-right-content">
            <AlertPromo t={this.props.t} />
          </div>
        </AbstractText>
      )
    }
    return null;
  }

  private putFilterHeader(isElected: boolean, isPetition: boolean) {
    if (isElected || isPetition) {
      const queryStrings = queryString.parse(queryString.extract(this.props.currentUrl));

      const params: IReqParams[] = setfilterAttr(queryStrings);
      const paramsMap: Map<string, any> = params.reduce((map, obj) => {
        map.set(obj.key, obj.value);
        return map;
      }, new Map());

      const style = (this.props.content.mtl_images && this.props.content.mtl_images.length > 0 && this.props.content.mtl_images[0]) ? { backgroundImage: `url(${cloudinaryUrlAdapter(this.props.content.mtl_images[0].id)})` } : undefined;
      const headerClasses = classnames({
        "content-header": true,
        "content-header-hub": true,
        "content-header-overlay": this.props.content.mtl_images && this.props.content.mtl_images.length > 0 && this.props.content.mtl_images[0]
      })

      return (
        <>
          <header className={headerClasses} style={style}>
            <div className="container-fluid container-fluid-xl container-fluid container-fluid-xl">
              <div className="row">
                <div className="col-12">
                  <h1 className="h4">{this.props.content.dc_title}</h1>
                  {(this.props.content.mtl_content.collectrices.subtitle) &&
                    <h2 className="display-1">{this.props.content.mtl_content.collectrices.subtitle}</h2>
                  }

                </div>
                <div className="col-12 col-lg-8">
                  <div className="form-group">
                    <div className="input-group-icon input-group-icon-lg input-group-icon-left">
                      <span className="vdm vdm-002-recherche " aria-hidden="true"></span>
                      <input type="text" data-query="q" className="form-control form-control-lg form-control-dark get-query" id="input-group-search" placeholder={this.placeHolder(isElected, isPetition)} defaultValue={queryStrings.q} />
                    </div>
                  </div>
                </div>
                {/* <div className="col-12">
                  <div className="quick-links">
                    <span className="quick-links-label">Recherche rapide : </span>
                    <ul className="list-inline">
                      <li className="list-inline-item"><a href="#">Maire d'arrondissement</a></li>
                      <li className="list-inline-item"><a href="#">Conseiller d'arrondissement</a></li>
                      <li className="list-inline-item"><a href="#">Conseiller de la Ville</a></li>
                    </ul>
                  </div>
                </div> */}
              </div>
            </div>
          </header>

          {/* On a enlever tout les filtres en attendant les taxonomies */}

          <div className="container">
          </div>

          {(isElected && this.props.extras) &&
          <>
            <ModalFilter params={paramsMap} taxoLabel={this.props.extras.taxoLabel} labelButton={this.props.t('content:hub.paragraph2.elected.btn_filter')} t={this.props.t}></ModalFilter>
            <ModalFilterSelectedBubble t={this.props.t} selectedParams={paramsMap} />
          </>
          }
        </>
      )
    }
    return null;
  }

  private placeHolder(isElected: boolean, isPetition: boolean) {
    if (isElected) {
      return this.props.t('content:hub.paragraph2.elected.inputPlaceHolder');
    }
    if (isPetition) {
      return this.props.t('content:hub.paragraph2.petition.inputPlaceholder');
    }
  }
}
