import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { INodeReferenceWithIcon } from '../../../../../content-api';
import { HubParagraph3Tile } from './HubParagraph3Tile';

interface IHubParagraph3Props {
  title?: string;
  references: INodeReferenceWithIcon[];
  t: TranslationFunction;
}

export class HubParagraph3 extends React.PureComponent<IHubParagraph3Props> {
  public render() {
    if (!this.props.references || this.props.references.length === 0) {
      return null;
    }

    return (
      <section className="page-section bg-gray-sky">
        <div className="container">
          <div className="row cards" data-component-type="populaire" data-component-position="haut">
            {this.props.title ? (
              <div className="col-12">
                <h2 className="section-heading">{this.props.title}</h2>
              </div>
            ) : null}
            {this.props.references.map((reference, index) => (
              <HubParagraph3Tile key={index} reference={reference} position={index} t={this.props.t} />
            ))}
          </div>
        </div>
      </section>
    );
  }
}
