import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { INodeReferenceWithIcon } from '../../../../../content-api';
import { LocalizedLink } from '../../../../LocalizedLink';

interface IHubParagraph3TileProps {
  reference: INodeReferenceWithIcon;
  position: number;
  t: TranslationFunction;
}

export class HubParagraph3Tile extends React.PureComponent<IHubParagraph3TileProps> {
  public render() {
    const references = this.props.reference.referenced_nodes || this.props.reference.external_references;
    if (!references || references.length === 0) {
      return null;
    }

    const referenceNode = references[0];
    // const iconClasses = classnames({
    //   vdm: true,
    //   [this.props.reference.icon!]: !!this.props.reference.icon,
    //   'hub-para1-noicon vdm-001-rosace': !this.props.reference.icon
    // });

    return (
      <div className="col-12 col-md-6 col-lg-4">
        <LocalizedLink className="card" to={referenceNode.url} t={this.props.t} data-link-position={(this.props.position + 1).toString()}>
            <span className={`card-icon-top vdm ${this.props.reference.icon}`} />
            <div className="card-body">
                <span className="card-title">{referenceNode.title}</span>
              </div>
        </LocalizedLink>
      </div>
    );
  }
}
