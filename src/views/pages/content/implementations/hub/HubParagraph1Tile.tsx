import * as classnames from 'classnames';
import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { INodeReferenceWithIcon } from '../../../../../content-api';
import { LocalizedLink } from '../../../../LocalizedLink';

interface IHubParagraph1TileProps {
  reference: INodeReferenceWithIcon;
  t: TranslationFunction;
}

export class HubParagraph1Tile extends React.PureComponent<IHubParagraph1TileProps> {
  public render() {
    const references = this.props.reference.referenced_nodes || this.props.reference.external_references;
    if (!references || references.length === 0) {
      return null;
    }

    const referenceNode = references[0];
    const iconClasses = classnames({
      vdm: true,
      [this.props.reference.icon!]: !!this.props.reference.icon,
      'card-icon-top': true
    });

    return (
      <div className="col-12 col-md-6">
        <LocalizedLink to={referenceNode.url} t={this.props.t} className="card card-large card-row-layout">
          <span className={iconClasses} aria-hidden="true"></span>
          <div className="card-body">
            <span className="card-title">{referenceNode.title}</span>
            <p className="card-text" dangerouslySetInnerHTML={{ __html: (this.props.reference.description) ? this.props.reference.description : "" } } ></p>
          </div>
        </LocalizedLink>
      </div>
    );
  }
}
