import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ILocationPaymentMethod } from '../../../../../content-api';

interface ILocationFeesSectionProps {
  t: TranslationFunction;
  label:string;
  payBy?: ILocationPaymentMethod[];
}

export class LocationFeesMethod extends React.PureComponent<ILocationFeesSectionProps> {
  public render() {

    if(!this.props.payBy) {
      return null;
    }

    return (
      <div className="list-group-item-column">
            <span className="list-group-item-label">{this.props.t(`${this.props.label}`)}</span>
              <ul>
                {this.props.payBy.map((method, index) => (
                  <li key="{index}">{this.props.t(`content:location.pay_by.values.${method}`)}</li>
                ))}
              </ul>
      </div>
    )

  }
}
