import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ILocationContent, IPhone } from '../../../../../content-api';
import { Phone } from '../../../../ui';

interface ILocationBookingProps {
  content: ILocationContent;
  phones: IPhone[];
  t: TranslationFunction;
}

export class LocationBooking extends React.PureComponent<ILocationBookingProps> {
  public render() {
    if (!this.props.content.mtl_content.lieux.extension.booking.possible || (this.props.content.mtl_content.lieux.extension.booking.phones.length === 0 && !this.props.content.mtl_content.lieux.extension.booking.website)) {
      return null;
    }

    return (
      <div className="list-group-item">
        <div className="list-group-item-body">
          <h2 className="list-group-item-title">{this.props.t('content:location.booking')}</h2>
          <div className="list-group-item-column">
            {this.props.phones && this.props.phones.length > 0 ? (
              <div className="row list-group-item-row">
                <div className="col-12 col-md-4">
                  <span className="list-group-item-label">{this.props.t('content:location.phone')}</span>
                </div>
                <div className="col-12 col-md-8">
                  {this.props.phones.map((contact, index) => (
                    <Phone key={index}
                      phone={contact}
                      t={this.props.t}
                    />
                  ))}
                </div>
              </div>
            ) : null}

            {this.props.content.mtl_content.lieux.extension.booking.website ? (
              <div className="row list-group-item-row">
                <div className="col-12 col-md-4">
                  <span className="list-group-item-label">{this.props.t('content:location.website')}</span>
                </div>
                <div className="col-12 col-md-8">
                  <a className="elided-text" href={this.props.content.mtl_content.lieux.extension.booking.website}>{this.props.content.mtl_content.lieux.extension.booking.website}</a>
                </div>
              </div>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}
