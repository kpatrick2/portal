import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ILocationService } from '../../../../../content-api';

interface ILocationServiceProps {
  services: ILocationService[];
  t: TranslationFunction;
}

export class LocationService extends React.PureComponent<ILocationServiceProps> {
  public render() {
    if (this.props.services.length === 0) {
      return null;
    }
    return (
        <div className="list-group-item">
          <div className="list-group-item-body">
            <h2 className="list-group-item-title">Services</h2>
            <div className="list-group-item-column">
              <ul>
                {this.props.services.map((service, index) => (
                  <li key="{index}">{this.props.t(`content:location.service_list.values.${service}`)}</li>
                ))}
              </ul>
            </div>
          </div>
        </div>
    );
  }
}
