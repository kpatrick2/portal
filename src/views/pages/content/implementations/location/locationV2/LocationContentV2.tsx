// import * as classnames from 'classnames';
import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { renderToString } from 'react-dom/server';
import { IEventContent, ITaxoCode } from '../../../../../../content-api';
import { IBlocHour, IHours, IInstallation, IIntervalHour, ILocationContentV2 } from '../../../../../../content-api/interfaces/data/location-data-v2';
import { datetimeHelpers, Language, latLongToFeatureCollection } from '../../../../../../utils';
import { DateTime } from '../../../../../DateTime';
import { LocalizedLink } from '../../../../../LocalizedLink';
import { Address, CardList, HeaderJumbotron, MapBoxMap } from '../../../../../ui';
import { IFeatureProperties } from '../../../../../ui/MapBoxMap';

interface IEventProps {
  content: ILocationContentV2;
  extras: IExtrasEvent;
  t: TranslationFunction;
}

interface IExtrasEvent {
  events: IEventContent[];
}

export class LocationContentV2 extends React.PureComponent<IEventProps> {
  public render() {
    const mapExists: boolean = this.props.content.dc_spatial ? true : false;
    return (
      <main className="main-content" id="main-content" aria-label="Main Content">
        <HeaderJumbotron title={this.props.content.dc_title}
          images={this.props.content.mtl_content.lieux.photo_gallery}
          contact_informations={this.props.content.mtl_content.lieux.contact_informations}
          borough={this.props.content.dc_coverage.boroughs[0]}
          address={this.props.content.mtl_content.lieux.address}
          maps={mapExists}
          t={this.props.t}>
        </HeaderJumbotron>
        <div className="region-content ">
          <div className="container">
            <div className="row">
              {this.renderOpeningTime(this.props.content.mtl_content.lieux.opening_hours_schedules)}
              <article className="col-12 col-lg-7 col-xl-8">
                {this.props.content.dc_abstract || this.props.content.dc_description ?
                  <div className="content-block">
                    {this.props.content.dc_abstract &&
                      <p className="lead" dangerouslySetInnerHTML={{ __html: this.props.content.dc_abstract }}></p>
                    }
                    {this.props.content.dc_description &&
                      <p dangerouslySetInnerHTML={{ __html: this.props.content.dc_description }}></p>
                    }
                  </div>
                  : null}

                {/* Affichage du bloc Commodité */}
                {this.props.content.mtl_content.lieux.amenities.length > 0 &&
                  <div className="content-block">
                    <h2 className="h5">{this.props.t("content:locationV2.ameneties_title")}</h2>
                    {this.renderCodeNameBloc(this.props.content.mtl_content.lieux.amenities)}
                  </div>
                }

                {/* Affichage du bloc Installation */}
                {this.props.content.mtl_content.lieux.installations.length > 0 &&
                  <div className="content-block">
                    <h2 className="h5">{this.props.t("content:locationV2.Installations_title")}</h2>
                    <div className="row">
                      {this.props.content.mtl_content.lieux.installations.map((installation: IInstallation) => this.renderBlocInstallation(installation))}
                    </div>
                  </div>
                }

                {/* Affichage du bloc Accessibilité */}
                {this.props.content.mtl_content.lieux.access.length > 0 &&
                  <div className="content-block">
                    <h2 className="h5">{this.props.t("content:locationV2.accessibily_title")}</h2>
                    {this.renderCodeNameBloc(this.props.content.mtl_content.lieux.access)}
                  </div>
                }

                {/* Affichage du bloc Mode de paiement */}
                {this.props.content.mtl_content.lieux.payment_mode.length > 0 &&
                  <div className="content-block">
                    <h2 className="h5">{this.props.t("content:locationV2.payment_mode_title")}</h2>
                    {this.renderCodeNameBloc(this.props.content.mtl_content.lieux.payment_mode)}
                  </div>
                }

                {/* Affichage du bloc Événement*/}
                {this.props.extras && this.props.extras.events.length > 0 &&
                  <div className="content-block">
                    <h2 className="h5">{this.props.t("content:locationV2.event_label")}</h2>
                    {this.renderBlocEvent(this.props.extras.events)}
                  </div>
                }

                {/* Affichage du bloc Thèmes associés */}
                {this.props.content.mtl_content.lieux.category.length > 0 &&
                  <div className="content-block">
                    <h2 className="h5">{this.props.t("content:locationV2.category_title")}</h2>
                    {this.renderBlocCategory(this.props.content.mtl_content.lieux.category)}
                  </div>
                }


              </article>
            </div>
            {mapExists &&
              <div className="row">
                {/* Affichage du map */}
                <div className="col-12">
                  <h2 className="h5">{this.props.t("content:locationV2.location_title")}</h2>
                  {this.renderMap()}
                </div>


              </div>
            }
          </div>
        </div>
      </main>
    )
  }

  private renderMap() {
    const address: IFeatureProperties = this.setPropertiesToFeature();
    const coordinates = latLongToFeatureCollection(this.props.content.dc_spatial.coordinates[0], this.props.content.dc_spatial.coordinates[1], address);

    return (
      <>
        <div className="map-section">
          <div id="mapLieu001" style={{ height: '500px' }} className="mapboxgl-map">
            <MapBoxMap coordinates={coordinates} zoom={15} dragging={false} zooming={false}/>
          </div>
        </div>
      </>
    );
  }

  private renderBlocEvent(events: IEventContent[]) {
    return (
      <CardList className="list-group list-group-hub mb-4">
        {events.map((event: IEventContent) => this.renderEventListItem(event))}
      </CardList>
    );
  }
  private renderEventListItem(event: IEventContent) {
    if (!event) {
      return null;
    }
    return (
      <LocalizedLink to={event.dc_identifier} className="list-group-item list-group-item-action" t={this.props.t}>

        <div className="list-group-item-body">
          <div className="list-group-max-width">
            <span className="list-group-item-title">{event.dc_title}</span>

            {event.dc_temporal &&
              <div className="list-group-item-content ">
                <span><DateTime date={event.dc_temporal.start} format="date-time-long" capitalizeFirst t={this.props.t} /></span>
              </div>
            }

          </div>
        </div>
        <div className="list-group-item-footer">
          <span className="vdm vdm-063-fleche-droite " aria-hidden="true"></span>
        </div>
      </LocalizedLink>
    );
  }
  private renderOpeningTime(hours: IHours[]) {

    if (!hours || hours.length === 0) {
      return null;
    }
    datetimeHelpers.sortSchedulebyPeriode(hours); // Trier les horaires en fonction de la saison
    return (
      <aside className="col-12 col-lg-5 col-xl-4 order-lg-last" role="complementary">
        <div className="sidebar ">
          <section className="sb-block ">
            <h2 className="sidebar-title d-none d-sm-block">{this.props.t("content:locationV2.schedule_title")}</h2>
            {hours.map((hour: IHours, index: number) => this.renderBlocDateHour(hour, index))}
            {hours.length > 1 &&
              <div className="w-100 text-center">
                <button id="moreless" className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseextrasScheduleData" aria-expanded="false" aria-controls="collapseextrasScheduleData"
                  data-show-more={this.props.t("common:show_more")} data-show-less={this.props.t("common:show_less")}>{this.props.t("common:show_more")}</button>
              </div>
            }
          </section>
        </div>
      </aside>
    );
  }
  private renderBlocDateHour(hour: IHours, index: number) {
    if (!hour || !hour.opening_hours_specification || hour.opening_hours_specification.length === 0) {
      return null;
    }
    return (
      <>
        {index === 0 ? (
          <>
            {this.renderListBlocDateHour(hour)}
          </>
        ) : (
            <div className="collapse-content collapse" id="collapseextrasScheduleData" style={undefined}>
              {this.renderListBlocDateHour(hour)}
            </div>
          )}
      </>

    );
  }

  private renderListBlocDateHour(hour: IHours) {
    return (
      <div className="list-icon">

        <div className="list-icon-item">
          <span className="vdm vdm-037-horloge " aria-hidden="true"></span>
          <div className="list-icon-content">
            <span className="list-icon-label">{hour.title}</span>
            <div className="schedule-content">
              {hour.comments &&
                <div className="schedule-description mb-2">{hour.comments}</div>
              }
              {hour.opening_hours_specification.map((blocHour: IBlocHour, index: number) => this.renderBlocHour(blocHour))}

            </div>
          </div>
        </div>
      </div>

    );
  }
  private renderBlocHour(blocHour: IBlocHour) {
    return (
      <div className="row mb-2">
        <div className="col-12 col-sm-5">
          <span className="schedule-day">{this.props.t(datetimeHelpers.getFormattedDayOfWeek(blocHour.day_of_week))}</span>
        </div>
        <div className="col-auto">
          {!blocHour.is_closed ? (
            <>
              {blocHour.hours.map((hour: IIntervalHour) => this.renderHour(hour))}
            </>
          ) : (
              <div className="schedule-data">
                {this.props.t("content:locationV2.closed")}
              </div>

            )}
        </div>
      </div>
    );
  }

  private renderHour(hour: IIntervalHour) {
    const language: Language = this.props.t('common:languageCode');
    return (
      <div className="schedule-data">
        <span >{datetimeHelpers.startOpenHours(hour, language)}</span>
        &nbsp; {this.props.t('common:toTime')} &nbsp;
        <span >{datetimeHelpers.endOpenHours(hour, language)}</span>
      </div>
    );
  }

  private renderBlocInstallation(installation: IInstallation) {
    return (
      <div className="col-12 col-md-6 col-lg-12 col-xl-6">
        <div className="content-block-icon h-100">
          <span className="content-block-icon-label">{installation.title_installation[0].name}</span>
          <ul className="mb-0">
            {installation.installation.map((element: ITaxoCode) => {
              return (
                <li>
                  {element.name}
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }

  private renderCodeNameBloc(items: ITaxoCode[]) {
    return (
      <ul className="list-unstyled row">
        {items.map((item: ITaxoCode) => {
          return (
            <li className="col-12 col-md-6">{item.name}</li>
          );
        })}
      </ul>
    );
  }

  private renderBlocCategory(category: ITaxoCode[]) {
    return (
      <ul className="list-inline badges-lg">
        {category.map((element: ITaxoCode) => {
          return (
            <li className="list-inline-item">
              <span className="badge">{element.name}</span>
            </li>
          );
        })}
      </ul>
    );
  }

  private setPropertiesToFeature(): IFeatureProperties {
    let description = '';
    if (this.props.content.mtl_content.lieux.address) {
      description = renderToString(
        <Address address={this.props.content.mtl_content.lieux.address}
          coordinates={this.props.content.dc_spatial.coordinates as number[]}
          t={this.props.t}>
        </Address>
      )
    }

    const featureProperties = { description };
    return featureProperties;
  }
}