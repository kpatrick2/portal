import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IContentType } from '../../../../../content-api';

interface ILocationDescriptionProps {
  content: IContentType;
  t: TranslationFunction;
}

export class LocationDescription extends React.PureComponent<ILocationDescriptionProps> {
  public render() {
    if (!this.props.content.dc_description) {
      return null;
    }
    return (
          <p dangerouslySetInnerHTML={{ __html: this.props.content.dc_description }}></p>
    );
  }
}
