import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ILocationContent } from '../../../../../content-api';
import { Language } from '../../../../../utils';

interface ILocationAccessProps {
  content: ILocationContent;
  t: TranslationFunction;
}

export class LocationAccess extends React.PureComponent<ILocationAccessProps> {
  public render() {
    const language: Language = this.props.t('common:languageCode');
    if (!this.props.content.mtl_content.lieux.extension.capacity && !this.props.content.mtl_content.lieux.extension.condition_admission) {
      return null;
    }
    return (
        <div className="list-group-item">
          <div className="list-group-item-body">
            <h2 className="list-group-item-title">{this.props.t('content:location.access')}</h2>
            <div className="list-group-item-column">
            {this.props.content.mtl_content.lieux.extension.capacity ? (
              <div className="row list-group-item-row">
                <div className="col-12 col-md-4">
                  <span className="list-group-item-label">{this.props.t('content:location.capacity')}</span>
                </div>
                <div className="col-12 col-md-8">
                  <span>{this.props.content.mtl_content.lieux.extension.capacity } places}</span>
                </div>
              </div>
            ) : null }
            
            {this.props.content.mtl_content.lieux.extension.condition_admission && this.props.content.mtl_content.lieux.extension.condition_admission[language] ? (
              <div className="row list-group-item-row">
                <div className="col-12 col-md-4">
                  <span className="list-group-item-label">{this.props.t('content:location.admission_condition')}</span>
                </div>
                <div className="col-12 col-md-8">
                <a className="elided-text" href={this.props.content.mtl_content.lieux.extension.condition_admission[language]}>{this.props.content.mtl_content.lieux.extension.condition_admission[language]}</a>
                </div>
              </div>
            ) : null }
            </div>
          </div>
        </div>
    );
  }
}
