import { TranslationFunction } from 'i18next';
import * as React from 'react';

interface ILocationFeesSectionProps {
  t: TranslationFunction;
  label: string;
  feesData?: string;
}

export class LocationFeesSection extends React.PureComponent<ILocationFeesSectionProps> {
  public render() {

    if(!this.props.feesData) {
      return null;
    }

    return (
      <div className="list-group-item-column">
                  <span className="list-group-item-label">{this.props.t(`${this.props.label}`)}</span>
                  <p dangerouslySetInnerHTML={{ __html: this.props.feesData }}></p>
                </div>
    )

  }
}
