import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ILocationContent } from '../../../../../content-api';
import { Language } from '../../../../../utils';
import { LocationFeesMethod } from './LocationFeesMethod';
import { LocationFeesSection } from './LocationFeesSection';

interface ILocationFeesProps {
  content: ILocationContent;
  t: TranslationFunction;
}

export class LocationFees extends React.PureComponent<ILocationFeesProps> {
  public render() {
    const language: Language = this.props.t('common:languageCode');
    if (!this.props.content.mtl_content.lieux.extension.fees && !this.props.content.mtl_content.lieux.extension.free_fees) {
      return null;
    }


    return (
      <div className="list-group-item ">
        <div className="list-group-item-body">
          <h2 className="list-group-item-title">
            {this.props.t('content:location.fees')}
          </h2>

          {this.props.content.mtl_content.lieux.extension.free_fees ? (
            <div className="list-group-item-column">
              <span className="list-group-item-label">{this.props.t('content:location.free')}</span>
            </div>
          ) : null}

          {this.props.content.mtl_content.lieux.extension.fees && this.props.content.mtl_content.lieux.extension.fees.price ? (
            <LocationFeesSection feesData={this.props.content.mtl_content.lieux.extension.fees.price[language]} t={this.props.t} label='content:location.price' />
          ) : null}

          {this.props.content.mtl_content.lieux.extension.fees && this.props.content.mtl_content.lieux.extension.fees.description ? (
            <LocationFeesSection feesData={this.props.content.mtl_content.lieux.extension.fees.description[language]} t={this.props.t} label='content:location.fees_description' />
          ) : null}

          {this.props.content.mtl_content.lieux.extension.fees && this.props.content.mtl_content.lieux.extension.fees.group ? (
            <LocationFeesSection feesData={this.props.content.mtl_content.lieux.extension.fees.group[language]} t={this.props.t} label='content:location.group_fees_description' />
          ) : null}

          {this.props.content.mtl_content.lieux.extension.fees && this.props.content.mtl_content.lieux.extension.fees.payBy.length > 0 ? (
            <LocationFeesMethod payBy={this.props.content.mtl_content.lieux.extension.fees.payBy} t={this.props.t} label='content:location.pay_by.label' />
          ) : null}

          {this.props.content.mtl_content.lieux.extension.fees && this.props.content.mtl_content.lieux.extension.fees.website && this.props.content.mtl_content.lieux.extension.fees.website[language] ? (
            <div className="list-group-item-column">
              <span className="list-group-item-label">{this.props.t('content:location.price_website')}</span>
              <a className="elided-text" href={this.props.content.mtl_content.lieux.extension.fees.website[language]}>{this.props.content.mtl_content.lieux.extension.fees.website[language]}</a>
            </div>

          ) : null}
        </div>
      </div>
    );
  }

}
