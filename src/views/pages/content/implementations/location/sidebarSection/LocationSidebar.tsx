import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ILocationContent } from '../../../../../../content-api';
import { SideBar, SideBarSection } from '../../../../../ui';
import { SidebarSectionAddress } from './SidebarSectionAddress';
import { SidebarSectionContact } from './SidebarSectionContact';
import { SidebarSectionSchedule } from './SidebarSectionSchedule';
import { SocialMedias } from './SidebarSectionSocialMedias';

interface ILocationSidebarProps {
  content: ILocationContent;
  t: TranslationFunction;
}

export class LocationSidebar extends React.PureComponent<ILocationSidebarProps> {
  public render() {
    return (
      <SideBar stroke>
        <section className="sb-block">
          <ul className="list-icon">
            <SideBarSection title={this.props.t("common:informations")}>
              <SidebarSectionSchedule
                openHours={this.props.content.mtl_content.lieux.extension.openHours}
                t={this.props.t}
              />
              <SidebarSectionAddress address={this.props.content.mtl_content.lieux.address} t={this.props.t} />
              <SidebarSectionContact
                content={this.props.content}
                phones={this.props.content.mtl_content.lieux.extension.phones}
                t={this.props.t}
              />
            </SideBarSection>
          </ul>
          {this.props.content.mtl_content.lieux.extension.social_medias ? (
            <SocialMedias content={this.props.content} t={this.props.t} />
          ) : null}
        </section>
      </SideBar>
    );
  }
}
