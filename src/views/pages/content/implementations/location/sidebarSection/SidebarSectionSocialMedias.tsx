import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ILocationContent } from '../../../../../../content-api';

interface ISocialMediasProps {
  content: ILocationContent;
  t: TranslationFunction;
}

export class SocialMedias extends React.PureComponent<ISocialMediasProps> {
  public render() {

    if (!this.props.content.mtl_content.lieux.extension.social_medias) {
      return null;
    }

    return (
      <ul className="social-media-list social-media-list-icons ">
        {this.props.content.mtl_content.lieux.extension.social_medias.facebook ? (
          <li>
            <a href={this.props.content.mtl_content.lieux.extension.social_medias.facebook}>
              <span className="vdm vdm-101-social-facebook"></span>
              {/* <span className="sr-only">{this.props.t('common:followUsOn')}</span> Facebook */}
              </a>
          </li>
        ) : null}
        {this.props.content.mtl_content.lieux.extension.social_medias.twitter ? (
          <li>
            <a href={this.props.content.mtl_content.lieux.extension.social_medias.twitter}>
              <span className="vdm vdm-099-social-twitter"></span>
              {/* <span className="sr-only">{this.props.t('common:followUsOn')}</span> Twitter */}
              </a>
          </li>
        ) : null}
        {this.props.content.mtl_content.lieux.extension.social_medias.youtube ? (
          <li>
            <a href={this.props.content.mtl_content.lieux.extension.social_medias.youtube}>
              <span className="vdm vdm-100-social-youtube"></span>
              {/* <span className="sr-only">{this.props.t('common:followUsOn')}</span> YouTube */}
              </a>
          </li>
        ) : null}
      </ul>
    );
  }
}
