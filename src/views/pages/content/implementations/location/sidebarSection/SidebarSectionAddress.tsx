import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ILocationAddress } from '../../../../../../content-api';
import { SideBarSection } from '../../../../../ui';
import { SideBarItemLi } from '../../../../../ui/sidebar/SideBarItemLi';

interface ISidebarSectionAddressProps {
  address: ILocationAddress;
  t: TranslationFunction;
}

export class SidebarSectionAddress extends React.PureComponent<ISidebarSectionAddressProps> {
  public render() {
    if (!this.props.address) {
      return null;
    }

    return (
      <SideBarSection>
        <SideBarItemLi icon="004-drapeau"  label={this.props.t('content:location.address')}>
          <address>
            {this.props.address.civicNumber}
            {this.props.address.civicNumber && this.props.address.street ? ( ", " ) : null}
            {this.props.address.street}
            {this.props.address.civicNumber || this.props.address.street ? (<br />) : null}
            {this.props.address.extra} {this.props.address.extra ? ( <br /> ) : null}
            {this.props.address.city} {this.props.address.province ? (`(${this.props.address.province})`) : null} {this.props.address.zipCode}
          </address>
        </SideBarItemLi>
      </SideBarSection>
    );
  }
}
