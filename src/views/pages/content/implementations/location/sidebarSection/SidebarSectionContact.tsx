import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ILocationContent, IPhone } from '../../../../../../content-api';
import { SideBarSection } from '../../../../../ui';
import { PhoneLabel } from '../../../../../ui/PhoneLabel';
import { SideBarItemLi } from '../../../../../ui/sidebar/SideBarItemLi';

interface ISidebarSectionContactProps {
  content: ILocationContent;
  phones: IPhone[];
  t: TranslationFunction;
}

export class SidebarSectionContact extends React.PureComponent<ISidebarSectionContactProps> {
  public render() {
    if (!this.props.phones.length && !this.props.content.mtl_content.lieux.extension.website && !this.props.content.mtl_content.lieux.extension.email) {
      return null;
    }

    return (
      <SideBarSection>
        <SideBarItemLi icon="012-telephone" label={this.props.t('content:location.contact')}>
          {this.props.phones.map((contact, index) => (
            <PhoneLabel
              phone={contact}
              t={this.props.t}
            />
          ))}
          {this.props.content.mtl_content.lieux.extension.website ? (
            <a className="elided-text" href={this.props.content.mtl_content.lieux.extension.website}>{this.props.t("common:more_informations")}</a>
          ) : null}
          {this.props.content.mtl_content.lieux.extension.email ? (
            <a className="elided-text" href={`mailto:${this.props.content.mtl_content.lieux.extension.email}`}>{this.props.t("common:write_email")}</a>
          ) : null}

        </SideBarItemLi>
      </SideBarSection>
    );
  }
}
