import { TranslationFunction } from 'i18next';
import * as _ from 'lodash';
import * as React from 'react';
import { ILocationOpenHours, ILocationOpenHoursSchedulesData } from '../../../../../../content-api';
import { Language } from '../../../../../../utils';
import { DateTime } from '../../../../../DateTime';
import { SideBarSection } from '../../../../../ui';
import { SideBarItemLi } from '../../../../../ui/sidebar/SideBarItemLi';

interface ISidebarSectionScheduleProps {
  openHours?: ILocationOpenHours;
  t: TranslationFunction;
}

export class SidebarSectionSchedule extends React.PureComponent<ISidebarSectionScheduleProps> {
  
  public render() {
    if (!this.props.openHours || !this.props.openHours.schedules || this.props.openHours.schedules.length === 0) {
      return null;
    }

    const appropriateSchedule = this.props.openHours.schedules.find(this.isScheduleApplicable);

    if (!appropriateSchedule) {
      return null;
    }

    return (
      <SideBarSection>
        <SideBarItemLi icon="037-horloge" label={this.props.t('content:location.horaires')}>
          {this.renderSchedules(appropriateSchedule)}
        </SideBarItemLi>
      </SideBarSection>
    );
  }

  private renderSchedules(appropriateSchedule: ILocationOpenHoursSchedulesData) {

    const language: Language = this.props.t('common:languageCode');
    
    const schedules = appropriateSchedule.weeklySchedules;
    if (!schedules) {
      return null;
    }
    const groupedByDay = _.groupBy(schedules, 'dayOfWeek');

    return Object.keys(groupedByDay)
      .map((dayOfWeek, index) => {
        const schedulesOfTheDay = groupedByDay[dayOfWeek];        
        return (
          <div key={index} className="schedule-content">
          
            {schedulesOfTheDay.map((scheduleOfTheDay, indexSchedules) => (
              <div className="row" key={indexSchedules}>
                <div className="col-3">
                  {indexSchedules === 0 ? (
                    <span className="schedule-day">{this.getFormattedDayOfWeek(dayOfWeek)}</span>
                  ) : null}
                </div>
                <div className="col-9">
                  <div className="schedule-data"> 
                    <span className="schedule-start-time">
                      <DateTime date={scheduleOfTheDay.startTime} format="time" t={this.props.t} />
                    </span>
                    <span className="schedule-end-time">
                      <DateTime date={scheduleOfTheDay.endTime} format="time" t={this.props.t} />
                    </span>
                    {scheduleOfTheDay.note && scheduleOfTheDay.note[language] ? (
                      <span className="vdm vdm-069-mini-plus-circulaire" data-toggle="collapse" data-target={`#collapse-${index}-${indexSchedules}`} aria-expanded="false" aria-controls={`collapse-${index}-${indexSchedules}`}></span>
                    ) : null}
                  </div>
                </div>
                {scheduleOfTheDay.note && scheduleOfTheDay.note[language] ? (
                  <div className="col-12 collapse show" id={`collapse-${index}-${indexSchedules}`}>
                    <small className="d-block note">{scheduleOfTheDay.note[language]}</small>
                  </div>
                ) : null}
              </div>
            ))}
          </div>
        )
      });
  }

  private isScheduleApplicable(schedule: ILocationOpenHoursSchedulesData): boolean {
    const now = new Date();

    const matchesStartDate = !schedule.startDate || now >= new Date(schedule.startDate);
    const matchesEndDate = !schedule.endDate || now <= new Date(schedule.endDate);

    return matchesStartDate && matchesEndDate;
  }

  private getFormattedDayOfWeek(dayOfWeek: string) {
    const key = this.mapToTranslationKey(dayOfWeek);
      return `${this.props.t(key).substring(0,3)}.`;
  }
  
  private mapToTranslationKey(day: string) {
    // tslint:disable-next-line:switch-default
    switch (day) {
      case "0":
        return 'common:days.monday';
      case "1":
        return 'common:days.tuesday';
      case "2":
        return 'common:days.wednesday';
      case "3":
        return 'common:days.thursday';
      case "4":
        return 'common:days.friday';
      case "5":
        return 'common:days.saturday';
      case "6":
        return 'common:days.sunday';
      default:
        return '';
    }
  }
}
