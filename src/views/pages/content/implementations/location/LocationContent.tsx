import { Feature, FeatureCollection } from 'geojson';
import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { renderToString } from 'react-dom/server';
import { ILocationAddress, ILocationContent } from '../../../../../content-api';
import { Language } from '../../../../../utils';
import { LocalizedLink } from '../../../../LocalizedLink';
import { MapBoxMap } from '../../../../ui';
import { IFeatureProperties } from '../../../../ui/MapBoxMap';
import { LocationAccess } from './LocationAccess';
import { LocationAccessibility } from './LocationAccessibility';
import { LocationBooking } from './LocationBooking';
import { LocationDescription } from './LocationDescription';
import { LocationFees } from './LocationFees';
import { LocationService } from './LocationService';
import { LocationSidebar } from './sidebarSection';

interface ILocationContentProps {
  content: ILocationContent;
  t: TranslationFunction;
}

export class LocationContent extends React.PureComponent<ILocationContentProps> {
  public render() {
    const language: Language = this.props.t('common:languageCode');
    const badgeLink: string = this.props.t('link:allLocationUrl') +'?category=' + this.props.content.mtl_content.lieux.category.name[language];
    
    return (
      <main className="main-content " id="main-content" aria-label="Main Content">
        <header className="content-header bg-gray-sky">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <div className="content-header-title">
                  <h1 className="display-2">{this.props.content.mtl_content.lieux.locationName[language]}</h1>
                </div>
              </div>
              <div className="col order-1">
                <div className="content-header-metadata metadata-infos">
                  <div className="infos-event ">
                    <span className="infos-date">
                      {this.props.content.mtl_content.lieux.address.civicNumber},{' '}
                      {this.props.content.mtl_content.lieux.address.street}
                    </span>
                    <span className="infos-lieu">
                      {this.props.content.mtl_content.lieux.address.borough.name[language]}
                    </span>
                  </div>
                  <LocalizedLink to={badgeLink} title={this.props.content.mtl_content.lieux.category.name[language]} t={this.props.t} className="badge" />
                </div>
              </div>
              <div className="col-12 col-xl-auto order-first order-xl-last">
                <div className="hub-link">
                  <LocalizedLink to={this.props.t('link:allLocationUrl')} title={this.props.t('content:location.all_location')} t={this.props.t} />
                </div>
              </div>
            </div>
          </div>
        </header>
        <div className="region-content">
          <div className="container">
            <div className="row">
              <article className="col-12 col-lg-7 col-xl-8">
                <LocationDescription content={this.props.content} t={this.props.t} />
                <div className="list-group list-group-content list-group-flush-bottom">
                  <LocationService
                    services={this.props.content.mtl_content.lieux.extension.services}
                    t={this.props.t}
                  />

                  <LocationAccessibility content={this.props.content} t={this.props.t} />

                  <LocationAccess content={this.props.content} t={this.props.t} />

                  <LocationBooking
                    content={this.props.content}
                    phones={this.props.content.mtl_content.lieux.extension.booking.phones}
                    t={this.props.t}
                  />

                  <LocationFees content={this.props.content} t={this.props.t} />
                </div>
              </article>

              <LocationSidebar content={this.props.content} t={this.props.t} />
            </div>
          </div>
        </div>
        {this.addMap()}
      </main>
    );
  }

  private addMap() {
    if (!this.props.content.dc_spatial) {
      return null;
    }

    const coordinates = [this.props.content.dc_spatial.coordinates[0], this.props.content.dc_spatial.coordinates[1]];
    const leafletZoom = 15;

    const marker = {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates
      },
      properties: this.generateMessagePopUp()
    };

    const geojson: FeatureCollection = {
      type: 'FeatureCollection',
      features: [marker as Feature]
    };

    return (
      <section className="map-section">
        <div className="mapboxgl-map" style={{ height: '500px' }}>
          <MapBoxMap coordinates={geojson} zoom={leafletZoom} />
        </div>
      </section>
    );
  }

  private generateMessagePopUp(): IFeatureProperties {
    let description = '';
    if (this.props.content.mtl_content.lieux.address) {
      const address = this.props.content.mtl_content.lieux.address;
      description = renderToString(this.formatAddress(address));
    }

    const featureProperties = { description };
    return featureProperties;
  }

  private formatAddress(address: ILocationAddress) {
    const content = [];

    if (address.civicNumber) {
      content.push(address.civicNumber + ', ');
    }

    if (address.street) {
      content.push(address.street);
    }

    if (address.street || address.civicNumber) {
      content.push(<br />);
    }

    if (address.extra) {
      content.push(address.extra);
      content.push(<br />);
    }

    if (address.city) {
      content.push(address.city);
      content.push(' ');
    }

    if (address.province) {
      content.push('(' + address.province + ')');
      content.push(' ');
    }

    if (address.zipCode) {
      content.push(address.zipCode);
    }

    return <address>{content}</address>;
  }
}
