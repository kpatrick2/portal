import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ILocationContent } from '../../../../../content-api';

interface ILocationAccessibilityProps {
  content: ILocationContent;
  t: TranslationFunction;
}

export class LocationAccessibility extends React.PureComponent<ILocationAccessibilityProps> {
  public render() {
    if (!this.props.content.mtl_content.lieux.extension.universal_accessibility.services && !this.props.content.mtl_content.lieux.extension.universal_accessibility.location) {
      return null;
    }
    return (
        <div className="list-group-item">
          <div className="list-group-item-body">
            <h2 className="list-group-item-title">{this.props.t('content:location.universalAccessibility.title')}</h2>
            <div className="list-group-item-column">
            {this.props.content.mtl_content.lieux.extension.universal_accessibility.location ? (
              <div className="row list-group-item-row">
                <div className="col-12 col-md-4">
                  <span className="list-group-item-label">{this.props.t('content:location.universalAccessibility.locationAccessible')}</span>
                </div>
                <div className="col-12 col-md-8">
                  <span>{this.props.t('content:location.universalAccessibility.locationAccessible')}</span>
                </div>
              </div>
            ) : null }
            {this.props.content.mtl_content.lieux.extension.universal_accessibility.services ? (
              <div className="row list-group-item-row">
                <div className="col-12 col-md-4">
                  <span className="list-group-item-label">{this.props.t('content:location.universalAccessibility.servicesAccessible')}</span>
                </div>
                <div className="col-12 col-md-8">
                  <span>{this.props.t('content:location.universalAccessibility.servicesAccessible')}</span>
                </div>
              </div>
            ) : null }
            </div>
          </div>
        </div>
    );
  }

}
