import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IAppelAction, ITopicRatesPart, ITopicsBodyPart, ITopicsContent, topicBodyPartGuards, topicRatePartGuards } from '../../../../../content-api';
import { IDemarchesContent } from '../../../../../content-api/interfaces/data/demarches-data';
import { LocalizedLink } from '../../../../LocalizedLink';
import { SimpleVideoPlayerV2 } from '../../../../SimpleVideoPlayerV2';
import { Table } from '../../../../Table';
import { AbstractText, Contact, Header, ListDocuments, PageSection, SocialMedia } from '../../../../ui';
import { CardList } from '../../../../ui/cards/CardList';
import { CardV2 } from '../../../../ui/cards/CardV2';
import { SingleImage } from '../../../../ui/images/SingleImage';
import { BoroughRate } from '../../../../ui/rate/BoroughRate';
import { GlobalRates } from '../../../../ui/rate/GlobalRates';
interface ITopicContentProps {
  content: ITopicsContent;
  extras: IDemarchesContent[];
  t: TranslationFunction;
/*

Cette maquette devra être complétée au fur et à mesure que les données provenant de l'API
du type de contenu <Instances> et les analyses incluant maquettes seront disponibles.
SG. 12-09-2018
*/}

export class TopicContent extends React.PureComponent<ITopicContentProps> {
  private rates: any;

  public render() {

    return (
      <main className="main-content" id="main-content" aria-label="Main Content">
        <Header
          title={this.props.content.dc_title}
          date={this.props.content.dc_modified}
          t={this.props.t} />

        <AbstractText text={this.props.content.dc_abstract} />

        <div className="region-content ">
          <article className="section-content-fluid">
            {this.props.content.mtl_content.sujets.body && this.props.content.mtl_content.sujets.body.map(this.renderBodyTopic)}
            <ListDocuments documents={this.props.content.mtl_content.sujets.by_laws}
              t={this.props.t}
              title={this.props.t('common:rules')}></ListDocuments>
            {this.props.content.mtl_content.sujets.call_to_action && this.props.content.mtl_content.sujets.call_to_action.length > 0 ?
              <div className="section-inner section-inner-md">
                {this.props.content.mtl_content.sujets.call_to_action.map((action) => this.renderBtnCallAction(action, this.props.t))}
              </div>
              : null}

            {this.props.content.mtl_content.sujets.social_medias && this.props.content.mtl_content.sujets.social_medias.length > 0 ? (
              <div className="section-inner section-inner-md section-divider">
                <h2 className="section-subheading">{this.props.t('topic:title_social_media',{ sujet: this.props.content.dc_title })} </h2>
                <SocialMedia media={this.props.content.mtl_content.sujets.social_medias[0].data} />
              </div>

            ) : null

            }




            {this.props.content.mtl_content.sujets.contacts_information && this.props.content.mtl_content.sujets.contacts_information.length > 0 ? (
              <div className="section-inner section-inner-md section-divider">
                <h2>{this.props.t('topic:contact_title')}</h2>
                {this.props.content.mtl_content.sujets.contacts_information.map((contact) => {
                  return <Contact data={contact} />
                })}
              </div>
            ) : null

            }


          </article>
        </div>
        {this.props.content.mtl_content.sujets.rates && this.getRates() &&
          <PageSection title={this.props.t('topic:rate_title')} dark>
            {this.rates}
          </PageSection>
        }

        {this.props.extras && this.props.extras.length > 0 ? (
          <PageSection title={this.props.t("common:additional_content")} dark>
            <CardList>
              {this.props.extras.map((entry: IDemarchesContent) => {
                return (
                  <div className="col-12 col-md-6">
                    <CardV2 tag={this.props.t("content:demarches.title")}
                      title={entry.dc_title}
                      dc_identifier={entry.dc_identifier}
                      footerText={this.props.t("common:read_more")}
                      t={this.props.t}
                      type="large" />
                  </div>
                )
              })}
            </CardList>
          </PageSection>

        ) : null

        }
      </main>
    );
  }

  private getRates() {
    this.rates = this.props.content.mtl_content.sujets.rates!.map(
      (rate: ITopicRatesPart) => this.renderRateTopic(this.props.t, rate)
    );

    return !this.rates.every((el: any) => el === null);
  }

  private renderRateTopic(t: TranslationFunction, rate?: ITopicRatesPart) {
    if (rate) {
      if (topicRatePartGuards.isGlobal(rate)) {
        if (rate.global_rate.title) {
          return <GlobalRates t={t} data={rate.global_rate} />
        }
      }
      if (topicRatePartGuards.isBorough(rate)) {
        if (rate.borough_rate.title) {
          return <BoroughRate t={this.props.t} data={rate.borough_rate} />
        }
      }
    }
    return null;
  }
  private renderBodyTopic(body?: ITopicsBodyPart) {
    if (body) {
      if (topicBodyPartGuards.isText(body)) {
        return <div className="section-inner section-inner-md" dangerouslySetInnerHTML={{ __html: body.text }} />;
      }
      if (topicBodyPartGuards.isTitle(body)) {
        return (<div className="section-inner section-inner-md"><h2> {body.title} </h2></div>);
      }
      if (topicBodyPartGuards.isImage(body)) {
        return (
          <div className="section-inner section-inner-lg section-inner-media">
            <div className="media media-image">
              <SingleImage images={body.image} className="img-fluid" />
            </div>
          </div>
        );
      }

      if (topicBodyPartGuards.isVideo(body)) {
        return (
          <div className="section-inner section-inner-lg section-inner-media">
            <SimpleVideoPlayerV2 video={body.video} className="media embed-responsive embed-responsive-16by9" />
          </div>
        );
      }
      if (topicBodyPartGuards.isTable(body)) {
        return (

          <div className="section-inner section-inner-md">
            <Table tables={body.table} />
          </div>
        );
      }

    }
    return null;
  }
  private renderBtnCallAction(action: IAppelAction, t: TranslationFunction) {

    if (action) {

      return (
        <p>
          <LocalizedLink to={action.url} className="btn btn-primary" t={t}>{action.title}</LocalizedLink>
        </p>
      );
    }
    return null;
  }


}  