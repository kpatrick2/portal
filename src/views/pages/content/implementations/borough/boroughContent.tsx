import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IBoroughContent, IElectedContent, IEventContent, IFeaturedContent, IImportantDateContent, INewsContent, INodeReference, INodeReferenceWithIconV2 } from '../../../../../content-api';
import { LocalizedLink } from '../../../../LocalizedLink';
import { CardElu, CardIcon, CardImportantDate, CardList, CardV2, Header, PageSection, SearchSection } from '../../../../ui';
import { MessageBanner } from '../../../../ui/MessageBanner';

interface IBoroughContentProps {
  content: IBoroughContent;
  currentUrl: string;
  extras: IExtraBorough;
  t: TranslationFunction;
}
interface IExtraBorough {
  importantDates: IImportantDateContent[];
  eventsBorough: IEventContent[];
  newsBorough: INewsContent[];
  eluBorough: IElectedContent;
  eventsSeance: IEventContent[];
}

export class BoroughContent extends React.PureComponent<IBoroughContentProps> {
  public render() {

    // Define content of various elements
    const featuredCards: IFeaturedContent[] | undefined = this.props.content.mtl_content.vitrine_arrondissement.featured_content;
    const references: INodeReferenceWithIconV2[] = this.props.content.mtl_content.vitrine_arrondissement.need_help;
    const eluBorough: IElectedContent | undefined = this.props.extras.eluBorough;
    const instanceBorough: INodeReference = this.props.content.mtl_content.vitrine_arrondissement.democratic_participation.city_government;
    const newsBorough: INewsContent[] | undefined = this.props.extras.newsBorough;
    const eventsBorough: IEventContent[] | undefined = this.props.extras.eventsBorough;
    const eventsSeance: IEventContent[] | undefined = this.props.extras.eventsSeance;
    const importantDates: IImportantDateContent[] = this.props.extras.importantDates;

    const displayEventsBorough = eventsBorough && eventsBorough.length > 0;
    const displayNewsBorough = newsBorough && newsBorough.length > 0;
    const displayEluBorough = eluBorough && instanceBorough;
    const displaySeancesBorough = eventsSeance && eventsSeance.length > 0;
    const displayNeedHelp = references && references.length > 0;
    const displayImportantDates = importantDates && importantDates.length > 0;

    return (
      <main className="main-content " id="main-content" aria-label="Main Content">

        <Header
          title={this.props.content.dc_title}
          t={this.props.t}
          subtitle={this.props.t("common:content.borough")}
          fluid
        />

        {this.renderMessageBanner(this.props.content.mtl_content.vitrine_arrondissement.message)}

        {this.renderFeaturedCardsSection(featuredCards)}

        {displayNeedHelp &&
          <PageSection title={this.props.t("content:vitrine_arrondissement.need_help_title")} dark>
            <CardList>
              {references.map((reference, index) => (
                <CardIcon key={index} reference={reference} position={index} t={this.props.t} />
              ))}
            </CardList>
          </PageSection>
        }

        {displayImportantDates &&
          <PageSection title={this.props.t("content:vitrine_arrondissement.important_dates_title")} title_align>
            {this.renderImportantDates(importantDates)}
          </PageSection>
        }

        <SearchSection search={this.props.content.mtl_content.vitrine_arrondissement.search} currentUrl={this.props.currentUrl} t={this.props.t} />

        {displayNewsBorough &&
          <PageSection title={this.props.t("content:vitrine_arrondissement.news_title")} title_align>
            <CardList>
              {newsBorough.map(news => (
                <div className="col-12" key={news.dc_provenance.identifier}>
                  <CardV2 
                    title={news.dc_title}
                    dc_identifier={news.dc_identifier}
                    images={news.mtl_images}
                    imageFirst
                    date={news.dc_issued}
                    tc={news.dc_type.machine_name}
                    t={this.props.t}
                    type="row"
                    bg_variant
                  />
                </div>
              ))}
            </CardList>
            <div className="pagination-wrapper">
              <LocalizedLink
                to={this.props.t('link:allNewsUrl')}
                className="btn btn-secondary btn-sm"
                title={this.props.t('content:news.allNews')}
                t={this.props.t}
              />
            </div>
          </PageSection>
        }

        {displayEventsBorough &&
          <PageSection title={this.props.t("content:vitrine_arrondissement.event_title")} title_align>
            <CardList>
              {eventsBorough.map(event => (
                <div className="col-12 col-lg-4" key={event.dc_provenance.identifier}>
                  <CardV2 
                    card_type={event.mtl_content.evenements.event_type}
                    title={event.dc_title}
                    dc_identifier={event.dc_identifier}
                    images={event.mtl_images}
                    temporal={event.dc_temporal}
                    tc={event.dc_type.machine_name}
                    t={this.props.t}
                    type="teaser"
                    bg_variant
                  />
                </div>
              ))}
            </CardList>
          </PageSection>
        }

        {displayEluBorough &&
          <PageSection title={this.props.t("content:vitrine_arrondissement.borough_office")} dark>
            {this.renderEluBorough(instanceBorough, eluBorough)}
          </PageSection>
        }

        {displaySeancesBorough &&
          <PageSection title={this.props.t("content:vitrine_arrondissement.seances_title")} dark>
            <CardList>
              {eventsSeance.map(event => (
                <div className="col-12 col-lg-4" key={event.dc_provenance.identifier}>
                  <CardV2
                    title={event.dc_title}
                    dc_identifier={event.dc_identifier}
                    temporal={event.dc_temporal}
                    t={this.props.t}
                    type="teaser" 
                  />
                </div>
              ))}
            </CardList>

          </PageSection>
        }
      </main>
    );
  }

  private renderMessageBanner(boroughMessage:any) {
    if(boroughMessage) {
      return (
        <MessageBanner
          classname="alert-info-dark"
          title={boroughMessage.title}
          text={boroughMessage.text}
          linkUrl={boroughMessage.call_to_action.url}
          linkLabel={boroughMessage.call_to_action.title}
          linkClass="alert-link text-nowrap ml-2"
          t={this.props.t}
        />
      )
    }
  }

  private renderFeaturedCardsSection(featuredCards: IFeaturedContent[] | undefined) {

    if (featuredCards && featuredCards.length > 0) {

      const numbCards = featuredCards.length;
      const cardsData = featuredCards.map((result, index) => {
        return this.renderFeaturedCard(numbCards, result, index);      
      })

      return (
        <PageSection title={this.props.t("content:vitrine_arrondissement.featured_title")}>
          <CardList>
            {cardsData}
          </CardList>
        </PageSection>
      )
    }
  }

  private renderFeaturedCard(numbCards: any, result: any, index:number) {
    // Default layout is large col-md-6
    let gridClass: string = "col-12 col-md-6";
    let type: "featured" | "large" = "large";
    let footerText: string | undefined;
    let text: string | undefined;

    // Handle null call to action url
    // Remove once fixed in api
    const url: string = (result.call_to_action && result.call_to_action.url)  ? result.call_to_action.url : "#";

    if (numbCards !== 2 && index === 0) {
      // 1 or 3 cards, First is alway featured with footer and texte
      // First card always Featured card
      gridClass = "col-12";
      type = "featured";
      footerText = result.call_to_action.title;
      text = result.text;
    }

    return (
      <div className={gridClass} key={index}>
        <CardV2
          title={result.title}
          text={text}
          footerText={footerText}
          dc_identifier={url}
          images={[result.picture]}
          t={this.props.t}
          type={type}
          bg_variant
        />
      </div>
    )
  }

  private renderEluBorough(instanceBorough: INodeReference, eluBorough: IElectedContent) {

    const instanceUrl: string = `${instanceBorough.url}#members`;

    return (
      <>
        <CardList>
          <CardElu elu={this.props.extras.eluBorough} t={this.props.t} />
        </CardList>
        <div className="pagination-wrapper">
          <LocalizedLink
            to={instanceUrl}
            title={this.props.t("content:elu.all_elected")}
            t={this.props.t} className="btn btn-secondary btn-sm"
          />
        </div>

      </>
    )
  }

  private renderImportantDates(importantDates: IImportantDateContent[]) {

    const importantDatesCardsExtra: any = [];
    const importantDatesCardsData: any = [];

    if(!importantDates) {
      return null;
    }

    importantDates.map((result: IImportantDateContent, index:number) => {
      if(index < 3 ) {
        importantDatesCardsData.push(this.renderImportantDatesCards(result, index));
      }  else {
        importantDatesCardsExtra.push(this.renderImportantDatesCards(result, index));
      }   
    })

    return (
      <>
        <CardList>
          {importantDatesCardsData}
        </CardList>
        {importantDatesCardsExtra && importantDatesCardsExtra.length > 0 &&
          <>
            <div id="collapseDatesImportantesSection" className="row cards collapse-content collapse">
              {importantDatesCardsExtra}
            </div>
            <div className="pagination-wrapper text-center">
              <button 
                id="moreless"
                className="btn btn-link btn-collapse collapsed" 
                type="button" 
                data-toggle="collapse" 
                data-target="#collapseDatesImportantesSection" 
                aria-expanded="false" 
                aria-controls="collapseDatesImportantesSection"
                data-show-more={this.props.t("common:show_more")}
                data-show-less={this.props.t("common:show_less")}
              >
                {this.props.t("common:show_more")}
              </button>
            </div>
          </>
        }
      </>
    )
  }

  private renderImportantDatesCards(result:IImportantDateContent, index:number) {

    return (
      <div className="col-12 col-lg-4" key={index}>
        <CardImportantDate title={result.dc_abstract} linkLabel={result.mtl_content.dates_importantes.call_to_action.title} linkUrl={result.mtl_content.dates_importantes.call_to_action.url} t={this.props.t} timeSpan={result.mtl_content.dates_importantes.timespan} />
      </div>
    )
  }
}
