import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IServiceRecordBoroughBloc } from '../../../../../content-api';
import { LocalizedLink } from '../../../../LocalizedLink';

interface IServiceRecordAsideProps {
  bloc?: IServiceRecordBoroughBloc;
  t: TranslationFunction;
}

export class ServiceRecordAside extends React.PureComponent<IServiceRecordAsideProps> {
  public render() {
    if (!this.props.bloc) {
      return null;
    }

    const hasLawSettlements = this.props.bloc.law_settlements && this.props.bloc.law_settlements.length >= 1;

    if (!this.props.bloc.rates && !this.props.bloc.requis_title && !hasLawSettlements) {
      return null;
    }

    return (
      <aside className="col-12 col-lg-5 col-xl-4">
        <div className="sidebar sidebar-top-stroke">
          {this.props.bloc.rates ? (
            <section className="sb-block">
              <h4 className="h4 bold">{this.props.t('content:serviceRecord.aside.payment')}</h4>
              <div dangerouslySetInnerHTML={{ __html: this.props.bloc.rates }} />
            </section>
          ) : null}
          {this.props.bloc.requis_title ? (
            <section className="sb-block">
              <h4 className="h4 bold" dangerouslySetInnerHTML={{ __html: this.props.bloc.requis_title }} />
              {this.props.bloc.requis ? <div dangerouslySetInnerHTML={{ __html: this.props.bloc.requis }} /> : null}
            </section>
          ) : null}
          {this.props.bloc.law_settlements && this.props.bloc.law_settlements.length > 0 ? (
            <section className="sb-block">
              <h4 className="h4 bold">{this.props.t('content:serviceRecord.aside.regulations')}</h4>
              <ul>
                {this.props.bloc.law_settlements.map((settlement, index) => (
                  <li key={index}><LocalizedLink title={settlement.name} to={settlement.url} t={this.props.t} /></li>
                ))}
              </ul>
            </section>
          ) : null}
          {this.props.bloc.documents && this.props.bloc.documents.length > 0 ? (
            <section className="sb-block">
              <h4 className="h4 bold">{this.props.t('common:documents')}</h4>
              <ul>
                {this.props.bloc.documents.map((doc, index) => (
                  <li key={index}><LocalizedLink title={doc.description && doc.description.length > 0 ? doc.description : doc.file_name} to={doc.url} t={this.props.t} /></li>
                ))}
              </ul>
            </section>
          ) : null}
        </div>
      </aside>
    );
  }
}
