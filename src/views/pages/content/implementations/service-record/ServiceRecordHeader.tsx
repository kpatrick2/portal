import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { DateTime } from '../../../../DateTime';
import { PageHeader } from '../../../../layout';
import { LocalizedLink } from '../../../../LocalizedLink';

interface IServiceRecordHeaderProps {
  title: string;
  abstract?: string;
  date: string;
  currentUrl: string;
  t: TranslationFunction;
}

export class ServiceRecordHeader extends React.PureComponent<IServiceRecordHeaderProps> {

  public render() {
    return (
      <>
        <header className="content-header content-header-dark">
          <div className="container-fluid">
            <div className="row">
              <div className="col-12">
                <PageHeader text={this.props.title} />
              </div>
              <div className="col order-1">
                <div className="content-header-metadata">
                  <div className="date-time updated ">{this.props.t('common:publicationDate.lastUpdated.label')}
                    <DateTime date={this.props.date} format="date" t={this.props.t} /></div>
                </div>
              </div>
              <div className="col-12 col-xl-auto order-first order-xl-last">
                <div className="hub-link">
                  <LocalizedLink to={this.props.t("link:allServicesUrl")} title={this.props.t("common:serviceRecord.allServices")} t={this.props.t} />
                </div>
              </div>
            </div>
          </div>
        </header>
      </>
    );
  }
}
