import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IServiceRecordBoroughBloc } from '../../../../../content-api';
import { CardLink, CardList } from '../../../../ui';

interface IServiceRecordLinksProps {
  bloc?: IServiceRecordBoroughBloc;
  t: TranslationFunction;
}

export class ServiceRecordLinks extends React.PureComponent<IServiceRecordLinksProps> {
  public render() {
    if (!this.props.bloc || !this.props.bloc.liens_connexes || this.props.bloc.liens_connexes.length === 0) {
      return null;
    }

    return (
      <section id="informations" className="page-section bg-gray-sky">
        <div className="container">
          <div className="row">
            <div className="col-12 text-center">
              <h2 className="section-heading">{this.props.t('common:more_informations')}</h2>
            </div>
          </div>
          <CardList>
            {this.props.bloc.liens_connexes.map((link, index: any) => {
              if (link.name && link.url) {
                return (
                  <CardLink key={index} title={link.name} link={link.url} t={this.props.t} />
                );
              }
            })}

          </CardList>
        </div>
      </section>
    );
  }
}
