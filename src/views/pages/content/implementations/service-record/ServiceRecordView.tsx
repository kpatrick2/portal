import { TranslationFunction } from 'i18next';
import * as React from 'react';
import {
  IServiceRecordContent,
  ITaxonomy
} from '../../../../../content-api';
import ServiceRecordContent from './ServiceRecordContent';
import { ServiceRecordHeader } from './ServiceRecordHeader';

interface IServiceRecordViewProps {
  content: IServiceRecordContent;
  boroughs: ITaxonomy[];
  currentUrl: string;
  t: TranslationFunction;
}

export class ServiceRecordView extends React.PureComponent<IServiceRecordViewProps> {

  public render() {
    return (
      <div className="servicerecord">
        <ServiceRecordHeader
          title={this.props.content.dc_title}
          abstract={this.props.content.dc_abstract}
          currentUrl={this.props.currentUrl}
          date={this.props.content.dc_modified}
          t={this.props.t}
        />
        <ServiceRecordContent content={this.props.content} currentUrl={this.props.currentUrl} extra={this.props.boroughs} t={this.props.t} />
      </div>
    );
  }
}
