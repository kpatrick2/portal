import { TranslationFunction } from 'i18next';
import * as queryString from 'query-string';
import * as React from 'react';
import { IServiceRecordBoroughBloc, IServiceRecordContent, IServiceRecordStep } from '../../../../../content-api';
import { getSelectedBorough } from '../../../../../utils';
import { AbstractText, TabItem, Tabs } from '../../../../ui';
import { AbstractModalBorough } from '../../../../ui/AbstractModalBorough';
import { BoroughAd } from '../../../borough-select/borough-ad';
import { ServiceRecordAside } from './ServiceRecordAside';
import { ServiceRecordLinks } from './ServiceRecordLinks';
import { ServiceRecordStep } from './ServiceRecordStep';


interface IServiceRecordContentProps {
    content: IServiceRecordContent;
    extra?: any;
    t: TranslationFunction;
    currentUrl: string;
}

export default class ServiceRecordContent extends React.PureComponent<IServiceRecordContentProps> {
    private selectedBorough: string | undefined;
    private body: any;
    private get selectedIndex() {
        const params = queryString.parse(queryString.extract(this.props.currentUrl));
        return params.tab ? parseInt(params.tab, 10) : 0;
    }

    public render() {
        const selectedBloc = this.getSelectedBloc();
        const isFullCity = selectedBloc!.bloc_services.find(
            bloc => (bloc.arrondissement.length > 0 && bloc.arrondissement[0] === "Ville de Montréal")
        );

        if (!isFullCity) {
            this.body = this.getSelectedBoroughService();
        } else {
            this.body = isFullCity;
        }

        const tabsTitle = this.props.content.mtl_content.fiches_services.bloc_fiche_service.map(bloc => bloc.bloc_title);

        return (
            <>
                {(isFullCity ?
                    <AbstractText text={this.props.content.dc_abstract} /> :
                    <AbstractModalBorough
                        extra={this.props.extra}
                        currentUrl={this.props.currentUrl}
                        text={this.props.content.dc_abstract}
                        selectText={this.props.t("common:content.borough_select_demarche")}
                        t={this.props.t} />
                )}
                <div className="region-content" id="borough-no-data" style={{ display: (!this.body && this.selectedBorough ? 'block' : 'none') }}>
                    <div className="container">
                        <div className="row">
                            <article className="col-12 col-lg-7 col-xl-8">
                                {this.props.t("common:no_result")}
                            </article>
                        </div>
                    </div>
                </div>
                {!this.selectedBorough && !isFullCity && <BoroughAd text={this.props.t("common:content.borough_ad")} />}
                <>
                    <Tabs selectedTab={this.selectedIndex} hidden={!this.selectedBorough && !isFullCity}>
                        {tabsTitle.map(title => (
                            <TabItem key={title} title={title} currentUrl={this.props.currentUrl} t={this.props.t} />
                        ))}
                    </Tabs>
                    {this.props.content.mtl_content.fiches_services.bloc_fiche_service.map((blocService, index) => {
                        return blocService.bloc_services.map(bloc => {
                            if (isFullCity) {
                                return (<div id={"FullCity"+index} style={{ display: index === 0 ? 'block' : 'none'}}>
                                    <div className="region-content">
                                        <div className="container">
                                            <div className="row">
                                                <article className="col-12 col-lg-7 col-xl-8">
                                                    {this.renderSelectedBoroughService(bloc)}
                                                </article>
                                                <ServiceRecordAside bloc={bloc} t={this.props.t} />
                                            </div>
                                        </div>
                                    </div>
                                    <ServiceRecordLinks bloc={bloc} t={this.props.t} />
                                </div>
                                )
                            } else {
                                return bloc.arrondissement.map(borough => {
                                    return (<div id={borough + index} style={{ display: 'none'}}>
                                        <div className="region-content" >
                                            <div className="container">
                                                <div className="row">
                                                    <article className="col-12 col-lg-7 col-xl-8">
                                                        {this.renderSelectedBoroughService(bloc)}
                                                    </article>
                                                    <ServiceRecordAside bloc={bloc} t={this.props.t} />
                                                </div>
                                            </div>
                                        </div>
                                        <ServiceRecordLinks bloc={bloc} t={this.props.t} />
                                    </div>
                                    )
                                })
                            }
                        })
                    })}
                </>
                {/* // )} */}
            </>
        );
    }



    private getSelectedBoroughService() {
        const selectedBloc = this.getSelectedBloc();

        this.selectedBorough = getSelectedBorough(this.props.currentUrl, this.props.extra);

        if (!this.selectedBorough) {
            return undefined;
        }

        return selectedBloc!.bloc_services.find(bloc => bloc.arrondissement.includes(this.selectedBorough!));
    }

    private renderSelectedBoroughService(selectedBoroughService?: IServiceRecordBoroughBloc) {
        if (!selectedBoroughService) {
            return null;
        }

        return (
            <>
                {selectedBoroughService.description ? (
                    <div dangerouslySetInnerHTML={{ __html: selectedBoroughService.description }} />
                ) : null}
                {this.isMultiServices(selectedBoroughService)
                    ? this.renderMultipleServices(selectedBoroughService)
                    : this.renderSingleService(selectedBoroughService)}
            </>
        );
    }

    private isMultiServices(bloc: IServiceRecordBoroughBloc) {
        const services = [
            bloc.datas_service_comptoir,
            bloc.datas_service_courriel,
            bloc.datas_service_online,
            bloc.datas_service_telephone
        ].filter(steps => steps.length > 0);

        return services.length > 1 ? true : false;
    }

    private renderSingleService(selectedBoroughService: IServiceRecordBoroughBloc) {
        const services = [
            selectedBoroughService.datas_service_comptoir,
            selectedBoroughService.datas_service_telephone,
            selectedBoroughService.datas_service_online,
            selectedBoroughService.datas_service_courriel
        ].filter(items => items.length > 0);

        if (services.length === 0) {
            return null;
        }

        return services[0].map((step, index) => this.renderStep(step, index, step.title!));
    }

    private renderMultipleServices(selectedBoroughService: IServiceRecordBoroughBloc) {
        return (
            <>
                {this.renderServiceSteps(
                    selectedBoroughService.datas_service_online,
                    this.props.t('content:serviceRecord.bloc.online'),
                    1
                )}
                {this.renderServiceSteps(
                    selectedBoroughService.datas_service_comptoir,
                    this.props.t('content:serviceRecord.bloc.counter'),
                    2
                )}
                {this.renderServiceSteps(
                    selectedBoroughService.datas_service_telephone,
                    this.props.t('content:serviceRecord.bloc.phone'),
                    3
                )}
                {this.renderServiceSteps(
                    selectedBoroughService.datas_service_courriel,
                    this.props.t('content:serviceRecord.bloc.mail'),
                    4
                )}
            </>
        );
    }

    private renderServiceSteps(steps: IServiceRecordStep[], header: string, id: number) {
        if (steps.length === 0) {
            return null;
        }

        return (
            steps.map((step, index) => this.renderStep(step, id, header))
        );
    }

    private renderStep(step: IServiceRecordStep, index: number, header: string) {
        return <ServiceRecordStep key={index} step={step} id={index + 1} t={this.props.t} header={header ? header : step.title!} />;
    }

    private getSelectedBloc() {
        const selectedIndex = this.selectedIndex;

        if (this.props.content.mtl_content.fiches_services.bloc_fiche_service.length <= selectedIndex) {
            return null;
        }

        return this.props.content.mtl_content.fiches_services.bloc_fiche_service[selectedIndex];
    }
}
