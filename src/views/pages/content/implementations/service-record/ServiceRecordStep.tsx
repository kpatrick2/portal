import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IServiceRecordStep } from '../../../../../content-api';
import { LocalizedLink } from '../../../../LocalizedLink';
import { ToolboxAccordeon } from '../../../../ui';

interface IServiceRecordStepProps {
  step: IServiceRecordStep;
  header: string;
  id: number;
  t: TranslationFunction;
}
export class ServiceRecordStep extends React.PureComponent<IServiceRecordStepProps> {
  public render() {
    return (
      <ToolboxAccordeon header={this.props.header} id={this.props.id}>
        <div dangerouslySetInnerHTML={{ __html: this.props.step.description }}></div>
        {this.props.step.payment_method && <>
          <h3>{this.props.t("common:content.payment_mode")}</h3>
          <div dangerouslySetInnerHTML={{ __html: this.props.step.payment_method }}></div>
        </>
        }
        {this.renderActionButton()}
      </ToolboxAccordeon>
    );
  }

  private renderActionButton() {
    if (this.props.step.call_to_action && this.props.step.call_to_action.length > 0) {
      return <LocalizedLink
        className="btn btn-sm btn-primary"
        t={this.props.t}
        title={this.props.step.call_to_action[0].title}
        type={this.props.step.call_to_action[0].type}
        to={this.props.step.call_to_action[0].url}
      />
    }
  }
}
