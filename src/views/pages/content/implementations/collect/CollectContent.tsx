import { TranslationFunction } from 'i18next';
import { get } from 'lodash';
import * as React from 'react';
import { IBlocCollect, ICollectContent } from '../../../../../content-api';
import { getSelectedBorough, isFullCity } from '../../../../../utils';
import { getSelectedBoroughContent } from '../../../../../utils/borough-helper';
import {
  AbstractText,
  Anchor,
  CardLink,
  Header,
  NavAnchors,
  SideBar,
  SideBarItemBlockDocument,
  SideBarItemBlockLaw,
  ToolboxAccordeon,
  ToolboxColumnList
} from '../../../../ui';
import { AbstractModalBorough } from '../../../../ui/AbstractModalBorough';
import { CardList } from '../../../../ui/cards/CardList';
import { BoroughAd } from '../../../borough-select/borough-ad';

interface ICollectContentProps {
  content: ICollectContent;
  extra?: any;
  currentUrl: string;
  t: TranslationFunction
}


export class CollectContent extends React.PureComponent<ICollectContentProps> {
  private selectedBorough: any;
  private boroughContent!: IBlocCollect;
  private isFullCity: any;

  public render() {
    this.getSelectedBoroughService();
    return (
      <main className="main-content " id="main-content" aria-label="Main Content">
        <Header title={this.props.content.dc_title} date={this.props.content.dc_modified} t={this.props.t} />
        {this.isFullCity ?
          <AbstractText text={this.props.content.dc_abstract} />
          :
          <AbstractModalBorough extra={this.props.extra} text={this.props.content.dc_abstract}
            currentUrl={this.props.currentUrl}
            selectText={this.props.t("common:content.borough_select_collect")}
            t={this.props.t} />
        }

        {this.props.content.mtl_content.collectes && this.props.content.mtl_content.collectes.length > 0 &&
          this.props.content.mtl_content.collectes.map(bloc => {
            return bloc.boroughs.map(boroughRaw => {
              const borough = decodeURIComponent(boroughRaw);
              const b = borough.includes('Île-Bizard') ? 'ile-bizard' : borough.replace(/ /g, '-');
              return (<div id={borough} className="sticky-container" style={{ display: (!this.isFullCity && borough !== this.selectedBorough) ? 'none' : 'block' }}>

              
                {this.anchor(bloc, b)}

                {this.renderContent(bloc, b)}

                {this.liensConnexes(bloc, b)}
              </div>
              );
            });
          })

        }
        {
          !this.selectedBorough &&
          <BoroughAd text={this.props.t("common:content.borough_ad")} />
        }
        <div id="sticky-limit"></div>
      </main>
    );
  }

  private renderContent(bloc: IBlocCollect, borough: string) {
    if (bloc) {
      if (!Object.keys(bloc).length) {
        return null;
      }
      return (
        <div id={"navTop-"+borough} className="region-content" >
          <div className="container">
            <div className="row">
              <article className="col-12 col-lg-7 col-xl-8">
                {this.contentBlock(bloc, this.selectedBorough, borough)}
              </article>
              {this.renderSideBar()}
            </div>
          </div>
        </div >
      )

    }
  }

  private renderSideBar() {
    if (!this.boroughContent || this.boroughContent.length === 0) {
      return null;
    }
    if ((this.boroughContent.documents && this.boroughContent.documents.length > 0)
      && (this.boroughContent.law_settlements && this.boroughContent.law_settlements.length > 0)) {
      return (
        <SideBar stroke>
          <SideBarItemBlockDocument title={this.props.t("content:collect.documents")} documents={this.boroughContent.documents} t={this.props.t} />
          <SideBarItemBlockLaw title={this.props.t("content:collect.rules")} data={this.boroughContent.law_settlements} t={this.props.t} />
        </SideBar>
      )
    }
  }

  private getSelectedBoroughService() {
    if (this.props.content.mtl_content && this.props.content.mtl_content.collectes) {
      this.isFullCity = isFullCity(this.props.content.mtl_content.collectes);
      if (this.isFullCity) {
        this.selectedBorough = this.isFullCity;
      } else {
        this.selectedBorough = getSelectedBorough(this.props.currentUrl, this.props.extra);
      }
      this.boroughContent = getSelectedBoroughContent(this.isFullCity, this.selectedBorough, this.props.content.mtl_content.collectes);

    }
  }

  private contentBlock(content: IBlocCollect, borough: string, correspondingBorough: string) {
    return (
      <>
        {this.boroughDescriptionSection(content, borough)}
        {this.sections(content, correspondingBorough)}
      </>
    );
  }

  private sections(content: IBlocCollect, correspondingBorough: string) {
    if (
      !get(content, 'accepted_material.length') &&
      !get(content, 'denied_material.length') &&
      !get(content, 'accepted_content.length') &&
      !get(content, 'more_informations.length')
    ) {
      return null;
    }

    return (
      <>
        {this.acceptedMaterial(content, correspondingBorough)}
        {this.deniedMaterial(content, correspondingBorough)}
        {this.acceptedContent(content, correspondingBorough)}
      </>
    );
  }

  private anchor(bloc: IBlocCollect, borough: string) {
    if (!bloc || bloc.length === 0) {
      return null;
    }
    const collectsAnchor = [
      {
        type: 'accepted_material',
        anchor: '#matieres-acceptees'
      },
      {
        type: 'denied_material',
        anchor: '#matieres-refusees'
      },
      {
        type: 'accepted_content',
        anchor: '#contenants-autorises'
      },
      {
        type: 'more_informations',
        anchor: '#informations'
      }
    ];
    return (
      
      <NavAnchors idSuffix={borough}>
        <Anchor text={this.props.t('content:collect.nav_top')} to="#navTop" t={this.props.t} abstract />
        {collectsAnchor.map((category, index) => {
          if (bloc[category.type] && bloc[category.type].length > 0) {
            return (
              <Anchor text={this.props.t('content:collect.' + category.type)} to={category.anchor} t={this.props.t} />
            );
          }
        })}
      </NavAnchors>
    );
  }

  private boroughDescriptionSection(content: IBlocCollect, borough: string) {
    return (
      <>
        {content.description ? (
          <div dangerouslySetInnerHTML={{ __html: content.description }} />
        ) : null}
      </>
    );
  }

  private acceptedMaterial(content: IBlocCollect, correspondingBorough: string) {
    if (!content || !content.accepted_material || content.accepted_material.length === 0) {
      return null;
    }

    return (
      <div id={"matieres-acceptees-"+correspondingBorough} className="page-section-anchor">
        <h2 className="h3">{this.props.t('content:collect.accepted_material')}</h2>
        <div className="collapsible-variant" role="tablist" aria-multiselectable="true">
          {content.accepted_material.map((accepted, index) => {
            if (accepted.title === null) {
              return (
                <div key={index} className="list-wrapper">
                  <ToolboxColumnList items={accepted.material} nbColumn={3} />
                </div>
              );
            } else {
              return (
                <ToolboxAccordeon id={index} header={accepted.title}>
                  <ToolboxColumnList items={accepted.material} nbColumn={3} />
                </ToolboxAccordeon>
              );
            }
          })}
        </div>
      </div>
    );
  }

  private deniedMaterial(content: IBlocCollect, correspondingBorough: string) {
    if (!content || !content.denied_material || content.denied_material.length === 0) {
      return null;
    }

    return (
      <div id={"matieres-refusees-"+correspondingBorough} className="page-section-anchor">
        <h2 className="h3">{this.props.t('content:collect.denied_material')}</h2>
        <div className="collapsible-variant" role="tablist" aria-multiselectable="true">
          {content.denied_material.map((denied, index) => {
            if (denied.title === null) {
              return (
                <div key={index} className="list-wrapper">
                  <ToolboxColumnList items={denied.material} nbColumn={3} />
                </div>
              );
            } else {
              return (
                <ToolboxAccordeon id={`denied${index}`} key={index} header={denied.title}>
                  <ToolboxColumnList items={denied.material} nbColumn={3} />
                </ToolboxAccordeon>
              );
            }
          })}
        </div>
      </div>
    );
  }

  private acceptedContent(content: IBlocCollect, correspondingBorough: string) {
    if (!content || !content.accepted_content || content.accepted_content.length === 0) {
      return null;
    }

    return (
      <div className="page-section-anchor" id={"contenants-autorises-"+correspondingBorough}>
        <h3>{this.props.t('content:collect.accepted_content')}</h3>
        <ToolboxColumnList items={content.accepted_content} nbColumn={2} />
      </div>
    );
  }

  private liensConnexes(bloc: IBlocCollect, correspondingBorough: string) {
    if (!bloc || bloc.length === 0 || !bloc.more_informations || bloc.more_informations.length === 0) {
      return null;
    }

    return (
      <section id={"informations-"+correspondingBorough} className="page-section page-section-anchor  bg-gray-sky">
        <div className="container">
          <div className="row">
            <div className="col-12 text-center">
              <h2 className="section-heading">{this.props.t('common:more_informations')}</h2>
            </div>
          </div>
          <CardList>
            {bloc.more_informations.map((link, index: any) => {
              if (link.title && link.url) {
                return (
                  <CardLink key={index} title={link.title} link={link.url} t={this.props.t} />
                );
              }
            })}

          </CardList>
        </div>
      </section>
    );
  }
}
