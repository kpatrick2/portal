import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { Language } from '../../../../../utils';

interface IWufooFormProps {
  t: TranslationFunction;
  language: Language;
}

export class WufooForm extends React.PureComponent<IWufooFormProps> {

  public render() {


    return this.renderWufoo();
  }

  private renderWufoo() {

    return (

      <form id="form19" name="form19" className="wufoo" accept-charset="UTF-8" method="POST" action="https://vdmtl.wufoo.com/forms/z1vh95sz0g1mp6f/#public" noValidate={true}>
        <p dangerouslySetInnerHTML={{ __html: this.props.t('contact:contact.bymail.labels.warnings') }}></p>

        <div className="row">
          <div className="col-12 col-md">
            <div id="foli3" className="form-group required">
              <label id="title3" htmlFor="Field3">{this.props.t('contact:contact.bymail.labels.first_name')}</label>
              <input type="text" id="Field3" name="Field3" className="form-control" maxLength={255} aria-required="true" />
              <div className="invalid-feedback">{this.props.t('contact:contact.bymail.validation.first_name.required')}</div>
            </div>
          </div>
          <div className="col-12 col-md">
            <div id="foli4" className="form-group required">
              <label id="title4" htmlFor="Field4">{this.props.t('contact:contact.bymail.labels.last_name')}</label>
              <input type="text" id="Field4" name="Field4" className="form-control" maxLength={255} aria-required="true" />
              <div className="invalid-feedback">{this.props.t('contact:contact.bymail.validation.last_name.required')}</div>
            </div>
          </div>
        </div>

        <div id="foli5" className="form-group required">
          <label id="title5" htmlFor="Field5">{this.props.t('contact:contact.bymail.labels.email')}</label>
          <input type="email" id="Field5" name="Field5" className="form-control" aria-required="true" placeholder={this.props.t('contact:contact.bymail.labels.placeholder')} />
          <div className="invalid-feedback">{this.props.t('contact:contact.bymail.validation.email.required')}</div>
          <small id="textRequiredHelpBlock" className="form-text text-muted"></small>
        </div>

        <div id="foli8" className="form-group">
          <label id="title8" htmlFor="Field8">{this.props.t('contact:contact.bymail.labels.borough')}</label>
          <select id="Field8" name="Field8" className="form-control" >
            <option selected>

            </option>
            <option value="Ahuntsic-Cartierville" >
              Ahuntsic-Cartierville
              </option>
            <option value="Anjou" >
              Anjou
              </option>
            <option value="C&ocirc;te-des-Neiges&ndash;Notre-Dame-de-Gr&acirc;ce" >
              Côte-des-Neiges–Notre-Dame-de-Grâce
              </option>
            <option value="L&#039;&Icirc;le-Bizard&ndash;Sainte-Genevi&egrave;ve" >
              L'Île-Bizard–Sainte-Geneviève
              </option>
            <option value="Lachine" >
              Lachine
              </option>
            <option value="LaSalle" >
              LaSalle
              </option>
            <option value="Le Plateau-Mont-Royal" >
              Le Plateau-Mont-Royal
              </option>
            <option value="Le Sud-Ouest" >
              Le Sud-Ouest
              </option>
            <option value="Mercier&ndash;Hochelaga-Maisonneuve" >
              Mercier–Hochelaga-Maisonneuve
              </option>
            <option value="Montr&eacute;al-Nord" >
              Montréal-Nord
              </option>
            <option value="Outremont" >
              Outremont
              </option>
            <option value="Pierrefonds-Roxboro" >
              Pierrefonds-Roxboro
              </option>
            <option value="Rivi&egrave;re-des-Prairies&ndash;Pointe-aux-Trembles" >
              Rivière-des-Prairies–Pointe-aux-Trembles
              </option>
            <option value="Rosemont&ndash;La Petite-Patrie" >
              Rosemont–La Petite-Patrie
              </option>
            <option value="Saint-Laurent" >
              Saint-Laurent
              </option>
            <option value="Saint-L&eacute;onard" >
              Saint-Léonard
              </option>
            <option value="Verdun" >
              Verdun
              </option>
            <option value="Ville-Marie" >
              Ville-Marie
              </option>
            <option value="Villeray&ndash;Saint-Michel&ndash;Parc-Extension" >
              Villeray–Saint-Michel–Parc-Extension </option>
          </select>
        </div>


        <div id="foli6" className="form-group">
          <label id="title6" htmlFor="Fiedl6">{this.props.t('contact:contact.bymail.labels.subject')} </label>
          <input id="Field6" name="Field6" type="text" className="form-control" maxLength={255} />
        </div>

        <div id="foli7" className="form-group required">
          <label id="title7" htmlFor="Field7">{this.props.t('contact:contact.bymail.labels.message')} </label>
          <textarea id="Field7" name="Field7" className="form-control" aria-required="true" rows={7} ></textarea>
          <div className="invalid-feedback" dangerouslySetInnerHTML={{ __html: this.props.t('contact:contact.bymail.validation.message.required') }}></div>
        </div>

        <div id="foli9" className="notranslate d-none">
          <label id="title9">
            Language
            {/*<span className="req">*</span>*/}
            <div>
              <input id="Field9" name="Field9" type="hidden" value={this.props.language} />
            </div>
          </label>
        </div>

        <button id="saveForm" name="saveForm" className="btn btn-primary" type="submit" dangerouslySetInnerHTML={{ __html: this.props.t('contact:contact.bymail.labels.send') }} />


        <div className="d-none">
          <label>Do Not Fill This Out
                <textarea name="comment" id="comment"></textarea>
            <input type="hidden" id="idstamp" name="idstamp" value="kmBkFdCoXbsS3sKzVX5qH5ZoFajez3vM7lWfztxN5Ns=" />
          </label>
        </div>
      </form>
    );
  }

}
