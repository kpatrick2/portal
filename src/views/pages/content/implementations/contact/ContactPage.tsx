import * as React from 'react';

import { TranslationFunction } from 'i18next';
import { IContactPage, IContactPageBodyBloc } from '../../../../../content-api/interfaces/data/contact-data';
import { Language } from '../../../../../utils';
import { Analytics } from '../../../../analytics';
import { LocalizedLink } from '../../../../LocalizedLink';
import { Header } from '../../../../ui';
import { WufooForm } from './WufooForm';

interface IContactPageProps {
  content: IContactPage;
  language: Language;
  t: TranslationFunction;
}

export default class ContactPage extends React.PureComponent<IContactPageProps> {
  private warningText?: string;
  private sidePhone?: IContactPageBodyBloc[];
  private sideWalkIn?: IContactPageBodyBloc[];

  public render() {
    this.parseData();

    return (
      <main className="main-content" id="main-content" aria-label="Main Content">
        <Analytics.Form
          title={this.props.content.dc_title}
          eventType="page_vue"
          formName="Communiquer avec la Ville"
          formType="contact"
          status="demarree"
          stepNumber="1"
          stepName="courriel"
        />
        <Header title={this.props.content.dc_title} t={this.props.t} gray />
        <section className="page-section">
          <div className="container">
            <div className="row">
              {this.renderWufooForm()}
              {this.renderInfo()}
            </div>
          </div>
        </section>
      </main>

    );
  }

  private parseData() {
    for (const bloc of this.props.content.mtl_content.pages_speciales.body) {
      switch (bloc.code) {
        case "warning":
          this.warningText = bloc.text;
          break;
        case "contact_phone":
          this.sidePhone = JSON.parse(bloc.text) as IContactPageBodyBloc[];
          break;
        case "contact_walk_in":
          this.sideWalkIn = JSON.parse(bloc.text) as IContactPageBodyBloc[];
          break;
        default:
          break;
      }
    }
  }

  private renderInfo() {
    return (


      <aside className="col-12 col-lg-5 col-xl-4" role="complementary">
        <div className="sidebar sidebar-top-stroke">
          <section className="sb-block">
            <ul className="list-icon">
              <li className="list-icon-item">
                <span className="vdm vdm-012-telephone " aria-hidden="true"></span>
                <div className="list-icon-content">
                  <span className="h6 list-icon-label">{this.props.t('contact:contact.byphone.label')}</span>
                  {this.sidePhone && this.sidePhone.map(bloc => {
                    return this.renderData(bloc);
                  })}

                </div>
              </li>
              <li className="list-icon-item">
                <span className="vdm vdm-017-localisation-etoile " aria-hidden="true"></span>
                <div className="list-icon-content">
                  <span className="h6 list-icon-label">{this.props.t('contact:contact.inperson.label')}</span>
                  {this.sideWalkIn && this.sideWalkIn.map(bloc => {
                    return this.renderData(bloc);
                  })}
                </div>
              </li>
            </ul>
          </section>
        </div>
      </aside>

    );
  }

  private renderData(bloc: IContactPageBodyBloc) {
    if (bloc.primary) {
      return <span className="h2">{bloc.primary}</span>
    } else {
      if (bloc.action && bloc.label) {
        return (<>
          <div dangerouslySetInnerHTML={{ __html: bloc.label }}></div>
          <LocalizedLink to={bloc.action} className="icon right read-more pt-2" t={this.props.t}>{this.props.t('contact:contact.inperson.more_info')}<span className="vdm vdm-063-fleche-droite " aria-hidden="true"></span></LocalizedLink>
        </>)
      } else {
        let texts;
        if(Array.isArray(bloc.value)){
          texts = bloc.value.map((text, index) => {
            if(index === 0){
              return <span className="d-block pb-1">{text}</span>
            }
            return <span className="d-block">{text}</span>
          })
        }else{
          texts = <span className="d-block pb-1">{bloc.value}</span>;
        }
        return (<>
          <span className="list-icon-label pt-2">{bloc.label}</span>
          {texts}
        </>)
      }
    }
  }

  private renderWufooForm() {

    return (
      <div className="col-12 col-lg-7 col-xl-8">
        {this.props.content.dc_abstract &&
          <div className="lead" dangerouslySetInnerHTML={{ __html: this.props.content.dc_abstract }}></div>
        }
        <div className="alert alert-warning alert-icon mb-6" role="alert">
          <span className="vdm vdm-030-attention" aria-hidden="true"></span>
          {this.warningText &&
            <div dangerouslySetInnerHTML={{ __html: this.warningText }}></div>
          }
        </div>
        <WufooForm t={this.props.t} language={this.props.language}> </WufooForm>
      </div>
    );
  }

}
