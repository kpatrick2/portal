import { TranslationFunction } from 'i18next';
import * as React from 'react';
import {
  INewsBodyPart,
  INewsContent,
  newsBodyPartGuards
} from '../../../../content-api';
import { LocalizedLink } from '../../../LocalizedLink';
import { SimpleVideoPlayer } from '../../../SimpleVideoPlayer';
import { AbstractText, Header, SideBar, SideBarItemBlock, SideBarSection, VideoPlayer } from '../../../ui';
import { SingleImage } from '../../../ui/images/SingleImage';

interface INewsContentProps {
  content: INewsContent;
  t: TranslationFunction;
}

export class NewsContent extends React.PureComponent<INewsContentProps> {
  public render() {
    return (
      <main className="main-content " id="main-content" aria-label="Main Content">
        <Header
          title={this.props.content.dc_title}
          date={this.props.content.dc_modified}
          t={this.props.t}
          hub_link={this.props.t("content:news.allNews")}
          hub_url={this.props.t("link:allNewsUrl")}
        >
        </Header>

        <AbstractText text={this.props.content.dc_abstract} />

        <div className="region-content">
          <article className="section-content-fluid">
            {this.props.content.mtl_images && this.props.content.mtl_images.length > 0 &&
              <div className="section-inner section-inner-lg section-inner-media">
                <div className="media media-image">
                  <SingleImage images={this.props.content.mtl_images} className="img-fluid" />
                </div>
              </div>
            }
            {this.props.content.mtl_content.nouvelles.nouvelle ? (
              this.props.content.mtl_content.nouvelles.nouvelle.map(this.renderNewsPart)
            ) : (
                <div className="section-inner section-inner-md">
                  <div dangerouslySetInnerHTML={{ __html: this.props.content.dc_description }} />
                </div>
              )}
            {/* <ImageGallery images={this.props.content.mtl_content.nouvelles.photo_gallery} t={this.props.t} /> */}
            <VideoPlayer video={this.props.content.mtl_content.nouvelles.video_gallery} t={this.props.t} />
          </article>
        </div>
        {this.renderSidebar()}
      </main>
    );
  }

  private renderNewsPart(part: INewsBodyPart, index: number) {
    if (newsBodyPartGuards.isTitle(part)) {
      return <h2 key={index}>{part.title}</h2>;
    }
    if (newsBodyPartGuards.isText(part)) {
      return <div key={index} dangerouslySetInnerHTML={{ __html: part.text }} />;
    }
    if (newsBodyPartGuards.isImage(part)) {
      return <SingleImage key={index} images={part.image} />;
    }
    if (newsBodyPartGuards.isVideo(part)) {
      return <SimpleVideoPlayer key={index} video={part.video} />
    }

    const noPart: never = part;
    // tslint:disable-next-line:no-console
    console.warn(`Unknown news body part: ${noPart}`);
  }

  private renderSidebar() {
    if (!this.props.content.dc_relation || this.props.content.dc_relation.length === 0) {
      return null;
    }

    return (
      <SideBar stroke>
        <SideBarSection title={this.props.t('content:news.practicalInformations')}>
          {this.props.content.dc_relation.map((relation, index) => (
            <SideBarItemBlock key={index} icon="015-fleche">
              <LocalizedLink to={relation.url} className="news-link" t={this.props.t}>
                {relation.title}
              </LocalizedLink>
            </SideBarItemBlock>
          ))}
        </SideBarSection>
      </SideBar>
    );
  }
}
