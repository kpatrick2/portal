import { FeatureCollection } from 'geojson';
import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IAlertContent } from '../../../../../content-api';
import { geometryToFeature, latLongToFeatureCollection } from '../../../../../utils';
import { AlertPromo, CardList, Header, MapBoxMap, PageSection, SideBar, SideBarItemBlock } from '../../../../ui';
import { CardV2 } from '../../../../ui/cards/CardV2';

interface IAlertContentProps {
  content: IAlertContent;
  t: TranslationFunction;
}

export class AlertContent extends React.PureComponent<IAlertContentProps> {
  public render() {
    return (
      <main className="main-content " id="main-content" aria-label="Main Content">
        <Header
          title={this.props.content.dc_title}
          updatedAtLabel={this.props.t('common:publicationDate.publishedOn')}
          date={this.props.content.dc_modified}
          t={this.props.t}
          hub_link={this.props.t('content:alert.allAlerts')}
          hub_url={this.props.t('link:allAlertsUrl')}
          gray
          titleheader="display-4"
        />

        <div className="region-content">
          <div className="container">
            <div className="row">
              <article className="col-12 col-lg-7 col-xl-8">
                <div dangerouslySetInnerHTML={{ __html: this.props.content.dc_description }} />
                {this.renderMap()}
                {this.renderAdvice()}
              </article>
              <SideBar stroke>
                <SideBarItemBlock title={this.props.t('content:alert.promo.title')}>
                  <AlertPromo t={this.props.t} sideBar />
                </SideBarItemBlock>
              </SideBar>
            </div>
          </div>
        </div>
        {this.renderLinksSection()}
      </main>
    );
  }

  private renderAdvice() {
    if (!this.props.content.mtl_content.alertes.advice) {
      return null;
    }

    return (
      <>
        <h2>{this.props.t('content:alert.titleAdvice')}</h2>
        <p dangerouslySetInnerHTML={{ __html: this.props.content.mtl_content.alertes.advice }} />
      </>
    );
  }

  private renderMap() {
    let coordinates: FeatureCollection;

    if (
      this.props.content.dc_spatial &&
      this.props.content.dc_spatial.coordinates &&
      this.props.content.dc_spatial.coordinates.length === 2
    ) {
      coordinates = latLongToFeatureCollection(
        this.props.content.dc_spatial.coordinates[0],
        this.props.content.dc_spatial.coordinates[1]
      );
    } else if (
      this.props.content.mtl_content.alertes.location &&
      this.props.content.mtl_content.alertes.location.coordinates &&
      this.props.content.mtl_content.alertes.location.coordinates.type === 'GeometryCollection'
    ) {
      coordinates = geometryToFeature(this.props.content.mtl_content.alertes.location
        .coordinates) ;
    } else {
      return null;
    }

    return (
      <>
        <h2>{this.props.t('content:alert.location')}</h2>
        <section className="map-section pb-4">
          <div id="mapAvis001" style={{ height: '400px' }} className="mapboxgl-map">
            <MapBoxMap coordinates={coordinates} zoom={15} dragging={true} zooming={true} selected={true} />
          </div>
        </section>
      </>
    );
  }

  private renderLinksSection() {
    if (!this.props.content.dc_relation || this.props.content.dc_relation.length === 0) {
      return null;
    }
    return (
      <PageSection dark title={this.props.t('common:more_informations')}>
        <CardList>
          {this.props.content.dc_relation.map((result, index) => (
            <div className="col-12 col-md-6 col-lg-4">
              <CardV2 title={result.title} dc_identifier={result.url} t={this.props.t} type="link" />
            </div>
          ))}
        </CardList>
      </PageSection>
    );
  }
}
