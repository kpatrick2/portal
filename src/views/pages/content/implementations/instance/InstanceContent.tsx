import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IBlocMember, IElectedContent, IInstanceContent } from '../../../../../content-api';
import { IDemarchesContent } from '../../../../../content-api/interfaces/data/demarches-data';
import { Anchor, CardLink, CardMember, Header, NavAnchors, SideBarSeance, WebDiffusion } from '../../../../ui';

interface IInstanceContentProps {
  content: IInstanceContent;
  t: TranslationFunction;
  extras: {
    demarches: IDemarchesContent[],
    elus: IElectedContent[]
  }


/*

Cette maquette devra être complétée au fur et à mesure que les données provenant de l'API
du type de contenu <Instances> et les analyses incluant maquettes seront disponibles.
SG. 12-09-2018
*/}

export class InstanceContent extends React.PureComponent<IInstanceContentProps> {

  public render() {

    const indexMember = (this.props.content.mtl_content.instances.instance_type) ? +this.props.content.mtl_content.instances.instance_type.records_per_page : 6;

    return (
      <main className="main-content sticky-container" id="main-content" aria-label="Main Content">
        <Header
          title={this.props.content.dc_title}
          date={this.props.content.dc_modified}
          t={this.props.t} />
        {this.props.content.dc_abstract ? (
          <div className="bg-gray-sky lead-container">
            <div className="section-content-fluid">
              <div className="section-inner section-inner-lg">
                <div className="lead" dangerouslySetInnerHTML={{ __html: this.props.content.dc_abstract }}>
                </div>
              </div>
            </div>
          </div>
        ) : null}

        {this.anchor(this.props.content.mtl_content.instances)}


        <div id="navTop" className="region-content">
          <div className="container">
            <div className="row">
              <article className="col-12 col-lg-7 col-xl-8">
                <div dangerouslySetInnerHTML={{ __html: this.props.content.dc_description }} />
              </article>
              <SideBarSeance t={this.props.t} />
            </div>
          </div>
        </div>



        {(this.props.content.mtl_content.instances && this.props.content.mtl_content.instances.members && this.props.content.mtl_content.instances.members.length) ? (
          <>
            <section id="members" className="page-section bg-gray-sky">
              <div className="container">
                <div className="row">
                  <div className="col-12 text-center">
                    <h2 className="section-heading">{this.props.t('content:commission.members')}</h2>
                  </div>
                </div>
                <div className="row cards" id="card-show-hide">
                  {this.props.content.mtl_content.instances.members.map((member: any, index: number) => this.renderInstanceMember(member, index, this.props.t, indexMember))}
                </div>

                {(this.props.content.mtl_content.instances.members.length > indexMember) ? (
                  (
                    <div id="btn-container" className="pagination-wrapper">
                      <button id="btn-show-hide" type="button" className="btn btn-secondary" >{this.props.t('content:commission.label_all_members')}</button>
                    </div>)
                ) : null}
              </div>
            </section>
          </>
        ) : null}


        <WebDiffusion t={this.props.t} webDiffusion={this.props.content.mtl_content.instances.webcasts} btnLabel="Visionnez" />

        {/* Les liens de la section n'ont pas encore été identifiés. */}
        {(this.props.extras.demarches && this.props.extras.demarches.length > 0) ? this.renderCardLink(this.props.extras.demarches) : null}

        <div id="sticky-limit"></div>
      </main>
    );
  }

  private renderInstanceMember(member: IBlocMember, index: number, t: TranslationFunction, indexLimit: number) {
    if (member && member.elected_name && member.elected_name.length > 0) {
      const eluFinded = (this.props.extras.elus && this.props.extras.elus.length > 0) ? this.props.extras.elus.find((value: any) => {
        if (value) {
          return value.dc_title === member.elected_name![0].title
        }
        return false;

      }) : null;
      if (eluFinded) {
        const picElu: any = eluFinded.mtl_images;
        if (index < indexLimit) {
          return (
            <CardMember member={member} t={t} memberImage={picElu} />
          )
        }
        else {
          return (
            <CardMember member={member} displayNone t={t} memberImage={picElu} />
          )
        }
      }
      return null;
    }
    return null;
  }

  private renderCardLink(demarches: IDemarchesContent[]) {

    if (demarches) {

      const sectionData = [];
      for (const demarche of demarches) {

        sectionData.push(<CardLink title={demarche.dc_title} link={demarche.dc_identifier} t={this.props.t} />)



      }
      return (
        <section id="informations" className="page-section  bg-gray-sky">
          <div className="container">
            <div className="row">
              <div className="col-12 text-center">
                <h2 className="section-heading">{this.props.t('content:instance.more_informations')}</h2>
              </div>
            </div>
            <div className="row cards">
              {sectionData}
            </div>
          </div>
        </section>
      );
    }

    return null;
  }

  private anchor(content: any) {
    if (!content || content.length === 0) {
      return null;
    }


    // text={["Haut de page", "Membres", "Webdiffusions", "Plus d'informations"]}
    // tagLink={["#navTop", "#members", "#webdiffusion", "#informations"]}
    const instanceAnchor = [
      {
        type: 'members',
        anchor: '#members'
      },
      {
        type: 'webcasts',
        anchor: '#webdiffusion'
      },
      {
        type: 'more_informations',
        anchor: '#informations'
      }
    ];

    return (
      <NavAnchors>
        <Anchor text={this.props.t('content:instance.nav_top')} to="#navTop" t={this.props.t} abstract />
        {instanceAnchor.map((category, index) => {
          if (content[category.type] && content[category.type].length > 0) {
            return (
              <Anchor text={this.props.t('content:instance.' + category.type)} to={category.anchor} t={this.props.t} />
            );
          }
        })}
        {this.props.extras.demarches && this.props.extras.demarches.length > 0 ? (
          <Anchor text={this.props.t('content:instance.more_informations')} to="#informations" t={this.props.t} />
        ) : null}

      </NavAnchors>
    );
  }

}  