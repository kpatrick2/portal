import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IProductContent } from '../../../../../content-api';
import { LocalizedLink } from '../../../../LocalizedLink';
import { AbstractText, CardList, Header, PageSection, SideBar, SideBarItemBlock, SideBarItemLi } from '../../../../ui';
import { CardV2 } from '../../../../ui/cards/CardV2';

interface IProductContentProps {
  content: IProductContent;
  t: TranslationFunction;
}

export class ProductContent extends React.PureComponent<IProductContentProps> {
  public render() {
    return (
      <main className="main-content" id="main-content" aria-label="Main Content">
        <Header
          title={this.props.content.dc_title}
          date={this.props.content.dc_modified}
          t={this.props.t}
          hub_link={this.props.t("topic:hub.label")} />

        <AbstractText text={this.props.content.dc_abstract} />

        <div className="region-content ">
          <div className="container">
            <div className="row">
              <article className="section-content-fluid">
                <div className="section-inner section-inner-md" dangerouslySetInnerHTML={{ __html: this.props.content.dc_description }}>
                </div>
              </article>
              {this.renderAside()}
            </div>
          </div>
        </div>
        {this.renderLinksSection()}
      </main>
    );
  }

  private renderAside() {
    if (!this.props.content.dc_relation || this.props.content.dc_relation.length === 0) {
      return null;
    }

    return (
      <SideBar stroke>
        <SideBarItemBlock title={this.props.t('content:product.links')}>
          <SideBarItemLi>
            <ul>
              {this.props.content.dc_relation.map((result, index) => (
                <li><LocalizedLink title={result.title} to={result.url} t={this.props.t} /></li>
              ))}
            </ul>
          </SideBarItemLi>
        </SideBarItemBlock>
      </SideBar>
    )
  }

  private renderLinksSection() {
    if (
      !this.props.content.mtl_content.produits.more_informations ||
      this.props.content.mtl_content.produits.more_informations.length === 0
    ) {
      return null;
    }

    return (
      <PageSection title={this.props.t("content:product.moreInformations")} dark>
        <CardList>
          {this.props.content.mtl_content.produits.more_informations.map((result, index) => {
            return (
              <div className="col-12 col-md-6">
                <CardV2 tag="Service"
                  title={result.title}
                  dc_identifier={result.url}
                  t={this.props.t}
                  footerText={this.props.t("common:read_more")}
                  type="large" />
              </div>
            )
          })}
        </CardList>
      </PageSection>
    );
  }
}
