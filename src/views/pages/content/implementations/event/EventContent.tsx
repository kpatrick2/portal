import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { renderToString } from 'react-dom/server';
import { IAddress, ILocationContentV2, IUrl } from '../../../../../content-api';
import { IEventContent } from '../../../../../content-api/interfaces/data/event-data';
import { latLongToFeatureCollection } from '../../../../../utils';
// import { DateInterval } from '../../../../DateInterval';
import { LocalizedLink } from '../../../../LocalizedLink';
import { Address, HeaderJumbotron, MapBoxMap, VideoPlayer } from '../../../../ui';
import { IFeatureProperties } from '../../../../ui/MapBoxMap';
import { EventContentSideBar } from './EventContentSideBar';

interface IEventProps {
  content: IEventContent;
  t: TranslationFunction;
  extras: IExtrasContent;
}

export interface IExtrasContent {
  events: IEventContent[];
  location: ILocationContentV2;
}

export class EventContent extends React.PureComponent<IEventProps> {

  public render() {

    return (
      <>
        <main className="main-content" id="main-content" aria-label="Main Content">
          <HeaderJumbotron
            title={this.props.content.dc_title}
            images={this.props.content.mtl_images}
            rate={
              this.props.content.mtl_content.evenements.rates
                ? this.props.content.mtl_content.evenements.rates.name
                : undefined
            }
            entry_right={this.props.content.mtl_content.evenements.admission_type}
            temporal={this.props.content.dc_temporal}
            status={this.props.content.mtl_content.evenements.status}
            t={this.props.t}
          />
          <div className="region-content ">
            <div className="container ">
              <div className="row">
                <EventContentSideBar content={this.props.content} t={this.props.t} extras={this.props.extras} />
                <article className="col-12 col-lg-7 col-xl-8">
                  <div className="content-block">
                    {this.props.content.dc_abstract && (
                      <div className="lead" dangerouslySetInnerHTML={{ __html: this.props.content.dc_abstract }} />
                    )}
                    <div dangerouslySetInnerHTML={{ __html: this.props.content.dc_description }} />
                  </div>
                  {this.props.content.mtl_content.evenements.documents &&
                    this.props.content.mtl_content.evenements.documents.length > 0 && (
                      <div className="content-block">
                        <h2 className="h5">{this.props.t('content:evenements.documents')}</h2>
                        <ul>
                          {this.props.content.mtl_content.evenements.documents.map((document, index) => {
                            return (
                              <li key={index}>
                                <LocalizedLink
                                  to={document.url}
                                  title={document.description || document.file_name}
                                  t={this.props.t}
                                />
                              </li>
                            );
                          })}
                        </ul>
                      </div>
                    )}
                  {this.props.content.mtl_content.evenements.partners &&
                    this.props.content.mtl_content.evenements.partners.length > 0 && (
                      <div className="content-block">
                        <h2 className="h5">{this.props.t('content:evenements.partners')}</h2>
                        <ul>
                          {this.props.content.mtl_content.evenements.partners.map((partner, index) => {
                            return <li key={index}>{partner}</li>;
                          })}
                        </ul>
                      </div>
                    )}
                  {this.props.content.mtl_content.evenements.event_type &&
                    this.props.content.mtl_content.evenements.event_type.length > 0 && (
                      <div className="content-block">
                        <h2 className="h5">{this.props.t('content:evenements.tags')}</h2>
                        <ul className="list-inline badges-lg">{this.generateTags()}</ul>
                      </div>
                    )}
                  {this.props.content.mtl_content.evenements.video && (
                    <div className="content-block">
                      <h2 className="h5">{this.props.t('content:evenements.video')}</h2>
                      <div className="media-container">
                        <VideoPlayer video={[this.props.content.mtl_content.evenements.video]} t={this.props.t} />
                      </div>
                    </div>
                  )}
                </article>
              </div>

              {this.renderMap()}

            </div>
          </div>
          {/* this.props.extras && this.props.extras.length > 0 &&
            <section className="page-section bg-gray-sky">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <h2 className="section-heading">{this.props.t("content:evenements.similars")}</h2>
                        </div>
                    </div>
                    <div className="row cards">
                        {this.renderSimilarEvents()}
                    </div>
                    {this.props.extras.length > 0 &&
                        <div className="pagination-wrapper">
                            <a className="btn btn-secondary" href="#">{this.props.t("content:evenements.allEvents")}</a>
                        </div>
                    }
                </div>
            </section>
          */}
        </main>
      </>
    );
  }
  /*
    private renderSimilarEvents() {
        return this.props.extras.map((event, index) => {
            const classes: any = {
                'col-12 col-lg-4': true
            }
            if (index < 3) {
                return <div className={classnames(classes)}>
                    <LocalizedLink
                        to={event.dc_identifier}
                        t={this.props.t}
                        className="card card-teaser">
                        <div className="card-body ">
                            <span className="card-type ">{event.mtl_content.evenements.event_type}</span>
                            <span className="card-title">{event.dc_title}</span>
                            <ul className="list-icon mb-0">
                                <li className="list-icon-item ">
                                    <span className="vdm vdm-005-calendrier " aria-hidden="true"></span>
                                    <div className="list-icon-content">
                                        {event.dc_temporal &&
                                            <DateInterval en= {this.props.t('common:languageCode') === 'en'} format='short' start={event.dc_temporal.start} end={event.dc_temporal.start} t={this.props.t} />
                                        }
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </LocalizedLink>
                </div>
            }
        })
    }
    */
  private renderMap() {
    let coordinates;

    let eventInformationAddress: IAddress | undefined = this.props.content.mtl_content.evenements.event_information.address;
    let coordinatesSource: ILocationContentV2 | IEventContent = this.props.content;

    if (this.props.content.mtl_content.evenements.event_information.event_location && this.props.extras.location) {
      coordinatesSource = this.props.extras.location;
      eventInformationAddress = this.props.extras.location.mtl_content.lieux.address;
    }

    if (
      coordinatesSource.dc_spatial &&
      coordinatesSource.dc_spatial.coordinates &&
      coordinatesSource.dc_spatial.type === 'Point'
    ) {
      coordinates = latLongToFeatureCollection(
        coordinatesSource.dc_spatial.coordinates[0],
        coordinatesSource.dc_spatial.coordinates[1],
        this.generateMessagePopUp(eventInformationAddress, coordinatesSource.dc_spatial.coordinates)
      );
    } else {
      return null;
    }

    return (
      <div className="row">
        <div className="col-12">
          <h2 className="h5">{this.props.t('common:content.map_title')}</h2>
          <div className="map-section">
            <div id="mapEvnt" style={{ height: '500px' }} className="mapboxgl-map">
              <MapBoxMap coordinates={coordinates} zoom={15} dragging={false} zooming={false} />
            </div>
          </div>
        </div>
      </div>
    );
  }

  private generateMessagePopUp(eventInformationAddress: IAddress | undefined, coordinates: number[]): IFeatureProperties {
    let description: string = '';
    let addressLink: IUrl | undefined;
    if (eventInformationAddress) {

      if (this.props.extras.location) {
        addressLink = {
          uri: this.props.extras.location.dc_identifier,
          title: this.props.extras.location.dc_title
        }
      }
      description = renderToString(<Address address={eventInformationAddress} coordinates={coordinates} link={addressLink} t={this.props.t} />);
    }

    const featureProperties = { description };

    return featureProperties;
  }

  private generateTags() {
    let tags = [this.props.content.mtl_content.evenements.event_type].concat(
      this.props.content.mtl_content.evenements.target_audience
    );

    if (this.props.content.dc_coverage.municipalities && this.props.content.dc_coverage.municipalities.length > 0) {
      tags = tags.concat(this.props.content.dc_coverage.municipalities);
    } else {
      tags = tags.concat(this.props.content.dc_coverage.boroughs);
    }

    if (tags && tags.length > 0) {
      return tags.map((tag, index) => {
        return (
          <li key={index} className="list-inline-item">
            <span className="badge">{tag}</span>
          </li>
        );
      });
    }
  }

}
