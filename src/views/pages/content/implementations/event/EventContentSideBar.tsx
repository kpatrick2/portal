import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ILocationContentV2 } from '../../../../../content-api';
import { IAddress, IAppelAction, IContact, IUrl } from '../../../../../content-api/interfaces/common-interfaces';
import { IEventContent, IEventInformation } from '../../../../../content-api/interfaces/data/event-data';
import { LocalizedLink } from '../../../../LocalizedLink';
import { Address, SideBar, SideBarItemLi, SideBarSection, SocialMedia } from '../../../../ui';
import { IExtrasContent } from './EventContent';

interface IEventProps {
  content: IEventContent;
  t: TranslationFunction;
  extras: IExtrasContent;
}

export class EventContentSideBar extends React.PureComponent<IEventProps> {
  public render() {
    const socialNetworks: IAppelAction[] | undefined = this.props.content.mtl_content.evenements.social_networks;
    const contacts: IContact[] | undefined = this.props.content.mtl_content.evenements.contacts_information;
    const eventInformations: IEventInformation = this.props.content.mtl_content.evenements.event_information;

    const sidebar = (socialNetworks && socialNetworks.length > 0) || (contacts && contacts.length > 0) || eventInformations;

    if (sidebar) {
      return (
        <SideBar order="last">
          <section className="sb-block">
            <h2 className="sidebar-title d-none d-sm-block">{this.props.t("content:evenements.title_sidebar")}</h2>
            <ul className="list-icon">
              {this.renderLocation(eventInformations)}
              {this.renderContact(contacts)}
              {this.formatSocialMedia(socialNetworks)}
            </ul>
          </section>
        </SideBar>
      )
    }
    return null;
  }

  private formatSocialMedia(socialNetworks: IAppelAction[] | undefined) {

    if (socialNetworks && socialNetworks.length > 0) {
      return (
        <SideBarSection>
          <SocialMedia media={socialNetworks} />
        </SideBarSection>
      );
    }
  }

  private renderLocation(eventInformations: IEventInformation) {

    let eventInformationAddress: IAddress | undefined = this.props.content.mtl_content.evenements.event_information.address;
    let coordinatesSource: ILocationContentV2 | IEventContent = this.props.content;
    let addressLink: IUrl | undefined;

    // If event_information is a nodeReference, we need to get the reference from extras.location
    if(this.props.content.mtl_content.evenements.event_information.event_location && this.props.extras.location) {
      coordinatesSource = this.props.extras.location;
      eventInformationAddress = this.props.extras.location.mtl_content.lieux.address;
      addressLink = {
        title: this.props.extras.location.dc_title,
        uri: this.props.extras.location.dc_identifier
      }
    }

    if (eventInformationAddress) {
      return (
        <SideBarSection>
          <SideBarItemLi icon="091-localisation" label={this.props.t("content:evenements.location")}>
            <div className="list-icon-block">
              <Address address={eventInformationAddress} link={addressLink} t={this.props.t} />
            </div>

            {coordinatesSource.dc_spatial && coordinatesSource.dc_spatial.coordinates && coordinatesSource.dc_spatial.type === 'Point' &&
              <LocalizedLink to="#mapEvnt" className="js-scroll-trigger" t={this.props.t}>
                {this.props.t("common:content:map_display_link_label")}
              </LocalizedLink>
            }
          </SideBarItemLi>
        </SideBarSection>
      )
    }
  }

  private renderContact(contacts: IContact[] | undefined) {

    if (contacts && contacts.length > 0) {

      return (
        <SideBarSection>
          <SideBarItemLi icon="031-ordinateur" label={contacts[0].title}>
            {contacts[0].contact_name! &&
              <div className="list-icon-block mb-1">
                <span className="text-dark">{contacts[0].contact_name}</span>
              </div>
            }

            {contacts[0].phones!.length > 0 &&
              <div className="list-icon-block">
                {contacts[0].phones && contacts[0].phones!.map(phone => {
                  if (phone.phone) {
                    return <>{phone.phone}{phone.phone_note ? "," : null} {phone.phone_note!}<br /></>
                  }
                })}
              </div>
            }

            {contacts[0].email &&
              <div className="list-icon-block mb-1">
                <LocalizedLink to={'mailto:' + contacts[0].email} t={this.props.t}>{this.props.t("content:evenements.email")}</LocalizedLink>
              </div>
            }

            {contacts[0].webcast && contacts[0].webcast!.map(website => {
              return (
                <div className="list-icon-block mb-1">
                  <LocalizedLink to={website.url} t={this.props.t}>{website.title || website.url}</LocalizedLink>
                </div>
              )
            }
            )}

            {contacts[0].fax &&
              <div className="list-icon-block">{this.props.t("content:evenements.fax")} {contacts[0].fax}</div>
            }
          </SideBarItemLi>
        </SideBarSection>
      )
    }
  }
}