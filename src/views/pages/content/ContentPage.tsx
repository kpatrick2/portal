import * as _ from 'lodash';
import * as React from 'react';
import * as striptags from 'striptags';
import { IContentType } from '../../../content-api';
import { Analytics } from '../../analytics';
import { Layout } from '../../layout';
import { MetaCollection } from '../../MetaCollection';
import { IAppVersionProps } from '../app-version-interface';
import { ILocalsProps } from '../locals-interface';
import { ContentTypeFactory } from './ContentTypeFactory';

interface IContentPageProps {
  content: IContentType;
  extra?: any;
}

export default class ContentPage extends React.PureComponent<IContentPageProps & ILocalsProps & IAppVersionProps> {
  public render() {
    return (
      <Layout
        title={this.props.content.dc_title}
        changeLanguageLink={this.getChangeLanguageLink()}
        clientSideData={this.props.clientSideData}
        className="content"
        meta={this.buildMeta()}
        currentUrl={this.props.currentUrl}
        t={this.props.t}
        language={this.props.language}
        appVersion={this.props.appVersion}
      >
        <Analytics.ViewPage
          title={this.props.content.dc_title}
          id={this.props.content.dc_identifier}
          contentType={this.props.content.dc_type.machine_name}
          sections={this.props.content.dc_isPartOf}
        />
        <ContentTypeFactory
          language= {this.props.language}
          content={this.props.content}
          extra={this.props.extra}
          currentUrl={this.props.currentUrl}
          t={this.props.t}
        />
      </Layout>
    );
  }
  private getChangeLanguageLink() {
    if (!this.props.content.dc_hasVersion) {
      return this.props.t('common:urlPrefix');
    }

    const languageCode = this.props.t('common:languageCode') === 'fr' ? 'en' : 'fr';
    const urlPrefix = _.trimEnd(this.props.t('common:urlPrefix', { lng: languageCode }), '/');
    const identifier = _.trimStart(this.props.content.dc_hasVersion[languageCode], '/');
    return `${urlPrefix}/${identifier}`;
  }

  private buildMeta() {
    const metaCollection = new MetaCollection();
    const title = striptags(this.props.content.dc_title);

    metaCollection.addByProperty('og:title', title);
    metaCollection.addByProperty('og:url', `https://beta.montreal.ca${this.props.content.dc_identifier}`);
    metaCollection.addByProperty('twitter:title', title);

    if (this.props.content.dc_abstract) {
      metaCollection.addByName('description', striptags(this.props.content.dc_abstract));
      metaCollection.addByProperty('og:description', striptags(this.props.content.dc_abstract));
      metaCollection.addByProperty('twitter:description', striptags(this.props.content.dc_abstract));
    }

    if (this.props.content.mtl_images && this.props.content.mtl_images.length > 0) {
      metaCollection.addByProperty('og:image', this.props.content.mtl_images[0].url);
      metaCollection.addByProperty('twitter:image', this.props.content.mtl_images[0].url);
    }

    return metaCollection.meta;
  }
}
