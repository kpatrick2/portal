import * as React from 'react';
import { Analytics } from '../../analytics';
import { Layout } from '../../layout';
import { MetaCollection } from '../../MetaCollection';
import { IAppVersionProps } from '../app-version-interface';
import { ILocalsProps } from '../locals-interface';

export default class SuccessPage extends React.PureComponent<ILocalsProps & IAppVersionProps> {
  public render() {
    return (
      <Layout
        title={this.props.t!('success:title')}
        changeLanguageLink={this.props.t!('success:changeLanguageUrl')}
        clientSideData={this.props.clientSideData}
        meta={this.buildMeta()}
        currentUrl={this.props.currentUrl}
        t={this.props.t}
        language={this.props.language}
        appVersion={this.props.appVersion}
      >
        <Analytics.ViewPage title={this.props.t!('home:title')} />
        <div className="region-content">
          <div className="container">
            <div className="row">
              <article className="col-12">
                <div className="row">
                  <div className="alert-container col-12 col-lg-8 offset-lg-2">
                    <div className="alert alert-success" role="alert">
                      <h4 className="alert-heading">{this.props.t('success:title')}</h4>
                      {this.props.t('success:abstract')}
                    </div>
                  </div>
                </div>
                <div className="row mb-6">
                  <div className="col-md-8 col-xl-6 offset-md-2 offset-xl-3">
                    <div className="text-center">
                      <a href={this.props.t('success:homeUrl')} className="btn btn-primary">
                        {this.props.t('success:home')}
                      </a>
                    </div>
                  </div>
                </div>{' '}
              </article>
            </div>
          </div>
        </div>
      </Layout>
    );
  }

  private buildMeta() {
    const metaCollection = new MetaCollection();
    const title = this.props.t!('success:title');
    const description = this.props.t!('home:description');

    metaCollection.addByName('description', description);
    metaCollection.addByProperty('og:title', title);
    metaCollection.addByProperty('og:description', description);
    metaCollection.addByProperty('twitter:title', title);
    metaCollection.addByProperty('twitter:description', description);

    return metaCollection.meta;
  }
}
