import * as React from 'react';
import { IContentTypeSearch } from '../../../content-api';
import { Analytics } from '../../analytics';
import { Layout } from '../../layout';
import { paginationHelpers } from '../../ui';
import { IAppVersionProps } from '../app-version-interface';
import { ILocalsProps } from '../locals-interface';
import { SearchHeader } from './SearchHeader';
import { SearchResults } from './SearchResults';

interface ISearchPageProps {
  results: IContentTypeSearch;
  decodedTerm: string;
}

export default class SearchPage extends React.PureComponent<ISearchPageProps & ILocalsProps & IAppVersionProps> {
  private get numberOfPages() {
    return paginationHelpers.calculateNumberOfPages(this.props.results.total, this.props.results.limit);
  }

  private get currentPage() {
    return paginationHelpers.calculateCurrentPage(this.props.results.offset, this.props.results.limit);
  }

  public render() {
    const changeLanguageLink = `${this.props.t!('search:changeLanguageUrl')}?q=${this.props.decodedTerm}`;

    return (
      <Layout
        title={this.props.t!('search:title')}
        changeLanguageLink={changeLanguageLink}
        clientSideData={this.props.clientSideData}
        className="search"
        currentUrl={this.props.currentUrl}
        t={this.props.t}
        language={this.props.language}
        appVersion={this.props.appVersion}
      >
        <Analytics.SearchResult
          title={this.props.t!('search:title')}
          searchTerm={this.props.decodedTerm}
          numberOfResults={this.props.results.total}
          totalPages={this.numberOfPages}
          currentPage={this.currentPage}
        />
        <SearchHeader term={this.props.decodedTerm} t={this.props.t} />
        <SearchResults
          term={this.props.decodedTerm}
          results={this.props.results}
          currentUrl={this.props.currentUrl}
          t={this.props.t}
        />
      </Layout>
    );
  }
}
