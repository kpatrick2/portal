import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IContentTypeSearch } from '../../../content-api';
import { EmptySearchResult, Pagination } from '../../ui';
import { SearchResultItem } from './SearchResultItem';

interface ISearchResultsProps {
  results: IContentTypeSearch;
  term: string;
  currentUrl: string;
  t: TranslationFunction;
}

export class SearchResults extends React.PureComponent<ISearchResultsProps> {
  public render() {
    return (
      <section className="page-section">
        <div className="container">
          <div className="row">
            <div className="col-12">


              {this.props.results.entries.length === 0 ? (
                <EmptySearchResult term={this.props.term} t={this.props.t} />
              ) : (
                  this.renderResults()
                )}
            </div>
          </div>

        </div>

      </section>
    );
  }

  private renderResults() {
    const header = this.props.t('search:header', { count: this.props.results.total });

    return (
      <>
        <h1 className="list-group-heading">{header}</h1>
        
        <div className="list-group list-group-hub">
          {this.props.results.entries.map(entry => (
            <SearchResultItem key={entry.dc_identifier} content={entry} t={this.props.t} />
          ))}
        </div>
        <Pagination
          currentUrl={this.props.currentUrl}
          limit={this.props.results.limit}
          offset={this.props.results.offset}
          total={this.props.results.total}
          t={this.props.t}
        />
      </>
    );
  }
}
