import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ContentTypes, IContentType } from '../../../content-api';
import { DateTime } from '../../DateTime';
import { LocalizedLink } from '../../LocalizedLink';

interface ISearchResultItemProps {
  content: IContentType;
  t: TranslationFunction;
}

export class SearchResultItem extends React.PureComponent<ISearchResultItemProps> {
  public render() {
    return (
      <LocalizedLink to={this.getLink()} className="list-group-item list-group-item-action " t={this.props.t}>


        <div className="list-group-item-body">
          <div className="list-group-max-width">
            <span className="list-group-item-title">{this.props.content.dc_title}</span>
            <div className="list-group-item-content ">
              {this.renderAbstract()}
              {this.renderDate()}
            </div>
          </div>
        </div>
        <div className="list-group-item-footer">
          <span className="vdm vdm-063-fleche-droite" />
        </div>
        {/* <div className="row">
          <div className="col-10 col-md-11">
            <div className="search-results-item-title">{this.props.content.dc_title}</div>
            {this.renderAbstract()}
            {this.renderDate()}
          </div>
          <div className="col-2 col-md-1 align-self-center">
            <span className="vdm vdm-063-fleche-droite" />
          </div>
        </div> */}
      </LocalizedLink>
    );
  }

  private getLink() {
    if (this.props.content.dc_type.machine_name === ContentTypes.ExternalContent) {
      return this.props.content.dc_relation[0].url;
    }

    return this.props.content.dc_identifier;
  }

  private renderAbstract() {
    // The content API returns a dc_abstract with the string 'undefined' or 'null' sometimes
    if (this.props.content.dc_abstract === 'undefined' || this.props.content.dc_abstract === 'null') {
      return undefined;
    }

    const value = this.props.content.dc_abstract || '';

    return <span className="">{value.replace(/<(?:.|\n)*?>/gm, '')}</span>;
  }

  private renderDate() {
    if (this.props.content.dc_type.machine_name !== ContentTypes.News) {
      return null;
    }

    return (
      <span>
        <DateTime date={this.props.content.dc_issued} format="date" t={this.props.t} />
      </span>
    );
  }
}
