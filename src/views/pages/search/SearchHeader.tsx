import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { SearchBar } from '../../ui';

interface ISearchHeaderProps {
  t: TranslationFunction;
  term: string;
}

export class SearchHeader extends React.PureComponent<ISearchHeaderProps> {
  public render() {
    return (
      <header className="content-header content-header-dark">
        <div className="container pt-4 pb-12">
              <form method="GET">
                <SearchBar placeholder={this.props.t!('search:placeholder')} value={this.props.term} />
              </form>
        </div>
      </header>

    );
  }
}
