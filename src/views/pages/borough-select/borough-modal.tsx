import { decode } from 'he';
import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { ITaxonomy } from '../../../content-api';

interface IBoroughModalProps {
    boroughs: ITaxonomy[];
    t: TranslationFunction;
    currentUrl?: string;
}

export class BoroughModal extends React.PureComponent<IBoroughModalProps> {
    public render() {
        return <>
            {this.props.children}
            <div className="modal fade" id="selectArrondissementModal" role="dialog" aria-labelledby="selectArrondissementModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="selectArrondissementModalLabel">{this.props.t("common:content.borough_select")}</h5>
                            <button type="button" className="close btn-close" data-dismiss="modal" aria-label="Close">
                                <span className="vdm vdm-046-grand-x"></span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <ul>
                                {this.props.boroughs.map((borough, index) => {
                                        let url = "";
                                        if (this.props.currentUrl!.includes("arrondissement") ) {
                                            url = (borough.name) ? this.props.currentUrl!.replace(/arrondissement=([^&]+)/, "arrondissement=" + decode(borough.name)) : "";
                                        }else{
                                            url = (borough.name) ? this.props.currentUrl! + "?arrondissement=" + decode(borough.name) : "";
                                        }
                                            return <li key={index}>
                                                <a href={url} className="store-borough" data-dismiss="modal">{(borough.name) ? decode(borough.name) : ""}</a>
                                            </li>
                                   
                                })}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </>
    }
}