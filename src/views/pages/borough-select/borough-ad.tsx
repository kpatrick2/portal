import * as React from 'react';

interface IBoroughAdProps {
    text:string
}

export class BoroughAd extends React.PureComponent<IBoroughAdProps> {
    public render() {
        return (
            <div className="region-content" id="borough-ad">
                <div className="container">
                    <div className="row">
                        <article className="col-12">
                            <div className="empty-state-container">
                                <img src="/images/empty-state-arr-sm.png" className="d-sm-none" />
                                <img src="/images/empty-state-arr-lg.png" className="d-none d-sm-block" />
                                <p className="text-center" dangerouslySetInnerHTML={{__html:this.props.text}}></p>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        )
    }
}