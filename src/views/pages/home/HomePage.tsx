import * as React from 'react';
import { IContentTypeSearch, IHomeContent } from '../../../content-api';
import { Analytics } from '../../analytics';
import { Layout } from '../../layout';
import { MetaCollection } from '../../MetaCollection';
import { IAppVersionProps } from '../app-version-interface';
import { ILocalsProps } from '../locals-interface';
import { HomeFeed } from './HomeFeed';
import { HomeIntro } from './HomeIntro';
import { HomePromotions } from './HomePromotions';
import { HomeServices } from './HomeServices';


interface IHomePageProps {
  content: IHomeContent;
  news?: IContentTypeSearch;
}

export default class HomePage extends React.PureComponent<IHomePageProps & ILocalsProps & IAppVersionProps> {
  public render() {
    return (
      <Layout
        title={this.props.t!('home:title')}
        changeLanguageLink={this.props.t!('home:changeLanguageUrl')}
        clientSideData={this.props.clientSideData}
        className="home"
        meta={this.buildMeta()}
        currentUrl={this.props.currentUrl}
        t={this.props.t}
        language={this.props.language}
        appVersion={this.props.appVersion}
      >
        <main className="main-content " id="main-content" aria-label="Main Content">
          <Analytics.ViewPage title={this.props.t!('home:title')} />
          <HomeIntro content={this.props.content} />
          <HomeServices content={this.props.content} t={this.props.t} />
          <HomeFeed news={this.props.news} t={this.props.t} />
          <HomePromotions content={this.props.content} t={this.props.t} />
          </main>
      </Layout>
    );
  }

  private buildMeta() {
    const metaCollection = new MetaCollection();
    const title = this.props.t!('home:title');
    const description = this.props.t!('home:description');

    // Add meta tag for google site verification to make sur google tag manager is working
    metaCollection.addByName('google-site-verification', 'FBAKe4AHu2eyS10Br5SRq0nYhVKmFGV2MztdLl3OYwE');
    metaCollection.addByName('description', description);
    metaCollection.addByProperty('og:title', title);
    metaCollection.addByProperty('og:description', description);
    metaCollection.addByProperty('twitter:title', title);
    metaCollection.addByProperty('twitter:description', description);

    if (this.props.content.mtl_images && this.props.content.mtl_images.length > 0) {
      metaCollection.addByProperty('og:image', this.props.content.mtl_images[0].url);
      metaCollection.addByProperty('twitter:image', this.props.content.mtl_images[0].url);
    }

    return metaCollection.meta;
  }
}
