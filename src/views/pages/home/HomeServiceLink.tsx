import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { INodeReferenceWithIcon } from '../../../content-api';
import { LocalizedLink } from '../../LocalizedLink';

interface IHomeServiceLinkProps {
  service: INodeReferenceWithIcon;
  index: number;
  t: TranslationFunction;
}

export class HomeServiceLink extends React.PureComponent<IHomeServiceLinkProps> {
  public render() {
    const references = this.props.service.referenced_nodes || this.props.service.external_references;
    if (!references || references.length === 0) {
      return null;
    }

    return (
      <div className="col-12 col-md-6 col-lg-4">
        <LocalizedLink
          to={references[0].url}
          t={this.props.t}
          data-link-position={(this.props.index + 1).toString()}
          data-link-title={references[0].title}
          className="card"
        >
          <span className={`card-icon-top vdm ${this.props.service.icon}`} />
          <div className="card-body">
            <span className="card-title">{references[0].title}</span>
          </div>
        </LocalizedLink>
      </div>
    );
  }
}
