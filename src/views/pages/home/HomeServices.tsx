import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IHomeContent } from '../../../content-api';
import { HomeServiceLink } from './HomeServiceLink';

interface IHomeServicesProps {
  content: IHomeContent;
  t: TranslationFunction;
}

export class HomeServices extends React.PureComponent<IHomeServicesProps> {
  public render() {
    return (
      <section className="page-section bg-gray-sky" id="services" data-link-section="rangee 3" >
        <div className="container">
          <div className="row cards ">
            <div className="col-12">
              <h2 className="section-heading">{ this.props.content.mtl_content.accueil.popular_services_title }</h2>
            </div>
            { this.props.content.mtl_content.accueil.popular_services.map((popularService, index) => (
              <HomeServiceLink key={index} service={popularService} index={index} t={this.props.t} />
            )) }
          </div>
        </div>
      </section>
    );
  }
}
