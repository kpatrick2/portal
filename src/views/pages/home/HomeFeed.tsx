import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IContentTypeSearch, INewsContent } from '../../../content-api';
import { LocalizedLink } from '../../LocalizedLink';
import { CardList, CardV2 } from '../../ui';

interface IHomeFeedProps {
  news?: IContentTypeSearch;
  t: TranslationFunction;
}

export class HomeFeed extends React.PureComponent<IHomeFeedProps> {
  public render() {
    if (!this.props.news) {
      return null;
    }

    return (
      <section className="page-section bg-gray-sky">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <h2 className="h2">{this.props.t('home:news')}</h2>
            </div>
          </div>
          <CardList>
            {this.props.news.entries.map((entry: INewsContent) => (
              <div className="col-12" key={entry.dc_identifier}>
                <CardV2
                  title={entry.dc_title}
                  dc_identifier={entry.dc_identifier}
                  date={entry.dc_issued}
                  t={this.props.t}
                  images={entry.mtl_images}
                  tc={entry.dc_type.machine_name}
                />
              </div>
            ))}
          </CardList>

          <div className="pagination-wrapper">
            <LocalizedLink
              to={this.props.t('home:allNewsUrl')}
              className="btn btn-secondary"
              t={this.props.t}
            >
              {this.props.t('home:allNews')}
            </LocalizedLink>
          </div>
        </div>
      </section>


    );
  }
}
