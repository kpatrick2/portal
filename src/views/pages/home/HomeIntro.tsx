import * as React from 'react';
import { IHomeContent } from '../../../content-api';
import { SingleImage } from '../../ui/images/SingleImage';

interface IHomeIntroProps {
  content: IHomeContent;
}

export class HomeIntro extends React.PureComponent<IHomeIntroProps> {
  public render() {
    return [
      <>
      <section className="page-section">
        <div className="container">
          <div className="row">
            <div className="col-12 col-lg-8 offset-lg-2 text-center">
              <h1>{this.props.content.mtl_content.accueil.display_title}</h1>
              <div className="lead">
                <div dangerouslySetInnerHTML={{ __html: this.props.content.dc_description }} />
                <a href="#services" data-smooth-scrolling="true" className="js-scroll-trigger border-0">
                  {this.props.content.mtl_content.accueil.anchor_label}
                  <span className="vdm vdm-057-chevron-bas ml-1" aria-hidden="true"></span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <SingleImage images={this.props.content.mtl_images} className="w-100" />
      </>
    ];
  }
}
