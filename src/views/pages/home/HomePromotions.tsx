import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { IHomeContent } from '../../../content-api';
import { LocalizedLink } from '../../LocalizedLink';
import { SingleImage } from '../../ui/images/SingleImage';

interface IHomePromotionsProps {
  content: IHomeContent;
  t: TranslationFunction;
}

export class HomePromotions extends React.PureComponent<IHomePromotionsProps> {
  public render() {
    return (
      <section className="section-promo">
      <div className="home-promo">
        {this.renderFirstPromo()}
      </div>
      </section>
    );
  }
  /*
  private renderFirstPromo() {
    if (this.props.content.mtl_content.accueil.text_promo.length < 1) {
      return null;
    }

    const promo = this.props.content.mtl_content.accueil.text_promo[0];
    const imageStyle = {
      background: promo.image.length >= 0 ? `url(${promo.image[0].url}) right center / cover no-repeat` : 'initial'
    };

    return (
      <div className="container-fluid home-promo-first" data-link-section="rangee 5">
        <div className="row no-gutters">
          <div className="col-xs-12 col-lg-5 home-promo-first-image" style={imageStyle} />
          <div className="col-xs-12 col-lg-7">
            <div className="home-promo-first-text">
              <h2 className="h2">{promo.title}</h2>
              <div
                dangerouslySetInnerHTML={{ __html: promo.description }}
                className="home-promo-first-text-description"
              />
              <LocalizedLink to={promo.url.uri} title={promo.url.title} className="btn btn-secondary" t={this.props.t}>
                {promo.url.title}
              </LocalizedLink>
            </div>
          </div>
        </div>
      </div>
    );
  }
  */

  private renderFirstPromo() {
    if (this.props.content.mtl_content.accueil.text_promo.length < 1) {
      return null;
    }

    const promo = this.props.content.mtl_content.accueil.text_promo[0];

    return (
      <div className="container home-promo-second" data-link-section="rangee 6">
        <div className="row no-gutters">
          <div className="col-12 col-lg-6">
            <div className="promo-content">
              <h2>{promo.title}</h2>
              <div
                dangerouslySetInnerHTML={{ __html: promo.description }}
              />
              <LocalizedLink to={promo.call_to_action.url!} title={promo.call_to_action.title} className="btn btn-primary" t={this.props.t}>
                {promo.call_to_action.title}
              </LocalizedLink>
            </div>
          </div>
          <div className="col-12 col-lg-6">
            <SingleImage images={promo.image} className="img-fluid" />
          </div>
        </div>
      </div>
    );
  }
}
