import { TranslationFunction } from 'i18next';
import { Language } from '../../utils';

export interface ILocalsProps {
  currentUrl: string;
  clientSideData: { [key: string]: any };
  t: TranslationFunction;
  language: Language;
}
