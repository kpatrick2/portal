import { TranslationFunction } from 'i18next';
import * as React from 'react';
import { config, Language } from '../../utils';
import { IMeta, isNamedMeta, MetaCollection } from '../MetaCollection';
import { Feedback } from '../ui';
const { version } = require('../../../package.json');

interface ILayoutProps {
  title: string;
  changeLanguageLink: string;
  clientSideData: { [key: string]: string };
  className?: string;
  meta?: IMeta[];
  currentUrl: string;
  t: TranslationFunction;
  language: Language;
  appVersion: string;
}

export class Layout extends React.PureComponent<ILayoutProps> {
  public render() {
    return (
      <html>
        <head>
        <link href='https://api.mapbox.com/mapbox-gl-js/v0.38.0/mapbox-gl.css' rel='stylesheet' />

          {/* Google Tag Manager */}
          <script
            dangerouslySetInnerHTML={{
              __html: `
  (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-TFDV82X');
`
            }}
          />
          <title>{this.props.title}</title>
          <base href="/" />
          <link
            href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,400,400i,600,600i,700,700i"
            rel="stylesheet"
          />
          <link href={`style.css?v=${version}`} rel="stylesheet" />
          <link href={`vdm-icon.css?v=${version}`} rel="stylesheet" />
          <link href={this.getLinkWithoutHash(this.props.currentUrl)} rel="canonical" />
          {this.renderMetaTags()}
          <script
            dangerouslySetInnerHTML={{
              __html: `
  var languageLink = "${encodeURI(this.props.changeLanguageLink)}";
  var config = JSON.parse('${JSON.stringify(this.props.clientSideData)}');
`
            }}
          />
        </head>
        <body className={this.props.className} data-portal-version={version}>
          {/* Google Tag Manager (noscript) */}
          <noscript>
            <iframe
              src="https://www.googletagmanager.com/ns.html?id=GTM-TFDV82X"
              height="0"
              width="0"
              style={{ display: 'none', visibility: 'hidden' }}
            />
          </noscript>
          {this.props.children}
          <Feedback
            currentUrl={this.props.currentUrl}
            t={this.props.t}
            language={this.props.language}
            appVersion={this.props.appVersion}
          />
          <script src={`javascripts/main.js?v=${version}`} />
        </body>
      </html>
    );
  }

  private renderMetaTags() {
    return this.getMetaTags().map((meta, index) => {
      if (isNamedMeta(meta)) {
        return <meta key={index} name={meta.name} content={meta.content} />;
      }

      return <meta key={index} property={meta.property} content={meta.content} />;
    });
  }

  private getMetaTags() {
    return this.props.meta || new MetaCollection().meta;
  }

  private getLinkWithoutHash(currentUrl: string): string {
    const newLink: string = config.baseUrl+currentUrl.split("#")[0];
    return newLink;
  }
}
