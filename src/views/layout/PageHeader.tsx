import * as classnames from 'classnames';
import * as React from 'react';

interface IPageHeaderProps {
  text: string;
  small?: boolean;
  initial?: boolean;
}

export class PageHeader extends React.PureComponent<IPageHeaderProps> {
  public render() {
    const classNames = classnames({
      'content-header-title': true,
      // 'page-header-small': this.props.small,
      // 'page-header-initial': this.props.initial
    });

    return (
      <div className={classNames}>
        <h1 className="display-2">{this.props.text}</h1>
        {this.props.children}
      </div>
    );
  }
}
