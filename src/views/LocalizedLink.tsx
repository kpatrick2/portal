import { TranslationFunction } from 'i18next';
import * as _ from 'lodash';
import * as React from 'react';

interface ILocalizedLinkProps {
  to?: string;
  t: TranslationFunction;
  className?: string;
  id?: string;
  title?: string;
  disabled?: boolean;
  type?: string;
  'data-toggle'?: string;
  'data-link-category'?: string;
  'data-link-type'?: string;
  'data-link-position'?: string;
  'data-link-title'?: string;
  'data-document-title'?: string;
  'data-size'?: string;
  'data-index'?: string;
  'data-title'?: string;
  'data-author'?: string;
  'itemProp'?: string; 
}

export class LocalizedLink extends React.PureComponent<ILocalizedLinkProps> {
  private get ifFullLink() {
      return  this.props.to && (this.props.to.startsWith('http://') || this.props.to.startsWith('https://') || this.props.to.startsWith('mailto'));
  }

  private get fullUrl() {
    if (this.props.disabled) {
      return undefined;
    }

    if (this.ifFullLink) {
      return this.props.to;
    }

    const urlPrefix = this.props.t('common:urlPrefix');
    const url = _.trimStart(this.props.to, '/');

    if(this.props.type === 'tab') {
      return url;
    }

    return urlPrefix + url;
  }

  public render() {
    const {to,t,disabled,type, ...other} = this.props;
    return (
      <a
        href={this.fullUrl}
        target={undefined}
        {...other}
      >
        {this.props.children ? this.props.children : this.props.title}
      </a>
    );
  }
}