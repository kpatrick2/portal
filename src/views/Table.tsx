import * as classnames from 'classnames';
import * as React from 'react';
import { ITable } from '../content-api';

interface ITableProps {
  tables: ITable[];
  striped?: boolean;
}

export class Table extends React.PureComponent<ITableProps> {

  public render() {
    if (this.props.tables && this.props.tables instanceof Array && this.props.tables.length > 0) {
      const table = this.props.tables[0];
      const classTable = classnames({
        "table": true,
        "table-striped": this.props.striped
      })
      const headerRow = table.data[0];
      return (
        <div className="table-responsive ">
          <table className={classTable}>
            {this.putTHead(headerRow, table.header_type)}
            {this.putTBody(table)}
          </table>
        </div>
      );
    }
    return null;
  }

  private putTHead(table: string[], scope: string) {
    if (table && table.length > 0) {
      return (
        <thead className="thead-dark">
          <tr>
            {table.map((value, index) => {
              return <th key={index} scope={scope}>{value}</th>
            })}
          </tr>
        </thead>
      );
    }
    return null;
  }
  private putTBody(tables: ITable) {

    const rows: any[] = [];

    if (tables) {
      tables.data.map((row, index) => {
        if (index > 0) {
          if (row.some(r => r !== '')) {
            rows.push(<tr key={index}>
              {row.map((col, colIndex) => {
                if (colIndex === 0) {
                  return <th scope={tables.header_type}>{col}</th>
                } else {
                  return <td scope={tables.header_type}>{col}</td>
                }
              })}
            </tr>)
          }
        }
        return null;
      })

      return (
        <tbody>
          {rows}
        </tbody>
      );
    }
    return null;
  }
}
