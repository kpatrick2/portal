import { TranslationFunction } from 'i18next';
import * as moment from 'moment';
import * as React from 'react';
import { assertNever, Capitalize } from '../utils';

interface IDateTimeProps {
  date: string;
  format: 'date' | 'date-long' | 'time' | 'date-time-long' | 'date-time' | 'day-month' | 'day-month-long';
  type?: 'date';
  capitalizeFirst?: boolean;
  t: TranslationFunction;
}

export class DateTime extends React.PureComponent<IDateTimeProps> {
  public render() {
    const language = this.props.t('common:languageCode');
    let date = moment(this.props.date);

    if (!this.props.date.includes('Z')) {
      date = date.utc(this.props.date as any).local();
    }

    if (this.props.type === 'date') {
      date = moment(this.props.date, 'YYYY-MM-DD');
    }

    date.locale(language);

    if (this.props.capitalizeFirst) {
      if (language === 'en') {
        return Capitalize(this.renderEnglish(date));
      }

      return Capitalize(this.renderFrench(date));
    } else {
      if (language === 'en') {
        return this.renderEnglish(date);
      }

      return this.renderFrench(date);
    }
  }

  private renderEnglish(date: moment.Moment) {
    // tslint:disable-next-line:switch-default
    switch (this.props.format) {
      case 'day-month':
        return date.format('MMMM D');

      case 'day-month-long':
        return date.format('dddd, MMMM D');

      case 'date':
        return date.format('MMMM D, YYYY');

      case 'date-long': 
        return date.format('dddd, MMMM D, YYYY');

      case 'time':
        return date.format('h:mm[\xa0]A');

      case 'date-time':
        return date.format('MMMM D, YYYY [-] h:mm[\xa0]A');

      case 'date-time-long':
        return date.format('dddd, MMMM D, YYYY [-]h:mm[\xa0]A');

      default:
        return assertNever(this.props.format);
    }
  }

  private renderFrench(date: moment.Moment) {
    // tslint:disable-next-line:switch-default
    switch (this.props.format) {
      case 'day-month':
        return date.format('D MMMM');

      case 'day-month-long':
        return date.format('dddd D MMMM');

      case 'date':
        return date.format('D MMMM YYYY');

      case 'date-long': 
        return date.format('dddd D MMMM YYYY');

      case 'time':
        return date.format('H[\xa0h\xa0]mm');

      case 'date-time':
        return date.format('D MMMM YYYY ['+this.props.t("common:at")+'] H[\xa0h\xa0]mm');

      case 'date-time-long':
        return date.format('dddd D MMMM YYYY ['+this.props.t("common:at")+'] H[\xa0h\xa0]mm');

      default:
        return assertNever(this.props.format);
    }
  }
}
