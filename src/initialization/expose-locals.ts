import { Application } from 'express';
import { config } from '../utils';

export function exposeLocalsMiddleware(app: Application) {
  app.use((req, res, next) => {
    res.locals.currentUrl = req.url;

    res.locals.clientSideData = res.locals.clientSideData || {};
    res.locals.clientSideData.config = { environmentName: config.environmentName };
    next();
  });
}
