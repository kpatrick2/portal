import { Application, NextFunction, Request, Response } from 'express';

export function registerErrorHandlers(app: Application) {
  // catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err: any = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // error handler
  app.use((err: any, req: Request, res: Response, next: NextFunction) => {
    // tslint:disable-next-line:no-console
    console.error(err);

    // set locals, only providing error in development
    res.status(err.status || 500);
    res.send({
      message: err.message,
      error: req.app.get('env') === 'development' ? err.stack : undefined
    });
  });
}
