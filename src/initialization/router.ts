import { Router } from 'express';
import { contentRouter, homeRouter, searchRouter, successRouter } from '../routers';

const router = Router();

router.use(homeRouter);
router.use(successRouter);
router.use(searchRouter);
router.use(contentRouter);

export { router };
