import { Application } from 'express';
const { version } = require('../../package.json');

export function exposeAppVersionMiddleware(app: Application) {
  app.use((req, res, next) => {
    res.locals.appVersion = version;
    next();
  });
}
