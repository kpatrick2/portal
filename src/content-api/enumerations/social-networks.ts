export enum ESocialNetworks {
  facebook = 'vdm-101-social-facebook',
  twitter = 'vdm-099-social-twitter',
  linkedin = 'vdm-098-social-linkedin',
  youtube = 'vdm-100-social-youtube'
}
