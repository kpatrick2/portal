export enum ETaxonomyName {
  TAXONOMY_BOROUGH_FR = 'arrondissements',
  TAXONOMY_BOROUGH_EN = 'boroughs',
  TAXONOMY_TITRE_ROLES_FR = 'titres-roles',
  TAXONOMY_TITRE_ROLES_EN = 'titres-roles',
  TAXONOMY_POLITICAL_PARTIES_FR = 'partis-politiques',
  TAXONOMY_POLITICAL_PARTIES_EN = 'political-parties'
}
