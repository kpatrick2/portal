export enum EImagePlaceholder {
  NOUVELLES = 'picto/Generique-Nouvelles.png',
  EVENEMENTS = 'picto/Generique-Evenement.png',
  PROJETS = 'picto/Generique-Projet.png',
  PROGRAMMES = 'picto/Generique-Programme.png'
}
