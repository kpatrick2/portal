import axios, { AxiosError, AxiosResponse } from 'axios';
import { Agent } from 'https';
import { config, Language, logger } from '../utils';
import { IContentType, IContentTypeSearch, IHomeContent, ITaxonomy } from './interfaces';

const contentApi = axios.create({
  baseURL: `${config.contentApiUrl}/api/communications/information/portal`,
  httpsAgent: new Agent({
    // SSL certificate is self-signed in dev
    rejectUnauthorized: !config.contentApiBypassCertificateErrors
  })
});

contentApi.interceptors.response.use(onResponseSuccess, onResponseError);

export async function getHome(language: Language): Promise<IHomeContent> {
  const response = await contentApi.get(`/v2/sites/portail-${language}/home`);
  return response.data;
}

interface ISearchOptions {
  term?: string;
  type?: string;
  filtersBy?: {
    key?: string;
    value?: string;
  }[];
  excludedFilters?: {
    key: string;
    value: string;
  }[];
  offset?: number;
  limit?: number;
  orderBy?: string;
}

export async function getEntries(language: Language, options: ISearchOptions): Promise<IContentTypeSearch> {
  const params: any = {
    noHtml: true
  };

  if (options.term) {
    params.q = options.term;
  }
  if (options.type) {
    params.type = options.type;
  }
  if (options.filtersBy) {
    options.filtersBy.forEach(filter => {
      if (filter.value) {
        params[`filters[${filter.key}]`] = filter.value;
      }
    });
  }
  if (options.excludedFilters) {
    options.excludedFilters.forEach(filter => {
      params[`excludedFilters[${filter.key}]`] = filter.value;
    });
  }
  if (options.offset) {
    params.offset = options.offset.toString();
  }
  if (options.limit) {
    params.limit = options.limit.toString();
  }
  if (options.orderBy) {
    params.orderBy = options.orderBy;
  }

  const response = await contentApi.get(`/v2/sites/portail-${language}/entries`, {
    params
  });
  return response.data;
}

export async function getEntry(language: Language, slug: string): Promise<IContentType> {
  const response = await contentApi.get(`/v2/sites/portail-${language}/entries/`, {
    params: {
      path: slug
    }
  });
  return response.data;
}

export async function getTaxonomy(language: Language, name: string): Promise<ITaxonomy[]> {
  const response = await contentApi.get(`/v1/sites/portail-${language}/taxonomies/${name}`);
  return response.data;
}

function onResponseSuccess(response: AxiosResponse) {
  logger.info(formatAxiosMessage(response));

  return response;
}

function onResponseError(error: AxiosError) {
  const details = error.response ? JSON.stringify(error.response.data, null, 2) : undefined;

  logger.error(formatAxiosMessage(error.response), details);

  return Promise.reject(error);
}

function formatAxiosMessage(response?: AxiosResponse) {
  if (!response) {
    return null;
  }

  const method = response.config.method ? `=> [${response.config.method.toUpperCase()}] ` : '';
  return `${method}${response.request.path} - ${response.status}`;
}
