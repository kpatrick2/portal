export interface IGeometry {
  type: string;
  coordinates: any[];
}

export interface IGeometryCollection {
  type: string;
  geometries: IGeometry[];
}

export interface IFeatureCollection {
  type: string;
  features: IFeature[];
}

export interface IFeature {
  type: string;
  geometry: IGeometry;
  properties?: {};
}
