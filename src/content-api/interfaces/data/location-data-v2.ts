import { IAddress, IAppelAction, IImage, ITaxoCode } from '../common-interfaces';
import { IBaseContentType } from '../content-type';
import { ContentTypes } from '../content-types';

export interface ILocationContentV2 extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: ContentTypes.Location;
  };
  mtl_content: {
    lieux: {
      localizationGps: number[];
      address: IAddress;
      access: ITaxoCode[];
      opening_hours_schedules: IHours[];
      category: ITaxoCode[];
      link: IAppelAction[];
      photo_gallery: IImage[];
      payment_mode: ITaxoCode[];
      amenities: ITaxoCode[];
      installations: IInstallation[];
      equipments: any[];
      contact_informations: IContactInformation;
    };
  };
}

export interface IHours {
  title: string;
  comments?: string;
  opening_hours_specification: IBlocHour[];
}

export interface IBlocHour {
  day_of_week: string;
  hours: IIntervalHour[];
  is_closed: boolean;
}

export interface IIntervalHour {
  startTime: string;
  endTime: string;
}
export interface IContactInformation {
  title?: string;
  address?: string;
  email?: string;
  phone?: string;
  phone_note?: string;
  fax?: string;
}
export interface IAmenity {
  title_amenity: ITaxoCode[];
  amenity: ITaxoCode[];
}
export interface IInstallation {
  title_installation: ITaxoCode[];
  installation: ITaxoCode[];
}
