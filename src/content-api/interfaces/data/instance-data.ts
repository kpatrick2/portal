import { ContentTypes, IAppelActionWebDiffusion, IBaseContentType, IBlocMember, IDocument } from '..';

export interface IInstanceContent extends IBaseContentType {
  dc_creator: string[];
  dc_subject: string[];
  sioc_topic: string[];
  dc_type: {
    title: string;
    machine_name: ContentTypes.Instance;
  };
  dc_format: string;
  dc_source: string;
  dc_language: string;
  dc_temporal?: any;
  dc_rights: string;
  dc_provenance: {
    system_name: string;
    content_type_machine_name: string;
    identifier: string;
  };
  mtl_content: {
    instances: IInstanceBloc;
  };
}

interface IInstanceBloc {
  webcasts?: IAppelActionWebDiffusion[];
  documents?: IDocument[][];
  members: IBlocMember[];
  instance_type: IInstanceType;
  [key: string]: any;
}

interface IInstanceType {
  code: string;
  name: string;
  records_per_page: string;
}
