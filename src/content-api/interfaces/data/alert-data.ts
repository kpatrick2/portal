import { IBaseContentType } from '../content-type';
import { ContentTypes } from '../content-types';

export interface IAlertContent extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: ContentTypes.Alert;
  };
  sioc_topic: string[];
  mtl_content: {
    alertes: {
      links?: {
        label: string;
        link: string;
      }[];
      advice?: string;
      topic: string;
      emergency: boolean;
      location?: {
        boroughs: string[];
        coordinates: any;
        showAddress: boolean;
        address: string;
      };
    };
  };
}
