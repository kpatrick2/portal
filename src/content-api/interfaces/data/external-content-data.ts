import { IBaseContentType } from '../content-type';
import { ContentTypes } from '../content-types';

export interface IExternalContentContent extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: ContentTypes.ExternalContent;
  };
  mtl_content: {
    contenus_externes: {};
  };
}
