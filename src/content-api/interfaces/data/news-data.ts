import { IVideo } from '..';
import { IImage } from '../common-interfaces';
import { IBaseContentType } from '../content-type';
import { ContentTypes } from '../content-types';

export interface INewsContent extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: ContentTypes.News;
  };
  mtl_content: {
    nouvelles: {
      photo_gallery: IImage[];
      video_gallery: IVideo[];
      nouvelle: INewsBodyPart[];
    };
  };
}

export type INewsBodyPart = INewsBodyTextPart | INewsBodyTitlePart | INewsBodyImagePart | INewsBodyVideoPart;

interface INewsBodyTextPart {
  text: string;
}

interface INewsBodyTitlePart {
  title: string;
}

interface INewsBodyImagePart {
  image: IImage[];
}

interface INewsBodyVideoPart {
  video: IVideo[];
}

function isNewsBodyTextPart(part: any): part is INewsBodyTextPart {
  return typeof part.text !== 'undefined';
}

function isNewsBodyTitlePart(part: any): part is INewsBodyTitlePart {
  return typeof part.title !== 'undefined';
}

function isNewsBodyImagePart(part: any): part is INewsBodyImagePart {
  return typeof part.image !== 'undefined';
}

function isNewsBodyVideoPart(part: any): part is INewsBodyVideoPart {
  return typeof part.video !== 'undefined';
}

export const newsBodyPartGuards = {
  isText: isNewsBodyTextPart,
  isTitle: isNewsBodyTitlePart,
  isImage: isNewsBodyImagePart,
  isVideo: isNewsBodyVideoPart
};
