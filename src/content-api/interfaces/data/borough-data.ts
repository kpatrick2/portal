import { IFeaturedContent, INodeReference, INodeReferenceWithIconV2, ISearchSection } from '../common-interfaces';
import { IBaseContentType } from '../content-type';
import { ContentTypes } from '../content-types';

export interface IBoroughContent extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: ContentTypes.Borough;
  };
  mtl_content: {
    vitrine_arrondissement: {
      message?: {
        title: string;
        text: string;
        call_to_action: {
          title?: string;
          type: string;
          url: string;
        };
      };
      featured_content: IFeaturedContent[];
      need_help: INodeReferenceWithIconV2[];
      important_dates: INodeReference[];
      search: ISearchSection;
      news: boolean;
      events: boolean;
      democratic_participation: {
        mayor: INodeReference;
        city_government: INodeReference;
      };
    };
  };
}
