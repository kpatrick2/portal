import { IAppelAction, IBaseContentType, IElectedFunction, IImage, IVideo } from '..';
import { IAddress } from '../common-interfaces';

export interface IElectedContent extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: string;
  };
  mtl_content: {
    elus: {
      courtesy_title: string | null;
      first_name: string | null;
      last_name: string | null;
      functions: IElectedFunction[];
      other_responsabilities: string[];
      political_party: string | null;
      district: string | null;
      title_contacts_information: string;
      contacts_information: IElectedCoordinates[];
      photo_gallery: string[];
      video_gallery: string[];
      social_networks?: IAppelAction[];
    };
  };
}

export interface IElectedCoordinates extends IAddress {
  title: string | null;
  email: string | null;
  phone: string | null;
  fax: string | null;
  phone_note: string | null;
}
export type IElectedBodyPart =
  | IElectedBodyTextPart
  | IElectedBodyTitlePart
  | IElectedBodyImagePart
  | IElectedBodyVideoPart;

interface IElectedBodyTextPart {
  text: string;
}

interface IElectedBodyTitlePart {
  title: string;
}

interface IElectedBodyImagePart {
  image: IImage[];
}

interface IElectedBodyVideoPart {
  video: IVideo[];
}

function isElectedBodyTextPart(part: any): part is IElectedBodyTextPart {
  return typeof part.text !== 'undefined';
}

function isElectedBodyTitlePart(part: any): part is IElectedBodyTitlePart {
  return typeof part.title !== 'undefined';
}

function isElectedBodyImagePart(part: any): part is IElectedBodyImagePart {
  return typeof part.image !== 'undefined';
}

function isElectedBodyVideoPart(part: any): part is IElectedBodyVideoPart {
  return typeof part.video !== 'undefined';
}

export const electedBodyPartGuards = {
  isText: isElectedBodyTextPart,
  isTitle: isElectedBodyTitlePart,
  isImage: isElectedBodyImagePart,
  isVideo: isElectedBodyVideoPart
};
