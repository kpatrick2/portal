import { IAppelAction, IContentReference, IDocument, IFileInformation } from '../common-interfaces';
import { IBaseContentType } from '../content-type';
import { ContentTypes } from '../content-types';

export interface IMaterial {
  title: string;
  material: string[];
}

export interface IRelatedLink {
  url: string;
  title: string;
  name: string;
  attributes: string[];
}

export interface IBlocCollect {
  boroughs: string[];
  description: string;
  accepted_material: IMaterial[];
  denied_material: IMaterial[];
  accepted_content: string[];
  service_links: IContentReference[];
  law_settlements: IDocument[];
  more_informations: IAppelAction[];
  documents: IFileInformation[];
  lieux: IContentReference[];
  [key: string]: any;
}

export interface ICollectContent extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: ContentTypes.Collect;
  };
  mtl_content: {
    collectes: IBlocCollect[];
  };
}
