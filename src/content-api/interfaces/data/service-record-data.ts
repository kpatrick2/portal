import { IAppelAction, IFileInformation, ILinkDescription } from '../common-interfaces';
import { IBaseContentType } from '../content-type';
import { ContentTypes } from '../content-types';

export interface IServiceRecordContent extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: ContentTypes.ServiceRecord;
  };
  mtl_content: {
    fiches_services: {
      bloc_fiche_service: IServiceRecordBloc[];
    };
  };
}

export interface IServiceRecordBloc {
  bloc_title: string;
  bloc_services: IServiceRecordBoroughBloc[];
}

export interface IServiceRecordBoroughBloc {
  arrondissement: string[];
  description: string;
  service_au_comptoir: boolean;
  service_au_telephone: boolean;
  service_en_ligne: boolean;
  service_par_courrier: boolean;
  datas_service_comptoir: IServiceRecordStep[];
  datas_service_telephone: IServiceRecordStep[];
  datas_service_online: IServiceRecordStep[];
  datas_service_courriel: IServiceRecordStep[];
  law_settlements?: ILinkDescription[];
  rates?: string;
  requis_title?: string;
  requis?: string;
  documents?: IFileInformation[];
  liens_connexes?: ILinkDescription[];
}

export interface IServiceRecordStep {
  title?: string;
  description: string;
  call_to_action?: IAppelAction[];
  payment_method?: string;
}
