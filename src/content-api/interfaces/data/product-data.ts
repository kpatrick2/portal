import { IContentReference, IFileInformation } from '../common-interfaces';
import { IBaseContentType } from '../content-type';
import { ContentTypes } from '../content-types';

export interface IProductContent extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: ContentTypes.Product;
  };
  mtl_content: {
    produits: {
      tags: string[];
      more_informations: IContentReference[];
      documents: IFileInformation[];
    };
  };
}
