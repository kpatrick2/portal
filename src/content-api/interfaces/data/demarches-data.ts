import { IAppelAction, IDocument, ITable } from '..';
import { IFileInformation } from '../common-interfaces';
import { IBaseContentType } from '../content-type';

export interface IDemarchesContent extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: string;
  };
  mtl_content: {
    demarches: {
      body: IDemarchesBodyPart[];
      by_laws?: IDocument[];
      related_documents?: IFileInformation[];
    };
  };
}

export interface IDemarchesBodyPart {
  boroughs: string[];
  description_title: string;
  description: string;
  requirement_title: string;
  requirement: string;
  service_walk_in: IDemarchesBodyServiceStep;
  service_telephone: IDemarchesBodyServiceStep;
  service_online: IDemarchesBodyServiceStep;
  service_mail: IDemarchesBodyServiceStep;
  service_email: IDemarchesBodyServiceStep;
  service_fax: IDemarchesBodyServiceStep;
  documents?: ISupportDocuments[];
  rates?: IDemarchesBodyServiceRate[];
  [key: string]: any;
}

export interface IDemarchesBodyServiceStep {
  title: string;
  description: string;
  payment_mode: string;
  call_to_action: IAppelAction[];
  address?: IDemarchesAddress[];
  documents?: any[];
}

export interface IDemarchesAddress {
  title: string;
  address_1: string;
  address_2: string;
  city: string;
  province: string;
  postal_code: string;
}

interface IDemarchesBodyServiceRate {
  service_rate: {
    title: string;
    condition: string;
    table?: ITable[];
  };
}

export interface ISupportDocuments {
  title?: string;
  comments?: string;
  supporting_document?: string[];
}
