import { IAppelActionGroup, IAppelActionWebDiffusion, IBlocMember, IDocument } from '..';
import { IBaseContentType } from '../content-type';
import { ContentTypes } from '../content-types';

export interface ICommissionContent extends IBaseContentType {
  dc_creator: string[];
  dc_subject: string[];
  sioc_topic: string[];
  dc_type: {
    title: string;
    machine_name: ContentTypes.Commission;
  };
  dc_format: string;
  dc_source: string;
  dc_language: string;
  dc_temporal?: any;
  dc_rights: string;
  dc_provenance: {
    system_name: string;
    content_type_machine_name: string;
    identifier: string;
  };
  mtl_content: {
    commissions: {
      webcasts?: IAppelActionWebDiffusion[];
      call_to_action?: IAppelActionGroup[];
      members: IBlocMember[];
      documents?: IDocument[][];
    };
  };
}
