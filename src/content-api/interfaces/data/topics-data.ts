import {
  IAdminContact,
  IAppelAction,
  IBoroughRate,
  IDocument,
  IGlobalRates,
  IImage,
  ISocialMedia,
  ITable,
  IVideoV2
} from '..';
import { IBaseContentType } from '../content-type';
import { ContentTypes } from '../content-types';

export interface ITopicsContent extends IBaseContentType {
  dc_creator: string[];
  dc_subject: string[];
  sioc_topic: string[];
  dc_type: {
    title: string;
    machine_name: ContentTypes.Commission;
  };
  dc_format: string;
  dc_source: string;
  dc_language: string;
  dc_temporal?: any;
  dc_rights: string;
  dc_provenance: {
    system_name: string;
    content_type_machine_name: string;
    identifier: string;
  };
  mtl_content: {
    sujets: {
      body?: ITopicsBodyPart[];
      rates?: ITopicRatesPart[];
      call_to_action?: IAppelAction[];
      by_laws?: IDocument[];
      social_medias?: ISocialMedia[];
      contacts_information?: IAdminContact[];
    };
  };
}

export type ITopicRatesPart = ITopicsGlobalRatePart | ITopicsBoroughtRatePart;
export type ITopicsBodyPart =
  | ITopicsBodyTextPart
  | ITopicsBodyTitlePart
  | ITopicsBodyImagePart
  | ITopicsBodyVideoPart
  | ITopicsBodyDocumentPart
  | ITopicsBodyExternalReferencePart
  | ITopicsBodyTablePart;
interface ITopicsBodyTextPart {
  text: string;
}

interface ITopicsBodyTitlePart {
  title: string;
}
interface ITopicsBodyTextPart {
  text: string;
}

interface ITopicsBodyTitlePart {
  title: string;
}

interface ITopicsBodyImagePart {
  image: IImage[];
}

interface ITopicsBodyVideoPart {
  video: IVideoV2[];
}

interface ITopicsBodyDocumentPart {
  document: IDocument[];
}

interface ITopicsBodyExternalReferencePart {
  external_reference: IAppelAction[];
}

interface ITopicsBodyTablePart {
  table: ITable[];
}

interface ITopicsGlobalRatePart {
  global_rate: IGlobalRates;
}

interface ITopicsBoroughtRatePart {
  borough_rate: IBoroughRate;
}

function isGlobalRate(part: any): part is ITopicsGlobalRatePart {
  return part && part.global_rate ? true : false;
}
function isBoroughRate(part: any): part is ITopicsBoroughtRatePart {
  return part && part.borough_rate ? true : false;
}

function isTopicBodyTextPart(part: any): part is ITopicsBodyTextPart {
  return part && part.text ? true : false;
}
function isTopicBodyTitlePart(part: any): part is ITopicsBodyTitlePart {
  return part && part.title ? true : false;
}

function isTopicBodyImagePart(part: any): part is ITopicsBodyImagePart {
  return part && part.image ? true : false;
}

function isTopicBodyVideoPart(part: any): part is ITopicsBodyVideoPart {
  return part && part.video ? true : false;
}

function isTobicBodyTablePart(part: any): part is ITopicsBodyTablePart {
  return part && part.table ? true : false;
}

export const topicBodyPartGuards = {
  isText: isTopicBodyTextPart,
  isTitle: isTopicBodyTitlePart,
  isImage: isTopicBodyImagePart,
  isVideo: isTopicBodyVideoPart,
  isTable: isTobicBodyTablePart
};
export const topicRatePartGuards = {
  isBorough: isBoroughRate,
  isGlobal: isGlobalRate
};
