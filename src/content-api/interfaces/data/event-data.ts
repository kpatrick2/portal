import {
  IAddress,
  IAppelAction,
  IContact,
  IFileInformation,
  IImage,
  INodeReference,
  ITaxoCode,
  IVideo
} from '../common-interfaces';
import { IBaseContentType } from '../content-type';

// arrondissement: string[];

export interface IEventContent extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: string;
  };
  dc_spatial?: {
    type: string;
    coordinates: [number, number];
  };
  dc_temporal?: ITemporal;
  mtl_content: {
    evenements: {
      contacts_information?: IContact[];
      event_information: IEventInformation;
      event_type: string;
      target_audience: string[];
      status?: ITaxoCode;
      admission_type?: string;
      documents?: IFileInformation[];
      social_networks?: IAppelAction[];
      rates?: ITaxoCode;
      partners?: string[];
      photo_gallery: IImage[];
      video: IVideo;
    };
  };
}

export interface ITemporal {
  name: string;
  start: string;
  end: string;
}

export interface IEventInformation {
  address?: IAddress;
  event_location?: INodeReference;
}
