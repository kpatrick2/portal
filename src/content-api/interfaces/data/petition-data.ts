import { IBaseContentType } from '../content-type';

// arrondissement: string[];

export interface IPetitionContent extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: string;
  };
  mtl_content: {
    petitions: {
      internalStatus: string;
      type: string;
      signatureCounter: {
        signaturesCount: number;
        signaturesNeeded: number;
      };
      publication: {
        publicationStart: IPetitionPublicationDate;
        publicationEnd: IPetitionPublicationDate;
      };
    };
  };
}

export interface IPetitionPublicationDate {
  createdAt: string;
  createdBy: {
    inum: string;
    name: string;
  };
  lastModifiedAt: string;
  lastModifiedBy: {
    inum: string;
    name: string;
  };
}
