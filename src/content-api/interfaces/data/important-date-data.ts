import { IAppelAction, ITimeSpan } from '../common-interfaces';
import { IBaseContentType } from '../content-type';
import { ContentTypes } from '../content-types';

export interface IImportantDateContent extends IBaseContentType {
  dc_abstract: string;
  dc_type: {
    title: string;
    machine_name: ContentTypes.ImportantDate;
  };
  mtl_content: {
    dates_importantes: {
      call_to_action: IAppelAction;
      timespan: ITimeSpan;
    };
  };
}
