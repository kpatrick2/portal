import {
  IAlertContent,
  IBoroughContent,
  ICollectContent,
  ICommissionContent,
  IContactPage,
  IDemarchesContent,
  IElectedContent,
  IEventContent,
  IExternalContentContent,
  IHomeContent,
  IHubContent,
  IInstanceContent,
  ILocationContent,
  INewsContent,
  IPetitionContent,
  IProductContent,
  IServiceRecordContent,
  ITopicsContent
} from '.';

export type IContentType =
  | IAlertContent
  | IBoroughContent
  | IExternalContentContent
  | IHomeContent
  | IHubContent
  | IInstanceContent
  | ICommissionContent
  | ILocationContent
  | INewsContent
  | IElectedContent
  | IServiceRecordContent
  | IProductContent
  | IContactPage
  | IDemarchesContent
  | IEventContent
  | IPetitionContent
  | ITopicsContent
  | any
  | ICollectContent;
