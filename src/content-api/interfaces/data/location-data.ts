import { IPhone } from '../common-interfaces';
import { IBaseContentType } from '../content-type';
import { ContentTypes } from '../content-types';

export interface ILocationContent extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: ContentTypes.Location;
  };
  mtl_content: {
    lieux: {
      locationName: ILocationLocalizedField;
      localizationGps: number[];
      category: {
        name: ILocationLocalizedField;
      };
      address: ILocationAddress;
      extension: {
        phones: IPhone[];
        services: ILocationService[];
        universal_accessibility: {
          location: boolean;
          services: boolean;
        };
        booking: {
          phones: IPhone[];
          possible: boolean;
          website?: string;
        };
        free_fees?: boolean;
        fees?: {
          website?: ILocationLocalizedField;
          payBy: ILocationPaymentMethod[];
          price?: ILocationLocalizedField;
          description?: ILocationLocalizedField;
          group?: ILocationLocalizedField;
        };
        email?: string;
        website?: string;
        capacity?: string;
        condition_admission?: ILocationLocalizedField;
        social_medias?: {
          facebook?: string;
          twitter?: string;
          youtube?: string;
        };
        openHours?: ILocationOpenHours;
      };
    };
  };
}

export interface ILocationLocalizedField {
  fr: string;
  en: string;
}

export interface ILocationWeeklySchedule {
  dayOfWeek: number;
  startTime: string;
  endTime: string;
  note?: ILocationLocalizedField;
}

export interface ILocationOpenHoursSchedulesData {
  name: string;
  startDate: string;
  endDate?: string;
  weeklySchedules?: ILocationWeeklySchedule[];
}

export interface ILocationOpenHours {
  temporarilyClosed: boolean;
  site?: ILocationLocalizedField;
  remark?: ILocationLocalizedField;
  closureReason?: ILocationLocalizedField;
  schedules?: ILocationOpenHoursSchedulesData[];
}

export interface ILocationAddress {
  borough: {
    name: ILocationLocalizedField;
  };
  civicNumber?: number;
  extra?: string;
  street?: string;
  city: string;
  zipCode?: string;
  province?: string;
}

export enum ILocationPaymentMethod {
  Cash = 'cash',
  Debit = 'interac',
  Visa = 'visa',
  Mastercard = 'mastercard',
  AmericanExpress = 'americanExpress'
}

export enum ILocationService {
  AirConditioning = 'air_climatise',
  Lifts = 'ascenseurs',
  Parking = 'stationnement',
  ParkingOnStreet = 'stationnement_rue',
  FreeWifi = 'wifi_gratuit',
  Barbecue = 'barbecues',
  FoodDistributor = 'distributeur_alimentaire',
  DrinksDistributor = 'distributeur_boisson',
  Snacks = 'casse_croute',
  DiningRoom = 'salle_manger',
  Restaurant = 'restaurant',
  ChangeableTables = 'table_langer',
  NursingRoom = 'salle_allaitement',
  FamilyCloakroom = 'vestiaire_familiale',
  HalteFamily = 'halte_famille',
  Bathroom = 'toilettes',
  Showers = 'douches',
  Cloakroom = 'vestiaire',
  SportsCloakroom = 'vestiaire_sport',
  TeamCloakroom = 'vestiaire_equipe',
  Sauna = 'sauna',
  Spa = 'spa'
}
