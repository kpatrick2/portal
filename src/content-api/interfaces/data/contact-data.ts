import { IBaseContentType } from '../content-type';
import { ContentTypes } from '../content-types';

export interface IContactPage extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: ContentTypes.Alert;
  };
  sioc_topic: string[];
  mtl_content: {
    pages_speciales: {
      display_type: 'Page standard' | 'Cards';
      body: [
        {
          code: string;
          text: string;
        }
      ];
      return_link: string[];
    };
  };
}

export interface IContactPageBodyBloc {
  primary?: string;
  label?: string;
  value?: string;
  action?: string;
}
