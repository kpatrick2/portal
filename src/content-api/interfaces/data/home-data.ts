import { IImage, INodeReferenceWithIcon, IParagraph2 } from '../common-interfaces';
import { IBaseContentType } from '../content-type';
import { ContentTypes } from '../content-types';

interface IPromition {
  title: string;
  description: string;
  image: IImage[];
  call_to_action: {
    title?: string;
    type?: string;
    url?: string;
  };
}

export interface IHomeContent extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: ContentTypes.Home;
  };
  mtl_content: {
    accueil: {
      display_title: string;
      anchor_label: string;
      popular_services_title: string;
      popular_services: INodeReferenceWithIcon[];
      paragraph_2: IParagraph2[];
      text_promo: IPromition[];
    };
  };
}
