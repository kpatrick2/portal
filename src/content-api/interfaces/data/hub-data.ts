import { INodeReference, INodeReferenceWithIcon, IParagraph2 } from '../common-interfaces';
import { IBaseContentType } from '../content-type';
import { ContentTypes } from '../content-types';

export interface IHubContent extends IBaseContentType {
  dc_type: {
    title: string;
    machine_name: ContentTypes.Hub;
  };
  mtl_content: {
    collectrices: {
      subtitle?: string;
      icon?: string;
      collectrice_para_1: INodeReferenceWithIcon[];
      para2_title?: string;
      collectrice_para_2: IParagraph2[];
      para3_title?: string;
      collectrice_para_3: INodeReferenceWithIcon[];
      para4_title?: string;
      collectrice_para_4: IParagraph4[];
    };
  };
}

export interface IParagraph4 {
  title: string;
  referenced_nodes?: {
    0?: INodeReference;
    description?: string;
  }[];
}
