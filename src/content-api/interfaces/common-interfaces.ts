import { IContentTypeSearch, ITaxonomy } from '.';
import { ETaxonomyName } from '../enumerations/taxonomy-name';
import { ISupportDocuments } from './data/demarches-data';

export interface IImage {
  url: string;
  id: string;
  file_name: string;
  alt: string;
  title: string;
  width: string;
  height: string;
}
export interface IAddress {
  address: string;
  address2: string;
  city: string;
  province: string;
  country: string;
  postal_code: string;
}
export interface IVideo {
  url: string;
  name: string;
}
export interface IVideoV2 {
  url: string;
  title: string;
  type: string;
}

export interface IUrl {
  uri: string;
  title: string;
}

export interface IParagraph2 {
  content_type: string;
  display_type: 'List' | 'List+map';
  sort_on?: string;
  sort?: 'Desc' | 'Asc';
  records_per_page?: number;
  pager: number;
  filters: IFilter[];
}

export interface IFilter {
  default_value: string;
  search_field: string;
  exposable: boolean;
  used_taxonomy: ETaxonomyName;
  label: string;
}

export interface ITaxonomyFilterWrapper {
  taxonomy: ITaxonomy[];
  label: string;
  taxoName: ETaxonomyName;
  query_param: string;
}

export interface INodeReferenceWithIcon {
  referenced_nodes?: INodeReference[];
  external_references?: INodeReference[];
  icon?: string;
  description?: string;
}

export interface INodeReferenceWithIconV2 {
  call_to_action: INodeReference;
  icon?: string;
}

export interface INodeReference {
  title: string;
  type: string;
  url: string;
}

export interface IFeaturedContent {
  title: string;
  text: string;
  call_to_action: IAppelAction;
  picture: IImage;
}

export interface IPhone {
  type?: string;
  number: string;
  extension?: string;
}

export interface IContentReference {
  title: string;
  type: string;
  url: string;
}

export interface IFileInformation {
  url: string;
  id: string;
  file_name: string;
  description?: string;
}

export interface ILinkDescription {
  url: string;
  name: string;
}
export interface ISortData {
  sortBy: string;
  sortOrder: 'Asc' | 'Desc';
}
export interface IGeoPoint {
  type: string;
  coordinates: number[];
}

export interface IBlocMember {
  elected_name: {
    title: string;
    type: string;
    url: string;
  }[];
  functions: IElectedFunction[];
  responsabilities?: string[];
}

export interface IDocument {
  title: string;
  type: string;
  url: string;
}

export interface IAppelAction {
  title: string;
  type?: string;
  url: string;
}

export interface ITable {
  header_type: string;
  data: string[][];
}

export interface IAppelActionWebDiffusion {
  call_to_action?: IAppelAction[];
  image?: IImage[][];
  title?: string;
  text?: string;
}

export interface IAppelActionGroup {
  title: string;
  text: string;
  call_to_action: IAppelAction[];
}

export interface ISocialMedia {
  bloc_title: string;
  data: IAppelAction[];
}

// rates
export interface IGlobalRates extends IBoroughRate {
  rate?: string;
  rate_currency?: string;
}

export interface IBoroughRate {
  title?: string;
  condition?: string;
  approach?: IAppelAction[];
  rate_details?: string;
}
export interface IAdminContact extends IAddress {
  service_name?: string;
  contact_name?: string;
  website_name?: string;
  website_url?: string;
  email?: string;
  phone?: string;
  phone_note?: string;
  fax?: string;
}

export interface IContact {
  service_name?: string;
  contact_name?: string;
  title?: string;
  email: string;
  fax?: string;
  phones?: [
    {
      phone: string;
      phone_note: string;
    }
  ];
  webcast?: [
    {
      title: string;
      type: string;
      url: string;
    }
  ];
}

export interface IItem {
  label: string;
  value: string;
}

export interface IExtraHub {
  search: IContentTypeSearch;
  taxoLabel?: ITaxonomyFilterWrapper[];
}

export interface IReqParams {
  [key: string]: string;
  value: any;
}

export interface IElectedFunction {
  code: string;
  label: string;
}

export interface ISearchSection {
  picture: IImage;
  suggested_links: IAppelAction[];
}

export interface ITaxoCode {
  code: string;
  name: string;
}

export interface ITimeSpan {
  type: string;
  start_date?: string;
  end_date?: string;
}

export type IGlobalDocument = IDocument | IFileInformation | ISupportDocuments;

function isFileInformation(files: any[]): files is IFileInformation[] {
  return typeof files[0].file_name !== 'undefined';
}

function isDocument(files: any[]): files is IDocument[] {
  return typeof files[0].title !== 'undefined';
}

function isSupportDocument(files: any[]): files is ISupportDocuments[] {
  return typeof files[0].supporting_document !== 'undefined';
}

export const documentGuards = {
  isFileInfo: isFileInformation,
  isDoc: isDocument,
  isSupportDoc: isSupportDocument
};

export interface ICloudinaryImageTransformations {
  width?: number;
  height?: number;
  ar?: number | string;
  crop?:
    | 'scale'
    | 'fit'
    | 'mfit'
    | 'fill'
    | 'lfill'
    | 'limit'
    | 'pad'
    | 'lpad'
    | 'mpad'
    | 'crop'
    | 'thumb'
    | 'imagga_crop'
    | 'imagga_scale';
  f_auto?: boolean;
  q_auto?: 'q_auto' | 'q_auto:best' | 'q_auto:good' | 'q_auto:eco' | 'q_auto:low';
}
