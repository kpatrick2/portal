export enum ContentTypes {
  Collect = 'collectes',
  Alert = 'alertes',
  Borough = 'vitrine_arrondissement',
  ExternalContent = 'contenus_externes',
  Home = 'accueil',
  Hub = 'collectrices',
  ImportantDate = 'dates_importantes',
  Instance = 'instances',
  Location = 'lieux',
  News = 'nouvelles',
  Elected = 'elus',
  ServiceRecord = 'fiches_services',
  Product = 'produits',
  Commission = 'commissions',
  Topic = 'sujets',
  Demarche = 'demarches',
  Event = 'evenements',
  SpecialPage = 'pages_speciales'
}

export enum ContentTypesParagraph {
  Collect = 'collectes',
  Alert = 'alertes',
  ExternalContent = 'recherche_contenu_externe',
  Home = 'page_accueil',
  Hub = 'collectrices',
  Location = 'lieux',
  News = 'nouvelle',
  Elected = 'elu',
  ServiceRecord = 'fiche_de_service',
  Product = 'produit',
  Instance = 'instance',
  Commission = 'comission',
  Topic = 'sujets',
  Petition = 'petitions'
}

// using dc_hasVersion.fr value
export enum ContentTypesSpecialPages {
  Contact = '/communiquer-avec-la-ville'
}
