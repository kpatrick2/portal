export interface ITaxonomy {
  name: string;
  field_code_3l?: string;
}
