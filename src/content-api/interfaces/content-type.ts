import { IImage } from './common-interfaces';
import { IContentType } from './data';

export interface IBaseContentType {
  dc_identifier: string;
  dc_title: string;
  dc_description: string;
  dc_abstract?: string;
  dc_created: string;
  dc_language: string;
  dc_modified: string;
  dc_issued: string;
  dc_valid: {
    name: string;
    start: string;
    end: string;
  };
  dc_type: IType;
  dc_hasVersion: {
    en?: string;
    fr: string;
  };
  dc_coverage: {
    addresses: string[];
    boroughs: string[];
    municipalities: any;
    locations: any;
  };
  dc_relation: {
    title: string;
    type: string;
    url: string;
  }[];
  dc_provenance: {
    system_name: string;
    content_type_machine_name: string;
    identifier: string;
  };
  dc_spatial?: any;
  dc_isPartOf: ISection[];
  mtl_images: IImage[];
}

export interface IContentTypeSearch {
  total: number;
  offset: number;
  limit: number;
  entries: IContentType[];
}

export interface ISection {
  title: string;
  type: string;
  url: string;
}

export interface IType {
  title: string;
  machine_name: string;
}
