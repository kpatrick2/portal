import { ContentTypesParagraph } from '.';
import { assertNever, Language } from '../utils';

export function mapContentTypes(language: Language, contentType: ContentTypesParagraph): string {
  if (!contentType || contentType === ContentTypesParagraph.Home) {
    return '';
  }

  if (language === 'en') {
    return mapEnglishContentTypes(contentType);
  }

  return mapFrenchContentTypes(contentType);
}

function mapEnglishContentTypes(contentType: ContentTypesParagraph): string {
  if ((contentType as string) === 'alertes') {
    return 'alerts';
  }

  // tslint:disable-next-line:switch-default
  // tslint:disable-next-line:cyclomatic-complexity
  switch (contentType) {
    case ContentTypesParagraph.Alert:
      return 'alerts';

    case ContentTypesParagraph.ExternalContent:
      return 'contenus-externes';

    case ContentTypesParagraph.Hub:
      return 'collectrices';

    case ContentTypesParagraph.Location:
      return 'locations';

    case ContentTypesParagraph.News:
      return 'news';

    case ContentTypesParagraph.ServiceRecord:
      return 'fiches-services';

    case ContentTypesParagraph.Product:
      return 'produits';

    case ContentTypesParagraph.Collect:
      return 'collectes';

    case ContentTypesParagraph.Instance:
      return 'instances';

    case ContentTypesParagraph.Elected:
      return 'elus';

    case ContentTypesParagraph.Commission:
      return 'commissions';

    case ContentTypesParagraph.Topic:
      return 'sujets';

    case ContentTypesParagraph.Petition:
      return 'petitions';

    case ContentTypesParagraph.Home:
      throw new Error("Home can't be searched");

    default:
      return assertNever(contentType);
  }
}

function mapFrenchContentTypes(contentType: ContentTypesParagraph): string {
  if ((contentType as string) === 'alertes') {
    return 'alertes';
  }

  // tslint:disable-next-line:switch-default
  // tslint:disable-next-line:cyclomatic-complexity
  switch (contentType) {
    case ContentTypesParagraph.Alert:
      return 'alertes';

    case ContentTypesParagraph.ExternalContent:
      return 'contenus-externes';

    case ContentTypesParagraph.Hub:
      return 'collectrices';

    case ContentTypesParagraph.Location:
      return 'lieux';

    case ContentTypesParagraph.News:
      return 'nouvelles';

    case ContentTypesParagraph.ServiceRecord:
      return 'fiches-services';

    case ContentTypesParagraph.Product:
      return 'produits';

    case ContentTypesParagraph.Collect:
      return 'collectes';

    case ContentTypesParagraph.Home:
      throw new Error("Home can't be searched");

    case ContentTypesParagraph.Instance:
      return 'instances';

    case ContentTypesParagraph.Elected:
      return 'elus';
    case ContentTypesParagraph.Commission:
      return 'commissions';
    case ContentTypesParagraph.Topic:
      return 'sujets';
    case ContentTypesParagraph.Petition:
      return 'petitions';
    default:
      return assertNever(contentType);
  }
}
