import * as $ from 'jquery-validation';
import { analytics } from './analytics';
// tslint:disable-next-line:max-func-body-length
export function validateWufooForm() {
  resetValues();
  $('#form19').validate({
    // tslint:disable-next-line:no-empty
    showErrors: (errorMap, errorList) => {},
    rules: {
      // first name
      Field3: {
        required: true,
        normalizer: (value: string) => {
          return $.trim(value);
        },
        maxlength: 255,
        minlength: 2
      },
      // last name
      Field4: {
        required: true,
        maxlength: 255,
        minlength: 2,
        normalizer: (value: string) => {
          return $.trim(value);
        }
      },
      // email
      Field5: {
        required: true,
        email: true
      },
      // subject
      Field6: {
        maxlength: 255
      },
      // message
      Field7: {
        required: true,
        maxlength: 255,
        normalizer: (value: string) => {
          return $.trim(value);
        }
      },
      // Language
      Field9: {
        maxlength: 255
      }
    },
    submitHandler: form => {
      analytics.form(document.title, 'page_vue', 'Communiquer avec la Ville', 'contact', 'completee', '2', 'courriel');
      form.submit();
    }
  });
  const fields = {
    Field3: 'Prénom',
    Field4: 'Nom de famille',
    Field5: 'Courriel',
    Field6: 'Sujet',
    Field7: 'Message',
    Field9: 'Langue'
  };
  $('#form19 input, #form19 textarea').keyup(evt => {
    if ($('#form19').valid()) {
      $('#saveForm').prop('disabled', false);
    } else {
      $('#saveForm').prop('disabled', 'disabled');
    }
  });
  $('#form19 input[name=Field3]').keyup(evt => {
    const field3Val = $('#form19 input[name=Field3]').val() as string;
    if ($.trim(field3Val).length <= 1) {
      $('#form19 input[name=Field3]').addClass('is-invalid');
      // $('#fisrtNameReq').show();
      sendErrorAnalytics(fields.Field3, 'required');
    } else {
      $('#form19 input[name=Field3]').removeClass('is-invalid');
      // $('#fisrtNameReq').hide();
    }
  });
  $('#form19 input[name=Field4]').keyup(evt => {
    const field4Val = $('#form19 input[name=Field4]').val() as string;
    if ($.trim(field4Val).length <= 1) {
      $('#form19 input[name=Field4]').addClass('is-invalid');
      // $('#lastNameReq').show();
      sendErrorAnalytics(fields.Field4, 'required');
    } else {
      $('#form19 input[name=Field4]').removeClass('is-invalid');
      // $('#lastNameReq').hide();
    }
  });
  $('#form19 input[name=Field5]').keyup(evt => {
    resetEmailArea();
    const field5Val: any = $('#form19 input[name=Field5]').val();
    if (field5Val === '') {
      $('#form19 input[name=Field5]').addClass('is-invalid');
      // $('#emailReq').show();
      sendErrorAnalytics(fields.Field5, 'required');
    } else {
      $('#form19 input[name=Field5]').removeClass('is-invalid');
      // $('#emailReq').hide();
      if (!validate_email(field5Val)) {
        $('#form19 input[name=Field5]').addClass('is-invalid');
        $('#saveForm').prop('disabled', 'disabled');
        // $('#emailValid').show();
        sendErrorAnalytics(fields.Field5, 'pattern');
      } else {
        $('#form19 input[name=Field5]').removeClass('is-invalid');
        // $('#emailValid').hide();
      }
    }
  });
  $('#form19 textarea[name=Field7]').keyup(evt => {
    const field7Val = $('#form19 textarea[name=Field7]').val() as string;
    if ($.trim(field7Val) === '') {
      $('#form19 textarea[name=Field7]').addClass('is-invalid');
      // $('#messageReq').show();
      sendErrorAnalytics(fields.Field7, 'pattern');
    } else {
      $('#form19 textarea[name=Field7]').removeClass('is-invalid');
      // $('#messageReq').hide();
    }
  });
}
function validate_email(email: string) {
  const emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  /* Test the email given against the expression and return the result. */
  return emailRegex.test(email);
}
function resetEmailArea() {
  $('#divEmail').removeClass('is-invalid');
  $('#emailReq').hide();
  $('#emailValid').hide();
}
function resetValues() {
  $('#saveForm').prop('disabled', 'disabled');
  $('#divFirstName').removeClass('is-invalid');
  $('#divLastName').removeClass('is-invalid');
  $('#divEmail').removeClass('is-invalid');
  $('#divEmail').removeClass('is-invalid');
  $('#divMessage').removeClass('is-invalid');
  $('#fisrtNameReq').hide();
  $('#lastNameReq').hide();
  $('#emailReq').hide();
  $('#emailValid').hide();
  $('#messageReq').hide();
  $('#form19')
    .find('input:text, select, textarea')
    .val('');
  $('#form19 input[name=Field5]').val('');
  resetEmailArea();
}
function sendErrorAnalytics(field: string, errorType: string) {
  let message = '';
  switch (errorType) {
    case 'required':
      message = `${field} est requis`;
      break;
    case 'pattern':
      message = `${field} n'a pas le bon format`;
      break;
    case 'maxLength':
      message = `${field} est trop long`;
      break;
    default:
      message = `${field} est invalide`;
      break;
  }
  analytics.formError(message, '');
}
