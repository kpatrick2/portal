import * as $ from 'jquery-validation';
import * as PhotoSwipe from 'photoswipe';
import * as PhotoSwipeUiDefault from 'photoswipe/dist/photoswipe-ui-default';

export function registerImageGalleryLinks() {
  $('.js-gallery-trigger').click(() => {
    openPhotoSwipe();
  });
}

function openPhotoSwipe() {
  $('.media-gallery').each(function() {
    const $pic = $(this);
    const getItems = () => {
      const ritems: any[] = [];
      $pic.find('a').each(function() {
        try {
          const $href = $(this).attr('href');
          const $size = $(this)
            .data('size')
            .split('x');

          const $width = $size[0];
          const $height = $size[1];

          const item = {
            src: $href,
            w: $width,
            h: $height,
            author: $(this).data('author'),
            title: $(this).data('title')
          };
          ritems.push(item);
        } catch (e) {
          // tslint:disable-next-line:no-console
          console.log(e);
        }
      });
      return ritems;
    };

    const items = getItems();

    const $pswp = $('.pswp')[0];
    let author = '';
    const options = {
      index: 0,
      showHideOpacity: true,
      shareEl: false,
      addCaptionHTMLFn: (item: any, captionEl: any, isFake: any) => {
        if (!item.title) {
          captionEl.children[0].innerText = '';
          return false;
        }

        if (item.author) {
          author = '<br/><small>Photo: ' + item.author + '</small>';
        }
        captionEl.children[0].innerHTML = item.title + author;
        return true;
      }
    };
    // Initialize PhotoSwipe
    const lightBox = new PhotoSwipe($pswp, PhotoSwipeUiDefault, items, options);
    lightBox.init();
  });
}
