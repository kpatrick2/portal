import * as bowser from 'bowser';
import * as $ from 'jquery-validation';
import { getQueryStringParameter } from '../utils/url-helpers';

export function registerAnchorSmoothScrolling(scrollId?: string) {
  if (bowser.msie) {
    return;
  }
  const anchorLinks = document.querySelectorAll('a[data-smooth-scrolling], .js-scroll-trigger');

  for (let i = 0; i < anchorLinks.length; i += 1) {
    const anchorLink = anchorLinks.item(i);
    anchorLink.addEventListener('click', scrollToAnchorFromClick(anchorLink));
  }

  const hash = location.hash;
  if (scrollId) {
    scrollToAnchor(scrollId, document.getElementsByClassName('nav-anchors')[0]);
  } else if (hash) {
    scrollToAnchor(hash, document.getElementsByClassName('nav-anchors')[0]);
  }
}

function scrollToAnchor(targetId: string, originEl: Element) {
  let id;
  if (location.href.includes('collectes') || location.href.includes('collections')) {
    if (location.href.includes('arrondissement=')) {
      let borough = decodeURIComponent(getQueryStringParameter(location.href, 'arrondissement')!).replace(/ /g, '-');
      if (borough.includes('Île-Bizard')) {
        borough = 'ile-bizard';
      }
      id = targetId.replace('#', '') + '-' + borough;
    } else {
      id = targetId.replace('#', '') + '-VilledeMontréal';
    }
  } else {
    id = targetId.replace('#', '');
  }

  try {
    // Scroll to the anchor
    const scrollTo = $('#' + id)!;
    if (scrollTo) {
      const positionScrollTo = getPosition(scrollTo[0]).y;
      const navAnchor = $(originEl).closest('.nav-anchors');
      let navAnchorHeight: number = 0;
      if (navAnchor.length > 0 && positionScrollTo) {
        navAnchorHeight = navAnchor[0].clientHeight;
      }

      $('html, body').animate(
        {
          scrollTop: scrollTo.offset()!.top - navAnchorHeight
        },
        1000
      );
    }
  } catch (e) {
    // scroll element not found
  }
}

function scrollToAnchorFromClick(link: Element) {
  return (evt: Event) => {
    evt.preventDefault();

    const name = link.attributes.getNamedItem('href');
    // Get node ID by removing the '#' from the href
    if (name) {
      const id = getHashFromUrl(name.value);
      // Change the URL hash to link directly to the anchor
      replaceHash(id);

      scrollToAnchor(id, link);
    }
  };
}

function getHashFromUrl(url: string) {
  const urlParts = url.split('#');
  return urlParts[urlParts.length - 1];
}

function replaceHash(newHash: string) {
  const hash = newHash.charAt(0) !== '#' ? `#${newHash}` : newHash;
  let search = '';
  if (location.search) {
    search = location.search;
  }
  history.replaceState('', '', location.pathname + search + hash);
}

// recursively get top offset of each nested div to get total top offset
function getPosition(entryElement: any) {
  let xPosition = 0;
  let yPosition = 0;
  let element = entryElement;

  while (element) {
    xPosition += element.offsetLeft - element.scrollLeft + element.clientLeft;
    yPosition += element.offsetTop - element.scrollTop + element.clientTop;
    element = element.offsetParent;
  }

  return { x: xPosition, y: yPosition };
}
