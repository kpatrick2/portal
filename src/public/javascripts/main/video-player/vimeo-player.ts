export interface IVimeoVideoInformation {
  html: string;
  thumbnail_url: string;
}

export async function inlineVimeoPlayer(link: Element, info: IVimeoVideoInformation) {
  link.parentElement!.innerHTML = info.html;
}
