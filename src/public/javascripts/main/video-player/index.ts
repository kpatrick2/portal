import { jsonp } from '../../utils';
import { inlineVimeoPlayer, IVimeoVideoInformation } from './vimeo-player';
import { inlineYouTubePlayer } from './youtube-player';

export async function renderVideoPlayer() {
  const videoPlayerLinks = document.querySelectorAll('.ui-videoplayer a');

  for (let i = 0; i < videoPlayerLinks.length; i++) {
    const videoPlayerLink = videoPlayerLinks.item(i) as HTMLLinkElement;
    await inlineVideoPlayer(videoPlayerLink);
  }
}

async function inlineVideoPlayer(link: HTMLElement) {
  const itemName = link.attributes.getNamedItem('href');
  const url = itemName ? itemName.value : '';

  const youTubeMatch = url.match(/youtube\.com.*(?:\?v=|\/embed\/)(.{11})/) || url.match(/youtu\.be\/(.{11})/);
  if (youTubeMatch) {
    return await inlineYouTubePlayer(link, youTubeMatch[1]);
  }

  const vimeoVideoInformation = await jsonp<IVimeoVideoInformation>('https://vimeo.com/api/oembed.json', {
    params: {
      url
    }
  });
  if (vimeoVideoInformation) {
    return inlineVimeoPlayer(link, vimeoVideoInformation);
  }

  // tslint:disable-next-line:no-console
  console.warn(`Unknown video URL: ${url}`);
}
