import { assetsLoader } from '../../utils';

declare global {
  // tslint:disable-next-line:interface-name
  interface Window {
    onYouTubeIframeAPIReady: () => void;
  }
}

let isInitialized = false;

export function initialize() {
  if (!isInitialized) {
    return new Promise((resolve, reject) => {
      window.onYouTubeIframeAPIReady = () => {
        delete window.onYouTubeIframeAPIReady;
        isInitialized = true;
        resolve();
      };

      assetsLoader.loadJavaScript('https://www.youtube.com/iframe_api').catch(reject);
    });
  }
}
