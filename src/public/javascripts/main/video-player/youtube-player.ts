import { initialize } from './youtube-init';

declare global {
  // tslint:disable-next-line:interface-name
  interface Window {
    YT: typeof YT;
  }
}

export async function inlineYouTubePlayer(link: HTMLElement, videoId: string) {
  await initialize();
  // tslint:disable-next-line:no-unused-expression
  new YT.Player(link, {
    videoId
  });
}
