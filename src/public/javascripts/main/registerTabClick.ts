import * as $ from 'jquery-validation';
import { rmElemClassByClassname, setElemClassByClassname, setElemStyleById } from '../utils/dom-helpers';
import { getQueryStringParameter } from '../utils/url-helpers';
import { storeArrondissement } from './locationStorage';

let previousTab: string = '0';

export function registerTabClick() {
  $('.tab-async')
    .unbind('click')
    .on('click', e => {
      e.preventDefault();
      const href = $(e.target).attr('href');
      let tab;
      let arr;

      if (href) {
        const targetHref = parseArrandTab(href);
        arr = targetHref.arr;
        tab = targetHref.tab;
        if (document.getElementById('FullCity0')) {
          arr = 'FullCity';
        }

        if (href.includes('fiches-services')) {
          const previousHref = parseArrandTab(location.href);
          previousTab = previousHref.tab ? previousHref.tab : '0';

          const elem = setElemStyleById(arr + previousTab, 'display', 'none');
          if (elem) {
            rmElemClassByClassname('nav-link tab-async active', 'active');
          }
        }

        const displayBloc = setElemStyleById(arr + tab, 'display', 'block');
        if (displayBloc) {
          if (href.includes('fiches-services')) {
            setElemClassByClassname(tab as any, 'active');
          }
          setElemStyleById('borough-no-data', 'display', 'none');
        } else {
          setElemStyleById('borough-no-data', 'display', 'none');
        }
        storeArrondissement(arr, tab);
      }
    });
}

function parseArrandTab(href: string): { arr: any; tab: string } {
  const ret: { arr: any; tab: string } = { arr: '', tab: '' };
  if (href.includes('arrondissement') && href.includes('tab')) {
    const parts = href.split('?');
    const parts2 = parts[1].split('&');
    ret.tab = parts2[1].replace('tab=', '');
  } else if (!href.includes('arrondissement') && href.includes('tab')) {
    const parts2 = href.split('?');
    ret.tab = parts2[1].replace('tab=', '');
  } else {
    ret.tab = '0';
  }
  ret.arr = getQueryStringParameter(location.href, 'arrondissement');
  if (ret.arr) {
    ret.arr = decodeURIComponent(ret.arr);
  }

  return ret;
}
