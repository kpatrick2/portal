import * as L from 'leaflet';
import { IMapMarkerOptions } from '../map-interfaces';

interface IMapExtensions {
  __markers?: L.Marker[];
}

interface IMarkerExtensions {
  __map?: L.Map;
  __options?: IMapMarkerOptions;
}

// Keep a reference in the marker to the map, and from the map to all of its markers
export function setMapMarkers(map: L.Map, markers: L.Marker[]) {
  const mapExtensions = map as IMapExtensions;
  mapExtensions.__markers = markers;

  markers.forEach((marker: any) => {
    marker.__map = map;
  });
}

export function getMapMarkers(map: L.Map): L.Marker[] {
  const mapExtensions = map as IMapExtensions;
  return mapExtensions.__markers || [];
}

export function getMarkerMap(marker: L.Marker): L.Map {
  const markerExtensions = marker as IMarkerExtensions;
  return markerExtensions.__map!;
}

export function setMarkerOptions(marker: L.Marker, markerOptions: IMapMarkerOptions) {
  const markerExtensions = marker as IMarkerExtensions;
  markerExtensions.__options = markerOptions;
}

export function getMarkerOptions(marker: L.Marker): IMapMarkerOptions {
  const markerExtensions = marker as IMarkerExtensions;
  return markerExtensions.__options!;
}
