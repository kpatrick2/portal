import { createMap } from './create-map';

export function renderLeafletMaps() {
  const maps = document.querySelectorAll('[data-leaflet-map]');

  for (let i = 0; i < maps.length; i += 1) {
    const map = maps.item(i);
    createMap(map as HTMLElement);
  }
}
