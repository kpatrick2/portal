import * as L from 'leaflet';
import { IMapOptions } from '../map-interfaces';
import * as leafletHelpers from './leaflet-helpers';
const markerIcon = require('../../../../images/marker.png');
const selectedMarkerIcon = require('../../../../images/marker-selected.png');

const markerIcons = {
  default: L.icon({
    iconUrl: markerIcon,
    iconSize: [40, 52],
    iconAnchor: [20, 52]
  }),
  selected: L.icon({
    iconUrl: selectedMarkerIcon,
    iconSize: [40, 52],
    iconAnchor: [20, 52]
  })
};

export function createMap(mapElement: HTMLElement) {
  const serializedOptions = mapElement.getAttribute('data-leaflet-map') || '{}';
  const options: IMapOptions = JSON.parse(serializedOptions);

  const layer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution:
      '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, Tiles courtesy of <a href="http://hot.openstreetmap.org/" target="_blank">Humanitarian OpenStreetMap Team</a>'
  });

  const map = L.map(mapElement, {
    center: L.latLng(options.center.latitude, options.center.longitude),
    zoom: options.zoom,
    layers: [layer],
    minZoom: 11,
    maxZoom: 17,
    zoomControl: options.zooming,
    doubleClickZoom: options.zooming ? 'center' : false,
    dragging: options.dragging,
    scrollWheelZoom: options.dragging,
    maxBounds: [[45.3984338, -73.990778], [45.7139284, -73.4474681]]
  });

  if (options.coordinates) {
    addGeojson(map, options);
  } else {
    addMarkers(map, options);
  }
}

function addGeojson(map: L.Map, options: IMapOptions) {
  const geoJsonLayer = L.geoJSON(options.coordinates, {
    pointToLayer: (feature, latlng) => {
      const smallIcon = new L.Icon({
        iconUrl: selectedMarkerIcon,
        iconSize: [40, 52],
        iconAnchor: [20, 52]
      });
      return L.marker(latlng, { icon: smallIcon });
    }
  });

  geoJsonLayer.setStyle(getShapeStyle('#E30003'));

  geoJsonLayer.addTo(map);
  map.fitBounds(geoJsonLayer.getBounds());
}

function getShapeStyle(color: string): any {
  return {
    color,
    fillColor: color,
    opacity: 1,
    fillOpacity: 0.2,
    width: 3
  };
}

function addMarkers(map: L.Map, options: IMapOptions) {
  const markers = options.markers || [];
  const markerIconState = options.selected ? markerIcons.selected : markerIcons.default;
  const markerClickable = options.clickable === false ? options.clickable : true;

  const leafletMarkers = markers.map(markerOptions => {
    const marker = L.marker(
      {
        lat: markerOptions.latitude,
        lng: markerOptions.longitude
      },
      { interactive: markerClickable }
    )
      .setIcon(markerIconState)
      .on('click', handleMarkerClick)
      .addTo(map);

    leafletHelpers.setMarkerOptions(marker, markerOptions);
    return marker;
  });

  leafletHelpers.setMapMarkers(map, leafletMarkers);
}

function handleMarkerClick(event: L.LeafletEvent) {
  const marker = event.target as L.Marker;

  // The marker is already selected
  if (marker.options.icon === markerIcons.selected) {
    return;
  }

  selectSingleMarker(marker);
}

function selectSingleMarker(marker: L.Marker) {
  const map = leafletHelpers.getMarkerMap(marker);
  const allMarkers = leafletHelpers.getMapMarkers(map);

  allMarkers.forEach(m => {
    if (m === marker) {
      m.setIcon(markerIcons.selected);
      setSelectedClassToLinkedId(m, true);
    } else {
      m.setIcon(markerIcons.default);
      setSelectedClassToLinkedId(m, false);
    }
  });
}

function setSelectedClassToLinkedId(marker: L.Marker, addClass: boolean) {
  const markerOptions = leafletHelpers.getMarkerOptions(marker);

  if (markerOptions.onSelectionAddClassToId) {
    const linkedElement = document.getElementById(markerOptions.onSelectionAddClassToId)!;

    if (addClass) {
      linkedElement.classList.add('active');
    } else {
      linkedElement.classList.remove('active');
    }
  }
}
