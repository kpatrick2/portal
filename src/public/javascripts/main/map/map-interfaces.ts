import { Feature, FeatureCollection } from 'geojson';

// This interface has to match what the LeafletMap component sets in the HTML attribute
export interface IMapOptions {
  center: IMapCoordinates;
  zoom: number;
  dragging?: boolean;
  zooming?: boolean;
  markers?: IMapMarkerOptions[];
  coordinates?: FeatureCollection;
  selected?: boolean;
  clickable?: boolean;
}

export interface IMapCoordinates {
  latitude: number;
  longitude: number;
}

export interface IMapMarkerOptions {
  latitude: number;
  longitude: number;
  onSelectionAddClassToId?: string;
}

export interface IFeatureProperties {
  id?: string;
  description?: string;
}

export interface IMarkerFeatureAndElement {
  oldMarker?: Feature;
  oldMarkerElem?: HTMLDivElement;
}
