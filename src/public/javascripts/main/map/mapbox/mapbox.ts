import { Feature, FeatureCollection } from 'geojson';
import * as mapboxgl from 'mapbox-gl';
import * as turf from 'turf';
import { IFeatureProperties, IMapMarkerOptions, IMapOptions, IMarkerFeatureAndElement } from '../map-interfaces';
const markerIcon = require('../../../../images/marker.png');
const markerIconSelected = require('../../../../images/marker-selected.png');

const tmpContainer: IMarkerFeatureAndElement = {};
const selectedColor = '#E30003';
const fillOpacity = 0.2;
const lineWidth = 2;
const markerIconWidth = 40;
const markerIconHeight = 52;
const mtlDataSource = 'mapbox-mtl-data';

// Mapbox styles from Gestion de territoire
const mapboxStyle: any = {
  version: 8,
  name: 'basic',
  sources: {
    'vector-tile-api': {
      type: 'vector',
      tiles: [`https://api.montreal.ca/api/it-platforms/geomatic/vector-tiles/maps/v1/basemap/{z}/{x}/{y}.pbf`]
    }
  },
  glyphs: '/glyphs/{fontstack}/{range}.pbf',
  layers: [
    {
      id: 'background',
      type: 'background',
      paint: {
        'background-color': '#e7e5e0'
      }
    },

    {
      id: 'cities',
      type: 'fill',
      source: 'vector-tile-api',
      'source-layer': 'cities',
      filter: ['==', '$type', 'Polygon'],
      paint: {
        'fill-color': '#E6E4E0'
      }
    },

    {
      id: 'water',
      type: 'fill',
      source: 'vector-tile-api',
      'source-layer': 'water',
      filter: ['==', '$type', 'Polygon'],
      paint: {
        'fill-color': '#75cff0'
      }
    },

    {
      id: 'large_parks',
      type: 'fill',
      source: 'vector-tile-api',
      'source-layer': 'parks',
      filter: ['>=', 'SHAPE_AREA', 150000],
      paint: {
        'fill-color': '#B6E59E'
      }
    },
    {
      id: 'small_parks',
      type: 'fill',
      source: 'vector-tile-api',
      'source-layer': 'parks',
      filter: ['<', 'SHAPE_AREA', 150000],
      minzoom: 12,
      paint: {
        'fill-color': '#B6E59E'
      }
    },
    {
      id: 'lakes',
      type: 'fill',
      source: 'vector-tile-api',
      'source-layer': 'lakes',
      filter: ['==', '$type', 'Polygon'],
      paint: {
        'fill-color': '#75cff0'
      }
    },
    {
      id: 'streams',
      type: 'fill',
      source: 'vector-tile-api',
      'source-layer': 'streams',
      filter: ['==', '$type', 'Polygon'],
      paint: {
        'fill-color': '#75cff0'
      }
    },
    {
      id: 'canals',
      type: 'fill',
      source: 'vector-tile-api',
      'source-layer': 'canals',
      filter: ['==', '$type', 'Polygon'],
      paint: {
        'fill-color': '#75cff0'
      }
    },
    {
      id: 'roads_0',
      type: 'line',
      source: 'vector-tile-api',
      'source-layer': 'road-sections',
      minzoom: 14,
      layout: {
        'line-cap': 'round',
        'line-join': 'round'
      },
      paint: {
        'line-color': '#D6D9E6',
        'line-width': {
          base: 1.4,
          stops: [[10, 1.45], [14, 6], [18, 38]]
        }
      }
    },
    {
      id: 'arterial_network_0',
      type: 'line',
      source: 'vector-tile-api',
      'source-layer': 'road-sections',
      filter: ['any', ['==', 'classification', 6], ['==', 'classification', 7]],
      layout: {
        'line-cap': 'round',
        'line-join': 'round'
      },
      paint: {
        'line-color': '#DBDDE3',
        'line-width': {
          base: 1.2,
          stops: [[12, 1.45], [14, 5], [18, 38]]
        }
      }
    },
    {
      id: 'roads',
      type: 'line',
      source: 'vector-tile-api',
      'source-layer': 'road-sections',
      minzoom: 11,
      layout: {
        'line-cap': 'round',
        'line-join': 'round'
      },
      paint: {
        'line-color': '#F8F8F8',
        'line-width': {
          base: 1.55,
          stops: [[12, 1.35], [14, 5], [18, 35]]
        }
      }
    },
    {
      id: 'arterial network',
      type: 'line',
      source: 'vector-tile-api',
      'source-layer': 'road-sections',
      filter: ['any', ['==', 'classification', 6], ['==', 'classification', 7]],
      layout: {
        'line-cap': 'round',
        'line-join': 'round'
      },
      paint: {
        'line-color': '#F8F8F8',
        'line-width': {
          base: 1.4,
          stops: [[10, 1.25], [14, 3], [18, 35]]
        }
      }
    },

    {
      id: 'highways',
      type: 'line',
      source: 'vector-tile-api',
      'source-layer': 'road-sections',
      filter: ['==', 'classification', 8],
      layout: {
        'line-cap': 'round',
        'line-join': 'round'
      },
      paint: {
        'line-color': '#FFA35C',
        'line-width': {
          base: 1.4,
          stops: [[6, 0.5], [20, 30]]
        }
      }
    },
    {
      id: 'buildings',
      type: 'fill',
      source: 'vector-tile-api',
      'source-layer': 'buildings',
      minzoom: 13,
      paint: {
        'fill-color': ['interpolate', ['linear'], ['zoom'], 15, '#DCDCDC', 16, '#D3D3D3', 17, '#CECECE', 18, '#C3C3C3']
      }
    },
    {
      id: 'highways_labels',
      type: 'symbol',
      source: 'vector-tile-api',
      'source-layer': 'road-sections',
      filter: ['all', ['==', 'classification', 8], ['!=', 'name', 'voie Non-nommée'], ['!=', 'name', 'voie Privée']],
      layout: {
        'text-font': ['Open Sans SemiBold'],
        'text-transform': 'uppercase',
        'text-field': '{name}',
        'text-size': 10,
        'symbol-placement': 'line',
        'text-letter-spacing': 0.1
      },
      paint: {
        'text-color': '#666',
        'text-halo-color': 'rgba(255,255,255,0.75)',
        'text-halo-width': 2
      }
    },
    {
      id: 'roads_labels',
      type: 'symbol',
      source: 'vector-tile-api',
      'source-layer': 'road-sections',
      filter: ['all', ['!=', 'classification', 8], ['!=', 'name', 'voie Non-nommée'], ['!=', 'name', 'voie Privée']],
      minzoom: 14,
      layout: {
        'text-font': ['Open Sans Regular'],
        'text-field': '{name}',
        'text-size': 11,
        'symbol-placement': 'line',
        'text-letter-spacing': 0.1,
        'text-max-angle': 70
      },
      paint: {
        'text-color': '#787878',
        'text-halo-color': '#fff',
        'text-halo-width': 1,
        'text-halo-blur': 0.5
      }
    },
    {
      id: 'large_parks_labels',
      type: 'symbol',
      source: 'vector-tile-api',
      'source-layer': 'parks',
      filter: ['>=', 'SHAPE_AREA', 150000],
      minzoom: 12,
      layout: {
        'text-anchor': 'center',
        'text-font': ['Open Sans Regular'],
        'text-field': '{NOM_PARC}',
        'text-size': 11.5,
        'symbol-placement': 'point',
        'text-letter-spacing': 0.1,
        'text-padding': 60
      },
      paint: {
        'text-color': '#26660D',
        'text-halo-color': '#fff',
        'text-halo-width': 1,
        'text-halo-blur': 0.5
      }
    },
    {
      id: 'small_parks_labels',
      type: 'symbol',
      source: 'vector-tile-api',
      'source-layer': 'parks',
      filter: ['<', 'SHAPE_AREA', 150000],
      minzoom: 14.5,
      layout: {
        'text-anchor': 'center',
        'text-font': ['Open Sans Regular'],
        'text-field': '{NOM_PARC}',
        'text-size': 11.5,
        'symbol-placement': 'point',
        'text-letter-spacing': 0.1,
        'text-padding': 30
      },
      paint: {
        'text-color': '#26660D',
        'text-halo-color': '#fff',
        'text-halo-width': 1,
        'text-halo-blur': 0.5
      }
    },

    {
      id: 'boroughs_labels',
      type: 'symbol',
      source: 'vector-tile-api',
      'source-layer': 'boroughs-center',
      maxzoom: 14,
      layout: {
        'text-anchor': 'center',
        'text-font': ['Open Sans Light'],
        'text-field': '{NOM_ARROND}',
        'text-transform': 'uppercase',
        'symbol-placement': 'point',
        'text-letter-spacing': 0.1,
        'text-size': 9
      },
      paint: {
        'text-color': '#666',
        'text-halo-color': 'rgba(255,255,255,0.75)',
        'text-halo-width': 1,
        'text-halo-blur': 1
      }
    },

    {
      id: 'cities_labels',
      type: 'symbol',
      source: 'vector-tile-api',
      'source-layer': 'cities-center',
      filter: ['!=', 'NOM_VILLE', 'Montréal'],
      maxzoom: 14,
      'text-transform': 'uppercase',
      layout: {
        'text-anchor': 'center',
        'text-font': ['Open Sans SemiBold'],
        'text-field': '{NOM_VILLE}',
        'text-size': 10.5,
        'symbol-placement': 'point',
        'text-letter-spacing': 0.1
      },
      paint: {
        'text-color': '#666',
        'text-halo-color': 'rgba(255,255,255,0.75)',
        'text-halo-width': 1,
        'text-halo-blur': 1
      }
    }
  ]
};

export function registerMapBox() {
  const maps = document.querySelectorAll('[data-mapbox-map]');

  for (let i = 0; i < maps.length; i += 1) {
    const map = maps.item(i);
    createMap(map as HTMLElement);
  }
}

function createMap(mapElement: HTMLElement) {
  if (mapElement) {
    const serializedOptions = mapElement.getAttribute('data-map-map') || '{}';
    const options: IMapOptions = JSON.parse(serializedOptions);
    // Create map
    const map = new mapboxgl.Map({
      container: mapElement, // container id
      style: mapboxStyle, // stylesheet location
      center: [options.center.longitude, options.center.latitude], // starting position [lng, lat]
      zoom: options.zoom || 10, // starting zoom
      minZoom: 10,
      maxZoom: 17,
      scrollZoom: options.zooming, // possibility of zooming
      dragPan: options.dragging, // possibigily of dragging
      dragRotate: false
    });

    // Add zoom control
    if (options.zooming) {
      map.addControl(new mapboxgl.NavigationControl({ showCompass: false }), 'top-left');
    }

    // Set data
    let data;
    if (options.coordinates) {
      data = options.coordinates;
    }

    // Convert to feature collection
    if (options.markers) {
      data = convertToFeatureCollection(options.markers);
    }

    data = data as FeatureCollection;

    // Fitbounds
    const bbox = turf.bbox(data);
    map.fitBounds([[bbox[0], bbox[1]], [bbox[2], bbox[3]]], { padding: 100, duration: 0 });

    addEventListenerOnMap(map, options, data);
  }
}

function addEventListenerOnMap(map: mapboxgl.Map, options: IMapOptions, data: FeatureCollection) {
  map.on('load', () => {
    // Add all necessary layers
    addGeojsonSource(map, data);
    addMarkerLayerFromGeojson(map, data, options);
    addPolygonLayerFromGeojson(map);
    addPolygonOutlineLayerFromGeojson(map);
    addLineStringLayerFromGeojson(map);
    setPopUpOnMarker(map, data);
  });

  // Set default cursor icon whe zooming and draggin are false
  if (!options.zooming && !options.dragging) {
    map.on('mousemove', () => {
      map.getCanvas().style.cursor = 'default';
    });
  }
}

function convertToFeatureCollection(markers: IMapMarkerOptions[]): FeatureCollection {
  const data = markers.map((marker: IMapMarkerOptions) => {
    return {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [marker.longitude, marker.latitude]
      },
      properties: {
        id: marker.onSelectionAddClassToId
      }
    };
  });

  const featureCollection = {
    type: 'FeatureCollection',
    features: data
  };
  return featureCollection as FeatureCollection;
}

// Add geojson data source
function addGeojsonSource(map: mapboxgl.Map, data: FeatureCollection) {
  const options: mapboxgl.GeoJSONSourceRaw = {
    type: 'geojson',
    data
  };
  map.addSource(mtlDataSource, options);
}

// Add polygon shapes layer
function addPolygonLayerFromGeojson(map: mapboxgl.Map) {
  const layer: mapboxgl.Layer = {
    id: 'mtl-data-polygon',
    type: 'fill',
    source: mtlDataSource,
    filter: ['==', '$type', 'Polygon'],
    paint: {
      'fill-color': selectedColor,
      'fill-opacity': fillOpacity,
      'fill-outline-color': selectedColor
    }
  };
  map.addLayer(layer);
}

// Add polygon outline layer
function addPolygonOutlineLayerFromGeojson(map: mapboxgl.Map) {
  const layer: mapboxgl.Layer = {
    id: 'mtl-data-polygon-outline',
    type: 'line',
    source: mtlDataSource,
    filter: ['==', '$type', 'Polygon'],
    paint: {
      'line-color': selectedColor,
      'line-width': lineWidth
    }
  };
  map.addLayer(layer);
}

// Add linestring layer
function addLineStringLayerFromGeojson(map: mapboxgl.Map) {
  const layer: mapboxgl.Layer = {
    id: 'mtl-data-line',
    type: 'line',
    source: mtlDataSource,
    filter: ['==', '$type', 'LineString'],
    paint: {
      'line-color': selectedColor,
      'line-width': lineWidth
    }
  };
  map.addLayer(layer);
}

// Add markers on a map
function addMarkerLayerFromGeojson(map: mapboxgl.Map, data: FeatureCollection, options: IMapOptions) {
  const markers = data.features.filter((feature: Feature) => feature.geometry.type === 'Point');
  // Change marker icon to images
  markers.forEach((marker: Feature) => {
    const el = document.createElement('div') as HTMLDivElement;
    el.className = 'marker';
    const markerImage = options.selected ? markerIconSelected : markerIcon;
    el.style.backgroundImage = 'url(' + markerImage + ')';
    el.style.width = markerIconWidth + 'px';
    el.style.height = markerIconHeight + 'px';

    setEventListenerToMarker(map, el, marker, options);
    new mapboxgl.Marker(el, { anchor: 'bottom' }).setLngLat((marker.geometry as any).coordinates).addTo(map);
  });
}

function setEventListenerToMarker(map: mapboxgl.Map, el: HTMLDivElement, marker: Feature, options: IMapOptions) {
  if (options.clickable) {
    el.addEventListener('click', (event: MouseEvent) => {
      selectSingleMarker(marker, event.target as HTMLDivElement);
    });

    el.addEventListener('mousemove', (event: MouseEvent) => {
      (event.target! as HTMLDivElement).style.cursor = 'pointer';
    });
  }

  if (!options.dragging && !options.zooming && !options.clickable) {
    el.addEventListener('mousemove', (event: MouseEvent) => {
      (event.target as HTMLDivElement).style.cursor = 'default';
    });
  }
}

// Set old marker and new marker in a object to set the right active image
function selectSingleMarker(m: Feature, mapMarker: HTMLDivElement) {
  if (tmpContainer.oldMarker && tmpContainer.oldMarkerElem && mapMarker) {
    setSelectedClassToLocationList(tmpContainer.oldMarker, false);
    tmpContainer.oldMarkerElem.style.backgroundImage = 'url(' + markerIcon + ')';
  }
  setSelectedClassToLocationList(m, true);
  mapMarker.style.backgroundImage = 'url(' + markerIconSelected + ')';
  tmpContainer.oldMarker = m;
  tmpContainer.oldMarkerElem = mapMarker;
}

// Add active class to list element that represent the marker
function setSelectedClassToLocationList(marker: Feature, addClass: boolean) {
  const markerOptions = marker;
  if (markerOptions.properties) {
    const properties: IFeatureProperties = markerOptions.properties;
    const linkedElement = document.getElementById(properties.id!)!;
    if (addClass) {
      linkedElement.classList.add('active');
    } else {
      linkedElement.classList.remove('active');
    }
  }
}

function setPopUpOnMarker(map: mapboxgl.Map, data: FeatureCollection) {
  const features = data.features.filter((feature: Feature) => feature.geometry.type === 'Point');
  for (const f of features) {
    const text = textToShowInPopup(f.properties as IFeatureProperties);
    if (text) {
      addPopUp(map, text, (f.geometry as any).coordinates);
    }
  }
}

function textToShowInPopup(properties: IFeatureProperties): string {
  let text = '';
  if (properties.description) {
    text = properties.description;
  }

  return text;
}

function addPopUp(map: mapboxgl.Map, text: string, coordinates: number[]) {
  const options = {
    closeOnClick: false,
    closeButton: false,
    offset: markerIconHeight + 20
  };
  new mapboxgl.Popup(options)
    .setLngLat(coordinates)
    .setHTML(text)
    .addTo(map);
}
