import { changeLanguageLink } from '@villemontreal/shell-portal-client-lib/dist/public';

declare var languageLink: string;

export function setChangeLanguageLink() {
  changeLanguageLink(languageLink);
}
