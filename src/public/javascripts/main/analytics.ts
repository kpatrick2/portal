import { config } from '../utils';

export interface ISection {
  title: string;
}

export interface IAnalyticField {
  type: 'radio' | 'zone de texte';
  question: string;
  answerType: '';
  answer: string;
}

class AnalyticsService {
  public viewPage(title: string, id?: string, contentType?: string, sections?: ISection[], borough?: string) {
    const tag = this.getBaseSection(title, id, contentType, sections, borough);
    this.setDigitalTrack(tag);
  }

  public searchResult(
    title: string,
    searchTerm: string,
    numberResults: number,
    totalPages: number,
    currentPage: number,
    contentTypeFilter = '',
    categories: string[] = []
  ) {
    const tag = this.getBaseSection(title);
    tag.page.type = 'recherche_resultats';
    tag.search = {
      contentTypeFilter,
      categories,
      number_results: numberResults,
      total_pages: totalPages,
      current_page: currentPage,
      search_term: searchTerm || ''
    };

    this.setDigitalTrack(tag);
  }

  public form(
    title: string,
    eventType: string,
    formName: string,
    formType: string,
    status: 'demarree' | 'completee',
    stepNumber: string,
    stepName: string
  ) {
    const tag = this.getBaseSection(title);
    tag.event.type = eventType;
    tag.page.type = 'formulaire';
    tag.form = {
      status,
      step_number: stepNumber,
      step_name: stepName,
      platform_system: 'wufoo',
      type: formType,
      name: formName
    };

    this.setDigitalTrack(tag);
  }

  public survey(
    title: string,
    eventType: string,
    status: 'demarree' | 'completee',
    stepNumber: string,
    stepName: string,
    fields: IAnalyticField[]
  ) {
    const tag = this.getBaseSection(title);
    tag.event.type = eventType;
    tag.page.type = 'formulaire';
    tag.form = {
      status,
      step_number: stepNumber,
      step_name: stepName,
      platform_system: 'wufoo',
      type: 'sondage',
      name: 'rectroaction a chaud'
    };
    tag.survey = {
      fields,
      name: 'retroaction a chaud',
      type: 'passif',
      trigger_rule: 'pied de page'
    };

    this.setDigitalTrack(tag);
  }

  public formError(reason: string, message: string) {
    const tag = {
      event: {
        type: 'form_erreur'
      },
      errors: [
        {
          reason,
          message,
          category: 'formulaire',
          type: 'entree',
          code: ''
        }
      ]
    };

    this.setDigitalTrack(tag);
  }

  public errorNotFound() {
    const tag = this.getBaseSection('Page introuvable');
    tag.page.type = 'erreur';
    tag.errors = [
      {
        category: 'page',
        type: 'server',
        code: '404',
        reason: 'page introuvable',
        message: ''
      }
    ];

    this.setDigitalTrack(tag);
  }

  private getBaseSection(title: string, id = '', contentType = '', sections: ISection[] = [], borough = ''): any {
    return {
      event: {
        type: 'page_vue'
      },
      site: {
        environment: config.config.environmentName,
        platform_type: 'site web',
        platform_sub_type: '',
        name: 'Portail'
      },
      page: {
        // only on the borough page
        borough,
        destination_url: location.href,
        language: config.language,
        site_section: this.getFromSection(sections, 0),
        sub_section1: this.getFromSection(sections, 1),
        sub_section2: this.getFromSection(sections, 2),
        sub_section3: this.getFromSection(sections, 3),
        sub_section4: this.getFromSection(sections, 4),
        content_title: title,
        content_id: id,
        content_type: contentType
      }
    };
  }

  private setDigitalTrack(obj: any) {
    const windowAny = window as any;
    windowAny.digital_track = windowAny.digital_track || [];
    windowAny.digital_track.push(obj);
  }

  private getFromSection(sections: ISection[], index: number): string {
    if (index >= sections.length) {
      return '';
    }

    return sections[index].title;
  }
}

export const analytics = new AnalyticsService();

// Expose the analytics service globally so it can be called in any page
// Use pre-configured components in /views/analytics to access the different methods
(window as any).analytics = analytics;
