import * as $ from 'jquery-validation';
import { getQueryStringParameter } from '../utils/url-helpers';

export function registerStickyBar() {
  const stickyToggle = (sticky: any, stickyWrapper: any, stickyLimit: any, scrollElement: any) => {
    const stickyHeight = sticky.outerHeight();
    const stickyTop = stickyWrapper.offset().top;
    const stickyLimitTop = stickyLimit.offset().top;

    // We handle view all pages Sticky behavior by setting a stickyLimit.
    // Need to handle scrollspy and multiple ID on same page for view all templates pages...
    if (scrollElement.scrollTop() > stickyLimitTop - stickyHeight) {
      sticky.css({ top: (scrollElement.scrollTop() + stickyHeight - stickyLimitTop) * -1 });
    } else if (scrollElement.scrollTop() > stickyTop) {
      sticky.css({ top: 0 });
      stickyWrapper.height(stickyHeight);
      sticky.addClass('is-sticky');
    } else {
      sticky.removeClass('is-sticky');
      stickyWrapper.height('auto');
    }
  };
  let borough = '';
  if (location.href.includes('collectes') || location.href.includes('collections')) {
    if (location.href.includes('arrondissement=')) {
      borough = getQueryStringParameter(location.href, 'arrondissement') || '';
      borough = borough ? decodeURIComponent(borough).replace(/ /g, '-') : '';
      if (borough.includes('Île-Bizard')) {
        borough = 'ile-bizard';
      }
    }
  }
  const boroughSuffix = borough ? '-' + borough : '';
  // Find all data-toggle="sticky-onscroll" elements
  $(`[data-toggle="sticky-onscroll${boroughSuffix}"]`).each(function() {
    const sticky = $(this);
    let stickyWrapper = $('.sticky-wrapper');
    stickyWrapper = stickyWrapper.length > 0 ? stickyWrapper : $('<div>').addClass('sticky-wrapper'); // insert hidden element to maintain actual top offset on page
    let stickyLimit = $('#sticky-limit');
    if (stickyLimit && !Object.keys(stickyLimit).length) {
      stickyLimit = sticky.siblings().last();
    }
    sticky.before(stickyWrapper);
    sticky.addClass('sticky');

    // Scroll & resize events
    $(document).scroll(() => {
      if (
        $(this)
          .parent('.sticky-container')
          .is(':visible')
      ) {
        if (matchMedia('(min-width: 992px)').matches) {
          stickyToggle(sticky, stickyWrapper, stickyLimit, $(window));
        } else {
          sticky.removeClass('is-sticky');
          stickyWrapper.height('auto');
        }
      }
    });
  });

  const navAnchorElem = $('#navAnchors' + borough);
  if (navAnchorElem.length !== 0) {
    let replacePattern = '';

    if (location.pathname.includes('/en/')) {
      replacePattern = '/en/';
    } else {
      replacePattern = '/';
    }

    const topMenu = navAnchorElem;
    const topMenuHeight = topMenu!.outerHeight();
    // All list items
    const menuItems = topMenu.find('a');
    // Anchors corresponding to menu items
    const scrollItems = menuItems.map((i, e: any) => {
      const id = e.hash + boroughSuffix;
      const item = $(id);
      if (item.length) {
        return item;
      }
    });

    // Bind to scroll
    $(document).scroll(e => {
      // Get container scroll position
      const fromTop = $(window).scrollTop()! + 2 * topMenuHeight!;

      // Get id of current scroll item
      const cur = scrollItems.map((i, el) => {
        if ($(el).offset()!.top < fromTop) {
          return el.attr('id');
        }
      });
      // Get the id of the current element
      const curPossible = cur[cur.length - 1];
      const currentId = curPossible ? curPossible : '';

      // Set/remove active class
      menuItems.filter("[class*='active']").removeClass('active');
      menuItems
        .filter("[href='" + replacePattern + '#' + currentId.replace(boroughSuffix, '') + "']")
        .addClass('active');
    });
  }
}
