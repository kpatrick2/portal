import * as $ from 'jquery-validation';
import { removeParam, updateQueryStringParameter } from '../utils/url-helpers';

export function selectFilterEvent() {
  $('.get-filter').on('click', e => bindEvent(e));
  $('.get-query').on('keypress', e => bindEvent(e));

  bindResetFilter();
  bindDismissBadgeFilter();
}

function bindEvent(e: any) {
  const queryParam = $(e.target).data('query');
  const url = removeParam('page', document.URL);
  const val = $(e.target).val() as string;
  if (e.type !== 'keypress') {
    document.location!.href = updateQueryStringParameter(url, queryParam, val);
  } else {
    if (val.length > 2 && e.which === 13) {
      e.preventDefault();
      document.location!.href = updateQueryStringParameter(url, queryParam, val);
    }
  }
}

function bindResetFilter() {
  const resetBtns = document.querySelectorAll('.btn-reset');
  for (let i = 0; i < resetBtns.length; i++) {
    const resetBtn = resetBtns.item(i);
    resetBtn.addEventListener('click', rmAllFilterFromUrl);
  }
}

function bindDismissBadgeFilter() {
  const badges = document.querySelectorAll('.badge-dismiss');
  for (let i = 0; i < badges.length; i++) {
    const badge = badges.item(i);
    badge.addEventListener('click', e => {
      e.preventDefault();
      document.location!.href = removeParam(badge.getAttribute('data-filter-label'), document.URL);
    });
  }
}

function rmAllFilterFromUrl() {
  const urlParts = location.href.split('?');
  location.href = urlParts[0];
}
