import * as $ from 'jquery-validation';
import { setElemStyleById } from '../utils/dom-helpers';
import { getQueryStringParameter } from '../utils/url-helpers';
import { storeArrondissement } from './locationStorage';
import { registerStickyBar } from './registerStickyBar';

let previousBorough: string;
let actualBorough: string;
let paramTab: string = '';

export function registerStoreBoroughClick() {
  const b = getQueryStringParameter(location.href, 'arrondissement');
  if (b) {
    displayBoroughContent(b);
  }

  $('.store-borough').on('click', e => {
    e.preventDefault();
    if (previousBorough !== $(e.target).text()) {
      actualBorough = $(e.target).text();
      if (!previousBorough) {
        const b2 = getQueryStringParameter(location.href, 'arrondissement');
        if (b2) {
          previousBorough = decodeURIComponent(b2);
        }
      }
      storeArrondissement($(e.target).text());

      updateNavLinkUrls();

      displayBoroughContent($(e.target).text());

      registerStickyBar();
    }
    previousBorough = actualBorough;
  });
}

function cleanBoroughSelection() {
  setElemStyleById('borough-ad', 'display', 'none');
  setElemStyleById('borough-abstract-active', 'display', 'block');
  setElemStyleById('borough-abstract-unselected', 'display', 'none');
}

function updateNavLinkUrls() {
  if (location.href.includes('fiches-services')) {
    const elems = document.getElementsByClassName('nav-link tab-async');
    for (const el of elems as any) {
      if (el.href.includes('arrondissement') && getQueryStringParameter(el.href, 'arrondissement') !== '') {
        el.href = decodeURIComponent(el.href).replace(previousBorough, actualBorough);
      } else {
        el.href = decodeURIComponent(el.href).replace(
          /\?(arrondissement)?=?&?/,
          '?arrondissement=' + actualBorough + '&'
        );
      }
    }
  }
}

function displayBoroughContent(boroughName: string) {
  const fullCity = document.getElementById('FullCity0') || document.getElementById('FullCity');
  if (!fullCity) {
    const borough = decodeURIComponent(boroughName);
    if (location.href.includes('tab')) {
      const param = location.href.split('tab=')[1];
      paramTab = param;
    } else {
      if (location.href.includes('fiches-services')) {
        paramTab = '0';
      } else {
        paramTab = '';
      }
    }

    if (borough) {
      if (location.href.includes('arrondissement')) {
        if (previousBorough) {
          setElemStyleById(previousBorough + paramTab, 'display', 'none');
          setElemStyleById(previousBorough + 'rates', 'display', 'none');
        }
        cleanBoroughSelection();
      }

      setElemStyleById('nav-tabs', 'display', 'block');
      const displayBloc = setElemStyleById(borough + paramTab, 'display', 'block');
      if (displayBloc) {
        setElemStyleById('borough-no-data', 'display', 'none');
      } else {
        setElemStyleById('borough-no-data', 'display', 'block');
      }

      const block = $('.lead-left-content .title')[0];
      if (block) {
        block.innerHTML = borough;
      }
    }
  } else {
    fullCity.style.display = 'block';
  }
}
