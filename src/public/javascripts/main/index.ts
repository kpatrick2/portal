import './polyfills';
// tslint:disable-next-line:ordered-imports
// polyfills must be the first import

import 'bootstrap/dist/js/bootstrap';
import './analytics';
import { registerAnchorSmoothScrolling } from './anchor-smooth-scrolling';
import { registerAutoSubmitForm } from './auto-submit-form';
import { initializeFeedBackForm } from './feedback-form-validation';
import { registerImageGalleryLinks } from './image-gallery';
import { storeArrondissement } from './locationStorage';
import { renderLeafletMaps } from './map/leaflet';
import { registerMapBox } from './map/mapbox/mapbox';
import { registerStickyBar } from './registerStickyBar';
import { registerStoreBoroughClick } from './registerStoreBoroughClick';
import { registerTabClick } from './registerTabClick';
import { selectFilterEvent } from './select-filter';
import { setChangeLanguageLink } from './set-change-language-link';
import { registerEventBtn } from './show-hide';
import { renderVideoPlayer } from './video-player';
import { validateWufooForm } from './wufooForm-validation';

document.addEventListener('DOMContentLoaded', () => registerAllEvents(), false);

async function registerAllEvents() {
  try {
    initializeFeedBackForm();
    setChangeLanguageLink();
    registerAnchorSmoothScrolling();
    registerImageGalleryLinks();
    registerAutoSubmitForm();
    storeArrondissement();
    registerStoreBoroughClick();
    registerStickyBar();
    registerTabClick();
    renderLeafletMaps();
    validateWufooForm();
    registerEventBtn();
    selectFilterEvent();
    registerMapBox();
    await renderVideoPlayer();
  } catch (error) {
    // tslint:disable-next-line:no-console
    console.error(error);
  }
}
