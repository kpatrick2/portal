import * as bowser from 'bowser';
import * as $ from 'jquery-validation';
import { analytics, IAnalyticField } from './analytics';

export function initializeFeedBackForm() {
  validateForm();
  getCurrentDevice();
}

function validateForm() {
  let textareaElement: JQuery<HTMLElement>;

  $('#form20 input[name=Field10]').prop('checked', false);

  $('#form20').validate({
    showErrors: (errorMap, errorList) => {
      if ('Field14' in errorMap && textareaElement) {
        const sibling = textareaElement.next();
        if (sibling) {
          sibling.show();
        }
      }
    },
    rules: {
      Field10: 'required',
      Field11: 'required',
      Field12: 'required',
      Field14: 'required'
    },
    submitHandler: form => {
      if ($('#form20 input[name=Field10]:checked').val()) {
        const fields: IAnalyticField[] = [
          {
            type: 'radio',
            question: "Est-ce le contenu qu'il faut améliorer?",
            answerType: '',
            answer: $('#form20 input[name=Field11]:checked').val() as string
          },
          {
            type: 'zone de texte',
            question: 'Expliquer',
            answerType: '',
            answer: $('#form20 textarea[name=Field14]').val() as string
          },
          {
            type: 'radio',
            question: "Est-ce l'affichage qu'il faut améliorer?",
            answerType: '',
            answer: $('#form20 input[name=Field12]:checked').val() as string
          }
        ];

        analytics.survey(document.title, 'page_vue', 'completee', '2', 'merci', fields);
      }

      form.submit();
    }
  });

  $('#form20 input[name=Field10]').change(evt => {
    const elementOnClick = ['oui_en_partie', 'non'];
    const inputValue = $(evt.target).val() as string;
    if (elementOnClick.indexOf(inputValue) > -1) {
      initSectionValues();
    } else {
      resetSectionValues();
    }

    if (inputValue) {
      $('#form20 button').prop('disabled', false);
      $('#form20 button').prop('hidden', false);
    }

    const fields: IAnalyticField[] = [
      {
        type: 'radio',
        question: 'Cette page répond-elle à vos besoins?',
        answerType: '',
        answer: inputValue
      }
    ];

    analytics.survey(document.title, 'form_etape', 'demarree', '1', 'niveau 1', fields);

    if (elementOnClick.indexOf(inputValue) > -1) {
      analytics.viewPage(document.title);
    }
  });

  $('#form20 input[name=Field11]').change(evt => {
    resetTextArea();
    const elementOnClick = [
      'contenu_pas_assez_precis',
      'contenu_trop_complexe',
      'contenu_errone',
      'contenu_non_attendu'
    ];
    const inputValue = $(evt.target).val() as string;
    if (elementOnClick.indexOf(inputValue) > -1) {
      textareaElement = $(evt.target)
        .parent()
        .next()
        .find('textarea');
      textareaElement.prop('name', 'Field14');
      textareaElement.show();
    }
  });

  $('#form20 button.reset-button').click(evt => {
    resetAllValues();
  });
}

function getCurrentDevice() {
  const devices = [
    'mac',
    'windows',
    'windowsphone',
    'linux',
    'chromeos',
    'android',
    'ios',
    'blackberry',
    'firefoxos',
    'webos',
    'bada',
    'tizen',
    'sailfish'
  ];

  const currentNavigator = bowser.name || '';
  const currentNavigatorVersion = bowser.version || '';

  const currentDevices = devices.filter(x => {
    return x in bowser;
  });

  const currentDevice = currentDevices[0] || 'unknown';

  $('#form20 input[name=Field5]').val(currentDevice);
  $('#form20 input[name=Field6]').val(currentNavigator);
  $('#form20 input[name=Field7]').val(currentNavigatorVersion);
}

function initSectionValues() {
  $('#form20 .feedback-details-content, #form20 .feedback-details-display').show();
  $('#form20 input[name=Field11][value="contenu_adequat"]').prop('checked', true);
  $('#form20 input[name=Field12][value="affichage_adequat"]').prop('checked', true);
  resetTextArea();
}

function resetSectionValues() {
  $('#form20 input[name=Field11]').prop('checked', false);
  $('#form20 input[name=Field12]').prop('checked', false);
  $('#form20 .feedback-details-content, #form20 .feedback-details-display').hide();
  resetTextArea();
}

function resetTextArea() {
  $('#form20 .feedback-details-content textarea').removeAttr('name');
  $('#form20 .feedback-details-content textarea').val('');
  $('#form20 .feedback-details-content textarea').hide();
  $('#form20 .feedback-details-content .alert-danger').hide();
}

function resetAllValues() {
  resetSectionValues();
  $('#form20 input[name=Field10]').prop('checked', false);
  $('#form20 button').prop('disabled', true);
  $('#form20 button#cancel').prop('hidden', true);
}
