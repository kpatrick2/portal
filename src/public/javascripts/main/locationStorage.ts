import { getQueryStringParameter } from '../utils/url-helpers';

// tslint:disable-next-line:cyclomatic-complexity
export function storeArrondissement(selectedBorough?: string, tab?: any) {
  const url = location.href;
  const urlPath = location.pathname;
  let urlAnchor = '';
  let needTab = false;
  let paramTab = '';
  const splitAnchorUrl = location.href.split('#');
  let urlDisplay = location.protocol + '//' + location.host + urlPath;

  if (splitAnchorUrl[1]) {
    urlAnchor = '#' + splitAnchorUrl[1];
  }

  if (url.includes('tab') || tab) {
    needTab = true;
    const param = url.split('tab=')[1];
    paramTab = tab ? tab : param;
  }

  // if the content is specific by borough (if we can changeour borough on the page)
  // then we process the arrondissement url param
  if (document.getElementById('selectArrondissementModal')) {
    if (selectedBorough && selectedBorough !== 'FullCity') {
      localStorage.setItem('arrondissement', selectedBorough);
    } else if (url.indexOf('arrondissement=') > -1) {
      const uriDec = getQueryStringParameter(url, 'arrondissement');
      if (uriDec && uriDec !== 'FullCity') {
        localStorage.setItem('arrondissement', uriDec);
      } else {
        localStorage.removeItem('arrondissement');
      }
    }

    const localArrondissement = localStorage.getItem('arrondissement');
    let secUriDec = '';

    if (localArrondissement) {
      secUriDec = decodeURIComponent(localArrondissement);
      urlDisplay += `?arrondissement=${secUriDec}`;

      if (needTab) {
        if (urlDisplay.search('arrondissement') !== -1) {
          urlDisplay += `&tab=${paramTab}`;
        } else {
          urlDisplay += `?tab=${paramTab}`;
        }
      }

      urlDisplay += urlAnchor;
      history.pushState({}, '', urlDisplay);
    }
  } else if (document.getElementById('FullCity') || document.getElementById('FullCity0')) {
    if (needTab) {
      urlDisplay += `?tab=${paramTab}`;
    }

    // Update url in browser bar
    history.pushState({}, '', urlDisplay);
  }
}
