import * as $ from 'jquery-validation';
export function registerEventBtn() {
  $('#moreless').on('click', e => {
    const ariaExpanded = $('#moreless').attr('aria-expanded');
    const moreInfo = $('#moreless').data('showMore');
    const lessInfo = $('#moreless').data('showLess');
    if (ariaExpanded === 'false') {
      $('#moreless').html(lessInfo);
    } else {
      $('#moreless').html(moreInfo);
    }
  });

  const gallery = document.getElementById('btn-container');
  if (gallery) {
    bindEventToBtn(gallery);
  }
}

function bindEventToBtn(element: Element) {
  const btns = element.querySelectorAll('button');
  for (let i = 0; i < btns.length; i++) {
    const btn = btns.item(i);
    btn.addEventListener('click', showElement(element));
  }
}

function showElement(element: any) {
  return (event: MouseEvent) => {
    event.preventDefault();
    const elementCard = document.getElementById('card-show-hide');
    // ce bloc de code va changer le style des cards de display:none a display:bloc
    const cardsElement = elementCard ? elementCard.querySelectorAll('.d-none') : null;
    if (cardsElement) {
      for (let i = 0; i < cardsElement.length; i++) {
        const card = cardsElement.item(i) as HTMLElement;
        card.classList.remove('d-none');
      }

      // ce bloc de code va rendre le bouton a display:none
      const btnShow = element.querySelectorAll('button');
      for (let i = 0; i < btnShow.length; i++) {
        const btn = btnShow.item(i) as HTMLElement;
        btn.classList.add('d-none');
      }
    }
  };
}
