export function registerAutoSubmitForm() {
  const inputs = document.querySelectorAll('[data-auto-submit]');

  for (let i = 0; i < inputs.length; i += 1) {
    const input = inputs.item(i);
    input.addEventListener('change', submitForm(input as any));
  }
}

function submitForm(input: HTMLSelectElement | HTMLInputElement) {
  return () => {
    input.form!.submit();
  };
}
