let count = 0;

interface IJsonpOptions {
  prefix?: string;
  param?: string;
  params?: any;
}

export function jsonp<T>(url: string, options: IJsonpOptions = {}): Promise<T> {
  const optionsOrDefault = options || {};

  const prefix = optionsOrDefault.prefix || '__jp';
  const param = optionsOrDefault.param || 'callback';
  const params = optionsOrDefault.params || {};
  const target = document.getElementsByTagName('script')[0] || document.head;
  let script: HTMLScriptElement;

  // Generate a unique id for the request.
  const id = prefix + count++;
  params[param] = id;

  function cleanup() {
    // Remove the script tag.
    if (script && script.parentNode) {
      script.parentNode.removeChild(script);
    }

    (window as any)[id] = noop;
  }

  return new Promise((resolve, reject) => {
    (window as any)[id] = (response: any) => {
      cleanup();
      resolve(response);
    };

    // Create script.
    script = document.createElement('script');
    script.src = buildUrl(url, params);
    target.parentNode!.insertBefore(script, target);
  });
}

function buildUrl(baseUrl: string, params: any) {
  const query = Object.keys(params)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(params[key]))
    .join('&');

  if (baseUrl.indexOf('?') === -1) {
    return `${baseUrl}?${query}`;
  }

  return `${baseUrl}&${query}`;
}

function noop() {
  /**/
}
