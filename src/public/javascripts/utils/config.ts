// Data passed from server-side
export const config: IConfig = (window as any).config;

interface IConfig {
  language: 'fr' | 'en';
  config: {
    environmentName: string;
  };
}
