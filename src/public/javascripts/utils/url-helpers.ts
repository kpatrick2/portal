export function updateQueryStringParameter(uri: string, key: string, value: string) {
  const separator = uri.indexOf('?') !== -1 ? '&' : '?';
  const regex = new RegExp('([?&])' + key + '=.*?(&|$)', 'i');
  if (uri.match(regex)) {
    return uri.replace(regex, '$1' + key + '=' + value + '$2');
  } else {
    return uri + separator + key + '=' + value;
  }
}

export function getQueryStringParameter(uri: string, key: string) {
  const regex = new RegExp('([?&])' + key + '=.*?(#|&|$)', 'i');
  const matches = uri.match(regex);
  if (matches) {
    const borough = matches[0]
      .split('=')[1]
      .replace('&', '')
      .replace('#', '');
    return borough;
  }
}

export function removeParam(key: string | undefined | null, sourceURL: string) {
  if (key) {
    let rtn = sourceURL.split('?')[0];
    let paramsArr = [];
    const queryStringRem = sourceURL.indexOf('?') !== -1 ? sourceURL.split('?')[1] : '';
    if (queryStringRem !== '') {
      paramsArr = queryStringRem.split('&');
      for (let i = paramsArr.length - 1; i >= 0; i -= 1) {
        const param = paramsArr[i].split('=')[0];
        if (param === key) {
          paramsArr.splice(i, 1);
        }
      }
      if (paramsArr.length > 0) {
        rtn = rtn + '?' + paramsArr.join('&');
      }
    }
    return rtn;
  } else {
    return sourceURL;
  }
}
