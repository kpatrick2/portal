export function setElemStyleById(id: string, prop: string, value: string): boolean {
  const el = document.getElementById(id);
  if (el) {
    el.style[prop as any] = value;
    return true;
  }
  return false;
}

export function setElemClassByClassname(index: number, classname: string): boolean {
  const navLinks = document.getElementsByClassName('nav-link tab-async');
  if (navLinks && navLinks.length >= index) {
    navLinks[index].classList.add(classname);
    return true;
  }
  return false;
}

export function rmElemClassByClassname(target: string, classtorm: string) {
  const els = document.getElementsByClassName(target);
  if (els && els.length > 0) {
    for (const el of els as any) {
      el.classList.remove(classtorm);
    }
  }
}
