// tslint:disable-next-line:no-console
const debug = console.debug.bind(console);
// tslint:disable-next-line:no-console
const info = console.info.bind(console);
// tslint:disable-next-line:no-console
const warn = console.warn.bind(console);
// tslint:disable-next-line:no-console
const error = console.error.bind(console);

export { debug, info, warn, error };
