import { IAddress } from '../content-api';
export function addressExists(contact: IAddress): boolean {
  let exists: boolean = true;
  if (!contact.address && !contact.address2 && !contact.city && !contact.province && !contact.postal_code) {
    exists = false;
  }
  return exists;
}

export function addressToString(address: IAddress): string {
  const content = [];
  if (address.address) {
    content.push(address.address);
    content.push('<br />');
  }

  if (address.address2) {
    content.push(address.address2);
    content.push('<br />');
  }

  if (address.city) {
    content.push(address.city);
    content.push(' ');
  }

  if (address.province) {
    content.push(`( ${address.province} )`);
    content.push(' ');
  }

  if (address.postal_code) {
    content.push(address.postal_code);
  }
  const addr = content.join('');
  return addr;
}
export function googleMapsAddessLink(address: IAddress) {
    const _address:string = `${address.address} ${address.city} ${address.postal_code} ${address.province}, ${address.country}`;
    const mapLink:string = encodeURI(`https://www.google.com/maps/search/?api=1&query=${_address}`);
   
  return mapLink;
}
