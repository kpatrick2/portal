import * as queryString from 'query-string';
import { ITaxonomy } from '../content-api';

export function isFullCity(content: any) {
  return content.find(
    (item: { boroughs: string[] }) => item.boroughs.length > 0 && item.boroughs[0] === 'Ville de Montréal'
  );
}

export function getSelectedBorough(currentUrl: string, boroughList: ITaxonomy[]) {
  const queryStrings = queryString.parse(queryString.extract(currentUrl));
  const boroughs = boroughList.map((item: ITaxonomy) => ({
    label: item.name,
    value: item.name
  }));

  if (queryStrings.arrondissement) {
    const selectedBoroughArray = boroughs.filter((item: any) => {
      return convert(item.label) === convert(queryStrings.arrondissement);
    });

    if (selectedBoroughArray.length > 0) {
      const selectedBoroughLabel = selectedBoroughArray[0].label;
      return selectedBoroughLabel ? convert(selectedBoroughLabel) : '';
    }
  }
}

export function getSelectedBoroughContent(isContentFullCity: boolean, selectedBorough: string, dataBloc: any) {
  if (isContentFullCity) {
    if (dataBloc instanceof Array && dataBloc.length > 0) {
      return dataBloc[0];
    }
    return dataBloc;
  } else if (selectedBorough) {
    const result = dataBloc.filter((item: any) => {
      return item.boroughs.indexOf(selectedBorough) > -1;
    });
    return result[0];
  }
}

function convert(text: string) {
  return text.replace(/&#(?:x([\da-f]+)|(\d+));/gi, (_, hex, dec) => {
    return String.fromCharCode(dec || +('0x' + hex));
  });
}
