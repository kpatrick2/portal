export function assertNever(value: never): never {
  const message = 'Unknown value: ' + value;
  throw new Error(message);
}
