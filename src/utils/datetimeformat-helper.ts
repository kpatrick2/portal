import * as moment from 'moment';

export function getFormattedDayOfWeek(dayOfWeek: string) {
  const key = mapToTranslationKey(dayOfWeek);
  return key;
}

export function mapToTranslationKey(day: string) {
  // tslint:disable-next-line:switch-default
  switch (day) {
    case '1':
      return 'common:days.monday';
    case '2':
      return 'common:days.tuesday';
    case '3':
      return 'common:days.wednesday';
    case '4':
      return 'common:days.thursday';
    case '5':
      return 'common:days.friday';
    case '6':
      return 'common:days.saturday';
    case '7':
      return 'common:days.sunday';
    default:
      return '';
  }
}

export function startOpenHours(scheduleOfTheDay: any, language: string) {
  if (language === 'en') {
    return tConvert24To12(scheduleOfTheDay.start_time);
  }
  const startTime = scheduleOfTheDay.start_time ? scheduleOfTheDay.start_time.replace(':', ' h ') : '';
  return startTime.replace(/^0+/, ''); // enlever les 0 au début des heures
}

export function endOpenHours(scheduleOfTheDay: any, language: string) {
  if (language === 'en') {
    return tConvert24To12(scheduleOfTheDay.end_time);
  }
  const endTime = scheduleOfTheDay.end_time ? scheduleOfTheDay.end_time.replace(':', ' h ') : '';
  return endTime.replace(/^0+/, ''); // enlever les 0 au début des heures
}

export function tConvert24To12(timeString: string) {
  const hourEnd = timeString.indexOf(':');
  const H = +timeString.substr(0, hourEnd);
  const h = (H % 12 || 12).toString();
  const ampm = H < 12 || H === 24 ? ' AM' : ' PM';
  return `${h + timeString.substr(hourEnd, 3) + ampm}`;
}

export function sortSchedulebyPeriode(schedule: any) {
  const firstHour: any = [];
  schedule.map((hour: any, index: number) => {
    if (
      hour.valid_from &&
      moment(hour.valid_from).isValid() &&
      hour.valid_through &&
      moment(hour.valid_through).isValid()
    ) {
      if (moment().isBetween(hour.valid_from, hour.valid_through)) {
        firstHour.push(hour);
        schedule.shift(index);
      }
    }
  });
  if (firstHour.length > 0) {
    firstHour.map((h: any, index: number) => {
      schedule.unshift(h);
    });
  }
}
