export function constructOrderBy(sortBy?: string, sortOrder?: 'Asc' | 'Desc'): string | undefined {
  let order: string | undefined;

  if (sortBy) {
    order = sortOrder === 'Desc' ? '-' : '';
    return order + sortBy;
  }

  return order;
}
