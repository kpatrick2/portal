import * as queryString from 'query-string';
import { IReqParams } from '../content-api';

export function replaceQueryString(url: string, key: string, value: string) {
  const urlWithoutQueryString = url.split('?')[0];

  const params = queryString.parse(queryString.extract(url));
  params[key] = value;
  const stringifiedQueryString = queryString.stringify(params);

  return `${urlWithoutQueryString}?${stringifiedQueryString}`;
}

export function updateQueryStringParameter(uri: string, key: string, value: string) {
  const separator = uri.indexOf('?') !== -1 ? '&' : '?';
  const regex = new RegExp('([?&])' + key + '=.*?(&|$)', 'i');
  if (uri.match(regex)) {
    return uri.replace(regex, '$1' + key + '=' + value + '$2');
  }
  return uri + separator + key + '=' + value;
}

export function setfilterAttr(params: any): IReqParams[] {
  const filtersBy: IReqParams[] = [];

  for (const [index, key] of Object.keys(params).entries()) {
    if (key !== 'q' && key !== 'page') {
      const filter = { key, value: params[Object.keys(params)[index]] };
      filtersBy.push(filter);
    }
  }
  return filtersBy;
}
