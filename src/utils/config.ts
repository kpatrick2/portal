import * as nodeConfig from 'config';

export const config = {
  environmentName: nodeConfig.get('environmentName') as string,
  contentApiUrl: nodeConfig.get('contentApiUrl') as string,
  baseUrl: nodeConfig.get('baseUrl') as string,
  contentApiBypassCertificateErrors: nodeConfig.get('contentApiBypassCertificateErrors') as boolean,
  cloudinaryEnvDirectory: nodeConfig.get('cloudinaryEnvDirectory') as string
};
