import { ICloudinaryImageTransformations } from '../content-api';

export function cloudinaryUrlAdapter(id: string, transformations?: ICloudinaryImageTransformations): string {
  const url = `https://res.cloudinary.com/villemontreal/image/upload${buildTransformationString(
    transformations
  )}/v1/${id}`;

  return url;
}

function buildTransformationString(transformationsObject?: ICloudinaryImageTransformations) {
  const transformations: string[] = [];
  // Optimize quality
  transformations.push('q_auto');
  // Optimize format
  transformations.push('f_auto');
  // Responsive image sizing
  transformations.push('dpr_auto');

  if (transformationsObject) {
    if (transformationsObject.width) {
      transformations.push(`w_${transformationsObject.width}`);
    }

    if (transformationsObject.height) {
      transformations.push(`h_${transformationsObject.height}`);
    }
    if (transformationsObject.ar) {
      transformations.push(`ar_${transformationsObject.ar}`);
    }
    if (transformationsObject.crop) {
      transformations.push(`c_${transformationsObject.crop}`);
    }

    // @todo : we can fine tune the quality with Cloudinary : q_auto:best, q_auto:good, q_auto:eco, q_auto:low
    // @link : https://cloudinary.com/blog/the_holy_grail_of_image_optimization_or_balancing_visual_quality_and_file_size
    if (transformationsObject.q_auto) {
      transformations.push(`${transformationsObject.q_auto}`);
    }
  }

  return `/${transformations.join(',')}`;
}
