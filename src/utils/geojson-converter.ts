import { Feature, FeatureCollection, Geometry, GeometryCollection } from 'geojson';

export function featureToGeometry(data: FeatureCollection): GeometryCollection {
  const features = data.features;
  const geometries: Geometry[] = [];

  features.forEach(feature => {
    geometries.push(feature.geometry);
  });

  return { geometries, type: 'GeometryCollection' };
}

export function geometryToFeature(data: GeometryCollection): FeatureCollection {
  const geometries = data.geometries;
  const features: Feature[] = [];

  geometries.forEach(geometry => {
    features.push({
      geometry,
      type: 'Feature'
    } as Feature);
  });

  return { features, type: 'FeatureCollection' };
}

export function geoPointToFeatureCollection(geometry: Geometry, properties?: any): FeatureCollection {
  const features = {
    geometry,
    type: 'Feature'
  } as Feature;

  if (properties) {
    features.properties = properties;
  }

  return { type: 'FeatureCollection', features: [features] };
}

export function latLongToFeatureCollection(long: number, lat: number, properties?: any): FeatureCollection {
  const geometries = {
    type: 'Point',
    coordinates: [long, lat]
  };

  const features = {
    type: 'Feature',
    geometry: geometries
  } as Feature;

  if (properties) {
    features.properties = properties;
  }

  return { type: 'FeatureCollection', features: [features] };
}

export function formatElasticSearchToFeatureCollection(geometryCollection: any): FeatureCollection {
  const geometries: any[] = geometryCollection.geometries;
  const newGeometries: any[] = [];

  geometries.forEach(item => {
    newGeometries.push({
      type: getShapeFormat(item.type, 'geojson'),
      coordinates: item.coordinates
    } as Geometry);
  });

  return geometryToFeature({ type: 'GeometryCollection', geometries: newGeometries });
}

export function getShapeFormat(shapeName: string, format: string): string {
  const lowerCaseShapeName = shapeName.toLocaleLowerCase();
  const allShapesFormat: any = {
    geojson: {
      point: 'Point',
      linestring: 'LineString',
      polygon: 'Polygon',
      multipoint: 'MultiPoint',
      multilinestring: 'MultiLineString',
      multipolygon: 'MultiPolygon',
      geometrycollection: 'GeometryCollection'
    },
    wkt: {
      point: 'POINT',
      linestring: 'LINESTRING',
      polygon: 'POLYGON',
      multipoint: 'MULTIPOINT',
      multilinestring: 'MULTILINESTRING',
      multipolygon: 'MULTIPOLYGON',
      geometrycollection: 'MULTIPOLYGON'
    },
    elasticsearch: {
      point: 'point',
      linestring: 'linestring',
      polygon: 'polygon',
      multipoint: 'multipoint',
      multilinestring: 'multilinestring',
      multipolygon: 'multipolygon',
      geometrycollection: 'geometrycollection'
    }
  };

  const newShapes = allShapesFormat[format][lowerCaseShapeName];

  return newShapes;
}
