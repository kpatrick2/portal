import { TranslationFunction } from 'i18next';
import { EImagePlaceholder, IImage } from '../content-api';
import { cloudinaryUrlAdapter } from './cloudinary-adapter';
import { config } from './config';

export function getImagePlaceholder(
  images: IImage[],
  t: TranslationFunction,
  contentType?: string
): IImage | undefined {
  let imagePlaceholder: IImage | undefined;

  if (images && !images.length && contentType) {
    const tc: any = contentType.toUpperCase();

    if (EImagePlaceholder[tc]) {
      const imageId: string = `${config.cloudinaryEnvDirectory}/${EImagePlaceholder[tc]}`;
      const placeholderUrl: string = cloudinaryUrlAdapter(imageId);

      imagePlaceholder = {
        url: placeholderUrl,
        id: imageId,
        file_name: EImagePlaceholder[tc],
        alt: t('common:placeholder.alt'),
        title: t('common:placeholder.title'),
        width: '768',
        height: '432'
      };
    }
  }
  return imagePlaceholder;
}
