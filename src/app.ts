import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as logger from 'morgan';
import {
  exposeAppVersionMiddleware,
  exposeLocalsMiddleware,
  initializeI18n,
  registerErrorHandlers,
  router,
  servePublic,
  viewEngine
} from './initialization';

export const app = express();

try {
  initializeApp();
} catch (err) {
  // tslint:disable-next-line:no-console
  console.error(err);
}

function initializeApp() {
  viewEngine(app);
  servePublic(app);
  app.use(logger('dev'));
  app.use(bodyParser.urlencoded({ extended: false }));
  initializeI18n(app);
  exposeLocalsMiddleware(app);
  exposeAppVersionMiddleware(app);
  app.use(router);
  registerErrorHandlers(app);
}
